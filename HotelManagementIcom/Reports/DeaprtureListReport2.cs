using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace HotelManagementIcom.Reports
{
    public partial class DeaprtureListReport2 : Telerik.WinControls.UI.RadForm
    {
        string startingDate;
        string endingDate;

        public DeaprtureListReport2()
        {
            InitializeComponent();
        }
        public DeaprtureListReport2(string sdate,string edate)
        {
            InitializeComponent();
            startingDate = sdate;
            endingDate = edate;
        }

        private void DeaprtureListReport2_Load(object sender, EventArgs e)
        {

            DepartureListDateRangeReport rpt = new DepartureListDateRangeReport();

            hotelTableAdapters.DepartureList2TableAdapter ta = new hotelTableAdapters.DepartureList2TableAdapter();

            hotel.DepartureList2DataTable table = ta.GetData(startingDate,endingDate);

            rpt.SetDataSource(table.DefaultView);

            crystalReportViewer1.ReportSource = rpt;

            rpt.Refresh();
        }
    }
}
