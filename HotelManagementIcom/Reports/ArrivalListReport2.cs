using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace HotelManagementIcom.Reports
{
    public partial class ArrivalListReport2 : Telerik.WinControls.UI.RadForm
    {
        string startingDate;
        string endDate;

        public ArrivalListReport2()
        {
            InitializeComponent();
        }

        //parameterized constructor
        public ArrivalListReport2(string sdate, string edate)
        {
            InitializeComponent();
            startingDate = sdate;
            endDate = edate;
        }

        private void ArrivalListReport2_Load(object sender, EventArgs e)
        {
            ArrivalListDateRangeReport rpt = new ArrivalListDateRangeReport();

            hotelTableAdapters.BookingsInDateRangeTableAdapter ta = new hotelTableAdapters.BookingsInDateRangeTableAdapter();

            hotel.BookingsInDateRangeDataTable table = ta.GetData(startingDate, endDate);

            rpt.SetDataSource(table.DefaultView);

            crystalReportViewer1.ReportSource = rpt;

            rpt.Refresh();
        }
    }
}
