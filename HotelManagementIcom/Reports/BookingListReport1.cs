using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace HotelManagementIcom.Reports
{
    public partial class BookingListReport1 : Telerik.WinControls.UI.RadForm
    {
        string startingDate;

        public BookingListReport1()
        {
            InitializeComponent();
        }

        public BookingListReport1(string sdate)
        {
            InitializeComponent();
            startingDate = sdate;
        }

        private void BookingListReport1_Load(object sender, EventArgs e)
        {
            BookingListToday rpt = new BookingListToday();

            hotelTableAdapters.BookingsList1TableAdapter ta = new hotelTableAdapters.BookingsList1TableAdapter();

            hotel.BookingsList1DataTable table = ta.GetData(startingDate);

            rpt.SetDataSource(table.DefaultView);

            crystalReportViewer1.ReportSource = rpt;

            rpt.Refresh();
        }
    }
}
