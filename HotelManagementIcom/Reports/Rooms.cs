using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace HotelManagementIcom.Reports
{
    public partial class Rooms : Telerik.WinControls.UI.RadForm
    {
        public Rooms()
        {
            InitializeComponent();
        }

        private void AllRooms_Load(object sender, EventArgs e)
        {

            RoomDetails rpt = new RoomDetails();

            hotelTableAdapters.roomTableAdapter rm = new hotelTableAdapters.roomTableAdapter();

            hotel.roomDataTable table = rm.GetData();

            rpt.SetDataSource(table.DefaultView);

            crystalReportViewer1.ReportSource = rpt;

            rpt.Refresh();
        }
    }
}
