using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace HotelManagementIcom.Reports
{
    public partial class ArraivalListReport1 : Telerik.WinControls.UI.RadForm
    {
        string startingDate;

        public ArraivalListReport1()
        {
            InitializeComponent();
        }

        //parameterized constructor
        public ArraivalListReport1(string sdate)
        {
            InitializeComponent();
            startingDate = sdate;
        }

        private void ArraivalList_Load(object sender, EventArgs e)
        {
            ArrivalTodayReport rpt = new ArrivalTodayReport();

            hotelTableAdapters.BookingsTableAdapter ta = new hotelTableAdapters.BookingsTableAdapter();

            hotel.BookingsDataTable table = ta.GetData(startingDate);

            rpt.SetDataSource(table.DefaultView);

            crystalReportViewer1.ReportSource = rpt;

            rpt.Refresh();
        }
    }
}
