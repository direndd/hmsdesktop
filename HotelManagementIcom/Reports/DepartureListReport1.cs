using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace HotelManagementIcom.Reports
{
    public partial class DepartureListReport1 : Telerik.WinControls.UI.RadForm
    {
        string endingDate;
        public DepartureListReport1()
        {
            InitializeComponent();
        }
        public DepartureListReport1(string edate)
        {
            InitializeComponent();
            endingDate = edate;
        }


        private void DepartureListReport1_Load(object sender, EventArgs e)
        {
            DepartureTodayReport rpt = new DepartureTodayReport();

            hotelTableAdapters.DepartureList1TableAdapter ta = new hotelTableAdapters.DepartureList1TableAdapter();

            hotel.DepartureList1DataTable table = ta.GetData(endingDate);

            rpt.SetDataSource(table.DefaultView);

            crystalReportViewer1.ReportSource = rpt;

            rpt.Refresh();
        }
    }
}
