using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace HotelManagementIcom.Reports
{
    public partial class BookingListReport2 : Telerik.WinControls.UI.RadForm
    {

        string startingDate;
        string endingDate;

        public BookingListReport2()
        {
            InitializeComponent();
        }
        public BookingListReport2(string sdate,string edate)
        {
            InitializeComponent();
            startingDate = sdate;
            endingDate = edate;
        }


        private void BookingListReport2_Load(object sender, EventArgs e)
        {
            BookingListDateRange rpt = new BookingListDateRange();

            hotelTableAdapters.BookingsList2TableAdapter ta = new hotelTableAdapters.BookingsList2TableAdapter();

            hotel.BookingsList2DataTable table = ta.GetData(startingDate, endingDate);

            rpt.SetDataSource(table.DefaultView);

            crystalReportViewer1.ReportSource = rpt;

            rpt.Refresh();
        }
    }
}
