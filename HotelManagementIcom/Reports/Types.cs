using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace HotelManagementIcom.Reports
{
    public partial class Types : Telerik.WinControls.UI.RadForm
    {
        public Types()
        {
            InitializeComponent();
        }

        private void Types_Load(object sender, EventArgs e)
        {

            RoomTypes rpt = new RoomTypes();

            hotelTableAdapters.roomTypeTableAdapter rm= new hotelTableAdapters.roomTypeTableAdapter();

            hotel.roomTypeDataTable table = rm.GetData();

            rpt.SetDataSource(table.DefaultView);

            crystalReportViewer2.ReportSource = rpt;

            rpt.Refresh();
        }
    }
}
