using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using HotelManagementIcom.Controller;
using HotelManagementIcom.DataAccess;
using HotelManagementIcom.Reports;


namespace HotelManagementIcom.View
{
    public partial class ManageRooms : Telerik.WinControls.UI.RadForm
    {
        DataController data = new DataController();
        

        public ManageRooms()
        {
            try
            {
                InitializeComponent();
                timer.Tick += new EventHandler(timer_Tick);
                timer.Interval = 1000;
                timer.Start();


                lblUserName.Text = new LoggedUser().UserName1;
                lblUserLevel.Text = new LoggedUser().UserLevel1;
            }
            catch (Exception ex1)
            {
                var msg = ex1.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                string[] Time = DateTime.Now.ToString().Split(' ');
                lblTime.Text = Time[1] + " " + Time[2];
            }
            catch (Exception ex2)
            {
                var msg = ex2.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void clear() 
        {
            lblRoomNo.Visible = false;
            lblRoomType.Visible = false;

            txtNoOfAdults1.Text = string.Empty;
            txtNoOfChildren1.Text = string.Empty;
            txtFullPrice1.Text = string.Empty;
            txtHalfPrice1.Text = string.Empty;


            txtRoomType2.Text = string.Empty;
            txtFloor2.Text = string.Empty;
            txtDescription2.Text = string.Empty;
            txtAvailability2.Text = string.Empty;
            txtNoOfAdults2.Text = string.Empty;
            txtNoOfChildren2.Text = string.Empty;
            txtFullPrice2.Text = string.Empty;
            txtHalfPrice2.Text = string.Empty;
        
        }

        private void Room_Load(object sender, EventArgs e)
        {
            try
            {
                lblRoomNo.Visible = false;
                lblRoomType.Visible = false;
                data.roomTypeTableFilll(dgvRoomType);
                data.fillRoomType(cmbRoomType);
                data.fillRoomNo(cmbRoomNo);
                data.roomTableFilll(dgvRooms);
                clear();
            }
            catch (Exception ex3)
            {
                var msg = ex3.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        
        private void button8_Click(object sender, EventArgs e)
        {            
            try
            {
                AddRooms rm = new AddRooms();
                rm.Show();
                this.Hide();
            }
            catch (Exception ex4)
            {
                var msg = ex4.Message;
                MessageBox.Show(msg, "Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                AddRoomType rmt = new AddRoomType();
                rmt.Show();
                this.Hide();
            }
            catch (Exception ex5)
            {
                var msg = ex5.Message;
                MessageBox.Show(msg, "Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                string logID = new LoggedUser().LogHisId.ToString();
                String[] tokens3 = System.DateTime.Today.ToString().Split(' ');
                String onlyDate = tokens3[1];

                ////log history record
                data.logHistorySave(logID);


                //clearing the detils 
                LoggedUser lu = new LoggedUser();
                lu.UserName1 = "";
                lu.UserLevel1 = "";
                lu.LogHisId = "";
                Login login = new Login();
                login.Show();
                this.Dispose();
            }
            catch (Exception ex6)
            {
                var msg = ex6.Message;
                MessageBox.Show(msg, "Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbRoomType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String roomtype = cmbRoomType.SelectedItem.ToString();
                List<roomType> type = data.searchRoomType(roomtype);
                roomType rmt = (roomType)type[0];
                clear();
                cmbRoomType.Text = rmt.typeName;
                txtNoOfAdults1.Text = rmt.noOfAdults.ToString();
                txtNoOfChildren1.Text = rmt.noOfChildren.ToString();
                txtFullPrice1.Text = rmt.price.ToString();
                txtHalfPrice1.Text = rmt.priceHalf.ToString();
                data.roomTypeTableFilll(dgvRoomType);
            }
            catch (Exception ex7)
            {
                var msg = ex7.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvRoomType_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                data.roomTypeTableFilll(dgvRoomType);
                int cell = e.RowIndex;
                clear();
                cmbRoomType.Text = dgvRoomType.Rows[cell].Cells[0].Value.ToString();
                txtNoOfAdults1.Text = dgvRoomType.Rows[cell].Cells[1].Value.ToString();
                txtNoOfChildren1.Text = dgvRoomType.Rows[cell].Cells[2].Value.ToString();
                txtFullPrice1.Text = dgvRoomType.Rows[cell].Cells[3].Value.ToString();
                txtHalfPrice1.Text = dgvRoomType.Rows[cell].Cells[4].Value.ToString();
            }
            catch (Exception ex8)
            {
                var msg = ex8.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRoomTypeDetails_Click(object sender, EventArgs e)
        {
            try
            {
                data.roomTypeTableFilll(dgvRoomType);
                String name = cmbRoomType.Text;

                List<roomType> rmt = data.getRoomTypes(name);
                if (rmt.Count == 0)
                {

                    clear();
                    dgvRoomType.DataSource = null;
                    MessageBox.Show("No Records Found", "Searching Fail", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);




                }
                else if (rmt.Count == 1)
                {
                    roomType rm = (roomType)rmt[0];
                    clear();
                    txtNoOfAdults1.Text = rm.noOfAdults.ToString();
                    txtNoOfChildren1.Text = rm.noOfChildren.ToString();
                    txtFullPrice1.Text = rm.price.ToString();
                    txtHalfPrice1.Text = rm.priceHalf.ToString();
                    dgvRoomType.DataSource = rmt;
                    MessageBox.Show("One Record Found", "Searching Successful", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);


                }
                else
                {

                    clear();
                    dgvRoomType.DataSource = rmt;
                    MessageBox.Show("More Than One Record Found", "Searching Successful", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
            }
            catch (Exception ex9)
            {
                var msg = ex9.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

      

        private void cmbRoomNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String roomNo = cmbRoomNo.SelectedItem.ToString();
                List<room> room = data.searchRoom(roomNo);

                room rm = (room)room[0];
                clear();
                cmbRoomNo.Text = rm.roomNo;
                txtRoomType2.Text = rm.roomType;
                txtFloor2.Text = rm.floorNo;
                txtDescription2.Text = rm.descrip;
                txtNoOfAdults2.Text = rm.noOfAdults.ToString();
                txtNoOfChildren2.Text = rm.noOfChildren.ToString();
                txtFullPrice2.Text = rm.price.ToString();
                txtHalfPrice2.Text = rm.priceHalf.ToString();


                string aval = rm.avalability.ToString();
                if (aval.Equals("True") || aval.Equals("true"))
                {
                    txtAvailability2.Text = "Available";
                }
                else
                    txtAvailability2.Text = "Not Available";

                data.roomTableFilll(dgvRooms);
            }
            catch (Exception ex10)
            {
                var msg = ex10.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dgvRooms_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                data.roomTableFilll(this.dgvRooms);
                int cell = e.RowIndex;
                clear();
               
                if (dgvRooms.Rows[e.RowIndex].Cells[4].Value == null || dgvRooms.Rows[e.RowIndex].Cells[4].Value.ToString() == "" || dgvRooms.Rows[e.RowIndex].Cells[4].Value.ToString() == "False") //not available
                {
                    txtAvailability2.Text = "Not Available";
                }
                else //value = true, available
                {
                    txtAvailability2.Text = "Available";
                }

                cmbRoomNo.Text = dgvRooms.Rows[cell].Cells[0].Value.ToString();
                txtRoomType2.Text = dgvRooms.Rows[cell].Cells[1].Value.ToString();
                txtFloor2.Text = dgvRooms.Rows[cell].Cells[2].Value.ToString();
                txtDescription2.Text = dgvRooms.Rows[cell].Cells[3].Value.ToString();
                txtNoOfAdults2.Text = dgvRooms.Rows[cell].Cells[5].Value.ToString();
                txtNoOfChildren2.Text = dgvRooms.Rows[cell].Cells[6].Value.ToString();
                txtFullPrice2.Text = dgvRooms.Rows[cell].Cells[7].Value.ToString();
                txtHalfPrice2.Text = dgvRooms.Rows[cell].Cells[8].Value.ToString();

            }
            catch (Exception ex11)
            {
                var msg = ex11.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRoomDetails_Click(object sender, EventArgs e)
        {
            data.roomTableFilll(dgvRooms);
            String no = cmbRoomNo.Text;
            try
            {
                List<room> rm = data.getRooms(no);
                if (rm.Count == 0)
                {
                    clear();
                    dgvRooms.DataSource = null;
                    MessageBox.Show("No Records Found", "Searching Fail", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);


                }
                else if (rm.Count == 1)
                {
                    room rms = (room)rm[0];
                    clear();
                    cmbRoomNo.Text = rms.roomNo;
                    txtRoomType2.Text = rms.roomType;
                    txtFloor2.Text = rms.floorNo;
                    txtDescription2.Text = rms.descrip;
                    txtNoOfAdults2.Text = rms.noOfAdults.ToString();
                    txtNoOfChildren2.Text = rms.noOfChildren.ToString();
                    txtFullPrice2.Text = rms.price.ToString();
                    txtHalfPrice2.Text = rms.priceHalf.ToString();

                    string aval = rms.avalability.ToString();
                    if (aval.Equals("True") || aval.Equals("true"))
                    {
                        txtAvailability2.Text = "Available";
                    }

                    else
                        txtAvailability2.Text = "Not Available";


                    dgvRooms.DataSource = rm;
                    MessageBox.Show("One Record Found", "Searching Successful", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);


                }
                else
                {
                    clear();
                    dgvRooms.DataSource = rm;
                    MessageBox.Show("More Than One Record Found", "Searching Successful", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
            }
            catch (Exception ex12)
            {
                var msg = ex12.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        

        private void button1_Click(object sender, EventArgs e)
        {
            
            //Refresh text fields
            data.roomTypeTableFilll(dgvRoomType);
            data.fillRoomType(cmbRoomType);
            data.fillRoomNo(cmbRoomNo);
            data.roomTableFilll(dgvRooms);
            clear();
        }

        private void cmbRoomNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Textbox only accepting numbers
            if (char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
                lblRoomNo.Visible = false;
                
            }
            else
            {
                e.Handled = true;
                lblRoomNo.Visible = true;
            } 
        }

        private void cmbRoomType_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Textbox only accepting numbers and letters
            if ((char.IsLetter(e.KeyChar) || char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar)))
            {
                e.Handled = false;
                lblRoomType.Visible = false;

            }

            else
            {
                e.Handled = true;
                lblRoomType.Visible = true;
            } 
        }

        private void btnAllRoomReport_Click(object sender, EventArgs e)
        {
            try
            {
                Rooms rm = new Rooms();
                rm.Show();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAllRoomTypeReport_Click(object sender, EventArgs e)
        {
            try
            {
                Types rmt = new Types();
                rmt.Show();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnManageRoomFacilities_Click(object sender, EventArgs e)
        {
            ManageFacilities mf = new ManageFacilities();
            mf.Show();
            this.Hide();
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            Home h = new Home();
            h.Show();
            this.Dispose();
        }

        private void lblLogout_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //string logID = new LoggedUser().LogHisId.ToString();
            //String[] tokens3 = System.DateTime.Today.ToString().Split(' ');
            //String onlyDate = tokens3[1];

            ////log history record
            //hotelmsEntities1 hmsLog = new hotelmsEntities1();
            //logHistory log = hmsLog.logHistories.Where(iii => iii.logHistoryId == logID.Trim()).Single();
            //log.logHistoryId = logID.ToString().Trim();
            //log.logOutTime = System.DateTime.Now;
            //hmsLog.SaveChanges();

            ////clearing the detils 
            //LoggedUser lu = new LoggedUser();
            //lu.UserName1 = "";
            //lu.UserLevel1 = "";
            //lu.LogHisId = "";
            //Login login = new Login();
            //login.Show();
            //this.Dispose();
        }

        private void btnAllRoomReport_Click_1(object sender, EventArgs e)
        {
            try
            {
                Rooms rm = new Rooms();
                rm.Show();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAllRoomTypeReport_Click_1(object sender, EventArgs e)
        {
            try
            {
                Types rmt = new Types();
                rmt.Show();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            //Refresh text fields
            data.roomTypeTableFilll(dgvRoomType);
            data.fillRoomType(cmbRoomType);
            data.fillRoomNo(cmbRoomNo);
            data.roomTableFilll(dgvRooms);
            clear();
        }
    }
}
