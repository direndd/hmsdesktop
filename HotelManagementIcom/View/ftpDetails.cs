using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using System.Text.RegularExpressions;
using System.Xml;
using System.IO;
using HotelManagementIcom.Controller;

namespace HotelManagementIcom.View
{
    public partial class ftpDetails : Telerik.WinControls.UI.RadForm
    {
        static string startPath = System.IO.Directory.GetCurrentDirectory();
        static string[] lines = Regex.Split(startPath, "bin");
        public static string ftpPath = lines[0] + @"ftpDet.xml";  // The Path to the ftpDet.Xml file //

        

        public ftpDetails()
        {
            InitializeComponent();

        }

        private void btnApplyFtp_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtBackupUrl.Text.ToString().Trim().Equals("") | txtBackupUrl.Text.ToString().Trim().Equals("") | txtBackupUrl.Text.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("All the details must be filled to succesfully connect to the ftp server", "Mistake", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                }
                else
                {
                    string ftpPath = Settings.ftpPath;

                    using (XmlWriter writer = XmlWriter.Create(ftpPath))
                    {
                        writer.WriteStartDocument();

                        writer.WriteStartElement("ftpDetails");

                        writer.WriteStartElement("ftp");

                        writer.WriteElementString("url", txtBackupUrl.Text.ToString().Trim());
                        writer.WriteElementString("username", txtFtpUname.Text.ToString().Trim());
                        writer.WriteElementString("password", txtftpPwd.Text.ToString().Trim());

                        writer.WriteEndElement();

                        writer.WriteEndElement();

                        writer.WriteEndDocument();
                    }
                }
            }
            catch (Exception ef)
            {
                MessageBox.Show(ef.Message);
            }

            if (File.Exists(Settings.ftpPath)) 
            {
                MessageBox.Show("FTP Details succesfuly saved","Success!",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }

        private void ftpDetails_Load(object sender, EventArgs e)
        {
            SettingsControl set = new SettingsControl();
            this.ThemeName = set.getTheme();
            this.BackColor = System.Drawing.Color.FromName(set.getColor());

            if (File.Exists(ftpPath))
            {

                FileStream READER = new FileStream(ftpPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //Set up the filestream (READER) //
                System.Xml.XmlDocument ftpDetails = new System.Xml.XmlDocument();// Set up the XmlDocument //
                ftpDetails.Load(READER); //Load the data from the file into the XmlDocument  //
                System.Xml.XmlNodeList NodeList = ftpDetails.GetElementsByTagName("ftpDetails"); // Create a list of the nodes in the xml file //

                txtBackupUrl.Text = NodeList[0].FirstChild.ChildNodes[0].InnerText.ToString();
                txtFtpUname.Text = NodeList[0].FirstChild.ChildNodes[1].InnerText.ToString();
                txtftpPwd.Text = NodeList[0].FirstChild.ChildNodes[2].InnerText.ToString();
            }
        }

        private void btnCancelFtp_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
