using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using HotelManagementIcom.Controller;
using System.Drawing.Drawing2D;

namespace HotelManagementIcom.View
{
    public partial class GallerySettings : Telerik.WinControls.UI.RadForm
    {
        private Stream requestStream = null;

        static string startPath = System.IO.Directory.GetCurrentDirectory();
        static string[] lines = Regex.Split(startPath, "bin");
        public static string galleryPagePath = lines[0] + @"galleryPage.xml"; // The Path to the galleryPage.Xml file 
        public static string sliderImagesPath = lines[0] + @"Images\Gallery\Slider\"; // The Path to slider images 
        public static string locationImagesPath = lines[0] + @"Images\Gallery\Locations\large\"; // The Path to location-large images 
        public static string locationSmallImagesPath = lines[0] + @"Images\Gallery\Locations\small\"; // The Path to location-large images 

        public GallerySettings()
        {
            InitializeComponent();
        }

        private void GallerySettings_Load(object sender, EventArgs e)
        {
            SettingsControl set = new SettingsControl();
            this.ThemeName = set.getTheme();
            this.BackColor = System.Drawing.Color.FromName(set.getColor());

            Console.WriteLine(sliderImagesPath);
            Console.WriteLine(locationImagesPath);
            Console.WriteLine(locationSmallImagesPath);

            FileStream READER = new FileStream(galleryPagePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //Set up the filestream (READER) //
            System.Xml.XmlDocument settings = new System.Xml.XmlDocument();// Set up the XmlDocument //
            settings.Load(READER); //Load the data from the file into the XmlDocument  //
            System.Xml.XmlNodeList NodeList = settings.GetElementsByTagName("GalleryPage"); // Create a list of the nodes in the xml file //

            txtSiteURL.Text = NodeList[0].FirstChild.ChildNodes[0].InnerText.ToString();
            txtSiteUserName.Text = NodeList[0].FirstChild.ChildNodes[1].InnerText.ToString();
            txtSitePassword.Text = NodeList[0].FirstChild.ChildNodes[2].InnerText.ToString();
            richTxtBoxDescription.Text = NodeList[0].FirstChild.ChildNodes[3].InnerText.ToString();
            rchTxtLocation1.Text = NodeList[0].FirstChild.ChildNodes[4].InnerText.ToString();
            rchTxtLocation2.Text = NodeList[0].FirstChild.ChildNodes[5].InnerText.ToString();
            rchTxtLocation3.Text = NodeList[0].FirstChild.ChildNodes[6].InnerText.ToString();
            rchTxtLocation4.Text = NodeList[0].FirstChild.ChildNodes[7].InnerText.ToString();
            rchTxtLocation5.Text = NodeList[0].FirstChild.ChildNodes[8].InnerText.ToString();
            rchTxtLocation6.Text = NodeList[0].FirstChild.ChildNodes[9].InnerText.ToString();
            rchTxtLocation7.Text = NodeList[0].FirstChild.ChildNodes[10].InnerText.ToString();
            rchTxtLocation8.Text = NodeList[0].FirstChild.ChildNodes[11].InnerText.ToString();
            rchTxtLocation9.Text = NodeList[0].FirstChild.ChildNodes[12].InnerText.ToString();
            rchTxtLocation10.Text = NodeList[0].FirstChild.ChildNodes[13].InnerText.ToString();
            rchTxtLocation11.Text = NodeList[0].FirstChild.ChildNodes[14].InnerText.ToString();
            rchTxtLocation12.Text = NodeList[0].FirstChild.ChildNodes[15].InnerText.ToString();

            picBoxSlider1.ImageLocation = sliderImagesPath + "1.png";
            picBoxSlider2.ImageLocation = sliderImagesPath + "2.png";
            picBoxSlider3.ImageLocation = sliderImagesPath + "3.png";
            picBoxSlider4.ImageLocation = sliderImagesPath + "4.png";
            picBoxSlider5.ImageLocation = sliderImagesPath + "5.png";
            picBoxSlider6.ImageLocation = sliderImagesPath + "6.png";
            picBoxSlider7.ImageLocation = sliderImagesPath + "7.png";
            picBoxSlider8.ImageLocation = sliderImagesPath + "8.png";
            picBoxSlider9.ImageLocation = sliderImagesPath + "9.png";
            picBoxSlider10.ImageLocation = sliderImagesPath + "10.png";
            picBoxSlider11.ImageLocation = sliderImagesPath + "11.png";
            picBoxSlider12.ImageLocation = sliderImagesPath + "12.png";

            picBoxLocation1.ImageLocation = locationImagesPath + "1.png";
            picBoxLocation2.ImageLocation = locationImagesPath + "2.png";
            picBoxLocation3.ImageLocation = locationImagesPath + "3.png";
            picBoxLocation4.ImageLocation = locationImagesPath + "4.png";
            picBoxLocation5.ImageLocation = locationImagesPath + "5.png";
            picBoxLocation6.ImageLocation = locationImagesPath + "6.png";
            picBoxLocation7.ImageLocation = locationImagesPath + "7.png";
            picBoxLocation8.ImageLocation = locationImagesPath + "8.png";
            picBoxLocation9.ImageLocation = locationImagesPath + "9.png";
            picBoxLocation10.ImageLocation = locationImagesPath + "10.png";
            picBoxLocation11.ImageLocation = locationImagesPath + "11.png";
            picBoxLocation12.ImageLocation = locationImagesPath + "12.png";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
              

       private void btnApply_Click(object sender, EventArgs e)
        {
                   
            try
            {
                if (txtSiteURL.Text.ToString().Trim().Equals("") | txtSitePassword.Text.ToString().Trim().Equals("") | txtSiteUserName.Text.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("Site Details are missing.Please fill all the fileds.","Sorry",MessageBoxButtons.OK,MessageBoxIcon.Error);
                    if (txtSiteURL.Text.ToString().Trim().Equals(""))
                    txtSiteURL.Focus();
                    if (txtSiteUserName.Text.ToString().Trim().Equals(""))
                    txtSiteUserName.Focus();
                    if (txtSitePassword.Text.ToString().Trim().Equals(""))
                    txtSitePassword.Focus();
                }
                else
                {

                    using (XmlWriter writer = XmlWriter.Create(galleryPagePath))
                    {

                        writer.WriteStartDocument();

                        writer.WriteStartElement("GalleryPage");

                        writer.WriteStartElement("gallery");

                        writer.WriteElementString("url", this.txtSiteURL.Text.ToString().Trim());               //Nodes with site details

                        writer.WriteElementString("username", this.txtSiteUserName.Text.ToString().Trim());

                        writer.WriteElementString("password", this.txtSitePassword.Text.ToString().Trim());

                        writer.WriteElementString("description", this.richTxtBoxDescription.Text.ToString().Trim()); //Node with Gallery Description

                        writer.WriteElementString("location1", this.rchTxtLocation1.Text.ToString().Trim());         //Nodes with location Details

                        writer.WriteElementString("location2", this.rchTxtLocation2.Text.ToString().Trim());

                        writer.WriteElementString("location3", this.rchTxtLocation3.Text.ToString().Trim());

                        writer.WriteElementString("location4", this.rchTxtLocation4.Text.ToString().Trim());

                        writer.WriteElementString("location5", this.rchTxtLocation5.Text.ToString().Trim());

                        writer.WriteElementString("location6", this.rchTxtLocation6.Text.ToString().Trim());

                        writer.WriteElementString("location7", this.rchTxtLocation7.Text.ToString().Trim());

                        writer.WriteElementString("location8", this.rchTxtLocation8.Text.ToString().Trim());

                        writer.WriteElementString("location9", this.rchTxtLocation9.Text.ToString().Trim());

                        writer.WriteElementString("location10", this.rchTxtLocation10.Text.ToString().Trim());

                        writer.WriteElementString("location11", this.rchTxtLocation11.Text.ToString().Trim());

                        writer.WriteElementString("location12", this.rchTxtLocation12.Text.ToString().Trim());

                        writer.WriteEndElement();

                        writer.WriteEndElement();

                        writer.WriteEndDocument();
                    }

                    //if (uploadToFtp(galleryPagePath,"ftp://" + txtSiteURL.Text.ToString().Trim() + "/App_Code/galleryPage.xml"))
                    //{
                    //    MessageBox.Show("Gallery Page Details Applied", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //}
                    //else
                    //{
                    //    MessageBox.Show("Gallery Page Details apllying failed", "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        
                    //}
                    
                    string temporaryXmlPath=@"E:\3 Project HMS\2nd Semester 2nd Iteration\19-08-2012 DD Web\HMS Final\App_Code\galleryPage.xml";
                    File.Copy(galleryPagePath, temporaryXmlPath);
                    
                    if (File.Exists(galleryPagePath) | File.Exists(temporaryXmlPath))
                    {
                        MessageBox.Show("Gallery Page Details Applied", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Gallery Page Details apllying failed", "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            
            }
            catch (Exception ef)
            {
                MessageBox.Show(ef.Message);
            }
        }

        public bool uploadToFtp(string sourcePath,string destinationPath)
        {
            try
            {
                
                FileStream READER = new FileStream(sourcePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //Set up the filestream (READER) //
                System.Xml.XmlDocument ftpDetails = new System.Xml.XmlDocument();// Set up the XmlDocument //
                ftpDetails.Load(READER); //Load the data from the file into the XmlDocument  //
                System.Xml.XmlNodeList NodeList = ftpDetails.GetElementsByTagName("GalleryPage"); // Create a list of the nodes in the xml file //

                String ftpserver = NodeList[0].FirstChild.ChildNodes[0].InnerText.ToString();
                String username = NodeList[0].FirstChild.ChildNodes[1].InnerText.ToString();
                String password = NodeList[0].FirstChild.ChildNodes[2].InnerText.ToString();


                // Get the object used to communicate with the server.
                String requesturl = destinationPath;
                Console.WriteLine(requesturl);

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(requesturl);
                request.Method = WebRequestMethods.Ftp.UploadFile;

                // This example assumes the FTP site uses anonymous logon.
                request.Credentials = new NetworkCredential(username, password);

                // Copy the contents of the file to the request stream.
                StreamReader sourceStream = new StreamReader(sourcePath);
                byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                sourceStream.Close();
                request.ContentLength = fileContents.Length;

                requestStream = request.GetRequestStream();
                this.Cursor = Cursors.WaitCursor;
                requestStream.Write(fileContents, 0, fileContents.Length);

                this.Cursor = Cursors.Default;
                Console.WriteLine("Upload Success!");
                
                requestStream.Close();

                return true;
            }
            catch (System.Net.WebException webExc)
            {
                MessageBox.Show("Problem with uploading backup file.Either the FTP account details are incorrect or your internet connection is down.", "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            
            return false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        

        private void upLoadToSlider(PictureBox picBox)
        {
            string sourceFilePath;
            string destFilePath;
            string destImageName;
            string destImageFullName;

            string[] picBoxName= Regex.Split(picBox.Name.ToString().Trim(),"picBoxSlider");
            destImageName=picBoxName[1];

            fileDialog.Title = "Select the image to be added to slider";
            fileDialog.FileName = "";
            fileDialog.Filter = "PNG Images (*.png*)|*.png*";
            fileDialog.Multiselect = false;

            if (fileDialog.ShowDialog() != DialogResult.Cancel)
            {
                sourceFilePath = fileDialog.FileName.ToString();

                Console.WriteLine("Source" + sourceFilePath);
                Console.WriteLine("Dest" + sliderImagesPath + destImageName + ".pmg");


                //if (sourceFilePath.EndsWith(".jpg") || sourceFilePath.EndsWith(".JPEG"))
                //{
                //    destImageFullName = destImageName + @".jpg";
                //    destFilePath = sliderImagesPath + destImageName + @".jpg";
                //}
                //else
                //{
                //    destImageFullName= destImageName + @".png";
                //    destFilePath = sliderImagesPath + destImageName + @".png";
                //}
                destImageFullName = destImageName + @".png";
                destFilePath = sliderImagesPath + destImageName + @".png";

                if (File.Exists(destFilePath))
                {
                    File.Delete(destFilePath);
                }
                File.Copy(sourceFilePath, destFilePath);

                //uploadToFtp(destFilePath, txtSiteURL.Text.ToString().Trim() + @"images\Gallery\Slider\" + destImageFullName);

                string temporaryPath = @"E:\3 Project HMS\2nd Semester 2nd Iteration\19-08-2012 DD Web\HMS Final\images\Gallery\Slider\" + destImageName + @".png";
                if (File.Exists(temporaryPath))
                {
                    File.Delete(temporaryPath);
                }
                File.Copy(sourceFilePath, temporaryPath);


                if (File.Exists(destFilePath) | File.Exists(temporaryPath))
                {
                    MessageBox.Show("Image succesfully added", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else 
                {
                    MessageBox.Show("Image adding failed", "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                picBox.Image = Image.FromFile(destFilePath);
            }
        }

        private void uploadToLocation(PictureBox picBox)
        {
            string sourceFilePath;
            string destFilePath;
            string destImageName;
            string destImageFullName;

            string[] picBoxName = Regex.Split(picBox.Name.ToString().Trim(), "picBoxLocation");
            destImageName = picBoxName[1];

            fileDialog.Title = "Select the Image to be added as a Location Image";
            fileDialog.FileName = "";
            fileDialog.Filter = "Jpeg Images (*.jpg*)|*.jpg*|PNG Images (*.png*)|*.png*";
            fileDialog.Multiselect = false;

            if (fileDialog.ShowDialog() != DialogResult.Cancel)
            {
                sourceFilePath = fileDialog.FileName.ToString();

                Console.WriteLine("Source" + sourceFilePath);
                Console.WriteLine("Dest" + locationImagesPath + destImageName + ".png");


                //if (sourceFilePath.EndsWith(".jpg") || sourceFilePath.EndsWith(".JPEG"))
                //{
                //    destImageFullName = destImageName + @".jpg";
                //    destFilePath = locationImagesPath + destImageName + @".jpg";
                //}
                //else
                //{
                //    destImageFullName = destImageName + @".png";
                //    destFilePath = locationImagesPath + destImageName + @".png";
                //}

                destImageFullName = destImageName + @".png";
                destFilePath = locationImagesPath + destImageName + @".png";

                if (File.Exists(destFilePath))
                {
                    File.Delete(destFilePath);
                }

                File.Copy(sourceFilePath, destFilePath);

                //uploadToFtp(destFilePath, txtSiteURL.Text.ToString().Trim() + @"images\Gallery\Slider\" + destImageFullName);

                string temporaryPath = @"E:\3 Project HMS\2nd Semester 2nd Iteration\19-08-2012 DD Web\HMS Final\images\Gallery\Locations\large\" + destImageName + @".png";
                if (File.Exists(temporaryPath))
                {
                    File.Delete(temporaryPath);
                }
                File.Copy(sourceFilePath, temporaryPath);

                if (File.Exists(destFilePath) | File.Exists(temporaryPath))
                {
                    MessageBox.Show("Image succesfully added", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Image adding failed", "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                picBox.Image = Image.FromFile(destFilePath);

                string smallDestPath = locationSmallImagesPath + destImageFullName;

                Image largeImage = Image.FromFile(destFilePath);
                Image smallImage = ResizeImage(largeImage, new Size(200, 150), false);
                MemoryStream memStream = new MemoryStream();
                smallImage.Save(memStream,System.Drawing.Imaging.ImageFormat.Png);
                FileStream writer = new FileStream(smallDestPath, FileMode.Create, FileAccess.ReadWrite);
                memStream.WriteTo(writer);

                string temporaryPathSmall = @"E:\3 Project HMS\2nd Semester 2nd Iteration\19-08-2012 DD Web\HMS Final\images\Gallery\Locations\small\" + destImageName + @".png";
                if (File.Exists(temporaryPathSmall))
                {
                    File.Delete(temporaryPathSmall);
                }
                File.Copy(smallDestPath, temporaryPathSmall);
            }
        }

        private System.Drawing.Image ResizeImage(Image image, Size size,
    bool preserveAspectRatio)
        {
            int newWidth;
            int newHeight;
            if (preserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)size.Width / (float)originalWidth;
                float percentHeight = (float)size.Height / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            else
            {
                newWidth = size.Width;
                newHeight = size.Height;
            }
            Image newImage = new Bitmap(newWidth, newHeight);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }
        
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       
        //Double Click events to add images - Slider Images
        //1
        private void picBoxSlider1_DoubleClick(object sender, EventArgs e)
        {
            upLoadToSlider(picBoxSlider1);
        }
        //2
        private void picBoxSlider2_DoubleClick(object sender, EventArgs e)
        {
            upLoadToSlider(picBoxSlider2);
        }
        //3
        private void picBoxSlider3_DoubleClick(object sender, EventArgs e)
        {
            upLoadToSlider(picBoxSlider3);
        }
        //4
        private void picBoxSlider4_DoubleClick(object sender, EventArgs e)
        {
            upLoadToSlider(picBoxSlider4);
        }
        //5
        private void picBoxSlider5_DoubleClick(object sender, EventArgs e)
        {
            upLoadToSlider(picBoxSlider5);
        }
        //6
        private void picBoxSlider6_DoubleClick(object sender, EventArgs e)
        {
            upLoadToSlider(picBoxSlider6);
        }
        //7
        private void picBoxSlider7_DoubleClick(object sender, EventArgs e)
        {
            upLoadToSlider(picBoxSlider7);
        }
        //8
        private void picBoxSlider8_DoubleClick(object sender, EventArgs e)
        {
            upLoadToSlider(picBoxSlider8);
        }
        //9
        private void picBoxSlider9_DoubleClick(object sender, EventArgs e)
        {
            upLoadToSlider(picBoxSlider9);
        }
        //10
        private void picBoxSlider10_DoubleClick(object sender, EventArgs e)
        {
            upLoadToSlider(picBoxSlider10);
        }
        //11
        private void picBoxSlider11_DoubleClick(object sender, EventArgs e)
        {
            upLoadToSlider(picBoxSlider11);
        }
        //12
        private void picBoxSlider12_DoubleClick(object sender, EventArgs e)
        {
            upLoadToSlider(picBoxSlider12);
        }


        //Double Click events to add images - Location Images
        
        //1
        private void picBoxLocation1_DoubleClick(object sender, EventArgs e)
        {
            uploadToLocation(picBoxLocation1);            
        }
        //2
        private void picBoxLocation2_DoubleClick(object sender, EventArgs e)
        {
            uploadToLocation(picBoxLocation2);
        }
        //3
        private void picBoxLocation3_DoubleClick(object sender, EventArgs e)
        {
            uploadToLocation(picBoxLocation3);
        }
        //4
        private void picBoxLocation4_DoubleClick(object sender, EventArgs e)
        {
            uploadToLocation(picBoxLocation4);
        }
        //5
        private void picBoxLocation5_DoubleClick(object sender, EventArgs e)
        {
            uploadToLocation(picBoxLocation5);
        }
        //6
        private void picBoxLocation6_DoubleClick(object sender, EventArgs e)
        {
            uploadToLocation(picBoxLocation6);
        }
        //7
        private void picBoxLocation7_DoubleClick(object sender, EventArgs e)
        {
            uploadToLocation(picBoxLocation7);
        }
        //8
        private void picBoxLocation8_DoubleClick(object sender, EventArgs e)
        {
            uploadToLocation(picBoxLocation8);
        }
        //9
        private void picBoxLocation9_DoubleClick(object sender, EventArgs e)
        {
            uploadToLocation(picBoxLocation9);
        }
        //10
        private void picBoxLocation10_DoubleClick(object sender, EventArgs e)
        {
            uploadToLocation(picBoxLocation10);
        }
        //11
        private void picBoxLocation11_DoubleClick(object sender, EventArgs e)
        {
            uploadToLocation(picBoxLocation11);
        }
        //12
        private void picBoxLocation12_DoubleClick(object sender, EventArgs e)
        {
            uploadToLocation(picBoxLocation12);
        }

        //Slider Images Animate Mouse Hover Mouse Leave
       //1
        private void picBoxSlider1_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        private void picBoxSlider1_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxSlider1.Image;
        }
        //2
        private void picBoxSlider2_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxSlider2.Image;
        }
        private void picBoxSlider2_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        //3
        private void picBoxSlider3_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxSlider2.Image;
        }
        private void picBoxSlider3_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        //4
        private void picBoxSlider4_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxSlider4.Image;
        }
        private void picBoxSlider4_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        //5
        private void picBoxSlider5_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxSlider5.Image;
        }
        private void picBoxSlider5_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        //6
        private void picBoxSlider6_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxSlider6.Image;
        }
        private void picBoxSlider6_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        //7
        private void picBoxSlider7_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxSlider7.Image;
        }
        private void picBoxSlider7_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        //8
        private void picBoxSlider8_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxSlider8.Image;
        }
        private void picBoxSlider8_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        //9
        private void picBoxSlider9_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxSlider9.Image;
        }
        private void picBoxSlider9_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        //10
        private void picBoxSlider10_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxSlider10.Image;
        }
        private void picBoxSlider10_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        //11
        private void picBoxSlider11_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxSlider11.Image;
        }
        private void picBoxSlider11_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        //12
        private void picBoxSlider12_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxSlider12.Image;
        }
        private void picBoxSlider12_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }

        //Location Images Animation Mouse Hover Mouse Leave
        private void picBoxLocation1_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxLocation1.Image;
        }
        private void picBoxLocation1_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        //2
        private void picBoxLocation2_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxLocation2.Image;
        }
        private void picBoxLocation2_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        //3
        private void picBoxLocation3_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxLocation3.Image;
        }
        private void picBoxLocation3_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        //4
        private void picBoxLocation4_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxLocation4.Image;
        }
        private void picBoxLocation4_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        //5
        private void picBoxLocation5_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxLocation5.Image;
        }
        private void picBoxLocation5_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        //6
        private void picBoxLocation6_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxLocation6.Image;
        }
        private void picBoxLocation6_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        //7
        private void picBoxLocation7_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxLocation7.Image;
        }
        private void picBoxLocation7_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        //8
        private void picBoxLocation8_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxLocation8.Image;
        }
        private void picBoxLocation8_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        //9
        private void picBoxLocation9_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxLocation9.Image;
        }
        private void picBoxLocation9_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        //10
        private void picBoxLocation10_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxLocation10.Image;
        }
        private void picBoxLocation10_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        //11
        private void picBoxLocation11_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxLocation11.Image;
        }
        private void picBoxLocation11_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }
        //12
        private void picBoxLocation12_MouseHover(object sender, EventArgs e)
        {
            picBoxEnlarge.Visible = true;
            picBoxEnlarge.BringToFront();
            picBoxEnlarge.Image = picBoxLocation12.Image;
        }
        private void picBoxLocation12_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnlarge.SendToBack();
            picBoxEnlarge.Visible = false;
        }

        
        
    }
}
