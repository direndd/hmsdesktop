using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using HotelManagementIcom.DataAccess;

namespace HotelManagementIcom.View
{
    public partial class ManageFacilities : Telerik.WinControls.UI.RadForm
    {
        DataController data = new DataController();

    

        public ManageFacilities()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void ManageFacilities_Load(object sender, EventArgs e)
        {
            data.hotelFacTableFilll(dgvHotelFacilities);
            data.hotelFacTableFilll(dgvAssinFacilities);
            data.fillRoomType(cmbAssignFac);
            data.fillRoomType(cmbViewFac);
            lblFacName.Visible = false;
        }
        public void clear()
        {
            txtFacName.Text=string.Empty;
        }
        public void refresh() 
        {
            clear();
            data.hotelFacTableFilll(dgvHotelFacilities);
            data.hotelFacTableFilll(dgvAssinFacilities);
            lblFacName.Visible = false;

        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try{

            if (this.txtFacName.Text == "")
            {
                MessageBox.Show("Facility Name must be provided");
                return;
            }
             DialogResult result = MessageBox.Show("Are These Information Correct ?", "Confirm Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    String facName = this.txtFacName.Text.Trim();
                    data.addNewFacility(facName);

                }
                else
                {
                    MessageBox.Show("User Canceled The Action", " Insertion Cancelled ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                clear();
                data.hotelFacTableFilll(dgvHotelFacilities);
                data.hotelFacTableFilll(dgvAssinFacilities);
                lblFacName.Visible = false;
                
                

            }
        
            catch (Exception ex8)
            {
                var msg = ex8.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void txtFacName_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Textbox only accepting numbers and letters
            if ((char.IsLetter(e.KeyChar) || char.IsDigit(e.KeyChar)||char.IsControl(e.KeyChar)))
            {
                e.Handled = false;
                lblFacName.Visible = false;

            }

            else
            {
                e.Handled = true;
                lblFacName.Visible = true;
            } 
        }

        private void dgvHotelFacilities_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int cell = e.RowIndex;
                txtFacName.Text = dgvHotelFacilities.Rows[cell].Cells[0].Value.ToString();
               
            }
            catch (Exception ex6)
            {
                var msg = ex6.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                String facName = this.txtFacName.Text.Trim();

                DialogResult result = MessageBox.Show("Are you sure ?", "Confirm Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    data.deleteFacility(facName);
                    
                }
                else
                {
                    MessageBox.Show("User Canceled The Action", " Deletion Cancelled ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                clear();
                data.hotelFacTableFilll(dgvHotelFacilities);
                data.hotelFacTableFilll(dgvAssinFacilities);
                lblFacName.Visible = false;
                

            }
            catch (Exception ex9)
            {
                var msg = ex9.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            refresh();
        }

        private void cmbViewFac_SelectedIndexChanged(object sender, EventArgs e)
        {
           String roomType = cmbViewFac.SelectedItem.ToString();
           dgvRoomFacilities.DataSource = data.getFac(roomType);
           
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                ManageRooms rm = new ManageRooms();
                rm.Show();
                this.Hide();
            }
            catch (Exception ex2)
            {
                var msg = ex2.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAssign_Click(object sender, EventArgs e)
        {
            string fac="";
            string roomtype=cmbAssignFac.SelectedItem.ToString();

            data.deleteRoomfacility(roomtype);
            int rowCount = dgvAssinFacilities.RowCount;
            for (int i = 0; i < rowCount; i++)
            {
                if (dgvAssinFacilities.Rows[i].Cells[0].Value != null) //Cells[0] Because in cell 0th cell we have added checkbox
                {
                        fac = dgvAssinFacilities.Rows[i].Cells[1].Value.ToString();
                        data.assinRoomFacility(roomtype, fac);

                }
            }
            dgvRoomFacilities.DataSource = data.getFac(roomtype);
            data.hotelFacTableFilll(dgvAssinFacilities);
            
        
        }
    }
}
