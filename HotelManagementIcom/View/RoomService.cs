using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using System.Collections;
using HotelManagementIcom.Controller;
using System.Data.Objects;


using HotelManagementIcom.Model;
using System.Linq;


namespace HotelManagementIcom.View
{
    public partial class RoomService : Telerik.WinControls.UI.RadForm
    {
        public RoomService()
        {
            InitializeComponent();
        }

        private void btnBck_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void RoomService_Load(object sender, EventArgs e)
        {
            SettingsControl set = new SettingsControl();
            this.ThemeName = set.getTheme();
            this.BackColor = System.Drawing.Color.FromName(set.getColor());

            hotelmsEntities1 hms2 = new hotelmsEntities1();
            dataGridView1.AutoGenerateColumns = true;
            dataGridView1.DataSource = hms2.roomServices.Select(i => new { i.OpenBy, i.user.userName, i.room, i.requestedBy, i.RequestTime, i.reason });
   
            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            hotelmsEntities1 hms2 = new hotelmsEntities1();
            try
            {
                serialPort1.Open();
                serialPort1.Write(txtRNo.Text.Trim());
                serialPort1.Close();


               // loggin  the details";
                MessageBox.Show("Opend");
                roomService Rservice = new roomService();
                Rservice.OpenBy = "usr1";
                Rservice.reason = txtboxReason.Text;
                Rservice.room = txtRNo.Text.Trim();
                Rservice.requestedBy=txtrequested.Text;
                Rservice.RequestTime = System.DateTime.Now;
                hms2.AddToroomServices(Rservice);
                hms2.SaveChanges();

                hotelmsEntities1 hms3 = new hotelmsEntities1();
                dataGridView1.AutoGenerateColumns = true;
                dataGridView1.DataSource = hms3.roomServices.Select(i => new { i.OpenBy,i.user.userName, i.room,i.requestedBy,i.RequestTime, i.reason });
   

            }
            catch (Exception)
            {
                MessageBox.Show("Port not found","Please Connect the SerializableAttribute port cable");


            }

        }

        private void dataGridView1_ColumnHeaderMouseClick(object sender, System.Windows.Forms.DataGridViewCellMouseEventArgs e)
        {

           
	             
            
        }
    }
}
