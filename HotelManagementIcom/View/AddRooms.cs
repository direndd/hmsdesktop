using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using HotelManagementIcom.DataAccess;
using HotelManagementIcom.Controller;

namespace HotelManagementIcom.View
{
    public partial class AddRooms : Telerik.WinControls.UI.RadForm
    {
        DataController data = new DataController();

        public AddRooms()
        {
            InitializeComponent();
        }
        
        private void button5_Click(object sender, EventArgs e)
        {
            ManageRooms rm = new ManageRooms();
            rm.Show();
            this.Hide();
        }

        private void AddRooms_Load(object sender, EventArgs e)
        {
            SettingsControl set = new SettingsControl();
            this.ThemeName = set.getTheme();
            this.BackColor = System.Drawing.Color.FromName(set.getColor());

            try
            {
                lblDescription.Visible = false;
                lblFloor.Visible = false;
                lblRoomNo.Visible = false;

                data.roomTableFilll(dgvRooms);
                data.fillRoomType(cmbRoomType);
                data.fillRoomNo(cmbRoomNo);
                data.checkAvailability(cmbAvailability);
                lblNoOfRooms.Enabled = false;
                nudNoOfRooms.Enabled = false;
                btnAddMul.Enabled = false;
            }
            catch (Exception ex1)
            {
                var msg = ex1.Message;
                MessageBox.Show(msg, "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void Clear() 
        {
            try
            {
                lblDescription.Visible = false;
                lblFloor.Visible = false;
                lblRoomNo.Visible = false;

                cmbRoomNo.SelectedIndex = 0;
                cmbRoomType.SelectedIndex = 0;

                txtFloorNo.Text = string.Empty;
                txtDescription.Text = string.Empty;
                cmbAvailability.SelectedIndex = 0;
            }
            catch (Exception ex2)
            {
                var msg = ex2.Message;
                MessageBox.Show(msg, "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        
        }
        
        private void dgvRooms_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int cell = e.RowIndex;
                cmbRoomNo.Text = dgvRooms.Rows[cell].Cells[0].Value.ToString();
                cmbRoomType.Text = dgvRooms.Rows[cell].Cells[1].Value.ToString();
                txtFloorNo.Text = dgvRooms.Rows[cell].Cells[2].Value.ToString();
                txtDescription.Text = dgvRooms.Rows[cell].Cells[3].Value.ToString();
                this.picImage.Image = data.getPicture(cmbRoomType.Text.Trim());

                if (dgvRooms.Rows[e.RowIndex].Cells[4].Value == null || dgvRooms.Rows[e.RowIndex].Cells[4].Value.ToString() == "" || dgvRooms.Rows[e.RowIndex].Cells[4].Value.ToString() == "False") //not available
                {
                    cmbAvailability.Text = "Not Available";
                }
                else //value = true, available
                {
                    cmbAvailability.Text = "Available";
                }
            }
            catch (Exception ex3)
            {
                var msg = ex3.Message;
                MessageBox.Show(msg, "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbRoomNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String roomNo = cmbRoomNo.SelectedItem.ToString();
                List<room> room = data.searchRoom(roomNo);

                room rm = (room)room[0];
                cmbRoomNo.Text = rm.roomNo;
                cmbRoomType.Text = rm.roomType;
                txtFloorNo.Text = rm.floorNo;
                txtDescription.Text = rm.descrip;

                string aval = rm.avalability.ToString();
                if (aval.Equals("True") || aval.Equals("true"))
                {
                    cmbAvailability.Text = "Available";
                }
                else
                    cmbAvailability.Text = "Not Available";

                this.picImage.Image = data.getPicture(cmbRoomType.Text.Trim());

            }
            catch (Exception ex4)
            {
                var msg = ex4.Message;
                MessageBox.Show(msg, "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAddOne_Click(object sender, EventArgs e)
        {
            try
            {
               
                if (this.cmbRoomNo.Text == "")
                {
                    MessageBox.Show("Room Number must be provided");
                    return;
                }

                if (this.cmbRoomType.Text == "")
                {
                    MessageBox.Show("Room Type must be provided");
                    return;
                }
                if (this.txtFloorNo.Text == "")
                {
                    MessageBox.Show("Floor Number must be provided");
                    return;
                }
                if (this.txtDescription.Text == "")
                {
                    MessageBox.Show("Small description must be provided");
                    return;
                }
                if (this.cmbAvailability.Text == "")
                {
                    MessageBox.Show("Availability must be provided");
                    return;
                }
                 DialogResult result = MessageBox.Show("Are These Information Correct ?", "Confirm Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
               
                String roomNo = this.cmbRoomNo.Text.Trim();
                String typeName = this.cmbRoomType.Text.Trim();
                String availability = this.cmbAvailability.Text.Trim();
                String floor = this.txtFloorNo.Text.Trim();
                String description = this.txtDescription.Text.Trim();
               

                data.addNewRoom(roomNo,typeName,floor,description,availability);
                }
                else
                {
                    MessageBox.Show("User Canceled The Action", " Insertion Cancelled ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Clear();
                data.roomTableFilll(this.dgvRooms);
                data.fillRoomNo(this.cmbRoomNo);
            }
            catch (Exception ex5)
            {
                var msg = ex5.Message;
                MessageBox.Show(msg, "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rdbAddOneRoom_CheckedChanged(object sender, EventArgs e)
        {
                 btnAddOne.Enabled = true;
                lblNoOfRooms.Enabled = false;
                nudNoOfRooms.Enabled = false;
                btnAddMul.Enabled = false;
            
        }

        private void rdbAddMultipleRooms_CheckedChanged(object sender, EventArgs e)
        {
            btnAddOne.Enabled = false;
            lblNoOfRooms.Enabled = true;
            nudNoOfRooms.Enabled = true;
            btnAddMul.Enabled = true;

        }

        private void btnAddMul_Click(object sender, EventArgs e)
        {
            try
            {

                if (this.cmbRoomNo.Text == "")
                {
                    MessageBox.Show("Room Number must be provided");
                    return;
                }

                if (this.cmbRoomType.Text == "")
                {
                    MessageBox.Show("Room Type must be provided");
                    return;
                }
                if (this.txtFloorNo.Text == "")
                {
                    MessageBox.Show("Floor Number must be provided");
                    return;
                }
                if (this.txtDescription.Text == "")
                {
                    MessageBox.Show("Small description must be provided");
                    return;
                }
                if (this.cmbAvailability.Text == "")
                {
                    MessageBox.Show("Availability must be provided");
                    return;
                }
                
                 DialogResult result = MessageBox.Show("Are These Information Correct ?", "Confirm Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    String roomNo = this.cmbRoomNo.Text.Trim();
                    String typeName = this.cmbRoomType.Text.Trim();
                    String availability = this.cmbAvailability.Text.Trim();
                    String floor = this.txtFloorNo.Text.Trim();
                    String description = this.txtDescription.Text.Trim();

                    int recurrence = Convert.ToInt32(nudNoOfRooms.Value);
                    int no= Convert.ToInt32(this.cmbRoomNo.Text);

                    for (int x = 1; x <= recurrence;x++ ) 
                    {
                        String rmno = no.ToString();
                        data.addNewRoom(rmno, typeName, floor, description, availability);
                        no++;

                    }
                    MessageBox.Show(recurrence + " Rooms Added Successfully");
                }
                else
                {
                    MessageBox.Show("User Canceled The Action", " Insertion Cancelled ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                
                Clear();
                data.roomTableFilll(this.dgvRooms);
                data.fillRoomNo(this.cmbRoomNo);
                
            }
            catch (Exception ex6)
            {
                var msg = ex6.Message;
                MessageBox.Show(msg, "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.cmbRoomNo.Text == "")
                {
                    MessageBox.Show("Room No must be provided");
                    return;
                }
                 DialogResult result = MessageBox.Show("Are These Information Correct ?", "Confirm Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {

                    String roomNo = this.cmbRoomNo.Text.Trim();
                    String typeName = this.cmbRoomType.Text.Trim();
                    String floor = this.txtFloorNo.Text.Trim();
                    String descriprtin = this.txtDescription.Text.Trim();
                    String availability = this.cmbAvailability.Text.Trim();


                    data.updateRoom(roomNo, typeName, floor, descriprtin, availability);
                }
                else
                {
                    MessageBox.Show("User Canceled The Action", " Updating Cancelled ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Clear();
                data.roomTableFilll(this.dgvRooms);
                data.fillRoomNo(this.cmbRoomNo);
               

            }
            catch (Exception ex7)
            {
                var msg = ex7.Message;
                MessageBox.Show(msg, "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                String no = this.cmbRoomNo.Text.Trim();
                DialogResult result = MessageBox.Show("Are you sure you want to delete this ?", "Confirm Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {

                    data.deleteRoom(no);
                }
                else
                {
                    MessageBox.Show("User Canceled the Action", " Deletion Cancelled ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Clear();
                data.roomTableFilll(this.dgvRooms);
                data.fillRoomNo(this.cmbRoomNo);
                
            }
            catch (Exception ex8)
            {
                var msg = ex8.Message;
                MessageBox.Show(msg, "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbRoomNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Room No Combox only accepting numbers
            if (char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
                lblRoomNo.Visible = false;
}
            // if (char.IsControl(e.KeyChar))
            //{
            //    e.Handled = false;
            //    lblRoomNo.Visible = false;

            //}
            else
            {
                e.Handled = true;
                lblRoomNo.Visible = true;
            } 
        }

        private void txtFloorNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Textbox only accepting numbers and letters
            if ((char.IsLetter(e.KeyChar) || char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar)))
            {
                e.Handled = false;
                lblFloor.Visible = false;

            }

            else
            {
                e.Handled = true;
                lblFloor.Visible = true;
            } 
        }

        private void txtDescription_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Textbox only accepting numbers and letters
            if ((char.IsLetter(e.KeyChar) || char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar)))
            {
                e.Handled = false;
                lblDescription.Visible = false;

            }

            else
            {
                e.Handled = true;
                lblDescription.Visible = true;
            } 
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

    }
}
