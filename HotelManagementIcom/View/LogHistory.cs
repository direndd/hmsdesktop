using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using HotelManagementIcom;
using HotelManagementIcom.Model;
using System.Linq;
using HotelManagementIcom.Controller;

namespace HotelManagementIcom.View
{
    public partial class LogHistory : Telerik.WinControls.UI.RadForm
    {
        public LogHistory()
        {
            InitializeComponent();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = 1000;
            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            string[] Time = DateTime.Now.ToString().Split(' ');

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Home h = new Home();
            h.Show();
            this.Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            reservationTableFilll();
        }
        public void reservationTableFilll()
        {
            hotelmsEntities1 hms = new hotelmsEntities1();
            dataGridLogHistory.AutoGenerateColumns = true;
            IEnumerable<roomre> bookedroms = hms.roomres;
            // var allReservations = hms.roomres.Select(i => new { Reservation = i.reservationId, RoomNo = i.room.roomNo, F_Name = i.reservation.customer.fName, L_Nname = i.reservation.customer.lName, CheckIn = i.startDate, CheckOut = i.endDate, Adults = i.noOfAdults, Children = i.noOfChildren, Invoice = i.reservation.invoiceId, Total = i.reservation.invoice.total, Balance = i.reservation.invoice.balance }).Where(ii => ii.Reservation == id);
            var allReservations = hms.logHistories.Select(i => new { Log_history_ID = i.logHistoryId, Log_In_Time = i.logInTime, Log_Out_Time = i.logOutTime, User_ID = i.userId, User_Name = i.user.userName, User_Level = i.user.userLevel });
            dataGridLogHistory.DataSource = allReservations;
        }

        private void LogHistory_Load(object sender, EventArgs e)
        {
            SettingsControl set = new SettingsControl();
            this.ThemeName = set.getTheme();
            this.BackColor = System.Drawing.Color.FromName(set.getColor());

            reservationTableFilll();
        }

        private void button1_Leave(object sender, EventArgs e)
        {

        }
        //private void LogHistory_UnLoad(object sender, eventFormClosingEventArgs e)
        //{
        //  Logout.logOut();
        //}



    }
}
