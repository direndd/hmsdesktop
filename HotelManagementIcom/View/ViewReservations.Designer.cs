namespace HotelManagementIcom.View
{
    partial class ViewReservations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewReservations));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtBoxNic = new System.Windows.Forms.TextBox();
            this.txtBoxPassPortS = new System.Windows.Forms.TextBox();
            this.txtBoxCustNameS = new System.Windows.Forms.TextBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.dataGridViewReservations = new System.Windows.Forms.DataGridView();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.office2007Black = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.vista = new Telerik.WinControls.Themes.VistaTheme();
            this.office2010 = new Telerik.WinControls.Themes.Office2010Theme();
            this.telerik = new Telerik.WinControls.Themes.TelerikTheme();
            this.aqua = new Telerik.WinControls.Themes.AquaTheme();
            this.desert = new Telerik.WinControls.Themes.DesertTheme();
            this.breeze = new Telerik.WinControls.Themes.BreezeTheme();
            this.office2007SilverTheme1 = new Telerik.WinControls.Themes.Office2007SilverTheme();
            this.windows7Theme1 = new Telerik.WinControls.Themes.Windows7Theme();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReservations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.txtBoxNic);
            this.groupBox1.Controls.Add(this.txtBoxPassPortS);
            this.groupBox1.Controls.Add(this.txtBoxCustNameS);
            this.groupBox1.Controls.Add(this.radLabel6);
            this.groupBox1.Controls.Add(this.radLabel1);
            this.groupBox1.Controls.Add(this.radLabel4);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Location = new System.Drawing.Point(12, 397);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(868, 71);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search pannel";
            // 
            // txtBoxNic
            // 
            this.txtBoxNic.Location = new System.Drawing.Point(372, 25);
            this.txtBoxNic.Name = "txtBoxNic";
            this.txtBoxNic.Size = new System.Drawing.Size(180, 23);
            this.txtBoxNic.TabIndex = 2;
            this.txtBoxNic.TextChanged += new System.EventHandler(this.txtBoxNic_TextChanged);
            // 
            // txtBoxPassPortS
            // 
            this.txtBoxPassPortS.Location = new System.Drawing.Point(658, 25);
            this.txtBoxPassPortS.Name = "txtBoxPassPortS";
            this.txtBoxPassPortS.Size = new System.Drawing.Size(180, 23);
            this.txtBoxPassPortS.TabIndex = 3;
            this.txtBoxPassPortS.TextChanged += new System.EventHandler(this.txtBoxPassPortS_TextChanged);
            // 
            // txtBoxCustNameS
            // 
            this.txtBoxCustNameS.Location = new System.Drawing.Point(86, 25);
            this.txtBoxCustNameS.Name = "txtBoxCustNameS";
            this.txtBoxCustNameS.Size = new System.Drawing.Size(180, 23);
            this.txtBoxCustNameS.TabIndex = 1;
            this.txtBoxCustNameS.TextChanged += new System.EventHandler(this.txtBoxCustNameS_TextChanged);
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(12, 27);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(74, 19);
            this.radLabel6.TabIndex = 76;
            this.radLabel6.Text = "Guest Name";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(310, 27);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(33, 19);
            this.radLabel1.TabIndex = 77;
            this.radLabel1.Text = "N I C";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(600, 27);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(53, 19);
            this.radLabel4.TabIndex = 78;
            this.radLabel4.Text = "Passport";
            // 
            // dataGridViewReservations
            // 
            this.dataGridViewReservations.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewReservations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewReservations.Location = new System.Drawing.Point(12, 12);
            this.dataGridViewReservations.Name = "dataGridViewReservations";
            this.dataGridViewReservations.Size = new System.Drawing.Size(868, 379);
            this.dataGridViewReservations.TabIndex = 3;
            this.dataGridViewReservations.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewReservations_RowHeaderMouseClick);
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.Color.Green;
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.Font = new System.Drawing.Font("Microsoft Tai Le", 10F);
            this.btnRefresh.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnRefresh.Location = new System.Drawing.Point(634, 474);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(120, 32);
            this.btnRefresh.TabIndex = 4;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Gray;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Tai Le", 10F);
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(760, 474);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 32);
            this.button1.TabIndex = 5;
            this.button1.Text = "Back";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ViewReservations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(892, 518);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.dataGridViewReservations);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Tai Le", 12F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "ViewReservations";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "View Reservations";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.ViewReservations_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReservations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtBoxPassPortS;
        private System.Windows.Forms.TextBox txtBoxCustNameS;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private System.Windows.Forms.DataGridView dataGridViewReservations;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtBoxNic;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007Black;
        private Telerik.WinControls.Themes.VistaTheme vista;
        private Telerik.WinControls.Themes.Office2010Theme office2010;
        private Telerik.WinControls.Themes.TelerikTheme telerik;
        private Telerik.WinControls.Themes.AquaTheme aqua;
        private Telerik.WinControls.Themes.DesertTheme desert;
        private Telerik.WinControls.Themes.BreezeTheme breeze;
        private Telerik.WinControls.Themes.Office2007SilverTheme office2007SilverTheme1;
        private Telerik.WinControls.Themes.Windows7Theme windows7Theme1;
    }
}

