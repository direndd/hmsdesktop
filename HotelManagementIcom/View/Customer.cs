using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using HotelManagementIcom.Model;
using HotelManagementIcom.Controller;
using System.Linq;
 

namespace HotelManagementIcom.View
{
    public partial class  Customer : Telerik.WinControls.UI.RadForm
    {
        hotelmsEntities1 hmsC = new hotelmsEntities1();
        hotelmsEntities1 hms2 = new hotelmsEntities1();
        //customer cus1 = new customer();
        DataTable dataTable1 = new DataTable();
        ValidateFields vf = new ValidateFields();
       
        public Customer()
        {
            InitializeComponent();

            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = 1000;
            timer.Start();
        }
               
        private void Users_Load(object sender, EventArgs e)
        {
            SettingsControl set = new SettingsControl();
            this.ThemeName = set.getTheme();
            this.BackColor = System.Drawing.Color.FromName(set.getColor());

            gridCustomer.AutoGenerateColumns = true;
            gridCustomer.DataSource = hmsC.customers;

            lblUserName.Text = new LoggedUser().UserName1;
            lblUserLevel.Text = new LoggedUser().UserLevel1;
        }

       
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            customer cus1 = new customer();
            if ((string.Compare(txtNic.Text, "") == 0 || string.Compare(txtNic.Text, " ") == 0) && (string.Compare(txtPassport.Text, "") == 0 || string.Compare(txtPassport.Text, " ") == 0))
            {
                MessageBox.Show("NIC or Passport must be provided","Sorry");
                return;
            }
            if (checkForSameNic()==false)
            {
             //   MessageBox.Show("NIC must have 9 digits and one letter");
                return;
            }
            //if (vf.ValidateNIC(txtNic.Text.Trim()) == false)
            //{
            //    MessageBox.Show("NIC must have 9 digits and one letter", "Sorry");
            //    return;
            //}
            
            if (vf.ValidateEmail(txtEmail.Text) == false)
            {
                MessageBox.Show("Invalid e-mail address", "Sorry");
                return;
            }
            try
            {
                string newCusId = generateId();
                cus1.cusId = newCusId;
                cus1.nic = txtNic.Text;
                cus1.passport = txtPassport.Text;
                cus1.nationalty = txtNationality.Text;
                cus1.allergies = txtAllergies.Text;
                cus1.dob =dateTimePicker1Dob.Value;
                cus1.fName = txtFname.Text;
                cus1.mName = txtMname.Text;
                cus1.lName = txtLname.Text;
                cus1.num = txtNo.Text;
                cus1.street = txtStreet.Text;
                cus1.city = txtCity.Text;
                cus1.town = txtTown.Text;
                cus1.land = txtLandPhone.Text;
                cus1.mobile = txtMoblePhone.Text;
                cus1.email = txtEmail.Text;
                if (radioFemale.Checked == true)
                    cus1.gender = "Female";
                if (radioMale.Checked == true)
                    cus1.gender = "Male";
              cus1.country = txtCountry.Text;              
                hmsC.AddTocustomers(cus1);
                hmsC.SaveChanges();
                reLoadGrid();
                MessageBox.Show("Customer added successfully","Succesful");
            }
            catch (Exception) { throw; }         
            clearFields();
        }   

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (vf.ValidateEmail(txtEmail.Text) == false)
            {
               return;
            }
            //if (vf.ValidateNIC(txtNic.Text.Trim()) == false)
            //{
            //    MessageBox.Show("NIC must have 9 digits and one letter", "Sorry");
            //    return;
            //}
          
            string n1 = lblCusId.Text.Trim().ToString();
            customer cus3 = hmsC.customers.Where(f => f.cusId== n1).First();
            cus3.nic = txtNic.Text;
            cus3.passport = txtPassport.Text;
            cus3.nationalty = txtNationality.Text;
            cus3.allergies = txtAllergies.Text;
            cus3.fName = txtFname.Text;
            cus3.mName = txtMname.Text;
            cus3.lName = txtLname.Text;
            cus3.num = txtNo.Text;
            cus3.street = txtStreet.Text;
            cus3.city = txtCity.Text;
            cus3.town = txtTown.Text;
            cus3.land = txtLandPhone.Text;
            cus3.mobile= txtMoblePhone.Text;
            cus3.email = txtEmail.Text;
            if (radioFemale.Checked == true)
                cus3.gender = "Female";
            if (radioMale.Checked == true)
                cus3.gender = "Male";
           cus3.country = txtCountry.Text; 
           cus3.dob =dateTimePicker1Dob.Value;
            reLoadGrid();
            hmsC.SaveChanges();
            MessageBox.Show("Guset details updated Successfully!","Succesful");
            reLoadGrid();
            clearFields();
        }

        private void gridCustomer_SelectionChanged(object sender, EventArgs e)
        {
           
        }

        public string generateId()
        {
            var cusIds = hms2.customers.Select(ii => new { ii.cusId});
            int MaxCId = 0;
            foreach (var item in cusIds)
            {
                if (item.cusId != string.Empty)
                {
                    string cId = item.cusId.Substring(3);
                    int cIdSubString = Int16.Parse(cId);
                    if (MaxCId < cIdSubString)
                        { MaxCId = cIdSubString; }
                }
                else
                    { MaxCId = 0; }
            }
            MaxCId++;
            // label1.Text = "usr" + MaxUId.ToString();
            string newCid = "cus" + MaxCId.ToString();
            return newCid;

        }
        private void reLoadGrid()
        {
            //  gridUser.DataSource = null;
            hotelmsEntities1 hms3 = new hotelmsEntities1();
            gridCustomer.AutoGenerateColumns = true;
            gridCustomer.DataSource = hms3.customers;
        }

        public void clearFields()
        {
            lblCusId.Text = null; ;
            txtFname.Text = null;
            txtMname.Text = null;
            txtLname.Text = null;
            txtNo.Text = null;
            txtStreet.Text = null;
            txtCity.Text = null;
            txtTown.Text = null;
            txtLandPhone.Text = null;
            txtMoblePhone.Text = null;
            txtEmail.Text = null;
            txtNic.Text = null;
            txtPassport.Text = null;
            txtNationality.Text = null;
            txtAllergies.Text = null;
            txtCountry.Text = null;
            radioMale.Checked = false;
            radioFemale.Checked = false;
        }

        private void radGridView1_SelectionChanged(object sender, EventArgs e)
        {
        }

        private void gridCustomer_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            
            StringBuilder cusId = new StringBuilder();
            StringBuilder nic = new StringBuilder();
            StringBuilder fname = new StringBuilder();
            StringBuilder mname = new StringBuilder();
            StringBuilder lname = new StringBuilder();
            StringBuilder num = new StringBuilder();
            StringBuilder street = new StringBuilder();
            StringBuilder city = new StringBuilder();
            StringBuilder town = new StringBuilder();
            StringBuilder landPhone = new StringBuilder();
            StringBuilder mobilePhone = new StringBuilder();
            StringBuilder email = new StringBuilder();
            StringBuilder passport = new StringBuilder();
            StringBuilder nationality = new StringBuilder();
            StringBuilder allergies = new StringBuilder();
            StringBuilder gender = new StringBuilder();
            StringBuilder country = new StringBuilder();
            StringBuilder dob = new StringBuilder();
            //string rowId = (gridCustomer.SelectedRows[0].Cells[0].Value.ToString());
            string rowId = (gridCustomer.Rows[e.RowIndex].Cells[0].Value.ToString());
            // MessageBox.Show("row id is " + rowId);
            try
            {
                IEnumerable<customer> c = hmsC.customers.Where(i => i.cusId == rowId.Trim());
                foreach (customer u in c)
                {
                    cusId.Append(u.cusId + Environment.NewLine);
                    nic.Append(u.nic + Environment.NewLine);
                    fname.Append(u.fName + Environment.NewLine);
                    mname.Append(u.mName + Environment.NewLine);
                    lname.Append(u.lName + Environment.NewLine);
                    num.Append(u.num + Environment.NewLine);
                    street.Append(u.street + Environment.NewLine);
                    city.Append(u.city + Environment.NewLine);
                    town.Append(u.town + Environment.NewLine);
                    landPhone.Append(u.land + Environment.NewLine);
                    mobilePhone.Append(u.mobile + Environment.NewLine);
                    email.Append(u.email + Environment.NewLine);
                    passport.Append(u.passport + Environment.NewLine);
                    nationality.Append(u.nationalty + Environment.NewLine);
                    allergies.Append(u.allergies + Environment.NewLine);
                    gender.Append(u.gender + Environment.NewLine);
                    country.Append(u.country + Environment.NewLine);
                    dob.Append(u.dob + Environment.NewLine);                
                }
            }
            catch 
            {
                MessageBox.Show("Invalid column selection");
            }
                
            lblCusId.Text = cusId.ToString();
            txtNic.Text = nic.ToString();
            txtFname.Text = fname.ToString();
            txtMname.Text = mname.ToString();
            txtLname.Text = lname.ToString();
            txtNo.Text = num.ToString();
            txtStreet.Text = street.ToString();
            txtCity.Text = city.ToString();
            txtTown.Text = town.ToString();
            txtLandPhone.Text = landPhone.ToString();
            txtMoblePhone.Text = mobilePhone.ToString();
            txtEmail.Text = email.ToString().Trim();
            txtPassport.Text = passport.ToString();
            txtNationality.Text = nationality.ToString();
            //txtAllergies.Text = allergies.ToString();
            //txtCountry.Text = country.ToString();
         //   dateTimePicker1Dob.Value =dob;
            if (string.Compare(gender.ToString().Trim(), "Female") == 0)
            {
                radioMale.Checked = false;
                radioFemale.Checked = true;
            }
            if (string.Compare(gender.ToString().Trim(), "Male") == 0)
            {
                radioFemale.Checked = false;
                radioMale.Checked = true;
            }
            if (string.Compare(gender.ToString().Trim(), "") == 0)
            {
                radioFemale.Checked = false;
                radioMale.Checked = false;
            }
          
            
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            clearFields();
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            string i1 = lblCusId.Text.Trim().ToString();
            customer cus4 = hmsC.customers.Where(f => f.cusId == i1).Single();
            hmsC.customers.DeleteObject(cus4);
            hmsC.SaveChanges();
            MessageBox.Show("Guest is deleted");
            reLoadGrid();
            clearFields();
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void txtFname_TextChanged(object sender, EventArgs e)
        {
            string s1 = vf.checkForLetters(txtFname.Text.Trim());
            txtFname.Text = s1;
        }

        private void txtMname_TextChanged(object sender, EventArgs e)
        {
            string s1 = vf.checkForLetters(txtMname.Text.Trim());
            txtMname.Text = s1;
        }

        private void txtLname_TextChanged(object sender, EventArgs e)
        {
            string s1 = vf.checkForLetters(txtLname.Text.Trim());
            txtLname.Text = s1;
        }

        private void txtNationality_TextChanged(object sender, EventArgs e)
        {
            //string s1 = vf.checkForLetters(txtNationality.Text.Trim());
            //txtNationality.Text = s1;
        }

        private void txtLandPhone_TextChanged(object sender, EventArgs e)
        {
            string s1 = vf.checkForDigits(txtLandPhone.Text.Trim());
            txtLandPhone.Text = s1;
            string s2 = vf.checkForTenDigits(txtLandPhone.Text.Trim());
            txtLandPhone.Text = s2;
        }

        private void txtMoblePhone_TextChanged(object sender, EventArgs e)
        {
            string s1 = vf.checkForDigits(txtMoblePhone.Text.Trim());
            txtMoblePhone.Text = s1;
            string s2 = vf.checkForTenDigits(txtMoblePhone.Text.Trim());
            txtMoblePhone.Text = s2;

        }

        public bool checkForSameNic()
        {
            IEnumerable<customer> c = hmsC.customers.Where(i => i.nic == txtNic.Text.Trim());
            foreach (customer s in c)
            {
                MessageBox.Show("This NIC is already used.","Sorry");
                return false;
            }
            return true;
        }

             
        

        private void lblLogout_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string logID = new LoggedUser().LogHisId.ToString();
            String[] tokens3 = System.DateTime.Today.ToString().Split(' ');
            String onlyDate = tokens3[1];

            //log history record
            hotelmsEntities1 hmsLog = new hotelmsEntities1();
            logHistory log = hmsLog.logHistories.Where(iii => iii.logHistoryId == logID.Trim()).Single();
            log.logHistoryId = logID.ToString().Trim();
            log.logOutTime = System.DateTime.Now;
            hmsLog.SaveChanges();

            //clearing the detils 
            LoggedUser lu = new LoggedUser();
            lu.UserName1 = "";
            lu.UserLevel1 = "";
            lu.LogHisId = "";
            Login login = new Login();
            login.Show();
            this.Dispose();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            string[] Time = DateTime.Now.ToString().Split(' ');
            lblTime.Text = Time[1] + " " + Time[2];
        }

        private void btnHome_Click_1(object sender, EventArgs e)
        {
            Home h = new Home();
            h.Show();
            this.Dispose();
        }

    }
}
