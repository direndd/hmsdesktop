using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
//using System.Collections.Generic;
using System.Linq;
//using System.Text;
using System.Windows;
using HotelManagementIcom.Model;
using HotelManagementIcom.Controller;
//using HotelManagementIcom.Controller;
 
 

 

namespace HotelManagementIcom.View
{
    public partial class Users : Telerik.WinControls.UI.RadForm
    {
        hotelmsEntities1 hmsC = new hotelmsEntities1();
        hotelmsEntities1 hms = new hotelmsEntities1();
       // user user1 = new user();
        DataTable dataTable1 = new DataTable();
        ValidateFields vf = new ValidateFields();
       
        public Users()
        {
            InitializeComponent();
        }

        private void Users_Load(object sender, EventArgs e)
        {
            //gridUser.AutoGenerateColumns = true;
            //gridUser.DataSource = hms.users;//.Select(i => new { UserId = i.userId });

            var usrDeatils = hmsC.users.Select(i => new { User_ID = i.userId, NIC = i.nic,First_Name = i.fname, Middle_Name = i.mname, Last_Name = i.lname, Address_Number = i.num, Address_Line1 = i.street, Address_Line2 = i.city, Address_Line3 = i.town,Mobile_Phone = i.mobilePhone, Land_Phone = i.landPhone, E_mail = i.email,User_Level=i.userLevel,User_Name=i.userName}).OrderBy(i => i.User_ID);
            gridUser.DataSource = usrDeatils;
            lblUserName.Text = new LoggedUser().UserName1;
            lblUserLevel.Text = new LoggedUser().UserLevel1;
            cmbNic.DataSource = hms.users;
            cmbNic.DisplayMember = "nic";
            IEnumerable<user> nics = hms.users;
            foreach (user usr in nics)
            {
                cmbNic.Items.Add(usr.nic);
                cmbNic.DisplayMember = "nic";
                cmbNic.ValueMember = "nic";
            }
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            user user1 = new user();
            if (string.Compare(cmbNic.Text, "") == 0 || string.Compare(cmbNic.Text, " ") == 0 ||
                txtLname.Text.Trim() == "" || txtLname.Text.Trim() == " " ||
                txtStreet.Text.Trim() == "" || txtStreet.Text.Trim() == " " ||
                txtTown.Text.Trim() == "" || txtTown.Text.Trim() == " " ||
                txtUserName.Text.Trim() == "" || txtUserName.Text.Trim() == " " ||
                txtPassword.Text.Trim() == "" || txtPassword.Text.Trim() == " " ||
                txtConfirm.Text.Trim() == "" || txtConfirm.Text.Trim() == " " ||
                string.Compare(cmbULevel.Text, "") == 0 || string.Compare(cmbULevel.Text, " ") == 0)
            {
                MessageBox.Show("Please provide fields with (*)", "Sorry!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (checkForSameNic() == false)
            {
                //   MessageBox.Show("NIC must have 9 digits and one letter");
                return;
            }
            if (comparePasswords() == false)
            {
                MessageBox.Show("Passsword and confirm password didn't match", "Sorry!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (vf.ValidateNIC(cmbNic.Text.Trim()) == false)
            {
                MessageBox.Show("Please provide a NIC with 9 digits and one letter", "Sorry!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (vf.ValidateEmail(txtEmail.Text.Trim()) == false)
            {
                MessageBox.Show("Please provide a valid e-mail address", "Sorry!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (string.Compare(cmbULevel.Text.Trim().ToString(), "Administrator") != 0 && string.Compare(cmbULevel.Text.Trim().ToString(), "Manager") != 0 && string.Compare(cmbULevel.Text.Trim().ToString(), "User") != 0)
            {
                MessageBox.Show("Please select a user level from drop down list", "Sorry!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                string userId = generateId();
                user1.userId = userId;
                user1.nic = cmbNic.Text.ToString();
                user1.fname = txtFname.Text;
                user1.mname = txtMname.Text;
                user1.lname = txtLname.Text;
                user1.num = txtNum.Text;
                user1.street = txtStreet.Text;
                user1.city = txtCity.Text;
                user1.town = txtTown.Text;
                user1.landPhone = txtLandPhone.Text;
                user1.mobilePhone = txtMobilePhone.Text;
                user1.email = txtEmail.Text;
                user1.userLevel = cmbULevel.Text.Trim().ToString();
                user1.userName = txtUserName.Text.Trim();
                user1.pwd = txtPassword.Text.Trim();
                hms.AddTousers(user1);
                hms.SaveChanges();
                reLoadGrid();
                MessageBox.Show(" User was successfully added", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            { MessageBox.Show("Please provide another NIC. This NIC is already used", "Sorry!", MessageBoxButtons.OK, MessageBoxIcon.Warning); }
            clearFields();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //IEnumerable<user> details= hms.users.Where (i=>i.nic== cmbNic.SelectedText);
            //txtUserName.Text = details.user;
            //IEnumerable<user> usrs = hms.users.Where();
            //  MessageBox.Show("usr id  is" + cmbNic.Text);
            if (comparePasswords() == false)
            {
                MessageBox.Show("Password and Confirmation of Password don't match");
                return;
            }
         
            if (vf.ValidateNIC(cmbNic.Text.Trim()) == false)
            {
                MessageBox.Show("NIC must have 9 digits and one letter");
                return;
            }
            if (vf.ValidateEmail(txtEmail.Text.Trim()) == false)
            {
                MessageBox.Show("Invalid e-mail address");
                return;
            }
            if (string.Compare(cmbULevel.Text.Trim().ToString(), "Administrator") != 0 && string.Compare(cmbULevel.Text.Trim().ToString(), "Manager") != 0 && string.Compare(cmbULevel.Text.Trim().ToString(), "User") != 0)
            {
                MessageBox.Show("User Level is invalid. Please select from the list");
                return;
            }
           string n1= lblUserId.Text.Trim().ToString();
           user user2 = hms.users.Where(f => f.userId == n1).First();         
           user2.nic = cmbNic.Text;
           user2.fname = txtFname.Text;
           user2.mname = txtMname.Text;
           user2.lname = txtLname.Text;
           user2.num = txtNum.Text;
           user2.street= txtStreet.Text;
           user2.city= txtCity.Text;
           user2.town= txtTown.Text;
           user2.landPhone= txtLandPhone.Text;
           user2.mobilePhone= txtMobilePhone.Text;
           user2.email = txtEmail.Text.Trim();
           user2.userLevel = cmbULevel.Text.Trim().ToString();
           user2.userName= txtUserName.Text.Trim();
           user2.pwd = txtPassword.Text.Trim();
           reLoadGrid();
           hms.SaveChanges();
           MessageBox.Show("User details updated Successfully!");
           reLoadGrid();
           clearFields();
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            string i1 = lblUserId.Text.Trim().ToString();
            user user3 = hms.users.Where(f => f.userId == i1).Single();
            hms.users.DeleteObject(user3);
            hms.SaveChanges();
            MessageBox.Show("User is deleted");
            reLoadGrid();
            clearFields();
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        //private void gridUser_SelectionChanged(object sender, EventArgs e)
        //{
          
        //}

        private void reLoadGrid()
        {
            try
            {
                //  gridUser.DataSource = null;
            hotelmsEntities1 hms3 = new hotelmsEntities1();
            var usrDeatils = hms3.users.Select(i => new { User_ID = i.userId, NIC = i.nic, First_Name = i.fname, Middle_Name = i.mname, Last_Name = i.lname, Address_Number = i.num, Address_Line1 = i.street, Address_Line2 = i.city, Address_Line3 = i.town, Mobile_Phone = i.mobilePhone, Land_Phone = i.landPhone, E_mail = i.email, User_Level = i.userLevel, User_Name = i.userName }).OrderBy(i => i.User_ID);
            gridUser.DataSource = usrDeatils;
            }
            catch (Exception)
            {
                
                
            }
            
        }

        private bool comparePasswords()
        {
            bool result = false;
            try
            {
            
            string pwd = txtPassword.Text.Trim();
            string confirmPwd = txtConfirm.Text.Trim();
            int res = string.Compare(pwd, confirmPwd);
            Console.WriteLine(res);
            if (res == 0)
                { result = true;}
            return result;
            }
            catch (Exception)
            {
                
               
            }
            return result;
           
        }      
        public string generateId()
        {         
           var userIds = hms.users.Select(i => new { i.userId });
           int MaxUId = 1;
            foreach (var item in userIds)
            {
                if (item.userId != string.Empty)
                {
                    string uId = item.userId.Substring(3);
                    int uIdSubString = Int16.Parse(uId);
                    if (MaxUId < uIdSubString)
                        { MaxUId = uIdSubString; }
                }
                else
                    { MaxUId = 1; }
            }
            MaxUId++;
           // label1.Text = "usr" + MaxUId.ToString();
            string newUid = "usr" + MaxUId.ToString();
            return newUid;
        }

       

        public void clearFields()
        {
            lblUserId.Text = null; ;
            txtFname.Text = null;
            txtMname.Text = null;
            txtLname.Text = null;
            txtNum.Text = null;
            txtStreet.Text = null;
            txtCity.Text = null;
            txtTown.Text = null;
            txtLandPhone.Text = null;
            txtMobilePhone.Text = null;
            txtEmail.Text = null;
            cmbULevel.Text = null;
            txtUserName.Text = null;
            txtPassword.Text = null;
            txtConfirm.Text = null;
            cmbNic.Text = null;
        }

         private void txtMobilePhone_TextChanged(object sender, EventArgs e)
        {
           string s1= vf.checkForDigits(txtMobilePhone.Text.Trim());
           txtMobilePhone.Text=s1;
           string s2= vf.checkForTenDigits(txtMobilePhone.Text.Trim());
           txtMobilePhone.Text = s2;
        }

        private void txtLandPhone_TextChanged(object sender, EventArgs e)
        {
            string s1 = vf.checkForDigits(txtLandPhone.Text.Trim());
            txtLandPhone.Text = s1;
            string s2 = vf.checkForTenDigits(txtLandPhone.Text.Trim());
            txtLandPhone.Text = s2;
        }

        private void txtFname_TextChanged(object sender, EventArgs e)
        {
            string s1 = vf.checkForLetters(txtFname.Text.Trim());
            txtFname.Text = s1;
        }

        private void txtMname_TextChanged(object sender, EventArgs e)
        {
            string s1 = vf.checkForLetters(txtMname.Text.Trim());
            txtMname.Text = s1;
        }

        private void txtLname_TextChanged(object sender, EventArgs e)
        {
            string s1 = vf.checkForLetters(txtLname.Text.Trim());
            txtLname.Text = s1;
        }

        public bool checkForSameNic()
        {
            IEnumerable<user> c = hms.users.Where(i => i.nic == cmbNic.Text.Trim());
            foreach (user s in c)
            {
                MessageBox.Show("This NIC is already used.");
                return false;
            }
            return true;
        }


        private void button2_Click_1(object sender, EventArgs e)
        {
            clearFields();
        }

        private void txtConfirm_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void gridUser_SelectionChanged_1(object sender, EventArgs e)
        {
            try
            {
                //// txtUserId.Text = (gridUser.SelectedRows[0].Cells[1].Value.ToString());
                StringBuilder userId = new StringBuilder();
                StringBuilder nic = new StringBuilder();
                StringBuilder fname = new StringBuilder();
                StringBuilder mname = new StringBuilder();
                StringBuilder lname = new StringBuilder();
                StringBuilder num = new StringBuilder();
                StringBuilder street = new StringBuilder();
                StringBuilder city = new StringBuilder();
                StringBuilder town = new StringBuilder();
                StringBuilder landPhone = new StringBuilder();
                StringBuilder mobilePhone = new StringBuilder();
                StringBuilder email = new StringBuilder();
                StringBuilder userLevel = new StringBuilder();
                StringBuilder userName = new StringBuilder();
                StringBuilder pwd = new StringBuilder();
                StringBuilder confirm = new StringBuilder();
                string rowId = (gridUser.SelectedRows[0].Cells[0].Value.ToString());
                IEnumerable<user> c = hms.users.Where(i => i.userId == rowId);
                foreach (user u in c)
                {
                    nic.Append(u.nic + Environment.NewLine);
                    userId.Append(u.userId + Environment.NewLine);
                    fname.Append(u.fname + Environment.NewLine);
                    mname.Append(u.mname + Environment.NewLine);
                    lname.Append(u.lname + Environment.NewLine);
                    num.Append(u.num + Environment.NewLine);
                    street.Append(u.street + Environment.NewLine);
                    city.Append(u.city + Environment.NewLine);
                    town.Append(u.town + Environment.NewLine);
                    landPhone.Append(u.landPhone + Environment.NewLine);
                    mobilePhone.Append(u.mobilePhone + Environment.NewLine);
                    email.Append(u.email + Environment.NewLine);
                    userLevel.Append(u.userLevel + Environment.NewLine);
                    userName.Append(u.userName + Environment.NewLine);
                    pwd.Append(u.pwd + Environment.NewLine);
                    confirm.Append(u.pwd + Environment.NewLine);
                }
                cmbNic.Text = null;
                cmbNic.SelectedText = nic.ToString();
                lblUserId.Text = userId.ToString();
                txtFname.Text = fname.ToString();
                txtMname.Text = mname.ToString();
                txtLname.Text = lname.ToString();
                txtNum.Text = num.ToString(); ;
                txtStreet.Text = street.ToString();
                txtCity.Text = city.ToString();
                txtTown.Text = town.ToString();
                txtLandPhone.Text = landPhone.ToString();
                txtMobilePhone.Text = mobilePhone.ToString();
                txtEmail.Text = email.ToString();
                cmbULevel.Text = null;
                cmbULevel.SelectedText = userLevel.ToString();
                txtUserName.Text = userName.ToString();
                txtPassword.Text = pwd.ToString();
                txtConfirm.Text = pwd.ToString();
            }
            catch (Exception)
            {


            }
           
        }

        private void btnHome_Click(object sender, EventArgs e)
        {

        }

        private void btmClear_Click(object sender, EventArgs e)
        {

        }

       
        //public void fillGrid()
        //{
        //    dataTable1.Columns.Add("Room", typeof(String));
        //    dataTable1.Columns.Add("CheckIn", typeof(DateTime));
        //    dataTable1.Columns.Add("CheckOut", typeof(DateTime));
        //    dataTable1.Columns.Add("Room Type", typeof(String));
        //    dataTable1.Columns.Add("Price", typeof(String));
        //    dataTable1.Columns.Add("Number of Days", typeof(int));
        //    dataTable1.Columns.Add("Number of Halfbords", typeof(int));

        //  //  dataTable1.Rows.Add("hima","hima");
        //    gridUser.DataSource = dataTable1.DefaultView;
        //}
       
    }

          
    }