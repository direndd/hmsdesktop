using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Objects;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using HotelManagementIcom.Model;
using System.Linq;
using HotelManagementIcom.Controller;
namespace HotelManagementIcom.View
{
    public partial class Reservations : Telerik.WinControls.UI.RadForm
    {
        //grid view Add rooms
        DataTable dataTable1 = new DataTable();
        //grid view free rooms
        DataTable dataTable2 = new DataTable();

        
       
        reservation newRes = new reservation();
        string facilityID;
        double roomPrice;
        double price=0.0;
        double priceHlf=0.0;
        double priceDiff=0.0;
        double totPrice=0.0;
        int numOfChil=0;
        int numOfAdul=0;
        int numOfDays = 0;
        int numOfRooms = 0;
        int numOfChilPerRoom = 0;
        int numOfAdulPerRoom = 0;
        int selectedRoomCount = 0;


        string roomIDChk;
        string invoiceIDChk;
        string reservationIDChk;
        string PaymentIDChk;
        string IDChk;
        string custIDChk;

        string numOfAdultsChk;
        string numOfChildChk;
        string sDateChk;
        string eDateChk;
        string numOfHlf;

        int adults;        
        int child ;
  


        public Reservations()
        {
            InitializeComponent();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = 1000;
            timer.Start();
            txtTotAmount.Enabled = false;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            string[] Time = DateTime.Now.ToString().Split(' ');
            lblTime.Text = Time[1] + " " + Time[2];
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
          
            txtBoxCusId.Text = "";
            txtBoxCustName.Text = "";
            txtBoxPassPort.Text = "";
            txtNationalty.Text = "";

            hotelmsEntities1 hms = new hotelmsEntities1();
            var list = from cust in hms.customers where cust.nic == cmbCusNic.Text select cust;
            StringBuilder nic = new StringBuilder();
            StringBuilder name = new StringBuilder();
            StringBuilder id = new StringBuilder();
            StringBuilder passport = new StringBuilder();
            StringBuilder gender = new StringBuilder();
            StringBuilder Nationalty = new StringBuilder();

            try
            {
                //Customer details filling     
 
                foreach (customer s in list)            
                {
                name.Clear();
                id.Clear();
                passport.Clear();

                gender.Append(s.gender + Environment.NewLine);
                name.Append(s.fName + Environment.NewLine);
                id.Append(s.cusId + Environment.NewLine);
                Nationalty.Append(s.nationalty + Environment.NewLine);
                if (gender.ToString().StartsWith("M"))
                {
                    rbMale.Checked = true;
                    rbFemale.Checked = false;
                }
                else {
                    rbFemale.Checked = true;
                   rbMale.Checked = false;
                }
               
                passport.Append(s.passport + Environment.NewLine);
                
                //setting details 
            txtBoxCusId.Text = id.ToString();
            txtBoxCustName.Text = name.ToString();
            txtBoxPassPort.Text = passport.ToString();
           txtNationalty.Text = Nationalty.ToString();
            IDChk = id.ToString();
            }
            }
            catch (Exception)
            {
                
               
            }


           
        }

        private void radTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void linkAvailability_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            ////get all facility records
            //IEnumerable<facility> all = hms.facilities;
            ////get reserved facilities in the given dates
            //IEnumerable<reservation> ress = from res in hms.reservations where res.endDate < datePickerCOut.Value && res.startDate < datePickerCIn.Value select res;
            //IEnumerable<facility> booked = from bo in ress select bo.facility;
            ////selecting the facilities which are not reserved
            //IEnumerable<facility> free = all.Except(booked); // ;
            ////selecting the roooms
            //IEnumerable<facility> free2 = free.Where(i => i.name == "room").Where(i => i.description == cmbRoomType.SelectedItem.ToString());

            //cmbAvlblRooms.Items.Clear();
            ////filling the free room numbers
            //foreach (facility fac in free2)
            //{

            //    cmbAvlblRooms.Items.Add(fac.room);
            //    cmbAvlblRooms.DisplayMember = "roomNo";
            //    cmbAvlblRooms.ValueMember = "roomNo";

            //}

        }

        private void button6_Click(object sender, EventArgs e)
        {         
          
            hotelmsEntities1 hms = new hotelmsEntities1();
           // try
           // {

            //customer
            customer cus = new customer();
            string custId;
            int custMaxID = 0;
            int custIdSubString = 0;

            //invoice
            invoice invo = new invoice();
            string InvId;
            int InvMaxID = 0;
            int InvIdSubString = 0;

            //payment
            payment pay = new payment();
            string payId;
            int payMaxID = 0;
            int payIdSubString = 0;

            //reservations
            reservation res = new reservation();
            string resId;
            int resMaxID = 0;
            int resIdSubString = 0;


            //get the max Customer Id
            var custID = hms.customers.Select(i => new { i.cusId });
            foreach (var item in custID)
            {
                if (item.cusId != string.Empty)
                {                 
                    custId = item.cusId.Substring(3);                
                    custIdSubString = Int16.Parse(custId);                
                    if (custMaxID < custIdSubString)               
                    {                    
                        custMaxID = custIdSubString;                
                    }
                }
                else
                {
                    custMaxID = 0;
                }
               
            }
            custMaxID++;
            custId = "cus" + custMaxID.ToString();
            custIDChk = custId;

            //get the max Invoice Id
            var invID = hms.invoices.Select(i => new { i.invoiceId });
            foreach (var item in invID)
            {
                if (item.invoiceId != string.Empty)
                { 
                    InvId = item.invoiceId.Substring(3);               
                    InvIdSubString = Int16.Parse(InvId);                
                    if (InvMaxID < InvIdSubString)                
                    {                    
                        InvMaxID = InvIdSubString;                
                    }
                }
               
                else
                {
                    InvMaxID = 0;
                }
            }
            InvMaxID++;
            InvId = "inv" + InvMaxID.ToString();
            invoiceIDChk = InvId;

            //get the max payment Id
            var payID = hms.payments.Select(i => new { i.paymentId });
            foreach (var item in payID)
            {
                if (item.paymentId != string.Empty)
                {   
                    payId = item.paymentId.Substring(3);
                    payIdSubString = Int16.Parse(payId);
                    if (payMaxID < payIdSubString)
                    {
                        payMaxID = payIdSubString;
                    }
                }
                else
                {
                    payMaxID = 0;
                }                
            }
            payMaxID++;
            payId = "pay" + payMaxID.ToString();
            PaymentIDChk = payId;

            //get the max Reservations Id
            var resID = hms.reservations.Select(i => new { i.reservationId });
            foreach (var item in resID)
            {
                if (item.reservationId!=string.Empty)
                {
                    resId = item.reservationId.Substring(3);                
                    resIdSubString = Int16.Parse(resId);                
                    if (resMaxID < resIdSubString)                
                    {                   
                        resMaxID = resIdSubString;                
                    }                
                
                }
                else
                {
                    resMaxID = 0;
                }               
            }
            resMaxID++;
            resId = "res" + resMaxID.ToString();
            reservationIDChk = resId;



            string gender;
            try
            {
                int count = (from cust in hms.customers where cust.passport == txtBoxPassPort.Text select cust).Count();
                count = (from cust in hms.customers where cust.nic == cmbCusNic.Text select cust).Count();
              // Adding a new customer
                if (count == 0)
                {
                    if (rbFemale.Checked)
                    {
                        gender = "Female";
                    }
                    else
                    {
                        gender = "Male";
                    }

                    string nicc = cmbCusNic.Text.ToString();
                    cus.cusId = custIDChk;
                    cus.fName = txtBoxCustName.Text;
                    cus.nic = nicc;
                    cus.passport = txtBoxPassPort.Text;
                    cus.gender = gender;
                    cus.nationalty = txtNationalty.Text;

                    hms.AddTocustomers(cus);
                    int status = hms.SaveChanges();

                    if (status == 0 || status < 0)
                    {
                        MessageBox.Show("Customer Not Added", "Error");
                    }
                    if (status > 0)
                    {   //  MessageBox.Show("Customer Added", "Successful");

                    }                   

                }
                    //if customer exists
                else 
                {              
                    custIDChk=IDChk;
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message, "Error");
            }
            finally
            {               
                this.Refresh();              
            }
            try
            {

            //inserting the invoice details
            invo.invoiceId = invoiceIDChk.Trim();
            invo.cusId = custIDChk.Trim();
            invo.payedDate = System.DateTime.Today;
            invo.total = Convert.ToDouble(txtTotAmount.Text);
            invo.balance = Convert.ToDouble(lblBalance.Text);
            invo.userId = new LoggedUser().LogUser;
            hms.AddToinvoices(invo);
               
            int statusInvoice = hms.SaveChanges();
            if (statusInvoice > 0)
            {

              // MessageBox.Show("invoice Added", "Successful");

               // inserting the payments
                pay.paymentId = PaymentIDChk.Trim();
                pay.paymentDate = System.DateTime.Today;
                pay.Amount = Convert.ToDouble(txtPaying.Text);
                pay.paymentType = cmbpaymentType.Text;
                pay.userId =  new LoggedUser().LogUser;
                pay.cusId = custIDChk.Trim();
                pay.invoiceId = invoiceIDChk.Trim();
                hms.AddTopayments(pay);
                int statusPayment = hms.SaveChanges();

                if (statusPayment > 0)
                {
                   // MessageBox.Show("payment Added", "Successful");
                    int rowCount = gridReservations.RowCount - 1;
                  

                    if (rowCount != 0)
                    {
                           //adding reservations                                                
                           res.reservationId = reservationIDChk.Trim();
                           res.invoiceId = invoiceIDChk.Trim();
                           res.cusId = custIDChk.Trim();
                           res.userId = new LoggedUser().LogUser;
                           hms.AddToreservations(res);
                           int statusResevation = hms.SaveChanges();
                                               
                        for (int i = 0; i < rowCount; i++)
                        {
                            roomIDChk = gridReservations.Rows[i].Cells[0].Value.ToString();
                            var room= hms.rooms.Where(ii => ii.roomNo ==roomIDChk ).Select(ii => new {ii.roomId}).First();
                                                          
                            roomIDChk =room.roomId;

                            numOfAdultsChk = gridReservations.Rows[i].Cells[7].Value.ToString();
                            numOfChildChk = gridReservations.Rows[i].Cells[8].Value.ToString();
                            sDateChk = gridReservations.Rows[i].Cells[1].Value.ToString();
                            eDateChk = gridReservations.Rows[i].Cells[2].Value.ToString();
                            numOfHlf = gridReservations.Rows[i].Cells[6].Value.ToString();


                           //date splitting
                            DateTime inDate = datePickerCIn.Value;
                            DateTime outDate = datePickerCOut.Value;
                            string inDateString = inDate.ToString("yyyy MM dd");
                            string outDateString = outDate.ToString("yyyy MM dd");

                            //MessageBox.Show(MyDateConversion(inDateString).ToString() + " " + MyDateConversion(outDateString).ToString());
                            //MessageBox.Show(MyDateConversion(inDateString).ToString() + " " + MyDateConversion(outDateString).ToString());



                            DateTime sDay = MyDateConversion(inDateString);
                            DateTime eDay = MyDateConversion(outDateString);
                  
                          //room_res adding
                          roomre room_res = new roomre();
                           
                          room_res.reservationId = reservationIDChk;
                          room_res.roomId = roomIDChk;
                          room_res.noOfAdults = Convert.ToInt16(numOfAdultsChk);
                          room_res.noOfChildren = Convert.ToInt16(numOfChildChk);
                          room_res.startDate = sDay;
                          room_res.endDate = eDay;                
                          room_res.numOfHalf = Convert.ToInt16(numOfHlf);
                          room_res.cancel = false;


                            hms.AddToroomres(room_res);
                           // statusResevation = 
                              
                            if (statusResevation > 0)
                            {
                              //  MessageBox.Show("Reservation Added sub", "Successful");
                            }

                        } hms.SaveChanges();
                    } 

                }
                
            }

                MessageBox.Show("Reservation Added", "Successful");            
                reservationTableFilll();

            //clear the gridreservation data grid view
            int countRows = gridReservations.Rows.Count - 1;
            for (int i = 0; i < countRows; i++)
            {
                gridReservations.Rows.RemoveAt(0);
            }            
            }
            catch (Exception ex2)
            {
                MessageBox.Show(ex2.Message);
            }


            try
            {
                // Customer chekin Status
                hotelmsEntities1 hmsCust = new hotelmsEntities1();               
                string customerIdStatus = txtBoxCusId.Text.Trim();
                customer cusStatus = hmsCust.customers.Where(i => i.cusId == customerIdStatus).Single();
                cusStatus.checkInstatus = true;
                hmsCust.SaveChanges();

            }
            catch (Exception eCust)
            {                
             
            }




        }

        private void btnViewRes_Click(object sender, EventArgs e)
        {
            ViewReservations vwRes = new ViewReservations();
            vwRes.Show();
        }
        public void reservationTableFilll() {
            hotelmsEntities1 hms = new hotelmsEntities1();
            grdReservations.AutoGenerateColumns = true;
            IEnumerable<roomre> bookedroms = hms.roomres;
            //projecting the fields
            var allReservations = hms.roomres.Select(i => new {Reservation= i.reservationId, RoomNo = i.room.roomNo, F_Name = i.reservation.customer.fName, L_Nname = i.reservation.customer.lName, CheckIn = i.startDate, CheckOut = i.endDate, Adults= i.noOfAdults,Children= i.noOfChildren,Invoice= i.reservation.invoiceId,Total= i.reservation.invoice.total,Balance= i.reservation.invoice.balance });
            grdReservations.DataSource = allReservations;
        }
        public void reservationTableFillSecond( string id)
        {
            hotelmsEntities1 hms = new hotelmsEntities1();
            grdReservations.AutoGenerateColumns = true;
            IEnumerable<roomre> bookedroms = hms.roomres;
            //projecting the fields
         
           // var allReservations = hms.roomres.Select(i => new { i.reservationId, i.room.roomNo, i.reservation.customer.fName, last_Nname = i.reservation.customer.lName, i.startDate, i.endDate, i.noOfAdults, i.noOfChildren, i.reservation.invoiceId, i.reservation.invoice.total, i.reservation.invoice.balance }).Where(ii=>ii.reservationId==id);
            var allReservations = hms.roomres.Select(i => new { Reservation = i.reservationId, RoomNo = i.room.roomNo, F_Name = i.reservation.customer.fName, L_Nname = i.reservation.customer.lName, CheckIn = i.startDate, CheckOut = i.endDate, Adults = i.noOfAdults, Children = i.noOfChildren, Invoice = i.reservation.invoiceId, Total = i.reservation.invoice.total, Balance = i.reservation.invoice.balance }).Where(ii => ii.Reservation== id);
            grdReservations.DataSource = allReservations;
        }
        private void Reservations_Load(object sender, EventArgs e)
        {
            hotelmsEntities1 hms = new hotelmsEntities1();
            //all reservations grid view
            reservationTableFilll();
            //grdReservations.AutoGenerateColumns = true;                   
            //IEnumerable<roomre> bookedroms = hms.roomres;
            ////projecting the fields
            //var allReservations = hms.roomres.Select(i => new { i.reservationId, i.room.roomNo, i.reservation.customer.fName, last_Nname = i.reservation.customer.lName, i.startDate, i.endDate, i.noOfAdults, i.noOfChildren, i.reservation.invoiceId, i.reservation.invoice.total, i.reservation.invoice.balance });
            //grdReservations.DataSource = allReservations;

            //gridReservations.AutoGenerateColumns = true;                 
            //var allReservations2= hms.rooms.Select(i => new { i.roomType });
            //gridReservations.DataSource = allReservations2;

            //loading room types

            //for all room types without distinct
            // var listRooms = from roomm in hms.rooms  select roomm;

            //distinct room types
            IEnumerable<room> rooms = hms.rooms.ToList();
            var types = rooms.Select(i => new { i.roomType });
            var distinctDiscription = types.Distinct();

            foreach (var fac in distinctDiscription)
            {

                cmbRoomType.Items.Add(fac.roomType);
                cmbRoomType.DisplayMember = "roomType";
                cmbRoomType.ValueMember = "roomType";
            }

            //setting Grid view header

            dataTable1.Columns.Add("Room", typeof(String));
            dataTable1.Columns.Add("CheckIn", typeof(DateTime));
            dataTable1.Columns.Add("CheckOut", typeof(DateTime));
            dataTable1.Columns.Add("Room Type", typeof(String));
            dataTable1.Columns.Add("Price", typeof(String));
            dataTable1.Columns.Add("Number of Days", typeof(int));
            dataTable1.Columns.Add("Number of Halfbords", typeof(int));
            dataTable1.Columns.Add("Number of Adults", typeof(int));
            dataTable1.Columns.Add("Number of Children", typeof(int));           


        }

        private void addRes_Click(object sender, EventArgs e)
        {
            try{
            if (rbAuto.Checked==false && rbOneRoom.Checked==false)
            {
                MessageBox.Show("Please select the reservatrion type  ", "Invalid selection");
            }
            else{
            hotelmsEntities1 hms = new hotelmsEntities1();
            room roomAvailability = new room();
            //date time
            DateTime inDate = datePickerCIn.Value;
            DateTime outDate = datePickerCOut.Value;
            string inDateString = inDate.ToString("yyyy MM dd");
            string outDateString = outDate.ToString("yyyy MM dd");
            DateTime sDay = MyDateConversion(inDateString);
            DateTime eDay = MyDateConversion(outDateString);

            //get all facility records
            IEnumerable<room> all = hms.rooms;
            //IEnumerable<roomre> reservationOb = from res in hms.roomres where res.endDate >= eDay.Date && res.startDate <= sDay.Date select res;
        


           // 10-9-12 modified
            IEnumerable<roomre> reservationOb = hms.roomres;
            //  IEnumerable<roomre> r = reservationOb.Where(res => res.endDate <= eDay.Date && res.startDate <= sDay.Date).Union(reservationOb.Where(res => res.endDate <= eDay.Date && res.startDate >= sDay.Date)).Union(reservationOb.Where(res => res.endDate >= eDay.Date && res.startDate >= sDay.Date));
            IEnumerable<roomre> r = reservationOb.Where(res => res.endDate <= eDay.Date).Intersect(reservationOb.Where(res => res.startDate >= sDay.Date));


            
            IEnumerable<room> notBooked = from bo in r select bo.room;
            //selecting the facilities which are not reserved
            IEnumerable<room> available = all.Except(notBooked); // ;
            //selecting the roooms
            IEnumerable<room> freeRooms = available.Where(i => i.roomType == cmbRoomType.SelectedItem.ToString()).Where(i => i.avalability == true);
            int roomCount = freeRooms.Count();
            //MessageBox.Show(selectedRoomCount.ToString());
            List<string> roomIDs = new List<string>();
            Boolean roomStatus = false;





           



            if (txtBoxNumOfRooms.Text != string.Empty)
            {
                selectedRoomCount = Convert.ToInt16(txtBoxNumOfRooms.Text);
            }

            try
            {
                //checks for empty values
                if (datePickerCIn.Value == null || datePickerCOut.Value == null || cmbRoomType.Text.ToString() == string.Empty)
                {
                    if (cmbRoomType.Text.ToString() == string.Empty)
                    {
                        //  MessageBox.Show("Please check the room Type","Empty fields");
                    }
                    if (datePickerCIn.Value == null)
                    {
                        // MessageBox.Show("Please check the Chech In date", "Empty fields");
                    }
                    if (datePickerCOut.Value == null)
                    {
                        //  MessageBox.Show("Please check the Chech Out date", "Empty fields");
                    }
                }
                else
                {
                    //checking for the number of rooms
                    if (txtBoxNumOfRooms.Text != string.Empty)
                    {

                        //automatic room selection 
                        if (rbAuto.Checked)
                        {

                            if (roomCount >= selectedRoomCount)
                            {

                                int rowCountForRomms = gridReservations.RowCount;
                                //getting the free roomNO s
                                foreach (room roo in freeRooms)
                                {
                                    //if room count is zero                             
                                    if (rowCountForRomms == 0)
                                    {
                                        roomIDs.Add(roo.roomNo);
                                    }
                                    //if free rooms available
                                    else
                                    {
                                        for (int i = 0; i < rowCountForRomms - 1; i++)
                                        {
                                            if (gridReservations.Rows[i].Cells[0].Value.ToString() == roo.roomNo)
                                            {
                                                roomStatus = true;
                                            }
                                        }
                                        if (roomStatus == false)
                                        {
                                            roomIDs.Add(roo.roomNo);
                                        }
                                        roomStatus = false;
                                    }
                                }

                                if (roomIDs.Count != 0)
                                {
                                    for (int i = 0; i < selectedRoomCount; i++)
                                    {
                                        int count = 0;
                                        if (chkBoxChkIn.Checked)
                                        {
                                            count++;
                                        }
                                        if (chkBoxChkOut.Checked)
                                        {
                                            count++;
                                        }

                                        //setting the room prices
                                        setPrice(roomIDs.ElementAt(i));


                                        price = roomPrice * Convert.ToInt32((datePickerCOut.Value - datePickerCIn.Value).TotalDays + 1);
                                        priceDiff = (roomPrice - priceHlf) * count;
                                        dataTable1.Rows.Add(roomIDs.ElementAt(i), datePickerCIn.Value, datePickerCOut.Value, cmbRoomType.SelectedItem.ToString(), price - priceDiff, (1 + (datePickerCOut.Value - datePickerCIn.Value).TotalDays), count, numOfAdul, numOfChil);
                                        totPrice = (price - priceDiff) + Convert.ToDouble(totPrice);
                                        txtBoxAmount.Text = Convert.ToString(totPrice);
                                        gridReservations.DataSource = dataTable1.DefaultView;

                                    }
                                    MessageBox.Show("Rooms selected Successfully ");
                                }
                                else
                                {
                                    MessageBox.Show("Sorry no rooms avalable rooms ", "Not enough rooms");
                                }

                            }
                            else
                            {
                                MessageBox.Show("avalable room count is " + roomCount.ToString(), "Not enough rooms");
                            }

                        }
                        //multiple manual selection



                    }//manual one by one room selections
                    if (rbOneRoom.Checked)
                    {
                      if (cmbAvlblRooms.Text.Equals(""))
                        {
                            MessageBox.Show("Please select a room ", "Invalid selection");
                        }
                      else if (cmbAvlblRooms.Items.Count!=0)                      
                        {                        
                            int count = 0;                        
                            if (chkBoxChkIn.Checked)                        
                            {                           
                                count++;                        
                            }                        
                            if (chkBoxChkOut.Checked)                        
                            {                           
                                count++;                        
                            }

                            //setting the room prices
                            setPrice(cmbAvlblRooms.Text); 
                            price = roomPrice * Convert.ToInt32((datePickerCOut.Value - datePickerCIn.Value).TotalDays + 1);     
                            priceDiff =( roomPrice - priceHlf)*count;
                            //adding data to the grid view
                            dataTable1.Rows.Add(cmbAvlblRooms.Text, datePickerCIn.Value, datePickerCOut.Value, cmbRoomType.SelectedItem.ToString(), price - priceDiff, (1 + (datePickerCOut.Value - datePickerCIn.Value).TotalDays), count, numOfAdul, numOfChil);                        
                            totPrice=  (price - priceDiff) + Convert.ToDouble(totPrice);                        
                            txtBoxAmount.Text =Convert.ToString(totPrice);                        
                            gridReservations.DataSource = dataTable1.DefaultView;
                        
                            cmbAvlblRooms.Items.Clear();                        
                            cmbAvlblRooms.Text = string.Empty;                        
                            int rowCountForRomms = gridReservations.RowCount;
                        
                            foreach (room roo in freeRooms)                        
                            {                           
                                //if room count is zero                             
                            
                                if (rowCountForRomms == 0)                           
                                {                                
                                    //filling the free room numbers                                
                                
                                    cmbAvlblRooms.Items.Add(roo.roomNo);                                
                                    cmbAvlblRooms.DisplayMember = "roomNo";                                
                                    cmbAvlblRooms.ValueMember = "roomNo";                            
                               
                                }                          
                                    //if free rooms available                           
                                else                           
                                {                                
                                    for (int i = 0; i < rowCountForRomms - 1; i++)                                
                                    {                                    
                                        if (gridReservations.Rows[i].Cells[0].Value.ToString() == roo.roomNo)                                    
                                        {                                       
                                            roomStatus = true;                                    
                                        }
                                        else 
                                        {
                                            //filling the free room numbers                             
                                            cmbAvlblRooms.Items.Add(roo.roomNo);
                                            cmbAvlblRooms.DisplayMember = "roomNo";
                                            cmbAvlblRooms.ValueMember = "roomNo";

                                        }
                                    }                               
                                    //if (roomStatus == false)                                
                                    //{                                    
                                    //    //filling the free room numbers                             
                                    //     cmbAvlblRooms.Items.Add(roo.roomNo);                                                                            
                                    //    cmbAvlblRooms.DisplayMember = "roomNo";                                   
                                    //    cmbAvlblRooms.ValueMember = "roomNo";                                    
                                
                                    //}
                                
                                    roomStatus = false;                            
                           
                                }                    
                        
                            }                 
                       
                        }                        
                        else
                        {
                            MessageBox.Show("Sorry no rooms avalable rooms ", "Not enough rooms");
                        }
                    }



                }
            }
            catch (Exception eee)
            {

                MessageBox.Show(eee.Message);
            }
       
        }
        }
            catch(Exception exe){
                MessageBox.Show("Please check your selections", "Invalid Selection");
            }       
        }

        private void cmbCusNic_KeyPress(object sender, KeyPressEventArgs e)
        {
            

        }

        private void btnPayment_Click(object sender, EventArgs e)
        {
   
        }

        private void cmbCusNic_TextChanged(object sender, EventArgs e)
        {
            try
            {
            if (cmbCusNic.Text.Length<=9)
            {
                validateTextInteger(sender,e);
            }
            if (cmbCusNic.Text.Length ==10 )
            {
               // cmbCusNic.Text = cmbCusNic.Text.Substring(10, cmbCusNic.Text.Length - 1);
              
            }
            if (cmbCusNic.Text.Length  > 10)
            {
               // validateTextInteger(sender, e);
                cmbCusNic.Text = cmbCusNic.Text.Substring(0, cmbCusNic.Text.Length - 1);
                // MessageBox.Show();
            }
            }
            catch (Exception)
            {
                
             
            }
            
           
        }

        private void txtBoxdiscount_TextChanged(object sender, EventArgs e)
        {
            if (txtBoxdiscount.Text==null)
            {
                
            }
            txtTotAmount.Text = (Convert.ToDouble(txtTotAmount.Text) - (Convert.ToDouble(txtTotAmount.Text) / 100) * Int32.Parse(txtBoxdiscount.Text)).ToString();
        }

        private void cmbCusNic_Enter(object sender, EventArgs e)
        {
            txtBoxCusId.Text = "";
            txtBoxCustName.Text = "";
            txtBoxPassPort.Text = "";
            txtNationalty.Text = "";

            hotelmsEntities1 hms = new hotelmsEntities1();
            cmbCusNic.Items.Clear();

            IEnumerable<customer> cust = hms.customers.Where(cu => cu.nic.Contains(cmbCusNic.Text)); ;
            //filling the Nic numbers
            foreach (customer fac in cust)
            {
                if (fac.nic != null)
                {
                  
                    cmbCusNic.Items.Add(fac.nic);
                    cmbCusNic.DisplayMember = "nic";
                    cmbCusNic.ValueMember = "nic";

                }


            }
        }

        private void cmbAvlblRooms_SelectedValueChanged(object sender, EventArgs e)
        {
           
        }

        private void btnNewCust_Click(object sender, EventArgs e)
        {
            
           
        }

        private void cmbAvlblRooms_SelectedIndexChanged(object sender, EventArgs e)
        {
            hotelmsEntities1 hms = new hotelmsEntities1();
            
            StringBuilder sbFacilitPrice = new StringBuilder();
            StringBuilder sPriceHlf = new StringBuilder();
            StringBuilder sbRoomId= new StringBuilder();
            StringBuilder sbChildNo = new StringBuilder();
            StringBuilder sbAdultNO = new StringBuilder();
          
              var list = from roomID in hms.rooms  where roomID.roomNo == cmbAvlblRooms.Text  select roomID;
              try
              {
                  foreach (room s in list)
                  {
                      sPriceHlf.Append(s.priceHalf + Environment.NewLine);
                      sbRoomId.Append(s.roomId + Environment.NewLine);
                      sbFacilitPrice.Append(s.price + Environment.NewLine);

                      sbAdultNO.Append(s.noOfAdults + Environment.NewLine);
                      sbChildNo.Append(s.noOfChildren + Environment.NewLine);
                  }

              }
              catch (Exception)
              {
                  
                 
              }
                

                    //setting room id 
                    facilityID = sbRoomId.ToString();


                    numOfChil = Convert.ToInt16(sbChildNo.ToString());
                    numOfAdul = Convert.ToInt16(sbAdultNO.ToString());
                    roomPrice = Convert.ToDouble( sbFacilitPrice.ToString());
                    priceHlf = Convert.ToDouble(sPriceHlf.ToString());

                    price = roomPrice * Convert.ToInt32((datePickerCOut.Value - datePickerCIn.Value).TotalDays + 1);
                    txtBoxAmount.Text = price.ToString();                          
         

        }

        public static DateTime MyDateConversion(string dateAsString)
        {

            return System.DateTime.ParseExact(dateAsString, "yyyy MM dd", System.Globalization.CultureInfo.CurrentCulture);

        }
        //public room notReserved(DateTime sDay, DateTime eDay,String roomType) {
        //    hotelmsEntities2 hms = new hotelmsEntities2();
        //    // IEnumerable<roomre> reservationOb = from res in hms.roomres where res.endDate >= eDay.Date && res.startDate <= sDay.Date select res;
        //    IEnumerable<roomre> reservationOb = hms.roomres;
        //    IEnumerable<roomre> r = reservationOb.Where(res => res.endDate <= eDay.Date && res.startDate <= sDay.Date).Union(reservationOb.Where(res => res.endDate <= eDay.Date && res.startDate >= sDay.Date)).Union(reservationOb.Where(res => res.endDate >= eDay.Date && res.startDate >= sDay.Date));

        //    IEnumerable<room> notBooked = from bo in r select bo.room;
        //    //  IEnumerable<room> notBooked = from bo in reservationOb select bo.room;
        //    //selecting the facilities which are not reserved
        //    IEnumerable<room> available = all.Except(notBooked); // ;
        //    //selecting the roooms
        //    IEnumerable<room> freeRooms = available.Where(i => i.roomType == cmbRoomType.SelectedItem.ToString()).Where(i => i.avalability == true);

        //}
        private void cmbRoomType_SelectedIndexChanged(object sender, EventArgs e)
        {
            hotelmsEntities1 hms = new hotelmsEntities1();
            try
            {
                DataTable dataTable4 = new DataTable();
                //// room facilitieds gridview
                //gridViewFacilitiyes.AutoGenerateColumns = true;
                //var roomFac = hms.roomfacs.Where(ii => ii.roomType == cmbRoomType.Text.Trim());
                //gridViewFacilitiyes.DataSource = roomFac.Select(i => i.facilitiy.facilityName);


            //foreach (var item in roomFac)
            //{
            //    dataTable4.Rows.Add(item.facilitiy.facilityName);
            //    gridViewFacilitiyes.DataSource = dataTable4.DefaultView; 
            //}

            }
            catch (Exception)
            {
                
                
            }



            //get all facility records
            IEnumerable<room> all = hms.rooms;

            DateTime inDate = datePickerCIn.Value;
            DateTime outDate = datePickerCOut.Value;
            string inDateString = inDate.ToString("yyyy MM dd");
            string outDateString = outDate.ToString("yyyy MM dd");




            DateTime sDay = MyDateConversion(inDateString);
            DateTime eDay = MyDateConversion(outDateString);


            // MessageBox.Show(eDay.ToString());
            //wrong logic
            // IEnumerable<roomre> reservationOb = from res in hms.roomres where res.endDate >= eDay.Date && res.startDate <= sDay.Date select res;
            IEnumerable<roomre> reservationOb = hms.roomres;
          //  IEnumerable<roomre> r = reservationOb.Where(res => res.endDate <= eDay.Date && res.startDate <= sDay.Date).Union(reservationOb.Where(res => res.endDate <= eDay.Date && res.startDate >= sDay.Date)).Union(reservationOb.Where(res => res.endDate >= eDay.Date && res.startDate >= sDay.Date));
            IEnumerable<roomre> r = reservationOb.Where(res => res.endDate <= eDay.Date).Intersect(reservationOb.Where(res => res.startDate >= sDay.Date));

            IEnumerable<room> Booked = from bo in r select bo.room;
            //  IEnumerable<room> notBooked = from bo in reservationOb select bo.room;
            //selecting the facilities which are not reserved
            IEnumerable<room> available = all.Except(Booked); // ;
            //selecting the roooms
            IEnumerable<room> freeRooms = available.Where(i => i.roomType == cmbRoomType.SelectedItem.ToString()).Where(i => i.avalability == true);


            cmbAvlblRooms.Items.Clear();
            //filling the free room numbers
            foreach (room fac in freeRooms)
            {

                cmbAvlblRooms.Items.Add(fac);
                cmbAvlblRooms.DisplayMember = "roomNo";
                cmbAvlblRooms.ValueMember = "roomNo";

            }
           
            //rooms gridview
            //gridViewRooms.AutoGenerateColumns = true;
            //// var allFacilities = freeRooms.Where(i => i.roomType == cmbRoomType.Text).Where(i => i.avalability == true).Select(i => new { i.roomNo, i.floorNo, i.descrip });//.Where(i => i.roomType == cmbRoomType.Text.ToString());
            //var allFacilities = freeRooms;//.Where(i => i.roomType == cmbRoomType.Text.ToString());
            //var distinctFacilities = allFacilities.Distinct();
            //gridViewRooms.DataSource = distinctFacilities;

            //  string line=string.Empty;
            //  string floor=string.Empty;
            //  int count = 0;

            //  foreach (var item in RoomsNO)
            //  {

            //     line=line+(","+item.roomNo);
            //     string f = item.roomNo.Substring(0,1);
            //     Console.WriteLine(item.roomNo + f);
            //   //  dataTable2.Columns.Add("Room222", typeof(String));
            //     if (f!=floor)
            //     {
            //         count++;

            //     }
            //    int a= dataTable2.Columns.Count;
            //    for (int i = 0; i <a; i++)
            //    {
            //        dataTable2.Columns.RemoveAt(i);
            //    }
            //     for (int i = 0; i <= count; i++)
            //     {
            //      dataTable2.Columns.Add("Floor"+i, typeof(String));   
            //     }

            ////      string [][] rooms=new string [][];

            // //    dataTable2.NewRow( );
            //     dataTable2.Rows. Add(item.roomNo);
            //     gridReservations.DataSource = dataTable2.DefaultView;
            //  }


            //setting the room price and number of people details
            StringBuilder sbFacilitPrice = new StringBuilder();
            StringBuilder sPriceHlf = new StringBuilder();
            StringBuilder sbChildNo = new StringBuilder();
            StringBuilder sbAdultNO = new StringBuilder();

            var list = (from roomID in hms.roomTypes where roomID.typeName == cmbRoomType.Text select roomID);
            var list2 = list.First();

            sPriceHlf.Append(list2.priceHalf + Environment.NewLine);
            sbFacilitPrice.Append(list2.price + Environment.NewLine);
            sbAdultNO.Append(list2.noOfAdults + Environment.NewLine);
            sbChildNo.Append(list2.noOfChildren + Environment.NewLine);


            numOfChil = Convert.ToInt16(sbChildNo.ToString());
            numOfAdul = Convert.ToInt16(sbAdultNO.ToString());
            roomPrice = Convert.ToDouble(sbFacilitPrice.ToString());
            priceHlf = Convert.ToDouble(sPriceHlf.ToString());
        }

        private void cmbPaymentId_Click(object sender, EventArgs e)
        {
           
        }

        private void cmbPaymentId_MouseClick(object sender, MouseEventArgs e)
        {
            hotelmsEntities1 hms = new hotelmsEntities1();
            //cmbPaymentId.Items.Clear();
             IEnumerable<payment> paymentId = hms.payments;
            foreach (payment fac in paymentId)
            {

               // cmbPaymentId.Items.Add(fac.paymentId);
                //cmbPaymentId.DisplayMember = "paymentId";
               // cmbPaymentId.ValueMember = "paymentId";

            }
            

        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                hotelmsEntities1 hms9 = new hotelmsEntities1();     
          IEnumerable <roomre> res = hms9.roomres.Where(i=>i.reservationId==txtBoxResId.Text.Trim());
           // IEnumerable<roomre> res = hms9.roomres.Where(i => i.reservationId == "res2");   
            foreach (roomre reservationCansel in res)        
            {
                reservationCansel.cancel = true;
                hms9.SaveChanges();
            }

            }
            catch (Exception)
            {
                
                
            } 
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void chkBoxChkOut_CheckedChanged(object sender, EventArgs e)
        {
            //if (chkBoxChkIn.Checked)
            //{
            //    if (chkBoxChkOut.Checked)
            //    {
            //        if (priceHlf != 0)
            //        {
            //            priceDiff = (roomPrice - priceHlf) * 2;
            //            price = roomPrice * Convert.ToInt32((datePickerCOut.Value - datePickerCIn.Value).TotalDays + 1);
            //            txtBoxAmount.Text = (price - priceDiff).ToString();
            //        }



            //    }
            //    else
            //    {
            //        priceDiff = (roomPrice - priceHlf) * 1;
            //        price = roomPrice * Convert.ToInt32((datePickerCOut.Value - datePickerCIn.Value).TotalDays + 1);
            //        txtBoxAmount.Text = (price - priceDiff).ToString();
            //    }
            //}
        }

        private void chkBoxChkIn_CheckedChanged(object sender, EventArgs e)
        {
            //double priceD = 0.0;
            //if (chkBoxChkIn.Checked)
            //{
            //    if (chkBoxChkOut.Checked)
            //    {
            //        if (priceHlf!=0)
            //        {
            //            priceDiff = (roomPrice - priceHlf) * 2;
            //         price = roomPrice * Convert.ToInt32((datePickerCOut.Value - datePickerCIn.Value).TotalDays + 1);
            //        txtBoxAmount.Text = (price - priceDiff).ToString();
            //        } 
                   
                   

            //    }
            //    else
            //    {
            //        priceDiff = (roomPrice - priceHlf) * 1;
            //        price = roomPrice * Convert.ToInt32((datePickerCOut.Value - datePickerCIn.Value).TotalDays + 1);
            //        txtBoxAmount.Text = (price - priceDiff).ToString();
            //    }
            //}
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            //gridViewRooms.Enabled = false;
        }

        private void txtBoxdiscount_TextChanged_1(object sender, EventArgs e)
        {
            validateTextDouble(sender, e);
            txtTotAmount.Text = string.Empty;
        if (txtBoxdiscount.Text!=string.Empty)
        {
            try
            {
                double amount = Convert.ToDouble(txtBoxAmount.Text);
                double dis = Convert.ToDouble(txtBoxdiscount.Text);
                double price = (amount / 100);
                txtTotAmount.Text = Convert.ToString(amount-( price * dis));
            }
            catch (Exception exj)
            {
                MessageBox.Show(exj.Message, "Error");
            }
           
        }
        else
        {
            txtTotAmount.Text = txtBoxAmount.Text;
        }           
          
           
        }

        private void txtPaying_TextChanged(object sender, EventArgs e)
        {
            try
            {
                validateTextDouble(sender, e);
                lblBalance.Text = string.Empty;
                lblBalance.Text = Convert.ToString(Convert.ToDouble(txtTotAmount.Text) - Convert.ToDouble(txtPaying.Text));

            }
            catch (Exception ex)
            {

                MessageBox.Show("please enter the total amount", "Error");
            }
            
        }

        private void txtBoxAdults_TextChanged(object sender, EventArgs e)
        {
            validateTextInteger(sender, e);
            if (numOfAdul == 0 && numOfChil==0)
            {
                MessageBox.Show("please select the room type", "Error");
              //  txtBoxAdults.Text = 0.ToString();
             //   txtBoxChildren.Text = 0.ToString();
               // txtBoxNumOfRooms.Text = 0.ToString();

            }
            else
            {
                try
                {                 

                    if (txtBoxAdults.Text == string.Empty)
                    {
                        txtBoxAdults.Text = 0.ToString();
                    }
                    if (txtBoxChildren.Text == string.Empty)
                    {
                        txtBoxChildren.Text = 0.ToString();
                    }

                    if (txtBoxAdults.Text != string.Empty && txtBoxChildren.Text != string.Empty)
                    {
                        if (txtBoxAdults.Text != string.Empty)
                        {
                            adults = Int16.Parse(txtBoxAdults.Text);
                        }
                        if (txtBoxChildren.Text != string.Empty)
                        {
                            child = Int16.Parse(txtBoxChildren.Text);
                        }

                        //if number of children are not greater adults
                        if (adults > child)
                        {
                            if (adults < numOfAdul)
                            {
                                numOfRooms = 1;
                            }
                            else
                            {
                                if ((adults % numOfAdul) == 0)
                                {
                                    numOfRooms = adults / numOfAdul;
                                }
                                else
                                {
                                    numOfRooms = adults / numOfAdul;
                                    numOfRooms++;
                                }

                            }//setting the number of rooms
                            txtBoxNumOfRooms.Text = Convert.ToString(numOfRooms);
                        }
                            //if number of children are greater adults
                        else
                        {
                            if (child < numOfChil)
                            {
                                numOfRooms = 1;
                            }
                            else
                            {
                                if ((child % numOfChil) == 0)
                                {
                                    numOfRooms = child / numOfChil;
                                }
                                else
                                {
                                    numOfRooms = child / numOfChil;
                                    numOfRooms++;
                                }
                            }//setting the number of rooms
                            txtBoxNumOfRooms.Text = Convert.ToString(numOfRooms);
                        }
                    }

                }
     
                catch (Exception ex)
                {

                    MessageBox.Show( "check number of adults","Error");
                } 
            }
        }

        private void txtBoxChildren_TextChanged(object sender, EventArgs e)
        {
            validateTextInteger(sender, e);
            if (numOfAdul == 0 && numOfChil == 0)
            {              
                MessageBox.Show("please select the room type", "Error");                
                //txtBoxAdults.Text=0.ToString();
             //   txtBoxChildren.Text = 0.ToString();
               // txtBoxNumOfRooms.Text = 0.ToString();

            }
            else
            {
       
            try{                
                if (txtBoxAdults.Text != string.Empty)
                {             
                 adults=Int16.Parse(txtBoxAdults.Text);                
                }
                if ( txtBoxChildren.Text != string.Empty)
                {
                    child = Int16.Parse(txtBoxChildren.Text);
                }              
               
                      //if number of children are not greater adults
                if (adults > child)           
                {              
                    if (adults < numOfAdul)
                
                    {
                    numOfRooms = 1;                
                    }
                
                    else               
                    {                  
                        if ((adults % numOfAdul) == 0)                   
                        {
                            numOfRooms = adults / numOfAdul;                   
                        }                    
                        else                   
                        {                        
                            numOfRooms = adults / numOfAdul;                        
                            numOfRooms++;                   
                        }
                    }   //setting the number of rooms             
                    txtBoxNumOfRooms.Text = Convert.ToString(numOfRooms);           
               
                }
                //if number of children are greater adults           
                else {

                if (child < numOfChil)
                {
                    numOfRooms = 1;

                }
                else
                {
                    if ((child % numOfChil) == 0)
                    {
                        numOfRooms = child / numOfChil;
                    }
                    else
                    {
                        numOfRooms = child / numOfChil;
                        numOfRooms++;
                    }
                }//setting the number of rooms
                txtBoxNumOfRooms.Text = Convert.ToString(numOfRooms);            
            
                }           
            }
            catch (Exception)
            {

               MessageBox.Show("Check number of children","Error");            
            }       
            }
        }

        private void btnChekOut_Click(object sender, EventArgs e)
        {
            
        }

        private void btnChekOut_Click_1(object sender, EventArgs e)
        {
            Checkout c = new Checkout();
            c.Show();
            
        }

        //validating to integer
        private void validateTextInteger(object sender, EventArgs e)
        {
            Exception X = new Exception();

            TextBox T = (TextBox)sender;

            try
            {
                if (T.Text != "-")
                {
                    int x = int.Parse(T.Text);
                }
            }
            catch (Exception)
            {
                try
                {
                    int CursorIndex = T.SelectionStart - 1;
                    T.Text = T.Text.Remove(CursorIndex, 1);

                    //Align Cursor to same index
                    T.SelectionStart = CursorIndex;
                    T.SelectionLength = 0;
                }
                catch (Exception) { }
            }
        }
        //validating to double
        private void validateTextDouble(object sender, EventArgs e)
        {
            Exception X = new Exception();

            TextBox T = (TextBox)sender;

            try
            {
                if (T.Text != "-")
                {
                    double x = double.Parse(T.Text);

                    if (T.Text.Contains(','))
                        throw X;
                }
            }
            catch (Exception)
            {
                try
                {
                    int CursorIndex = T.SelectionStart - 1;
                    T.Text = T.Text.Remove(CursorIndex, 1);

                    //Align Cursor to same index
                    T.SelectionStart = CursorIndex;
                    T.SelectionLength = 0;
                }
                catch (Exception) { }
            }
        }

        private void btnViewRes_Click_1(object sender, EventArgs e)
        {
            ViewReservations viewRes = new ViewReservations();
            viewRes.Show();
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {

        }

        private void grdReservations_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }


        public void setPrice(string roomNo) {

            hotelmsEntities1 hms = new hotelmsEntities1();

            StringBuilder sbFacilitPrice2 = new StringBuilder();
            StringBuilder sPriceHlf2 = new StringBuilder();
            StringBuilder sbRoomId2 = new StringBuilder();
            StringBuilder sbChildNo2 = new StringBuilder();
            StringBuilder sbAdultNO2 = new StringBuilder();

            var list = from roomID in hms.rooms where roomID.roomNo == roomNo select roomID;

            foreach (room s in list)
            {
                sPriceHlf2.Append(s.priceHalf + Environment.NewLine);
                sbRoomId2.Append(s.roomId + Environment.NewLine);
                sbFacilitPrice2.Append(s.price + Environment.NewLine);

                sbAdultNO2.Append(s.noOfAdults + Environment.NewLine);
                sbChildNo2.Append(s.noOfChildren + Environment.NewLine);
            }


            //setting room id 
            facilityID = sbRoomId2.ToString();

            //txtBoxFacilityId.Text = sbRoomId.ToString();
            //   MessageBox.Show(sbRoomId.ToString(),"roomID");
            // MessageBox.Show(sbFacilitPrice.ToString(),"price");
            //setting price

            numOfChil = Convert.ToInt16(sbChildNo2.ToString());
            numOfAdul = Convert.ToInt16(sbAdultNO2.ToString());
            roomPrice = Convert.ToDouble(sbFacilitPrice2.ToString());
            priceHlf = Convert.ToDouble(sPriceHlf2.ToString());

            // price = roomPrice * Convert.ToInt32((datePickerCOut.Value - datePickerCIn.Value).TotalDays + 1);
            //  txtBoxAmount.Text = price.ToString();
            ////  MessageBox.Show(price.ToString() + Convert.ToInt32((datePickerCOut.Value - datePickerCIn.Value).TotalDays + 1));
           
         
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            txtBoxResId.Text = "";
            reservationTableFilll();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            reservationTableFillSecond(txtBoxResId.Text.Trim());
        }

        private void txtBoxResId_TextChanged(object sender, EventArgs e)
        {
            try
            {
                reservationTableFillSecond(txtBoxResId.Text.Trim());
            }
            catch (Exception)
            {
                
              
            }
           
        }

        private void grdReservations_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
        
        }

        private void button9_Click(object sender, EventArgs e)
        { 
        }

        private void btnChekOut_Click_2(object sender, EventArgs e)
        {
            Home h = new Home();
            h.Show();
            this.Dispose();
        }

        private void lblLogout_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string logID = new LoggedUser().LogHisId.ToString();
            String[] tokens3 = System.DateTime.Today.ToString().Split(' ');
            String onlyDate = tokens3[1];

            //log history record
            hotelmsEntities1 hmsLog = new hotelmsEntities1();
            logHistory log = hmsLog.logHistories.Where(iii => iii.logHistoryId == logID.Trim()).Single();
            log.logHistoryId = logID.ToString().Trim();
            log.logOutTime = System.DateTime.Now;
            hmsLog.SaveChanges();

            //clearing the detils 
            LoggedUser lu = new LoggedUser();
            lu.UserName1 = "";
            lu.UserLevel1 = "";
            lu.LogHisId = "";
            Login login = new Login();
            login.Show();
            this.Dispose();
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            Home h = new Home();
            h.Show();
            this.Dispose();
        }

        private void datePickerCIn_ValueChanged(object sender, EventArgs e)
        {
            DateTime inDate = datePickerCIn.Value;
            DateTime today=System.DateTime.Today;
            if (inDate<today)
            {
                MessageBox.Show("Invalid CheckIn date", "Invalid Selection");
                datePickerCIn.Value = today;
            }
        }

        private void datePickerCOut_ValueChanged(object sender, EventArgs e)
        {
            DateTime inDate = datePickerCIn.Value;
            DateTime outDate = datePickerCOut.Value;
            DateTime today = System.DateTime.Today;
            if (inDate>outDate)
            {
                MessageBox.Show("Invalid CheckOut date", "Invalid Selection");
                datePickerCOut.Value = today;
            }
        }

        private void rbRs_CheckedChanged(object sender, EventArgs e)
        {
            lblAmount.Text= lblTotAmount.Text=lblPayingAmount.Text= "Rs";
            
                
        }

        private void rdDholler_CheckedChanged(object sender, EventArgs e)
        {
            lblAmount.Text = lblTotAmount.Text = lblPayingAmount.Text = "$";
        }

   
        
    } 
       
}
