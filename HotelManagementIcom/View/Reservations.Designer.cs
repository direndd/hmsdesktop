namespace HotelManagementIcom.View
{
    partial class Reservations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Reservations));
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.txtBoxResId = new System.Windows.Forms.TextBox();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtNationalty = new System.Windows.Forms.TextBox();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.rbMale = new System.Windows.Forms.RadioButton();
            this.rbFemale = new System.Windows.Forms.RadioButton();
            this.txtBoxPassPort = new System.Windows.Forms.TextBox();
            this.txtBoxCustName = new System.Windows.Forms.TextBox();
            this.txtBoxCusId = new System.Windows.Forms.TextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.cmbCusNic = new System.Windows.Forms.ComboBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.rbOneRoom = new System.Windows.Forms.RadioButton();
            this.gridReservations = new System.Windows.Forms.DataGridView();
            this.rbAuto = new System.Windows.Forms.RadioButton();
            this.addRes = new System.Windows.Forms.Button();
            this.cmbAvlblRooms = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBoxChildren = new System.Windows.Forms.TextBox();
            this.txtBoxNumOfRooms = new System.Windows.Forms.TextBox();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.txtBoxAdults = new System.Windows.Forms.TextBox();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.cmbRoomType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkBoxChkOut = new System.Windows.Forms.CheckBox();
            this.chkBoxChkIn = new System.Windows.Forms.CheckBox();
            this.datePickerCOut = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.datePickerCIn = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.grdReservations = new System.Windows.Forms.DataGridView();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblAmount = new System.Windows.Forms.Label();
            this.lblPayingAmount = new System.Windows.Forms.Label();
            this.lblTotAmount = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblBalance = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPaying = new System.Windows.Forms.TextBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.txtInvoiceID = new System.Windows.Forms.TextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.rdDholler = new System.Windows.Forms.RadioButton();
            this.rbRs = new System.Windows.Forms.RadioButton();
            this.txtTotAmount = new System.Windows.Forms.TextBox();
            this.txtBoxdiscount = new System.Windows.Forms.TextBox();
            this.txtBoxAmount = new System.Windows.Forms.TextBox();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.cmbpaymentType = new System.Windows.Forms.ComboBox();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblLogout = new System.Windows.Forms.LinkLabel();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblUserLevel = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.btnHome = new System.Windows.Forms.Button();
            this.office2007Black = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.vista = new Telerik.WinControls.Themes.VistaTheme();
            this.office2010 = new Telerik.WinControls.Themes.Office2010Theme();
            this.telerik = new Telerik.WinControls.Themes.TelerikTheme();
            this.aqua = new Telerik.WinControls.Themes.AquaTheme();
            this.desert = new Telerik.WinControls.Themes.DesertTheme();
            this.breeze = new Telerik.WinControls.Themes.BreezeTheme();
            this.office2007SilverTheme1 = new Telerik.WinControls.Themes.Office2007SilverTheme();
            this.windows7Theme1 = new Telerik.WinControls.Themes.Windows7Theme();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridReservations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdReservations)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.btnRefresh);
            this.groupBox1.Controls.Add(this.txtBoxResId);
            this.groupBox1.Controls.Add(this.radLabel9);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Location = new System.Drawing.Point(730, 88);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(300, 90);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.Color.Purple;
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.Font = new System.Drawing.Font("Microsoft Tai Le", 10F);
            this.btnRefresh.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnRefresh.Location = new System.Drawing.Point(173, 50);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(120, 32);
            this.btnRefresh.TabIndex = 17;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // txtBoxResId
            // 
            this.txtBoxResId.Location = new System.Drawing.Point(93, 21);
            this.txtBoxResId.Name = "txtBoxResId";
            this.txtBoxResId.Size = new System.Drawing.Size(200, 23);
            this.txtBoxResId.TabIndex = 16;
            this.txtBoxResId.TextChanged += new System.EventHandler(this.txtBoxResId_TextChanged);
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(2, 25);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(85, 19);
            this.radLabel9.TabIndex = 72;
            this.radLabel9.Text = "Reservation ID";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.txtNationalty);
            this.groupBox2.Controls.Add(this.radLabel14);
            this.groupBox2.Controls.Add(this.rbMale);
            this.groupBox2.Controls.Add(this.rbFemale);
            this.groupBox2.Controls.Add(this.txtBoxPassPort);
            this.groupBox2.Controls.Add(this.txtBoxCustName);
            this.groupBox2.Controls.Add(this.txtBoxCusId);
            this.groupBox2.Controls.Add(this.radLabel4);
            this.groupBox2.Controls.Add(this.cmbCusNic);
            this.groupBox2.Controls.Add(this.radLabel3);
            this.groupBox2.Controls.Add(this.radLabel1);
            this.groupBox2.Controls.Add(this.radLabel6);
            this.groupBox2.Controls.Add(this.radLabel5);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox2.Location = new System.Drawing.Point(730, 180);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(300, 210);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Guest Info";
            // 
            // txtNationalty
            // 
            this.txtNationalty.Location = new System.Drawing.Point(105, 141);
            this.txtNationalty.Name = "txtNationalty";
            this.txtNationalty.Size = new System.Drawing.Size(180, 23);
            this.txtNationalty.TabIndex = 22;
            // 
            // radLabel14
            // 
            this.radLabel14.Location = new System.Drawing.Point(20, 145);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(63, 19);
            this.radLabel14.TabIndex = 72;
            this.radLabel14.Text = "Nationalty";
            // 
            // rbMale
            // 
            this.rbMale.AutoSize = true;
            this.rbMale.Location = new System.Drawing.Point(105, 173);
            this.rbMale.Name = "rbMale";
            this.rbMale.Size = new System.Drawing.Size(52, 20);
            this.rbMale.TabIndex = 23;
            this.rbMale.TabStop = true;
            this.rbMale.Text = "Male";
            this.rbMale.UseVisualStyleBackColor = true;
            // 
            // rbFemale
            // 
            this.rbFemale.AutoSize = true;
            this.rbFemale.Location = new System.Drawing.Point(168, 173);
            this.rbFemale.Name = "rbFemale";
            this.rbFemale.Size = new System.Drawing.Size(64, 20);
            this.rbFemale.TabIndex = 24;
            this.rbFemale.TabStop = true;
            this.rbFemale.Text = "Female";
            this.rbFemale.UseVisualStyleBackColor = true;
            // 
            // txtBoxPassPort
            // 
            this.txtBoxPassPort.Location = new System.Drawing.Point(105, 109);
            this.txtBoxPassPort.Name = "txtBoxPassPort";
            this.txtBoxPassPort.Size = new System.Drawing.Size(180, 23);
            this.txtBoxPassPort.TabIndex = 21;
            // 
            // txtBoxCustName
            // 
            this.txtBoxCustName.Location = new System.Drawing.Point(105, 79);
            this.txtBoxCustName.Name = "txtBoxCustName";
            this.txtBoxCustName.Size = new System.Drawing.Size(180, 23);
            this.txtBoxCustName.TabIndex = 20;
            // 
            // txtBoxCusId
            // 
            this.txtBoxCusId.Enabled = false;
            this.txtBoxCusId.Location = new System.Drawing.Point(105, 18);
            this.txtBoxCusId.Name = "txtBoxCusId";
            this.txtBoxCusId.Size = new System.Drawing.Size(180, 23);
            this.txtBoxCusId.TabIndex = 18;
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(20, 112);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(53, 19);
            this.radLabel4.TabIndex = 61;
            this.radLabel4.Text = "Passport";
            // 
            // cmbCusNic
            // 
            this.cmbCusNic.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbCusNic.FormattingEnabled = true;
            this.cmbCusNic.Location = new System.Drawing.Point(105, 48);
            this.cmbCusNic.Name = "cmbCusNic";
            this.cmbCusNic.Size = new System.Drawing.Size(180, 24);
            this.cmbCusNic.TabIndex = 19;
            this.cmbCusNic.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            this.cmbCusNic.TextChanged += new System.EventHandler(this.cmbCusNic_TextChanged);
            this.cmbCusNic.Enter += new System.EventHandler(this.cmbCusNic_Enter);
            this.cmbCusNic.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbCusNic_KeyPress);
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(20, 172);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(46, 19);
            this.radLabel3.TabIndex = 59;
            this.radLabel3.Text = "Gender";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(20, 49);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(33, 19);
            this.radLabel1.TabIndex = 55;
            this.radLabel1.Text = "N I C";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(20, 82);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(74, 19);
            this.radLabel6.TabIndex = 52;
            this.radLabel6.Text = "Guest Name";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(20, 18);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(58, 19);
            this.radLabel5.TabIndex = 51;
            this.radLabel5.Text = "Guest ID*";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.groupBox6);
            this.groupBox3.Controls.Add(this.txtBoxChildren);
            this.groupBox3.Controls.Add(this.txtBoxNumOfRooms);
            this.groupBox3.Controls.Add(this.radLabel8);
            this.groupBox3.Controls.Add(this.radLabel12);
            this.groupBox3.Controls.Add(this.txtBoxAdults);
            this.groupBox3.Controls.Add(this.radLabel10);
            this.groupBox3.Controls.Add(this.cmbRoomType);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.chkBoxChkOut);
            this.groupBox3.Controls.Add(this.chkBoxChkIn);
            this.groupBox3.Controls.Add(this.datePickerCOut);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.datePickerCIn);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox3.Location = new System.Drawing.Point(5, 371);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(719, 315);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Checking Info";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.rbOneRoom);
            this.groupBox6.Controls.Add(this.gridReservations);
            this.groupBox6.Controls.Add(this.rbAuto);
            this.groupBox6.Controls.Add(this.addRes);
            this.groupBox6.Controls.Add(this.cmbAvlblRooms);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox6.Location = new System.Drawing.Point(335, 20);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(370, 266);
            this.groupBox6.TabIndex = 79;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Rooms";
            // 
            // rbOneRoom
            // 
            this.rbOneRoom.AutoSize = true;
            this.rbOneRoom.Location = new System.Drawing.Point(271, 43);
            this.rbOneRoom.Name = "rbOneRoom";
            this.rbOneRoom.Size = new System.Drawing.Size(80, 20);
            this.rbOneRoom.TabIndex = 14;
            this.rbOneRoom.TabStop = true;
            this.rbOneRoom.Text = "One room";
            this.rbOneRoom.UseVisualStyleBackColor = true;
            // 
            // gridReservations
            // 
            this.gridReservations.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.gridReservations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridReservations.Location = new System.Drawing.Point(19, 66);
            this.gridReservations.Name = "gridReservations";
            this.gridReservations.Size = new System.Drawing.Size(332, 145);
            this.gridReservations.TabIndex = 11;
            // 
            // rbAuto
            // 
            this.rbAuto.AutoSize = true;
            this.rbAuto.Location = new System.Drawing.Point(10, 43);
            this.rbAuto.Name = "rbAuto";
            this.rbAuto.Size = new System.Drawing.Size(133, 20);
            this.rbAuto.TabIndex = 11;
            this.rbAuto.TabStop = true;
            this.rbAuto.Text = "Automatic Selection";
            this.rbAuto.UseVisualStyleBackColor = true;
            this.rbAuto.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // addRes
            // 
            this.addRes.BackColor = System.Drawing.Color.Purple;
            this.addRes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addRes.Font = new System.Drawing.Font("Microsoft Tai Le", 10F);
            this.addRes.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.addRes.Location = new System.Drawing.Point(240, 221);
            this.addRes.Name = "addRes";
            this.addRes.Size = new System.Drawing.Size(120, 32);
            this.addRes.TabIndex = 15;
            this.addRes.Text = "Add Rooms";
            this.addRes.UseVisualStyleBackColor = false;
            this.addRes.Click += new System.EventHandler(this.addRes_Click);
            // 
            // cmbAvlblRooms
            // 
            this.cmbAvlblRooms.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbAvlblRooms.FormattingEnabled = true;
            this.cmbAvlblRooms.Location = new System.Drawing.Point(143, 16);
            this.cmbAvlblRooms.Name = "cmbAvlblRooms";
            this.cmbAvlblRooms.Size = new System.Drawing.Size(200, 24);
            this.cmbAvlblRooms.TabIndex = 10;
            this.cmbAvlblRooms.SelectedIndexChanged += new System.EventHandler(this.cmbAvlblRooms_SelectedIndexChanged);
            this.cmbAvlblRooms.SelectedValueChanged += new System.EventHandler(this.cmbAvlblRooms_SelectedValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "Available Rooms";
            // 
            // txtBoxChildren
            // 
            this.txtBoxChildren.Location = new System.Drawing.Point(123, 212);
            this.txtBoxChildren.Name = "txtBoxChildren";
            this.txtBoxChildren.Size = new System.Drawing.Size(120, 23);
            this.txtBoxChildren.TabIndex = 6;
            this.txtBoxChildren.TextChanged += new System.EventHandler(this.txtBoxChildren_TextChanged);
            // 
            // txtBoxNumOfRooms
            // 
            this.txtBoxNumOfRooms.Location = new System.Drawing.Point(123, 241);
            this.txtBoxNumOfRooms.Name = "txtBoxNumOfRooms";
            this.txtBoxNumOfRooms.Size = new System.Drawing.Size(120, 23);
            this.txtBoxNumOfRooms.TabIndex = 7;
            this.txtBoxNumOfRooms.TextChanged += new System.EventHandler(this.validateTextInteger);
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(24, 245);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(78, 19);
            this.radLabel8.TabIndex = 79;
            this.radLabel8.Text = "No. of rooms";
            // 
            // radLabel12
            // 
            this.radLabel12.Location = new System.Drawing.Point(24, 216);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(89, 19);
            this.radLabel12.TabIndex = 76;
            this.radLabel12.Text = "No. of Children";
            // 
            // txtBoxAdults
            // 
            this.txtBoxAdults.Location = new System.Drawing.Point(123, 184);
            this.txtBoxAdults.Name = "txtBoxAdults";
            this.txtBoxAdults.Size = new System.Drawing.Size(120, 23);
            this.txtBoxAdults.TabIndex = 5;
            this.txtBoxAdults.TextChanged += new System.EventHandler(this.txtBoxAdults_TextChanged);
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(24, 188);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(78, 19);
            this.radLabel10.TabIndex = 72;
            this.radLabel10.Text = "No. of Adults";
            // 
            // cmbRoomType
            // 
            this.cmbRoomType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbRoomType.FormattingEnabled = true;
            this.cmbRoomType.Location = new System.Drawing.Point(123, 154);
            this.cmbRoomType.Name = "cmbRoomType";
            this.cmbRoomType.Size = new System.Drawing.Size(200, 24);
            this.cmbRoomType.TabIndex = 4;
            this.cmbRoomType.SelectedIndexChanged += new System.EventHandler(this.cmbRoomType_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 157);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Room Type";
            // 
            // chkBoxChkOut
            // 
            this.chkBoxChkOut.AutoSize = true;
            this.chkBoxChkOut.Location = new System.Drawing.Point(167, 270);
            this.chkBoxChkOut.Name = "chkBoxChkOut";
            this.chkBoxChkOut.Size = new System.Drawing.Size(142, 20);
            this.chkBoxChkOut.TabIndex = 9;
            this.chkBoxChkOut.Text = "Half Board Check Out";
            this.chkBoxChkOut.UseVisualStyleBackColor = true;
            this.chkBoxChkOut.CheckedChanged += new System.EventHandler(this.chkBoxChkOut_CheckedChanged);
            // 
            // chkBoxChkIn
            // 
            this.chkBoxChkIn.AutoSize = true;
            this.chkBoxChkIn.Location = new System.Drawing.Point(27, 270);
            this.chkBoxChkIn.Name = "chkBoxChkIn";
            this.chkBoxChkIn.Size = new System.Drawing.Size(136, 20);
            this.chkBoxChkIn.TabIndex = 8;
            this.chkBoxChkIn.Text = "Half Board Checking";
            this.chkBoxChkIn.UseVisualStyleBackColor = true;
            this.chkBoxChkIn.CheckedChanged += new System.EventHandler(this.chkBoxChkIn_CheckedChanged);
            // 
            // datePickerCOut
            // 
            this.datePickerCOut.Location = new System.Drawing.Point(123, 67);
            this.datePickerCOut.Name = "datePickerCOut";
            this.datePickerCOut.Size = new System.Drawing.Size(200, 23);
            this.datePickerCOut.TabIndex = 2;
            this.datePickerCOut.ValueChanged += new System.EventHandler(this.datePickerCOut_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Checkout Date";
            // 
            // datePickerCIn
            // 
            this.datePickerCIn.Location = new System.Drawing.Point(123, 30);
            this.datePickerCIn.Name = "datePickerCIn";
            this.datePickerCIn.Size = new System.Drawing.Size(200, 23);
            this.datePickerCIn.TabIndex = 1;
            this.datePickerCIn.ValueChanged += new System.EventHandler(this.datePickerCIn_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Checking Date";
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Green;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button6.Location = new System.Drawing.Point(730, 654);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(145, 32);
            this.button6.TabIndex = 33;
            this.button6.Text = "Checkin";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Orange;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button7.Location = new System.Drawing.Point(885, 654);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(145, 32);
            this.button7.TabIndex = 34;
            this.button7.Text = "Cancel Reservation";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // grdReservations
            // 
            this.grdReservations.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdReservations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdReservations.Location = new System.Drawing.Point(5, 88);
            this.grdReservations.Name = "grdReservations";
            this.grdReservations.Size = new System.Drawing.Size(719, 277);
            this.grdReservations.TabIndex = 78;
            this.grdReservations.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdReservations_CellContentClick);
            this.grdReservations.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.grdReservations_RowStateChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.lblAmount);
            this.groupBox5.Controls.Add(this.lblPayingAmount);
            this.groupBox5.Controls.Add(this.lblTotAmount);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.lblBalance);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.txtPaying);
            this.groupBox5.Controls.Add(this.radLabel7);
            this.groupBox5.Controls.Add(this.txtInvoiceID);
            this.groupBox5.Controls.Add(this.radLabel2);
            this.groupBox5.Controls.Add(this.rdDholler);
            this.groupBox5.Controls.Add(this.rbRs);
            this.groupBox5.Controls.Add(this.txtTotAmount);
            this.groupBox5.Controls.Add(this.txtBoxdiscount);
            this.groupBox5.Controls.Add(this.txtBoxAmount);
            this.groupBox5.Controls.Add(this.radLabel13);
            this.groupBox5.Controls.Add(this.radLabel15);
            this.groupBox5.Controls.Add(this.radLabel16);
            this.groupBox5.Controls.Add(this.cmbpaymentType);
            this.groupBox5.Controls.Add(this.radLabel17);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox5.Location = new System.Drawing.Point(730, 396);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(300, 245);
            this.groupBox5.TabIndex = 79;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Payments";
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new System.Drawing.Point(270, 100);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(14, 16);
            this.lblAmount.TabIndex = 130;
            this.lblAmount.Text = "$";
            // 
            // lblPayingAmount
            // 
            this.lblPayingAmount.AutoSize = true;
            this.lblPayingAmount.Location = new System.Drawing.Point(271, 187);
            this.lblPayingAmount.Name = "lblPayingAmount";
            this.lblPayingAmount.Size = new System.Drawing.Size(14, 16);
            this.lblPayingAmount.TabIndex = 129;
            this.lblPayingAmount.Text = "$";
            // 
            // lblTotAmount
            // 
            this.lblTotAmount.AutoSize = true;
            this.lblTotAmount.Location = new System.Drawing.Point(271, 158);
            this.lblTotAmount.Name = "lblTotAmount";
            this.lblTotAmount.Size = new System.Drawing.Size(14, 16);
            this.lblTotAmount.TabIndex = 128;
            this.lblTotAmount.Text = "$";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(270, 129);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 16);
            this.label7.TabIndex = 88;
            this.label7.Text = "%";
            // 
            // lblBalance
            // 
            this.lblBalance.AutoSize = true;
            this.lblBalance.Location = new System.Drawing.Point(105, 216);
            this.lblBalance.Name = "lblBalance";
            this.lblBalance.Size = new System.Drawing.Size(35, 16);
            this.lblBalance.TabIndex = 86;
            this.lblBalance.Text = "00.00";
            this.lblBalance.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 216);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 16);
            this.label5.TabIndex = 85;
            this.label5.Text = "Balance";
            // 
            // txtPaying
            // 
            this.txtPaying.Location = new System.Drawing.Point(108, 184);
            this.txtPaying.Name = "txtPaying";
            this.txtPaying.Size = new System.Drawing.Size(155, 23);
            this.txtPaying.TabIndex = 32;
            this.txtPaying.TextChanged += new System.EventHandler(this.txtPaying_TextChanged);
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(14, 187);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(90, 19);
            this.radLabel7.TabIndex = 83;
            this.radLabel7.Text = "Paying Amount";
            // 
            // txtInvoiceID
            // 
            this.txtInvoiceID.Enabled = false;
            this.txtInvoiceID.Location = new System.Drawing.Point(108, 38);
            this.txtInvoiceID.Name = "txtInvoiceID";
            this.txtInvoiceID.Size = new System.Drawing.Size(180, 23);
            this.txtInvoiceID.TabIndex = 27;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(14, 40);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(59, 19);
            this.radLabel2.TabIndex = 81;
            this.radLabel2.Text = "Invoice Id";
            // 
            // rdDholler
            // 
            this.rdDholler.AutoSize = true;
            this.rdDholler.Location = new System.Drawing.Point(117, 17);
            this.rdDholler.Name = "rdDholler";
            this.rdDholler.Size = new System.Drawing.Size(32, 20);
            this.rdDholler.TabIndex = 25;
            this.rdDholler.TabStop = true;
            this.rdDholler.Text = "$";
            this.rdDholler.UseVisualStyleBackColor = true;
            this.rdDholler.CheckedChanged += new System.EventHandler(this.rdDholler_CheckedChanged);
            // 
            // rbRs
            // 
            this.rbRs.AutoSize = true;
            this.rbRs.Location = new System.Drawing.Point(190, 17);
            this.rbRs.Name = "rbRs";
            this.rbRs.Size = new System.Drawing.Size(38, 20);
            this.rbRs.TabIndex = 26;
            this.rbRs.TabStop = true;
            this.rbRs.Text = "Rs";
            this.rbRs.UseVisualStyleBackColor = true;
            this.rbRs.CheckedChanged += new System.EventHandler(this.rbRs_CheckedChanged);
            // 
            // txtTotAmount
            // 
            this.txtTotAmount.Location = new System.Drawing.Point(108, 155);
            this.txtTotAmount.Name = "txtTotAmount";
            this.txtTotAmount.Size = new System.Drawing.Size(155, 23);
            this.txtTotAmount.TabIndex = 31;
            this.txtTotAmount.TextChanged += new System.EventHandler(this.validateTextDouble);
            // 
            // txtBoxdiscount
            // 
            this.txtBoxdiscount.Location = new System.Drawing.Point(108, 126);
            this.txtBoxdiscount.Name = "txtBoxdiscount";
            this.txtBoxdiscount.Size = new System.Drawing.Size(155, 23);
            this.txtBoxdiscount.TabIndex = 30;
            this.txtBoxdiscount.TextChanged += new System.EventHandler(this.txtBoxdiscount_TextChanged_1);
            // 
            // txtBoxAmount
            // 
            this.txtBoxAmount.AllowDrop = true;
            this.txtBoxAmount.Location = new System.Drawing.Point(108, 97);
            this.txtBoxAmount.Name = "txtBoxAmount";
            this.txtBoxAmount.Size = new System.Drawing.Size(155, 23);
            this.txtBoxAmount.TabIndex = 29;
            this.txtBoxAmount.TextChanged += new System.EventHandler(this.validateTextDouble);
            this.txtBoxAmount.Validated += new System.EventHandler(this.validateTextDouble);
            // 
            // radLabel13
            // 
            this.radLabel13.Location = new System.Drawing.Point(14, 156);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(81, 19);
            this.radLabel13.TabIndex = 75;
            this.radLabel13.Text = "Total Amount";
            // 
            // radLabel15
            // 
            this.radLabel15.Location = new System.Drawing.Point(14, 128);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(60, 19);
            this.radLabel15.TabIndex = 74;
            this.radLabel15.Text = "Discounts";
            // 
            // radLabel16
            // 
            this.radLabel16.Location = new System.Drawing.Point(14, 101);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(51, 19);
            this.radLabel16.TabIndex = 73;
            this.radLabel16.Text = "Amount";
            // 
            // cmbpaymentType
            // 
            this.cmbpaymentType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbpaymentType.FormattingEnabled = true;
            this.cmbpaymentType.Items.AddRange(new object[] {
            "Cheque",
            "Cash",
            "Credit Card",
            "Credit"});
            this.cmbpaymentType.Location = new System.Drawing.Point(108, 67);
            this.cmbpaymentType.Name = "cmbpaymentType";
            this.cmbpaymentType.Size = new System.Drawing.Size(180, 24);
            this.cmbpaymentType.TabIndex = 28;
            // 
            // radLabel17
            // 
            this.radLabel17.Location = new System.Drawing.Point(14, 69);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(81, 19);
            this.radLabel17.TabIndex = 71;
            this.radLabel17.Text = "Payment type";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblLogout);
            this.panel1.Controls.Add(this.lblTime);
            this.panel1.Controls.Add(this.lblUserLevel);
            this.panel1.Controls.Add(this.lblUserName);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.btnHome);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1042, 80);
            this.panel1.TabIndex = 101;
            // 
            // lblLogout
            // 
            this.lblLogout.AutoSize = true;
            this.lblLogout.Font = new System.Drawing.Font("Microsoft Tai Le", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogout.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblLogout.Location = new System.Drawing.Point(948, 37);
            this.lblLogout.Name = "lblLogout";
            this.lblLogout.Size = new System.Drawing.Size(81, 29);
            this.lblLogout.TabIndex = 101;
            this.lblLogout.TabStop = true;
            this.lblLogout.Text = "Logout";
            this.lblLogout.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblLogout_LinkClicked);
            // 
            // lblTime
            // 
            this.lblTime.BackColor = System.Drawing.Color.Transparent;
            this.lblTime.Font = new System.Drawing.Font("Microsoft JhengHei", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTime.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTime.Location = new System.Drawing.Point(557, 2);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(334, 46);
            this.lblTime.TabIndex = 52;
            this.lblTime.Text = "Time";
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUserLevel
            // 
            this.lblUserLevel.AutoSize = true;
            this.lblUserLevel.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserLevel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblUserLevel.Location = new System.Drawing.Point(560, 48);
            this.lblUserLevel.Name = "lblUserLevel";
            this.lblUserLevel.Size = new System.Drawing.Size(76, 19);
            this.lblUserLevel.TabIndex = 48;
            this.lblUserLevel.Text = "User Level";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblUserName.Location = new System.Drawing.Point(663, 48);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(82, 19);
            this.lblUserName.TabIndex = 49;
            this.lblUserName.Text = "User Name";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Tai Le", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label16.Location = new System.Drawing.Point(112, 48);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(417, 18);
            this.label16.TabIndex = 34;
            this.label16.Text = "Backup and Restore Database , Select a Skin and Background Color";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Tai Le", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label17.Location = new System.Drawing.Point(110, 18);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(175, 29);
            this.label17.TabIndex = 33;
            this.label17.Text = "Reservations Tile";
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.Color.LimeGreen;
            this.btnHome.FlatAppearance.BorderSize = 0;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnHome.Font = new System.Drawing.Font("Microsoft Tai Le", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHome.Location = new System.Drawing.Point(4, 3);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(100, 68);
            this.btnHome.TabIndex = 16;
            this.btnHome.Text = "Home";
            this.btnHome.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // Reservations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1042, 698);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.grdReservations);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Tai Le", 12F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Reservations";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(0, 0);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reservations";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.Reservations_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridReservations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdReservations)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.DateTimePicker datePickerCIn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker datePickerCOut;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbRoomType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkBoxChkOut;
        private System.Windows.Forms.CheckBox chkBoxChkIn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button addRes;
        private System.Windows.Forms.DataGridView gridReservations;
        private System.Windows.Forms.ComboBox cmbAvlblRooms;
        private System.Windows.Forms.TextBox txtBoxCusId;
        private System.Windows.Forms.TextBox txtBoxPassPort;
        private System.Windows.Forms.TextBox txtBoxCustName;
        private System.Windows.Forms.TextBox txtBoxResId;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private System.Windows.Forms.TextBox txtBoxChildren;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private System.Windows.Forms.TextBox txtBoxAdults;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private System.Windows.Forms.DataGridView grdReservations;
        private System.Windows.Forms.TextBox txtNationalty;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private System.Windows.Forms.RadioButton rbMale;
        private System.Windows.Forms.RadioButton rbFemale;
        private System.Windows.Forms.ComboBox cmbCusNic;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton rbAuto;
        private System.Windows.Forms.TextBox txtBoxNumOfRooms;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private System.Windows.Forms.Label lblBalance;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPaying;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private System.Windows.Forms.TextBox txtInvoiceID;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private System.Windows.Forms.RadioButton rdDholler;
        private System.Windows.Forms.RadioButton rbRs;
        private System.Windows.Forms.TextBox txtTotAmount;
        private System.Windows.Forms.TextBox txtBoxdiscount;
        private System.Windows.Forms.TextBox txtBoxAmount;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        public System.Windows.Forms.ComboBox cmbpaymentType;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private System.Windows.Forms.RadioButton rbOneRoom;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblUserLevel;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.LinkLabel lblLogout;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007Black;
        private Telerik.WinControls.Themes.VistaTheme vista;
        private Telerik.WinControls.Themes.Office2010Theme office2010;
        private Telerik.WinControls.Themes.TelerikTheme telerik;
        private Telerik.WinControls.Themes.AquaTheme aqua;
        private Telerik.WinControls.Themes.DesertTheme desert;
        private Telerik.WinControls.Themes.BreezeTheme breeze;
        private Telerik.WinControls.Themes.Office2007SilverTheme office2007SilverTheme1;
        private Telerik.WinControls.Themes.Windows7Theme windows7Theme1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblPayingAmount;
        private System.Windows.Forms.Label lblTotAmount;
        private System.Windows.Forms.Label lblAmount;
    }
}

