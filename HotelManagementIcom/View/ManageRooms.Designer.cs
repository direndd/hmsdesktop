namespace HotelManagementIcom.View
{
    partial class ManageRooms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblRoomType = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnRoomTypeDetails = new System.Windows.Forms.Button();
            this.cmbRoomType = new System.Windows.Forms.ComboBox();
            this.txtHalfPrice1 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtFullPrice1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtNoOfChildren1 = new System.Windows.Forms.TextBox();
            this.txtNoOfAdults1 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.dgvRoomType = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.btnAllRoomTypeReport = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnAllRoomReport = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtNoOfChildren2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDescription2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAvailability2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.lblRoomNo = new System.Windows.Forms.Label();
            this.cmbRoomNo = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnRoomDetails = new System.Windows.Forms.Button();
            this.txtHalfPrice2 = new System.Windows.Forms.TextBox();
            this.txtFullPrice2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtNoOfAdults2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtFloor2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRoomType2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btnManageRoomFacilities = new System.Windows.Forms.Button();
            this.btnManageRoomTypes = new System.Windows.Forms.Button();
            this.btnManageRooms = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvRooms = new System.Windows.Forms.DataGridView();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblLogout = new System.Windows.Forms.LinkLabel();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblUserLevel = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnHome = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRoomType)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRooms)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(1, 120);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox8);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer1.Size = new System.Drawing.Size(1011, 572);
            this.splitContainer1.SplitterDistance = 337;
            this.splitContainer1.TabIndex = 101;
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.Transparent;
            this.groupBox8.Controls.Add(this.groupBox4);
            this.groupBox8.Controls.Add(this.txtHalfPrice1);
            this.groupBox8.Controls.Add(this.label16);
            this.groupBox8.Controls.Add(this.txtFullPrice1);
            this.groupBox8.Controls.Add(this.label11);
            this.groupBox8.Controls.Add(this.txtNoOfChildren1);
            this.groupBox8.Controls.Add(this.txtNoOfAdults1);
            this.groupBox8.Controls.Add(this.label12);
            this.groupBox8.Controls.Add(this.label13);
            this.groupBox8.Controls.Add(this.dgvRoomType);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox8.Location = new System.Drawing.Point(10, 113);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(315, 437);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Display Room Type Details";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.lblRoomType);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.btnRoomTypeDetails);
            this.groupBox4.Controls.Add(this.cmbRoomType);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox4.Location = new System.Drawing.Point(6, 174);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(300, 90);
            this.groupBox4.TabIndex = 71;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Room Types";
            // 
            // lblRoomType
            // 
            this.lblRoomType.AutoSize = true;
            this.lblRoomType.ForeColor = System.Drawing.Color.Red;
            this.lblRoomType.Location = new System.Drawing.Point(14, 67);
            this.lblRoomType.Name = "lblRoomType";
            this.lblRoomType.Size = new System.Drawing.Size(135, 16);
            this.lblRoomType.TabIndex = 47;
            this.lblRoomType.Text = "* Letters and Digits Only";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 16);
            this.label1.TabIndex = 46;
            this.label1.Text = "Select Room Type";
            // 
            // btnRoomTypeDetails
            // 
            this.btnRoomTypeDetails.BackColor = System.Drawing.Color.Purple;
            this.btnRoomTypeDetails.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRoomTypeDetails.Font = new System.Drawing.Font("Microsoft Tai Le", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRoomTypeDetails.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnRoomTypeDetails.Location = new System.Drawing.Point(162, 32);
            this.btnRoomTypeDetails.Name = "btnRoomTypeDetails";
            this.btnRoomTypeDetails.Size = new System.Drawing.Size(120, 32);
            this.btnRoomTypeDetails.TabIndex = 5;
            this.btnRoomTypeDetails.Text = "Display Details";
            this.btnRoomTypeDetails.UseVisualStyleBackColor = false;
            this.btnRoomTypeDetails.Click += new System.EventHandler(this.btnRoomTypeDetails_Click);
            // 
            // cmbRoomType
            // 
            this.cmbRoomType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbRoomType.FormattingEnabled = true;
            this.cmbRoomType.Location = new System.Drawing.Point(15, 37);
            this.cmbRoomType.Name = "cmbRoomType";
            this.cmbRoomType.Size = new System.Drawing.Size(140, 24);
            this.cmbRoomType.TabIndex = 4;
            this.cmbRoomType.SelectedIndexChanged += new System.EventHandler(this.cmbRoomType_SelectedIndexChanged);
            this.cmbRoomType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbRoomType_KeyPress);
            // 
            // txtHalfPrice1
            // 
            this.txtHalfPrice1.Location = new System.Drawing.Point(104, 395);
            this.txtHalfPrice1.Name = "txtHalfPrice1";
            this.txtHalfPrice1.ReadOnly = true;
            this.txtHalfPrice1.Size = new System.Drawing.Size(194, 23);
            this.txtHalfPrice1.TabIndex = 9;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(9, 398);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 16);
            this.label16.TabIndex = 69;
            this.label16.Text = "Half Board Price";
            // 
            // txtFullPrice1
            // 
            this.txtFullPrice1.Location = new System.Drawing.Point(104, 357);
            this.txtFullPrice1.Name = "txtFullPrice1";
            this.txtFullPrice1.ReadOnly = true;
            this.txtFullPrice1.Size = new System.Drawing.Size(194, 23);
            this.txtFullPrice1.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 360);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 16);
            this.label11.TabIndex = 67;
            this.label11.Text = "Full Board Price";
            // 
            // txtNoOfChildren1
            // 
            this.txtNoOfChildren1.Location = new System.Drawing.Point(104, 319);
            this.txtNoOfChildren1.Name = "txtNoOfChildren1";
            this.txtNoOfChildren1.ReadOnly = true;
            this.txtNoOfChildren1.Size = new System.Drawing.Size(194, 23);
            this.txtNoOfChildren1.TabIndex = 7;
            // 
            // txtNoOfAdults1
            // 
            this.txtNoOfAdults1.Location = new System.Drawing.Point(104, 281);
            this.txtNoOfAdults1.Name = "txtNoOfAdults1";
            this.txtNoOfAdults1.ReadOnly = true;
            this.txtNoOfAdults1.Size = new System.Drawing.Size(194, 23);
            this.txtNoOfAdults1.TabIndex = 6;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 322);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 16);
            this.label12.TabIndex = 64;
            this.label12.Text = "No Of Children";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(9, 285);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 16);
            this.label13.TabIndex = 63;
            this.label13.Text = "No Of Adults";
            // 
            // dgvRoomType
            // 
            this.dgvRoomType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvRoomType.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvRoomType.Location = new System.Drawing.Point(6, 21);
            this.dgvRoomType.Name = "dgvRoomType";
            this.dgvRoomType.ReadOnly = true;
            this.dgvRoomType.Size = new System.Drawing.Size(300, 147);
            this.dgvRoomType.TabIndex = 0;
            this.dgvRoomType.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRoomType_CellClick);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.btnRefresh);
            this.groupBox2.Controls.Add(this.groupBox9);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox2.Location = new System.Drawing.Point(8, 227);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(650, 325);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "View Room Details";
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.Color.MidnightBlue;
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.Font = new System.Drawing.Font("Microsoft Tai Le", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnRefresh.Location = new System.Drawing.Point(481, 268);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(120, 32);
            this.btnRefresh.TabIndex = 14;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.Color.Transparent;
            this.groupBox9.Controls.Add(this.btnAllRoomTypeReport);
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox9.Location = new System.Drawing.Point(452, 127);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(180, 100);
            this.groupBox9.TabIndex = 49;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "All Room Types";
            // 
            // btnAllRoomTypeReport
            // 
            this.btnAllRoomTypeReport.BackColor = System.Drawing.Color.Green;
            this.btnAllRoomTypeReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAllRoomTypeReport.Font = new System.Drawing.Font("Microsoft Tai Le", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAllRoomTypeReport.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAllRoomTypeReport.Location = new System.Drawing.Point(29, 38);
            this.btnAllRoomTypeReport.Name = "btnAllRoomTypeReport";
            this.btnAllRoomTypeReport.Size = new System.Drawing.Size(120, 32);
            this.btnAllRoomTypeReport.TabIndex = 13;
            this.btnAllRoomTypeReport.Text = "View Report";
            this.btnAllRoomTypeReport.UseVisualStyleBackColor = false;
            this.btnAllRoomTypeReport.Click += new System.EventHandler(this.btnAllRoomTypeReport_Click_1);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.btnAllRoomReport);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox3.Location = new System.Drawing.Point(452, 21);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(180, 100);
            this.groupBox3.TabIndex = 48;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "All Rooms";
            // 
            // btnAllRoomReport
            // 
            this.btnAllRoomReport.BackColor = System.Drawing.Color.Green;
            this.btnAllRoomReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAllRoomReport.Font = new System.Drawing.Font("Microsoft Tai Le", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAllRoomReport.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAllRoomReport.Location = new System.Drawing.Point(29, 39);
            this.btnAllRoomReport.Name = "btnAllRoomReport";
            this.btnAllRoomReport.Size = new System.Drawing.Size(120, 32);
            this.btnAllRoomReport.TabIndex = 12;
            this.btnAllRoomReport.Text = "View Report";
            this.btnAllRoomReport.UseVisualStyleBackColor = false;
            this.btnAllRoomReport.Click += new System.EventHandler(this.btnAllRoomReport_Click_1);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.txtNoOfChildren2);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.txtDescription2);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.txtAvailability2);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.groupBox6);
            this.groupBox5.Controls.Add(this.txtHalfPrice2);
            this.groupBox5.Controls.Add(this.txtFullPrice2);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.txtNoOfAdults2);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.txtFloor2);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.txtRoomType2);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox5.Location = new System.Drawing.Point(6, 21);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(440, 295);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Specific Room";
            // 
            // txtNoOfChildren2
            // 
            this.txtNoOfChildren2.Location = new System.Drawing.Point(314, 188);
            this.txtNoOfChildren2.Name = "txtNoOfChildren2";
            this.txtNoOfChildren2.ReadOnly = true;
            this.txtNoOfChildren2.Size = new System.Drawing.Size(87, 23);
            this.txtNoOfChildren2.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(218, 191);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 16);
            this.label8.TabIndex = 67;
            this.label8.Text = "No Of Children";
            // 
            // txtDescription2
            // 
            this.txtDescription2.Location = new System.Drawing.Point(111, 256);
            this.txtDescription2.Name = "txtDescription2";
            this.txtDescription2.ReadOnly = true;
            this.txtDescription2.Size = new System.Drawing.Size(290, 23);
            this.txtDescription2.TabIndex = 22;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 259);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 16);
            this.label5.TabIndex = 64;
            this.label5.Text = "Description";
            // 
            // txtAvailability2
            // 
            this.txtAvailability2.Location = new System.Drawing.Point(111, 120);
            this.txtAvailability2.Name = "txtAvailability2";
            this.txtAvailability2.ReadOnly = true;
            this.txtAvailability2.Size = new System.Drawing.Size(87, 23);
            this.txtAvailability2.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 16);
            this.label6.TabIndex = 62;
            this.label6.Text = "Availability";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.lblRoomNo);
            this.groupBox6.Controls.Add(this.cmbRoomNo);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.btnRoomDetails);
            this.groupBox6.Location = new System.Drawing.Point(22, 22);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(400, 80);
            this.groupBox6.TabIndex = 61;
            this.groupBox6.TabStop = false;
            // 
            // lblRoomNo
            // 
            this.lblRoomNo.AutoSize = true;
            this.lblRoomNo.ForeColor = System.Drawing.Color.Red;
            this.lblRoomNo.Location = new System.Drawing.Point(105, 57);
            this.lblRoomNo.Name = "lblRoomNo";
            this.lblRoomNo.Size = new System.Drawing.Size(74, 16);
            this.lblRoomNo.TabIndex = 45;
            this.lblRoomNo.Text = "* Digits Only";
            // 
            // cmbRoomNo
            // 
            this.cmbRoomNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbRoomNo.FormattingEnabled = true;
            this.cmbRoomNo.Location = new System.Drawing.Point(113, 30);
            this.cmbRoomNo.Name = "cmbRoomNo";
            this.cmbRoomNo.Size = new System.Drawing.Size(140, 24);
            this.cmbRoomNo.TabIndex = 10;
            this.cmbRoomNo.SelectedIndexChanged += new System.EventHandler(this.cmbRoomNo_SelectedIndexChanged);
            this.cmbRoomNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbRoomNo_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 16);
            this.label2.TabIndex = 44;
            this.label2.Text = "Select Room No";
            // 
            // btnRoomDetails
            // 
            this.btnRoomDetails.BackColor = System.Drawing.Color.Purple;
            this.btnRoomDetails.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRoomDetails.Font = new System.Drawing.Font("Microsoft Tai Le", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRoomDetails.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnRoomDetails.Location = new System.Drawing.Point(259, 25);
            this.btnRoomDetails.Name = "btnRoomDetails";
            this.btnRoomDetails.Size = new System.Drawing.Size(120, 32);
            this.btnRoomDetails.TabIndex = 11;
            this.btnRoomDetails.Text = "Display Details";
            this.btnRoomDetails.UseVisualStyleBackColor = false;
            this.btnRoomDetails.Click += new System.EventHandler(this.btnRoomDetails_Click);
            // 
            // txtHalfPrice2
            // 
            this.txtHalfPrice2.Location = new System.Drawing.Point(314, 222);
            this.txtHalfPrice2.Name = "txtHalfPrice2";
            this.txtHalfPrice2.ReadOnly = true;
            this.txtHalfPrice2.Size = new System.Drawing.Size(87, 23);
            this.txtHalfPrice2.TabIndex = 21;
            // 
            // txtFullPrice2
            // 
            this.txtFullPrice2.Location = new System.Drawing.Point(111, 219);
            this.txtFullPrice2.Name = "txtFullPrice2";
            this.txtFullPrice2.ReadOnly = true;
            this.txtFullPrice2.Size = new System.Drawing.Size(87, 23);
            this.txtFullPrice2.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(218, 225);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 16);
            this.label9.TabIndex = 58;
            this.label9.Text = "Half Board Price";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(19, 222);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 16);
            this.label10.TabIndex = 57;
            this.label10.Text = "Full Board Price";
            // 
            // txtNoOfAdults2
            // 
            this.txtNoOfAdults2.Location = new System.Drawing.Point(111, 188);
            this.txtNoOfAdults2.Name = "txtNoOfAdults2";
            this.txtNoOfAdults2.ReadOnly = true;
            this.txtNoOfAdults2.Size = new System.Drawing.Size(87, 23);
            this.txtNoOfAdults2.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 191);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 16);
            this.label7.TabIndex = 53;
            this.label7.Text = "No Of Adults";
            // 
            // txtFloor2
            // 
            this.txtFloor2.Location = new System.Drawing.Point(314, 153);
            this.txtFloor2.Name = "txtFloor2";
            this.txtFloor2.ReadOnly = true;
            this.txtFloor2.Size = new System.Drawing.Size(87, 23);
            this.txtFloor2.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(218, 157);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 16);
            this.label4.TabIndex = 47;
            this.label4.Text = "Floor No";
            // 
            // txtRoomType2
            // 
            this.txtRoomType2.Location = new System.Drawing.Point(111, 154);
            this.txtRoomType2.Name = "txtRoomType2";
            this.txtRoomType2.ReadOnly = true;
            this.txtRoomType2.Size = new System.Drawing.Size(87, 23);
            this.txtRoomType2.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 157);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 16);
            this.label3.TabIndex = 45;
            this.label3.Text = "Room Type";
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.Transparent;
            this.groupBox7.Controls.Add(this.btnManageRoomFacilities);
            this.groupBox7.Controls.Add(this.btnManageRoomTypes);
            this.groupBox7.Controls.Add(this.btnManageRooms);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox7.Location = new System.Drawing.Point(11, 86);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(315, 141);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Room Management";
            // 
            // btnManageRoomFacilities
            // 
            this.btnManageRoomFacilities.BackColor = System.Drawing.Color.Maroon;
            this.btnManageRoomFacilities.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManageRoomFacilities.Font = new System.Drawing.Font("Microsoft Tai Le", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManageRoomFacilities.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnManageRoomFacilities.Location = new System.Drawing.Point(55, 97);
            this.btnManageRoomFacilities.Name = "btnManageRoomFacilities";
            this.btnManageRoomFacilities.Size = new System.Drawing.Size(200, 32);
            this.btnManageRoomFacilities.TabIndex = 3;
            this.btnManageRoomFacilities.Text = "Manage Room Facilities";
            this.btnManageRoomFacilities.UseVisualStyleBackColor = false;
            this.btnManageRoomFacilities.Click += new System.EventHandler(this.btnManageRoomFacilities_Click);
            // 
            // btnManageRoomTypes
            // 
            this.btnManageRoomTypes.BackColor = System.Drawing.Color.Maroon;
            this.btnManageRoomTypes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManageRoomTypes.Font = new System.Drawing.Font("Microsoft Tai Le", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManageRoomTypes.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnManageRoomTypes.Location = new System.Drawing.Point(55, 59);
            this.btnManageRoomTypes.Name = "btnManageRoomTypes";
            this.btnManageRoomTypes.Size = new System.Drawing.Size(200, 32);
            this.btnManageRoomTypes.TabIndex = 2;
            this.btnManageRoomTypes.Text = "Manage Room Types";
            this.btnManageRoomTypes.UseVisualStyleBackColor = false;
            this.btnManageRoomTypes.Click += new System.EventHandler(this.button9_Click);
            // 
            // btnManageRooms
            // 
            this.btnManageRooms.BackColor = System.Drawing.Color.Maroon;
            this.btnManageRooms.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManageRooms.Font = new System.Drawing.Font("Microsoft Tai Le", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManageRooms.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnManageRooms.Location = new System.Drawing.Point(55, 19);
            this.btnManageRooms.Name = "btnManageRooms";
            this.btnManageRooms.Size = new System.Drawing.Size(200, 32);
            this.btnManageRooms.TabIndex = 1;
            this.btnManageRooms.Text = "Manage Rooms";
            this.btnManageRooms.UseVisualStyleBackColor = false;
            this.btnManageRooms.Click += new System.EventHandler(this.button8_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.dgvRooms);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Location = new System.Drawing.Point(349, 86);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(650, 255);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Display Room Details";
            // 
            // dgvRooms
            // 
            this.dgvRooms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvRooms.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvRooms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRooms.Location = new System.Drawing.Point(3, 19);
            this.dgvRooms.Name = "dgvRooms";
            this.dgvRooms.ReadOnly = true;
            this.dgvRooms.Size = new System.Drawing.Size(644, 233);
            this.dgvRooms.TabIndex = 0;
            this.dgvRooms.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRooms_CellClick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblLogout);
            this.panel1.Controls.Add(this.lblTime);
            this.panel1.Controls.Add(this.lblUserLevel);
            this.panel1.Controls.Add(this.lblUserName);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.btnHome);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel1.Location = new System.Drawing.Point(1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1011, 80);
            this.panel1.TabIndex = 102;
            // 
            // lblLogout
            // 
            this.lblLogout.AutoSize = true;
            this.lblLogout.Font = new System.Drawing.Font("Microsoft Tai Le", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogout.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblLogout.Location = new System.Drawing.Point(898, 37);
            this.lblLogout.Name = "lblLogout";
            this.lblLogout.Size = new System.Drawing.Size(81, 29);
            this.lblLogout.TabIndex = 101;
            this.lblLogout.TabStop = true;
            this.lblLogout.Text = "Logout";
            this.lblLogout.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblLogout_LinkClicked);
            // 
            // lblTime
            // 
            this.lblTime.BackColor = System.Drawing.Color.Transparent;
            this.lblTime.Font = new System.Drawing.Font("Microsoft JhengHei", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTime.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTime.Location = new System.Drawing.Point(557, 2);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(334, 46);
            this.lblTime.TabIndex = 52;
            this.lblTime.Text = "Time";
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUserLevel
            // 
            this.lblUserLevel.AutoSize = true;
            this.lblUserLevel.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserLevel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblUserLevel.Location = new System.Drawing.Point(560, 48);
            this.lblUserLevel.Name = "lblUserLevel";
            this.lblUserLevel.Size = new System.Drawing.Size(76, 19);
            this.lblUserLevel.TabIndex = 48;
            this.lblUserLevel.Text = "User Level";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblUserName.Location = new System.Drawing.Point(663, 48);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(82, 19);
            this.lblUserName.TabIndex = 49;
            this.lblUserName.Text = "User Name";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Tai Le", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label14.Location = new System.Drawing.Point(112, 48);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(417, 18);
            this.label14.TabIndex = 34;
            this.label14.Text = "Backup and Restore Database , Select a Skin and Background Color";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Tai Le", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label15.Location = new System.Drawing.Point(110, 18);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(144, 29);
            this.label15.TabIndex = 33;
            this.label15.Text = "Checkout Tile";
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.Color.LimeGreen;
            this.btnHome.FlatAppearance.BorderSize = 0;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnHome.Font = new System.Drawing.Font("Microsoft Tai Le", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHome.Location = new System.Drawing.Point(4, 3);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(100, 68);
            this.btnHome.TabIndex = 0;
            this.btnHome.Text = "Home";
            this.btnHome.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // ManageRooms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1012, 682);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("Microsoft Tai Le", 12F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1020, 720);
            this.Name = "ManageRooms";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(1020, 720);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Room Management";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.Room_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRoomType)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRooms)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgvRooms;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnRoomDetails;
        private System.Windows.Forms.ComboBox cmbRoomNo;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtHalfPrice2;
        private System.Windows.Forms.TextBox txtFullPrice2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtNoOfAdults2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtFloor2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtRoomType2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox txtHalfPrice1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtFullPrice1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtNoOfChildren1;
        private System.Windows.Forms.TextBox txtNoOfAdults1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridView dgvRoomType;
        private System.Windows.Forms.Button btnManageRoomFacilities;
        private System.Windows.Forms.Button btnManageRoomTypes;
        private System.Windows.Forms.Button btnManageRooms;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRoomTypeDetails;
        private System.Windows.Forms.ComboBox cmbRoomType;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label lblRoomNo;
        private System.Windows.Forms.Label lblRoomType;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.LinkLabel lblLogout;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblUserLevel;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button btnAllRoomTypeReport;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnAllRoomReport;
        private System.Windows.Forms.TextBox txtDescription2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAvailability2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNoOfChildren2;
        private System.Windows.Forms.Label label8;
    }
}

