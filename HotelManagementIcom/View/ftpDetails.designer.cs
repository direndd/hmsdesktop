namespace HotelManagementIcom.View
{
    partial class ftpDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ftpDetails));
            this.label19 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFtpUname = new System.Windows.Forms.TextBox();
            this.txtftpPwd = new System.Windows.Forms.TextBox();
            this.txtBackupUrl = new System.Windows.Forms.TextBox();
            this.btnApplyFtp = new System.Windows.Forms.Button();
            this.btnCancelFtp = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.office2007Black = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.vista = new Telerik.WinControls.Themes.VistaTheme();
            this.office2010 = new Telerik.WinControls.Themes.Office2010Theme();
            this.telerik = new Telerik.WinControls.Themes.TelerikTheme();
            this.aqua = new Telerik.WinControls.Themes.AquaTheme();
            this.desert = new Telerik.WinControls.Themes.DesertTheme();
            this.breeze = new Telerik.WinControls.Themes.BreezeTheme();
            this.office2007SilverTheme1 = new Telerik.WinControls.Themes.Office2007SilverTheme();
            this.windows7Theme1 = new Telerik.WinControls.Themes.Windows7Theme();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Microsoft Tai Le", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label19.Location = new System.Drawing.Point(227, 78);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(58, 16);
            this.label19.TabIndex = 34;
            this.label19.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Tai Le", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label3.Location = new System.Drawing.Point(27, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 16);
            this.label3.TabIndex = 33;
            this.label3.Text = "User Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Tai Le", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label2.Location = new System.Drawing.Point(27, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 16);
            this.label2.TabIndex = 32;
            this.label2.Text = "URL / IP Address";
            // 
            // txtFtpUname
            // 
            this.txtFtpUname.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtFtpUname.Font = new System.Drawing.Font("Microsoft Tai Le", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFtpUname.Location = new System.Drawing.Point(30, 98);
            this.txtFtpUname.Name = "txtFtpUname";
            this.txtFtpUname.Size = new System.Drawing.Size(160, 23);
            this.txtFtpUname.TabIndex = 2;
            // 
            // txtftpPwd
            // 
            this.txtftpPwd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtftpPwd.Font = new System.Drawing.Font("Microsoft Tai Le", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtftpPwd.Location = new System.Drawing.Point(230, 98);
            this.txtftpPwd.Name = "txtftpPwd";
            this.txtftpPwd.PasswordChar = '*';
            this.txtftpPwd.Size = new System.Drawing.Size(160, 23);
            this.txtftpPwd.TabIndex = 3;
            // 
            // txtBackupUrl
            // 
            this.txtBackupUrl.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtBackupUrl.Font = new System.Drawing.Font("Microsoft Tai Le", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBackupUrl.Location = new System.Drawing.Point(30, 47);
            this.txtBackupUrl.Name = "txtBackupUrl";
            this.txtBackupUrl.Size = new System.Drawing.Size(360, 23);
            this.txtBackupUrl.TabIndex = 1;
            // 
            // btnApplyFtp
            // 
            this.btnApplyFtp.BackColor = System.Drawing.Color.Green;
            this.btnApplyFtp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApplyFtp.Font = new System.Drawing.Font("Microsoft Tai Le", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApplyFtp.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnApplyFtp.Location = new System.Drawing.Point(193, 146);
            this.btnApplyFtp.Name = "btnApplyFtp";
            this.btnApplyFtp.Size = new System.Drawing.Size(120, 32);
            this.btnApplyFtp.TabIndex = 4;
            this.btnApplyFtp.Text = "Apply";
            this.btnApplyFtp.UseVisualStyleBackColor = false;
            this.btnApplyFtp.Click += new System.EventHandler(this.btnApplyFtp_Click);
            // 
            // btnCancelFtp
            // 
            this.btnCancelFtp.BackColor = System.Drawing.Color.Orange;
            this.btnCancelFtp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelFtp.Font = new System.Drawing.Font("Microsoft Tai Le", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelFtp.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCancelFtp.Location = new System.Drawing.Point(319, 146);
            this.btnCancelFtp.Name = "btnCancelFtp";
            this.btnCancelFtp.Size = new System.Drawing.Size(120, 32);
            this.btnCancelFtp.TabIndex = 5;
            this.btnCancelFtp.Text = "Cancel";
            this.btnCancelFtp.UseVisualStyleBackColor = false;
            this.btnCancelFtp.Click += new System.EventHandler(this.btnCancelFtp_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnCancelFtp);
            this.groupBox1.Controls.Add(this.txtBackupUrl);
            this.groupBox1.Controls.Add(this.btnApplyFtp);
            this.groupBox1.Controls.Add(this.txtftpPwd);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.txtFtpUname);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(448, 184);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Enter FTP account details";
            // 
            // ftpDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(472, 208);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Tai Le", 12F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ftpDetails";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FTP Account Details";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.ftpDetails_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFtpUname;
        private System.Windows.Forms.TextBox txtftpPwd;
        private System.Windows.Forms.TextBox txtBackupUrl;
        private System.Windows.Forms.Button btnApplyFtp;
        private System.Windows.Forms.Button btnCancelFtp;
        private System.Windows.Forms.GroupBox groupBox1;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007Black;
        private Telerik.WinControls.Themes.VistaTheme vista;
        private Telerik.WinControls.Themes.Office2010Theme office2010;
        private Telerik.WinControls.Themes.TelerikTheme telerik;
        private Telerik.WinControls.Themes.AquaTheme aqua;
        private Telerik.WinControls.Themes.DesertTheme desert;
        private Telerik.WinControls.Themes.BreezeTheme breeze;
        private Telerik.WinControls.Themes.Office2007SilverTheme office2007SilverTheme1;
        private Telerik.WinControls.Themes.Windows7Theme windows7Theme1;
    }
}

