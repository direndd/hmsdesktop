namespace HotelManagementIcom.View
{
    partial class HotelDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HotelDetails));
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnSkip = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.picBoxReports = new System.Windows.Forms.PictureBox();
            this.txtDolorRate = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnMainBrowse = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblContact = new System.Windows.Forms.Label();
            this.lblWeb = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.picBoxLogo = new System.Windows.Forms.PictureBox();
            this.txtWeb = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtContact = new System.Windows.Forms.TextBox();
            this.txtHotelName = new System.Windows.Forms.TextBox();
            this.btnLogoBrowse = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.fdlgImage = new System.Windows.Forms.OpenFileDialog();
            this.fdlgRead = new System.Windows.Forms.OpenFileDialog();
            this.lblWarning = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.office2007Black = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.vista = new Telerik.WinControls.Themes.VistaTheme();
            this.office2010 = new Telerik.WinControls.Themes.Office2010Theme();
            this.telerik = new Telerik.WinControls.Themes.TelerikTheme();
            this.aqua = new Telerik.WinControls.Themes.AquaTheme();
            this.desert = new Telerik.WinControls.Themes.DesertTheme();
            this.breeze = new Telerik.WinControls.Themes.BreezeTheme();
            this.office2007SilverTheme1 = new Telerik.WinControls.Themes.Office2007SilverTheme();
            this.windows7Theme1 = new Telerik.WinControls.Themes.Windows7Theme();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxReports)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // btnApply
            // 
            this.btnApply.BackColor = System.Drawing.Color.Green;
            this.btnApply.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApply.Font = new System.Drawing.Font("Microsoft Tai Le", 10F);
            this.btnApply.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnApply.Location = new System.Drawing.Point(299, 523);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(120, 32);
            this.btnApply.TabIndex = 12;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = false;
            this.btnApply.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnSkip
            // 
            this.btnSkip.BackColor = System.Drawing.Color.MidnightBlue;
            this.btnSkip.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSkip.Font = new System.Drawing.Font("Microsoft Tai Le", 10F);
            this.btnSkip.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSkip.Location = new System.Drawing.Point(425, 523);
            this.btnSkip.Name = "btnSkip";
            this.btnSkip.Size = new System.Drawing.Size(120, 32);
            this.btnSkip.TabIndex = 13;
            this.btnSkip.Text = "Skip";
            this.btnSkip.UseVisualStyleBackColor = false;
            this.btnSkip.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.txtDolorRate);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.btnMainBrowse);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.lblContact);
            this.groupBox1.Controls.Add(this.lblWeb);
            this.groupBox1.Controls.Add(this.lblEmail);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.txtWeb);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtEmail);
            this.groupBox1.Controls.Add(this.txtContact);
            this.groupBox1.Controls.Add(this.txtHotelName);
            this.groupBox1.Controls.Add(this.btnLogoBrowse);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(690, 490);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Enter Hotel Details";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.picBoxReports);
            this.groupBox3.Location = new System.Drawing.Point(32, 317);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(627, 142);
            this.groupBox3.TabIndex = 50;
            this.groupBox3.TabStop = false;
            // 
            // picBoxReports
            // 
            this.picBoxReports.Location = new System.Drawing.Point(6, 16);
            this.picBoxReports.Name = "picBoxReports";
            this.picBoxReports.Size = new System.Drawing.Size(615, 120);
            this.picBoxReports.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxReports.TabIndex = 28;
            this.picBoxReports.TabStop = false;
            this.picBoxReports.Tag = "Logo";
            // 
            // txtDolorRate
            // 
            this.txtDolorRate.Location = new System.Drawing.Point(156, 213);
            this.txtDolorRate.Name = "txtDolorRate";
            this.txtDolorRate.Size = new System.Drawing.Size(200, 23);
            this.txtDolorRate.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(29, 216);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 16);
            this.label11.TabIndex = 49;
            this.label11.Text = "Dollar Rate";
            // 
            // btnMainBrowse
            // 
            this.btnMainBrowse.BackColor = System.Drawing.Color.Purple;
            this.btnMainBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMainBrowse.Font = new System.Drawing.Font("Microsoft Tai Le", 10F);
            this.btnMainBrowse.Location = new System.Drawing.Point(156, 271);
            this.btnMainBrowse.Name = "btnMainBrowse";
            this.btnMainBrowse.Size = new System.Drawing.Size(200, 32);
            this.btnMainBrowse.TabIndex = 10;
            this.btnMainBrowse.Text = "Reports Heading Image";
            this.btnMainBrowse.UseVisualStyleBackColor = false;
            this.btnMainBrowse.Click += new System.EventHandler(this.btnMainBrowse_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Tai Le", 9F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(29, 467);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(316, 16);
            this.label12.TabIndex = 47;
            this.label12.Text = "*Select an Image with a aspect ratio similar to 550x100 ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(29, 279);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 16);
            this.label8.TabIndex = 45;
            this.label8.Text = "Reports Image ";
            // 
            // lblContact
            // 
            this.lblContact.AutoSize = true;
            this.lblContact.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.lblContact.ForeColor = System.Drawing.Color.Brown;
            this.lblContact.Location = new System.Drawing.Point(358, 90);
            this.lblContact.Name = "lblContact";
            this.lblContact.Size = new System.Drawing.Size(12, 13);
            this.lblContact.TabIndex = 44;
            this.lblContact.Text = "*";
            this.lblContact.Visible = false;
            // 
            // lblWeb
            // 
            this.lblWeb.AutoSize = true;
            this.lblWeb.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.lblWeb.ForeColor = System.Drawing.Color.Brown;
            this.lblWeb.Location = new System.Drawing.Point(358, 175);
            this.lblWeb.Name = "lblWeb";
            this.lblWeb.Size = new System.Drawing.Size(12, 13);
            this.lblWeb.TabIndex = 43;
            this.lblWeb.Text = "*";
            this.lblWeb.Visible = false;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.lblEmail.ForeColor = System.Drawing.Color.Brown;
            this.lblEmail.Location = new System.Drawing.Point(358, 134);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(12, 13);
            this.lblEmail.TabIndex = 42;
            this.lblEmail.Text = "*";
            this.lblEmail.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.picBoxLogo);
            this.groupBox2.Location = new System.Drawing.Point(420, 22);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(239, 224);
            this.groupBox2.TabIndex = 41;
            this.groupBox2.TabStop = false;
            // 
            // picBoxLogo
            // 
            this.picBoxLogo.Location = new System.Drawing.Point(6, 11);
            this.picBoxLogo.Name = "picBoxLogo";
            this.picBoxLogo.Size = new System.Drawing.Size(227, 207);
            this.picBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxLogo.TabIndex = 28;
            this.picBoxLogo.TabStop = false;
            this.picBoxLogo.Tag = "Logo";
            // 
            // txtWeb
            // 
            this.txtWeb.Location = new System.Drawing.Point(156, 170);
            this.txtWeb.Name = "txtWeb";
            this.txtWeb.Size = new System.Drawing.Size(200, 23);
            this.txtWeb.TabIndex = 8;
            this.txtWeb.TextChanged += new System.EventHandler(this.txtWeb_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Tai Le", 9F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(424, 250);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(170, 16);
            this.label9.TabIndex = 38;
            this.label9.Text = "*Select a Square Type Image ";
            // 
            // txtEmail
            // 
            this.txtEmail.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtEmail.Location = new System.Drawing.Point(156, 127);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(200, 23);
            this.txtEmail.TabIndex = 7;
            this.txtEmail.TextChanged += new System.EventHandler(this.txtEmail_TextChanged);
            // 
            // txtContact
            // 
            this.txtContact.Location = new System.Drawing.Point(156, 84);
            this.txtContact.Name = "txtContact";
            this.txtContact.Size = new System.Drawing.Size(200, 23);
            this.txtContact.TabIndex = 6;
            this.txtContact.TextChanged += new System.EventHandler(this.txtContact_TextChanged);
            // 
            // txtHotelName
            // 
            this.txtHotelName.Location = new System.Drawing.Point(156, 41);
            this.txtHotelName.Name = "txtHotelName";
            this.txtHotelName.Size = new System.Drawing.Size(200, 23);
            this.txtHotelName.TabIndex = 1;
            // 
            // btnLogoBrowse
            // 
            this.btnLogoBrowse.BackColor = System.Drawing.Color.Purple;
            this.btnLogoBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogoBrowse.Font = new System.Drawing.Font("Microsoft Tai Le", 10F);
            this.btnLogoBrowse.Location = new System.Drawing.Point(420, 271);
            this.btnLogoBrowse.Name = "btnLogoBrowse";
            this.btnLogoBrowse.Size = new System.Drawing.Size(239, 32);
            this.btnLogoBrowse.TabIndex = 11;
            this.btnLogoBrowse.Text = "Hotel Logo";
            this.btnLogoBrowse.UseVisualStyleBackColor = false;
            this.btnLogoBrowse.Click += new System.EventHandler(this.btnBrowse_Click_1);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(29, 173);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 16);
            this.label7.TabIndex = 27;
            this.label7.Text = "Web Site";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(29, 131);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 16);
            this.label6.TabIndex = 26;
            this.label6.Text = "E-Mail";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 16);
            this.label5.TabIndex = 25;
            this.label5.Text = "Contact Number";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 16);
            this.label1.TabIndex = 21;
            this.label1.Text = "Hotel Name";
            // 
            // fdlgImage
            // 
            this.fdlgImage.FileName = "openFileDialog1";
            // 
            // fdlgRead
            // 
            this.fdlgRead.FileName = "openFileDialog1";
            // 
            // lblWarning
            // 
            this.lblWarning.AutoSize = true;
            this.lblWarning.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.lblWarning.ForeColor = System.Drawing.Color.DarkRed;
            this.lblWarning.Location = new System.Drawing.Point(9, 530);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(136, 16);
            this.lblWarning.TabIndex = 21;
            this.lblWarning.Text = "*Some values are invalid";
            this.lblWarning.Visible = false;
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Orange;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Tai Le", 10F);
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCancel.Location = new System.Drawing.Point(551, 523);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(120, 32);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // HotelDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(717, 566);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblWarning);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnSkip);
            this.Controls.Add(this.btnApply);
            this.Font = new System.Drawing.Font("Microsoft Tai Le", 12F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "HotelDetails";
            // 
            // 
            // 
            this.RootElement.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.RootElement.AngleTransform = 0F;
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.FlipText = false;
            this.RootElement.Margin = new System.Windows.Forms.Padding(0);
            this.RootElement.Padding = new System.Windows.Forms.Padding(0);
            this.RootElement.Text = null;
            this.RootElement.TextOrientation = System.Windows.Forms.Orientation.Horizontal;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HotelDetails";
            this.ThemeName = "Office2007Black";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.HotelDetails_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxReports)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnSkip;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtContact;
        private System.Windows.Forms.TextBox txtHotelName;
        private System.Windows.Forms.Button btnLogoBrowse;
        private System.Windows.Forms.PictureBox picBoxLogo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.OpenFileDialog fdlgImage;
        private System.Windows.Forms.TextBox txtWeb;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.OpenFileDialog fdlgRead;
        private System.Windows.Forms.Label lblWarning;
        private System.Windows.Forms.Label lblWeb;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblContact;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnMainBrowse;
        private System.Windows.Forms.TextBox txtDolorRate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnCancel;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007Black;
        private Telerik.WinControls.Themes.VistaTheme vista;
        private Telerik.WinControls.Themes.Office2010Theme office2010;
        private Telerik.WinControls.Themes.TelerikTheme telerik;
        private Telerik.WinControls.Themes.AquaTheme aqua;
        private Telerik.WinControls.Themes.DesertTheme desert;
        private Telerik.WinControls.Themes.BreezeTheme breeze;
        private Telerik.WinControls.Themes.Office2007SilverTheme office2007SilverTheme1;
        private Telerik.WinControls.Themes.Windows7Theme windows7Theme1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox picBoxReports;
    }
}

