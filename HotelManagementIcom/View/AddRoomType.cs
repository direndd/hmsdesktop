using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using HotelManagementIcom.DataAccess;
using HotelManagementIcom.Controller;


namespace HotelManagementIcom.View
{
    public partial class AddRoomType : Telerik.WinControls.UI.RadForm
    {
        DataController data = new DataController();
        string imageUrlManageRoomTypes;

        public AddRoomType()
        {
            InitializeComponent();
        }

        public void clear()
        {
            try
            {
                lblRoomType.Visible = false;
                lblAdults.Visible = false;
                lblChildren.Visible = false;
                lblFullPrice.Visible = false;
                lblHalfPrice.Visible = false;
                
                cmbRoomType.Text = string.Empty;
                txtNoOfAdults.Text = string.Empty;
                txtNoOfChildren.Text = string.Empty;
                txtFullBoardPrice.Text = string.Empty;
                txtHalfBoardPrice.Text = string.Empty;
                picImage.Image = null;
            }
            catch (Exception ex1)
            {
                var msg = ex1.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void AddRoomType_Load(object sender, EventArgs e)
        {
            SettingsControl set = new SettingsControl();
            this.ThemeName = set.getTheme();
            this.BackColor = System.Drawing.Color.FromName(set.getColor());

            try
            {

            lblRoomType.Visible = false;
            lblAdults.Visible = false;
            lblChildren.Visible = false;
            lblFullPrice.Visible = false;
            lblHalfPrice.Visible = false;

                //Filling Room Type Combo box
                data.fillRoomType(this.cmbRoomType);
                //load data for gridview
                data.roomTypeTableFilll(this.dgvRoomType);
            }
            catch (Exception ex3)
            {
                var msg = ex3.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
             
        }

        private void btnImage_Click(object sender, EventArgs e)
        {
            try
            {

                string Chosen_File = "";

                openFileDialog1.Title = "Insert an Image";
                openFileDialog1.FileName = "";
                openFileDialog1.Filter = "JPEG Images|*.jpg|GIF Images|*.gif|PNG Images|*.png";

                if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                {
                    MessageBox.Show("Operation Failed");
                }
                else
                {
                    Chosen_File = openFileDialog1.FileName;
                    imageUrlManageRoomTypes = openFileDialog1.FileName;
                    //MessageBox.Show(Chosen_File);
                    this.picImage.Image = Image.FromFile(Chosen_File);
                }
            }
            catch (Exception ex5)
            {
                var msg = ex5.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvRoomType_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int cell = e.RowIndex;
                cmbRoomType.Text = dgvRoomType.Rows[cell].Cells[0].Value.ToString();
                txtNoOfAdults.Text = dgvRoomType.Rows[cell].Cells[1].Value.ToString();
                txtNoOfChildren.Text = dgvRoomType.Rows[cell].Cells[2].Value.ToString();
                txtFullBoardPrice.Text = dgvRoomType.Rows[cell].Cells[3].Value.ToString();
                txtHalfBoardPrice.Text = dgvRoomType.Rows[cell].Cells[4].Value.ToString();
                this.picImage.Image = data.getPicture(this.cmbRoomType.Text.Trim());
            }
            catch (Exception ex6)
            {
                var msg = ex6.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void cmbRoomType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String roomtype = cmbRoomType.SelectedItem.ToString();
                List<roomType> type = data.searchRoomType(roomtype);

                roomType rmt = (roomType)type[0];
                cmbRoomType.Text = rmt.typeName;
                txtNoOfAdults.Text = rmt.noOfAdults.ToString();
                txtNoOfChildren.Text = rmt.noOfChildren.ToString();
                txtFullBoardPrice.Text = rmt.price.ToString();
                txtHalfBoardPrice.Text = rmt.priceHalf.ToString();
                this.picImage.Image = data.getPicture(this.cmbRoomType.Text.Trim());
            }
            catch (Exception ex7)
            {
                var msg = ex7.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {

                if (this.picImage.Image == null) //not given an image
                {
                    MessageBox.Show("Please browse and select an image", "Required Field", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (this.cmbRoomType.Text == "")
                {
                    MessageBox.Show("Room Type must be provided");
                    return;
                }
                if (this.txtNoOfAdults.Text == "")
                {
                    MessageBox.Show("No Of Adults must be provided");
                    return;
                }
                if (this.txtNoOfChildren.Text == "")
                {
                    MessageBox.Show("No Of Children must be provided");
                    return;
                }
                if (this.txtFullBoardPrice.Text == "")
                {
                    MessageBox.Show("Full Board Price must be provided");
                    return;
                }
                if (this.txtHalfBoardPrice.Text == "")
                {
                    MessageBox.Show("Half Board Price must be provided");
                    return;
                }

                DialogResult result = MessageBox.Show("Are These Information Correct ?", "Confirm Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    String typeName = this.cmbRoomType.Text.Trim();
                    int noOfAdults = Convert.ToInt32(this.txtNoOfAdults.Text.Trim());
                    int noOfChildren = Convert.ToInt32(this.txtNoOfChildren.Text.Trim());
                    double priceHalf = Convert.ToDouble(this.txtHalfBoardPrice.Text.Trim());
                    double price = Convert.ToDouble(this.txtFullBoardPrice.Text.Trim());
                    byte[] picture = data.imageToByteArray(this.picImage.Image);

                    data.addNewRoomType(typeName, noOfAdults, noOfChildren, price, priceHalf, picture);

                    clear();
                    //MessageBox.Show("Room Type " + typeName + " Added", " Insertion Successful ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("User Canceled The Action", " Insertion Cancelled ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                clear();
                data.roomTypeTableFilll(this.dgvRoomType);
                data.fillRoomType(this.cmbRoomType);
                

            }
            catch (Exception ex8)
            {
                var msg = ex8.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            
                clear();
            
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                String typeName = this.cmbRoomType.Text.Trim();

                DialogResult result = MessageBox.Show("Are you sure ?", "Confirm Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    data.deleteRoomType(typeName);
                    //MessageBox.Show("Room Type " + typeName + " Deleted Successfully", " Deletion Successful ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("User Canceled The Action", " Deletion Cancelled ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                clear();
                data.roomTypeTableFilll(this.dgvRoomType);
                data.fillRoomType(this.cmbRoomType);

            }
            catch (Exception ex9)
            {
                var msg = ex9.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.cmbRoomType.Text == "")
                {
                    MessageBox.Show("Room Type must be provided");
                    return;
                }

                DialogResult result = MessageBox.Show("Are These Information Correct ?", "Confirm Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    String typeName = this.cmbRoomType.Text.Trim();
                    int noOfAdults = Convert.ToInt32(this.txtNoOfAdults.Text.Trim());
                    int noOfChildren = Convert.ToInt32(this.txtNoOfChildren.Text.Trim());
                    double priceHalf = Convert.ToDouble(this.txtHalfBoardPrice.Text.Trim());
                    double price = Convert.ToDouble(this.txtFullBoardPrice.Text.Trim());
                    byte[] picture = data.imageToByteArray(this.picImage.Image);

                    data.updateRoomType(typeName, noOfAdults, noOfChildren, price, priceHalf, picture);
                    //MessageBox.Show("Room Type " + typeName + " Updated", " Updating Successful ", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                else
                {
                    MessageBox.Show("User Canceled The Action", " Updating Cancelled ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                clear();
                data.roomTypeTableFilll(this.dgvRoomType);
                data.fillRoomType(this.cmbRoomType);
            }
            catch (Exception ex10)
            {
                var msg = ex10.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void txtNoOfAdults_KeyPress(object sender, KeyPressEventArgs e)
        {

            //Textbox only accepting numbers
            if (char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
                lblAdults.Visible = false;

            }
            else
            {
                e.Handled = true;
                lblAdults.Visible = true;
            } 
        }

        private void cmbRoomType_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Textbox only accepting numbers and letters
            if ((char.IsLetter(e.KeyChar) || char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar)))
            {
                e.Handled = false;
                lblRoomType.Visible = false;

            }

            else
            {
                e.Handled = true;
                lblRoomType.Visible = true;
            } 
        }

        private void txtNoOfChildren_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Textbox only accepting numbers
            if (char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
                lblChildren.Visible = false;

            }
            else
            {
                e.Handled = true;
                lblChildren.Visible = true;
            } 
        }

        private void txtFullBoardPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Textbox only accepting numbers
            if (char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
                lblFullPrice.Visible = false;

            }
            else
            {
                e.Handled = true;
                lblFullPrice.Visible = true;
            } 
        }

        private void txtHalfBoardPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Textbox only accepting numbers
            if (char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
                lblHalfPrice.Visible = false;

            }
            else
            {
                e.Handled = true;
                lblHalfPrice.Visible = true;
            } 
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            ManageRooms rm = new ManageRooms();
            rm.Show();
            this.Hide();
        }


    }
}
