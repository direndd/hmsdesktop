using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using HotelManagementIcom.Model;
using HotelManagementIcom.Controller;
using System.Linq;
using System.Collections;
namespace HotelManagementIcom.View
{
    public partial class Payments : Telerik.WinControls.UI.RadForm
    {  
        string invoiceIDChk;        
        string PaymentIDChk;        
        string custIDChk;
        double amountPrevious;

        payment pay = new payment();

        public Payments()
        {
            InitializeComponent();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = 1000;
            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            string[] Time = DateTime.Now.ToString().Split(' ');
            lblTime.Text = Time[1] + " " + Time[2];
        }

        private void btnPay_Click(object sender, EventArgs e)
        {
            hotelmsEntities1 hms = new hotelmsEntities1();

          
            //customer
            customer cus = new customer();
            string custId;
            int custMaxID = 0;
            int custIdSubString = 0;

            //invoice
            invoice invo = new invoice();
            string InvId;
            int InvMaxID = 0;
            int InvIdSubString = 0;

            //payment
            payment pay = new payment();
            string payId;
            int payMaxID = 0;
            int payIdSubString = 0;
           


            //get the max Customer Id
            var custID = hms.customers.Select(i => new { i.cusId });
            foreach (var item in custID)
            {
                if (item.cusId != string.Empty)
                {                 
                    custId = item.cusId.Substring(3);                
                    custIdSubString = Int16.Parse(custId);                
                    if (custMaxID < custIdSubString)               
                    {                    
                        custMaxID = custIdSubString;                
                    }
                }
                else
                {
                    custMaxID = 0;
                }
               
            }
            custMaxID++;
            custId = "cus" + custMaxID.ToString();
            custIDChk = custId;

            //get the max Invoice Id
            var invID = hms.invoices.Select(i => new { i.invoiceId });
            foreach (var item in invID)
            {
                if (item.invoiceId != string.Empty)
                { 
                    InvId = item.invoiceId.Substring(3);               
                    InvIdSubString = Int16.Parse(InvId);                
                    if (InvMaxID < InvIdSubString)                
                    {                    
                        InvMaxID = InvIdSubString;                
                    }
                }
               
                else
                {
                    InvMaxID = 0;
                }
            }
            InvMaxID++;
            InvId = "inv" + InvMaxID.ToString();
            invoiceIDChk = InvId;

            //get the max payment Id
            var payID = hms.payments.Select(i => new { i.paymentId });
            foreach (var item in payID)
            {
                if (item.paymentId != string.Empty)
                {   
                    payId = item.paymentId.Substring(3);
                    payIdSubString = Int16.Parse(payId);
                    if (payMaxID < payIdSubString)
                    {
                        payMaxID = payIdSubString;
                    }
                }

                else
                {
                    payMaxID = 0;
                }
                
            }
            payMaxID++;
            payId = "pay" + payMaxID.ToString();
            PaymentIDChk = payId;

            try
            {
            if (txtBoxCusId.Text != " ")
            {
                
                hotelmsEntities1 hms4 = new hotelmsEntities1();

                invoice invoBalance = hms4.invoices.Where(i => i.invoiceId == cmbInvoiceID.Text.Trim()).First();
                if (invoBalance.balance == 0)
                {
                    MessageBox.Show("You have already payed the invoice full amount ", "Unsuccessfull");
                }
                else
                {
                    hotelmsEntities1 hms2 = new hotelmsEntities1();
                    //making the payments
                    payment pay4 = new payment();

                    pay4.invoiceId = cmbInvoiceID.Text.Trim();
                    pay4.paymentId = PaymentIDChk.Trim();
                    pay4.paymentDate = datePickerDate.Value;
                    pay4.paymentType = cmbPaymentType.Text.ToString().Trim();
                    pay4.Amount = Convert.ToDouble(txtBoxAmount.Text);
                    pay4.userId = new LoggedUser().LogUser;
                    pay4.cusId = txtBoxCusId.Text.Trim();

                    hms2.AddTopayments(pay4);
                    hms2.SaveChanges();

                    PaymentTable();



                    invoice inv = hms2.invoices.Where(iii => iii.invoiceId == cmbInvoiceID.Text.Trim()).Single();
                    inv.balance = inv.balance - (Convert.ToDouble(txtBoxAmount.Text));
                    hms2.SaveChanges();
                    MessageBox.Show("payed successfully", "Success");

                
                    clear();
                }              
               
            } 
        }
                
        
        catch (Exception ee) 
    {                  
        MessageBox.Show(ee.Message, "Error");                  
    }

        
        }
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Payments_Load(object sender, EventArgs e)
        {
            SettingsControl set = new SettingsControl();
            this.ThemeName = set.getTheme();
            this.BackColor = System.Drawing.Color.FromName(set.getColor());

            PaymentTable();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //invoice grid
        void InvoiceTable()
        {
            hotelmsEntities1 hms = new hotelmsEntities1();
               int countRows = gridPayments.Rows.Count - 1;
            for (int i = 0; i < countRows; i++)
            {
                gridPayments.Rows.RemoveAt(0);
            }
            for (int i = 0; i < gridPayments.ColumnCount; i++)
            {
                gridPayments.Columns.RemoveAt(0);
            }
            
            gridPayments.AutoGenerateColumns = true;
            gridPayments.DataSource = hms.invoices.Select(i => new { Invoice_Id = i.invoiceId, Hndle_By = i.user.userName, Customer_Id = i.cusId, Customer_Nic = i.customer.nic, i.payedDate, i.total, i.balance });
        }
        //payment grid
        void PaymentTable()
        {
            hotelmsEntities1 hms = new hotelmsEntities1();
            int countRows = gridPayments.Rows.Count - 1;
            for (int i = 0; i < countRows; i++)
            {
                gridPayments.Rows.RemoveAt(0);
            }
            for (int i = 0; i < gridPayments.ColumnCount; i++)
            {
                gridPayments.Columns.RemoveAt(0);
            }
            gridPayments.AutoGenerateColumns = true;
            gridPayments.DataSource = hms.payments.Select(i => new { Payement_Id = i.paymentId, Handle_By = i.user.userName, Customer_Id = i.cusId,Customer_Nic = i.customer.nic, Invoice_Id = i.invoiceId, i.paymentDate, i.Amount, i.paymentType });
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnInvoice.Checked)
            {
                InvoiceTable();
                grpBoxInvoice.Enabled = true;
            }
            else {
                PaymentTable();
                grpBoxPaymnts.Enabled = true;
            }
        }

        private void txtBoxNic_TextChanged(object sender, EventArgs e)
        {
            hotelmsEntities1 hms = new hotelmsEntities1();
            var list = from cust in hms.customers where cust.nic == txtBoxNic.Text select cust;
            StringBuilder fname = new StringBuilder();
            StringBuilder lname = new StringBuilder();
            StringBuilder mname = new StringBuilder();
            StringBuilder nic = new StringBuilder();
            StringBuilder passport = new StringBuilder();


            foreach (customer s in list)
            {                
                fname.Append(s.fName + Environment.NewLine);
                mname.Append(s.mName + Environment.NewLine);
                lname.Append(s.lName + Environment.NewLine);                              
                passport.Append(s.passport + Environment.NewLine);
                nic.Append(s.cusId + Environment.NewLine);             

            }
           
            //setting details 
            txtFname.Text = fname.ToString();
            txtMname.Text = mname.ToString();
            txtLname.Text = lname.ToString();
            txtBoxCusId.Text = nic.ToString();
            txtPass.Text = passport.ToString();
            // txtBoxFacilityId.Text = sbRoomId.ToString();

            string customernic = txtBoxCusId.Text.ToString().Trim();
            IEnumerable<invoice> invoiceID = hms.invoices.Where(i => i.cusId == customernic);
            try
            {
                cmbInvoiceID.Items.Clear();
                //filling the invoices for the ustomers

                foreach (invoice fac in invoiceID)
                {
                  
                    cmbInvoiceID.Items.Add(fac);
                    cmbInvoiceID.DisplayMember = "invoiceId";
                    cmbInvoiceID.ValueMember = "invoiceId";

                }
            }
            catch (Exception)
            {
                
                
            } 
         
           
        }

        private void txtInvoiceID_TextChanged(object sender, EventArgs e)
        {
          
      
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void txtBoxCusId_TextChanged(object sender, EventArgs e)
        {
        }

        private void cmbInvoiceID_TextChanged(object sender, EventArgs e)
        {
            txtTotAmonut.Text ="";
            txtBalance.Text = "";
            txtInvoiceDate.Text = "";

            try
            {
                hotelmsEntities1 hms = new hotelmsEntities1();
                var list = from invo in hms.invoices where invo.invoiceId == cmbInvoiceID.Text select invo;
                StringBuilder nic = new StringBuilder();
                // StringBuilder payId = new StringBuilder();
                StringBuilder datePayed = new StringBuilder();
                StringBuilder Totamount = new StringBuilder();
                StringBuilder balance = new StringBuilder();
                StringBuilder fname = new StringBuilder();
                StringBuilder lname = new StringBuilder();
                StringBuilder mname = new StringBuilder();
                // StringBuilder nic = new StringBuilder();
                StringBuilder passport = new StringBuilder();
                StringBuilder custId = new StringBuilder();

                foreach (invoice s in list)
                {
                    nic.Append(s.customer.nic + Environment.NewLine);
                    //payId.Append(s.p + Environment.NewLine);
                    datePayed.Append(s.payedDate.ToString() + Environment.NewLine);
                    Totamount.Append(s.total.ToString() + Environment.NewLine);
                    balance.Append(s.balance.ToString() + Environment.NewLine);
                    custId.Append(s.cusId.ToString() + Environment.NewLine);
                    fname.Append(s.customer.fName + Environment.NewLine);
                    mname.Append(s.customer.mName + Environment.NewLine);
                    lname.Append(s.customer.lName + Environment.NewLine);
                    passport.Append(s.customer.passport + Environment.NewLine);
                    nic.Append(s.customer.nic + Environment.NewLine);


                }
                //setting details  
                txtTotAmonut.Text = Totamount.ToString();
                txtBalance.Text = balance.ToString();
                txtInvoiceDate.Text = datePayed.ToString();

            }
            catch (Exception)
            {
                
               
            } 

        }

        private void cmbInvoiceID_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtBoxId_TextChanged(object sender, EventArgs e)
        {
            try
            {

                hotelmsEntities1 hms = new hotelmsEntities1();
                var list = from pay in hms.payments where pay.paymentId == txtBoxId.Text select pay;
                StringBuilder nic = new StringBuilder();
                StringBuilder fname = new StringBuilder();
                StringBuilder lname = new StringBuilder();
                StringBuilder mname = new StringBuilder();
                StringBuilder custID = new StringBuilder();
                StringBuilder passport = new StringBuilder();
                StringBuilder invoiceId = new StringBuilder();
                StringBuilder payType = new StringBuilder();
                StringBuilder date = new StringBuilder();
                StringBuilder totamount = new StringBuilder();
                StringBuilder amount = new StringBuilder();
                StringBuilder balance = new StringBuilder();
                try
                {
                    amountPrevious = Convert.ToDouble(txtBoxAmount.Text);
                }
                catch (Exception)
                {


                }

                // StringBuilder gender = new StringBuilder();

                foreach (payment s in list)
                {
                    nic.Append(s.customer.nic + Environment.NewLine);
                    fname.Append(s.customer.fName + Environment.NewLine);
                    mname.Append(s.customer.mName + Environment.NewLine);
                    lname.Append(s.customer.lName + Environment.NewLine);
                    passport.Append(s.customer.passport + Environment.NewLine);
                    custID.Append(s.cusId + Environment.NewLine);
                    invoiceId.Append(s.invoiceId + Environment.NewLine);
                    payType.Append(s.paymentType + Environment.NewLine);
                    date.Append(s.invoice.payedDate + Environment.NewLine);
                    totamount.Append(s.invoice.total + Environment.NewLine);
                    amount.Append(s.Amount + Environment.NewLine);
                    balance.Append(s.invoice.balance + Environment.NewLine);

                }

                //setting details   
                txtBoxNic.Text = nic.ToString();
                txtFname.Text = fname.ToString();
                txtMname.Text = mname.ToString();
                txtLname.Text = lname.ToString();
                txtBoxCusId.Text = custID.ToString();
                cmbInvoiceID.Text = invoiceId.ToString();
                txtPass.Text = passport.ToString();
                cmbPaymentType.Text = payType.ToString();
                txtInvoiceDate.Text = date.ToString();
                txtTotAmonut.Text = totamount.ToString();
                txtBalance.Text = balance.ToString();
                txtBoxAmount.Text = amount.ToString();
            }
            catch (Exception)
            {
                
             
            }
        
        }

        private void btnPayEdit_Click(object sender, EventArgs e)
        {


            if (txtBoxCusId.Text != " ")
            {
                try
                  {
                    //checking for the balance
                hotelmsEntities1 hms4 = new hotelmsEntities1();
                invoice invoBalance = hms4.invoices.Where(i => i.invoiceId == cmbInvoiceID.Text.Trim()).First();
                if (invoBalance.balance == 0)
                {
                    MessageBox.Show("Your payment details arenot matching ", "exeeds the full amount");
                }
                else
                {
                    //handling the payment
                    hotelmsEntities1 hms3 = new hotelmsEntities1();               
                    payment pay4 = hms3.payments.Where(iii => iii.paymentId == txtBoxId.Text.Trim()).Single();
                    pay4.invoiceId = cmbInvoiceID.Text.Trim();                   
                    pay4.paymentDate = datePickerDate.Value;
                    pay4.paymentType = cmbPaymentType.Text.ToString().Trim();
                    pay4.Amount = Convert.ToDouble(txtBoxAmount.Text.ToString());
                    pay4.userId = "usr1";
                    pay4.cusId = txtBoxCusId.Text.Trim();


                    hms3.SaveChanges();
                    PaymentTable();


                    string invID = cmbInvoiceID.Text.Trim();
                    MessageBox.Show(invID.Trim());
                    invoice inv = hms3.invoices.Where(iii => iii.invoiceId == invID).Single();
                    inv.balance = (inv.balance + amountPrevious) - (Convert.ToDouble(txtBoxAmount.Text));
                    hms3.SaveChanges();
                    MessageBox.Show("updated successfully", "Success");
                //realoadin the  grid
                gridPayments.AutoGenerateColumns = true;
                gridPayments.DataSource = hms3.payments.Select(i => new { Payement_Id = i.paymentId, Handle_By = i.user.userName, Customer_Id = i.cusId,Customer_Nic = i.customer.nic, Invoice_Id = i.invoiceId, i.paymentDate, i.Amount, i.paymentType });
                
                
                }                  
                }
                catch (Exception eee)
                {
                    MessageBox.Show(eee.Message, "Error");
                }
            }


            clear();
        }
  
            public void clear(){            
               txtFname.Text = "";
            txtMname.Text = "";
            txtLname.Text ="";
            txtBoxCusId.Text ="";
            txtPass.Text = "";
            txtTotAmonut.Text = "";
            txtBalance.Text =  "";
            txtInvoiceDate.Text = "";
            txtBoxNic.Text = "";
            txtBoxCusId.Text = "";
            txtBoxId.Text = "";
            txtPass.Text = "";
             cmbInvoiceID.Text = "";
             cmbPaymentType.Text = "";
             txtBoxAmount.Text = "";
             txtPass.Text = "";
            }

            private void radButton1_Click(object sender, EventArgs e)
            { 
                hotelmsEntities1 hms7= new hotelmsEntities1();    
                string payId=txtBoxId.Text.ToString().Trim();

                payment pay = hms7.payments.Where(f => f.paymentId == payId).Single();            
                hms7.payments.DeleteObject(pay);            
                MessageBox.Show("Payment is deleted", "successfull");
            }

            private void rbtnInvoice_CheckedChanged(object sender, EventArgs e)
            {

            }

            private void gridPayments_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
            {
                	
                    try 	
                    {	        
		              string id=gridPayments.Rows[e.RowIndex].Cells[0].Value.ToString();
                      if ( id.StartsWith("pay"))                                    
                     {
                         clear();
                         txtBoxId.Text =  id;
                     }
                      if ( id.StartsWith("inv"))
                      {
                          clear();
                          cmbInvoiceID.Text =  id;
                      }                       


                    }	
                    catch (Exception)	
                    {

	
                    }
 
	             
            
               
               
            }

            private void radButton2_Click(object sender, EventArgs e)
            {
                clear();
            }

            private void btnReservation_Click(object sender, EventArgs e)
            {

            }

            private void btnChekOut_Click(object sender, EventArgs e)
            {
                Home h = new Home();
                h.Show();
                this.Dispose();
            }

            private void lblLogout_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
            {
                string logID = new LoggedUser().LogHisId.ToString();
                String[] tokens3 = System.DateTime.Today.ToString().Split(' ');
                String onlyDate = tokens3[1];

                //log history record
                hotelmsEntities1 hmsLog = new hotelmsEntities1();
                logHistory log = hmsLog.logHistories.Where(iii => iii.logHistoryId == logID.Trim()).Single();
                log.logHistoryId = logID.ToString().Trim();
                log.logOutTime = System.DateTime.Now;
                hmsLog.SaveChanges();

                //clearing the detils 
                LoggedUser lu = new LoggedUser();
                lu.UserName1 = "";
                lu.UserLevel1 = "";
                lu.LogHisId = "";
                Login login = new Login();
                login.Show();
                this.Dispose();
            }

            private void btnHome_Click(object sender, EventArgs e)
            {
                Home h = new Home();
                h.Show();
                this.Dispose();
            }

            private void txtBoxAmount_TextChanged(object sender, EventArgs e)
            {
                validateTextDouble(sender, e);
            }

            //validating to double
            private void validateTextDouble(object sender, EventArgs e)
            {
                Exception X = new Exception();

                TextBox T = (TextBox)sender;

                try
                {
                    if (T.Text != "-")
                    {
                        double x = double.Parse(T.Text);

                        if (T.Text.Contains(','))
                            throw X;
                    }
                }
                catch (Exception)
                {
                    try
                    {
                        int CursorIndex = T.SelectionStart - 1;
                        T.Text = T.Text.Remove(CursorIndex, 1);

                        //Align Cursor to same index
                        T.SelectionStart = CursorIndex;
                        T.SelectionLength = 0;
                    }
                    catch (Exception) { }
                }
            }

            private void txtTotAmonut_TextChanged(object sender, EventArgs e)
            {
                validateTextDouble(sender, e);
            }

            private void txtBalance_TextChanged(object sender, EventArgs e)
            {
                validateTextDouble(sender, e);
            }
}  }
