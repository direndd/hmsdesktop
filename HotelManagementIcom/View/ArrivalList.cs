using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using HotelManagementIcom.Reports;
using HotelManagementIcom.DataAccess;
using HotelManagementIcom.Controller;

namespace HotelManagementIcom.View
{
    public partial class ArrivalList : Telerik.WinControls.UI.RadForm
    {
        DataController data = new DataController();

        public ArrivalList()
        {
            InitializeComponent();
        }

        private void rdbToday_CheckedChanged(object sender, EventArgs e)
        {
            dtPickerToday.Enabled = true;

            dtPickerFrom.Enabled = false;
            dtPickerTo.Enabled = false;
        }

        private void rdbFrom_CheckedChanged(object sender, EventArgs e)
        {
            dtPickerToday.Enabled = false;

            dtPickerFrom.Enabled = true;
            dtPickerTo.Enabled = true;

            if (this.rdbFrom.Checked)
            {
                this.dtPickerTo.MinDate = this.dtPickerFrom.Value;
            }
        }

        private void ArrivalList_Load(object sender, EventArgs e)
        {
            SettingsControl set = new SettingsControl();
            this.ThemeName = set.getTheme();
            this.BackColor = System.Drawing.Color.FromName(set.getColor());

            dtPickerToday.Enabled = false;

            dtPickerFrom.Enabled = false;
            dtPickerTo.Enabled = false;
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (this.rdbToday.Checked)
            {
                string startingDate = data.FormatDate(this.dtPickerToday);
                HotelManagementIcom.Reports.ArraivalListReport1 rpt = new ArraivalListReport1(startingDate);
                rpt.Show();
            }
            else if (this.rdbFrom.Checked)
            {
                string startingDate = data.FormatDate(this.dtPickerFrom);
                string endingDate = data.FormatDate(this.dtPickerTo);
                HotelManagementIcom.Reports.ArrivalListReport2 rpt = new ArrivalListReport2(startingDate, endingDate);
                rpt.Show();
            }
        }

    

        private void dtPickerFrom_ValueChanged(object sender, EventArgs e)
        {
            this.dtPickerTo.MinDate = this.dtPickerFrom.Value;
        }

    }
}