namespace HotelManagementIcom.View
{
    partial class Payments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Payments));
            this.gridPayments = new System.Windows.Forms.DataGridView();
            this.grpBoxPaymnts = new System.Windows.Forms.GroupBox();
            this.txtBoxAmount = new System.Windows.Forms.TextBox();
            this.txtBoxId = new System.Windows.Forms.TextBox();
            this.datePickerDate = new System.Windows.Forms.DateTimePicker();
            this.cmbPaymentType = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBoxNic = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtFname = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMname = new System.Windows.Forms.TextBox();
            this.txtLname = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtBoxCusId = new System.Windows.Forms.TextBox();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.rbtnInvoice = new System.Windows.Forms.RadioButton();
            this.rbtnPay = new System.Windows.Forms.RadioButton();
            this.grpBoxInvoice = new System.Windows.Forms.GroupBox();
            this.cmbInvoiceID = new System.Windows.Forms.ComboBox();
            this.txtInvoiceDate = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtTotAmonut = new System.Windows.Forms.TextBox();
            this.txtBalance = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.office2010Theme1 = new Telerik.WinControls.Themes.Office2010Theme();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblLogout = new System.Windows.Forms.LinkLabel();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblUserLevel = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.btnHome = new System.Windows.Forms.Button();
            this.btnPay = new System.Windows.Forms.Button();
            this.btnPayEdit = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.office2007Black = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.vista = new Telerik.WinControls.Themes.VistaTheme();
            this.office2010 = new Telerik.WinControls.Themes.Office2010Theme();
            this.telerik = new Telerik.WinControls.Themes.TelerikTheme();
            this.aqua = new Telerik.WinControls.Themes.AquaTheme();
            this.desert = new Telerik.WinControls.Themes.DesertTheme();
            this.breeze = new Telerik.WinControls.Themes.BreezeTheme();
            this.office2007SilverTheme1 = new Telerik.WinControls.Themes.Office2007SilverTheme();
            this.windows7Theme1 = new Telerik.WinControls.Themes.Windows7Theme();
            ((System.ComponentModel.ISupportInitialize)(this.gridPayments)).BeginInit();
            this.grpBoxPaymnts.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.grpBoxInvoice.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // gridPayments
            // 
            this.gridPayments.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridPayments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridPayments.Location = new System.Drawing.Point(12, 86);
            this.gridPayments.Name = "gridPayments";
            this.gridPayments.Size = new System.Drawing.Size(968, 267);
            this.gridPayments.TabIndex = 0;
            this.gridPayments.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.gridPayments.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gridPayments_RowHeaderMouseClick);
            // 
            // grpBoxPaymnts
            // 
            this.grpBoxPaymnts.BackColor = System.Drawing.Color.Transparent;
            this.grpBoxPaymnts.Controls.Add(this.txtBoxAmount);
            this.grpBoxPaymnts.Controls.Add(this.txtBoxId);
            this.grpBoxPaymnts.Controls.Add(this.datePickerDate);
            this.grpBoxPaymnts.Controls.Add(this.cmbPaymentType);
            this.grpBoxPaymnts.Controls.Add(this.label5);
            this.grpBoxPaymnts.Controls.Add(this.label4);
            this.grpBoxPaymnts.Controls.Add(this.label2);
            this.grpBoxPaymnts.Controls.Add(this.label1);
            this.grpBoxPaymnts.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.grpBoxPaymnts.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.grpBoxPaymnts.Location = new System.Drawing.Point(656, 382);
            this.grpBoxPaymnts.Name = "grpBoxPaymnts";
            this.grpBoxPaymnts.Size = new System.Drawing.Size(315, 245);
            this.grpBoxPaymnts.TabIndex = 1;
            this.grpBoxPaymnts.TabStop = false;
            this.grpBoxPaymnts.Text = "Payments";
            this.grpBoxPaymnts.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // txtBoxAmount
            // 
            this.txtBoxAmount.Location = new System.Drawing.Point(114, 163);
            this.txtBoxAmount.Name = "txtBoxAmount";
            this.txtBoxAmount.Size = new System.Drawing.Size(180, 23);
            this.txtBoxAmount.TabIndex = 14;
            this.txtBoxAmount.TextChanged += new System.EventHandler(this.txtBoxAmount_TextChanged);
            // 
            // txtBoxId
            // 
            this.txtBoxId.Location = new System.Drawing.Point(114, 34);
            this.txtBoxId.Name = "txtBoxId";
            this.txtBoxId.Size = new System.Drawing.Size(180, 23);
            this.txtBoxId.TabIndex = 11;
            this.txtBoxId.TextChanged += new System.EventHandler(this.txtBoxId_TextChanged);
            // 
            // datePickerDate
            // 
            this.datePickerDate.Location = new System.Drawing.Point(114, 76);
            this.datePickerDate.Name = "datePickerDate";
            this.datePickerDate.Size = new System.Drawing.Size(180, 23);
            this.datePickerDate.TabIndex = 12;
            // 
            // cmbPaymentType
            // 
            this.cmbPaymentType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbPaymentType.FormattingEnabled = true;
            this.cmbPaymentType.Items.AddRange(new object[] {
            "Cheque",
            "Cash",
            "Credit",
            "Credit Card"});
            this.cmbPaymentType.Location = new System.Drawing.Point(114, 115);
            this.cmbPaymentType.Name = "cmbPaymentType";
            this.cmbPaymentType.Size = new System.Drawing.Size(180, 24);
            this.cmbPaymentType.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 166);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "Amount";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "Payment Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Payment ID";
            // 
            // txtBoxNic
            // 
            this.txtBoxNic.Location = new System.Drawing.Point(117, 64);
            this.txtBoxNic.Name = "txtBoxNic";
            this.txtBoxNic.Size = new System.Drawing.Size(180, 23);
            this.txtBoxNic.TabIndex = 2;
            this.txtBoxNic.TextChanged += new System.EventHandler(this.txtBoxNic_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.label6.Location = new System.Drawing.Point(21, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 16);
            this.label6.TabIndex = 14;
            this.label6.Text = "Guest NIC";
            // 
            // txtFname
            // 
            this.txtFname.Location = new System.Drawing.Point(118, 157);
            this.txtFname.Name = "txtFname";
            this.txtFname.Size = new System.Drawing.Size(180, 23);
            this.txtFname.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Tai Le", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(8, 127);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 16);
            this.label7.TabIndex = 16;
            this.label7.Text = "Guest Name";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // txtMname
            // 
            this.txtMname.Location = new System.Drawing.Point(117, 192);
            this.txtMname.Name = "txtMname";
            this.txtMname.Size = new System.Drawing.Size(180, 23);
            this.txtMname.TabIndex = 5;
            // 
            // txtLname
            // 
            this.txtLname.Location = new System.Drawing.Point(117, 227);
            this.txtLname.Name = "txtLname";
            this.txtLname.Size = new System.Drawing.Size(180, 23);
            this.txtLname.TabIndex = 6;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtBoxCusId);
            this.groupBox2.Controls.Add(this.txtPass);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtBoxNic);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtLname);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtMname);
            this.groupBox2.Controls.Add(this.txtFname);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox2.Location = new System.Drawing.Point(12, 362);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(315, 265);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Guest Details";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.label13.Location = new System.Drawing.Point(21, 33);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(51, 16);
            this.label13.TabIndex = 66;
            this.label13.Text = "Guest Id";
            // 
            // txtBoxCusId
            // 
            this.txtBoxCusId.Enabled = false;
            this.txtBoxCusId.Location = new System.Drawing.Point(117, 30);
            this.txtBoxCusId.Name = "txtBoxCusId";
            this.txtBoxCusId.Size = new System.Drawing.Size(180, 23);
            this.txtBoxCusId.TabIndex = 1;
            this.txtBoxCusId.TextChanged += new System.EventHandler(this.txtBoxCusId_TextChanged);
            // 
            // txtPass
            // 
            this.txtPass.Location = new System.Drawing.Point(117, 98);
            this.txtPass.Name = "txtPass";
            this.txtPass.Size = new System.Drawing.Size(180, 23);
            this.txtPass.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.label3.Location = new System.Drawing.Point(21, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 16);
            this.label3.TabIndex = 23;
            this.label3.Text = "Passport No";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(22, 152);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 16);
            this.label10.TabIndex = 22;
            this.label10.Text = "First Name";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(22, 192);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 16);
            this.label9.TabIndex = 21;
            this.label9.Text = "Middle Name";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(22, 232);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 16);
            this.label8.TabIndex = 20;
            this.label8.Text = "Last Name";
            // 
            // rbtnInvoice
            // 
            this.rbtnInvoice.AutoSize = true;
            this.rbtnInvoice.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.rbtnInvoice.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rbtnInvoice.Location = new System.Drawing.Point(364, 362);
            this.rbtnInvoice.Name = "rbtnInvoice";
            this.rbtnInvoice.Size = new System.Drawing.Size(102, 20);
            this.rbtnInvoice.TabIndex = 21;
            this.rbtnInvoice.TabStop = true;
            this.rbtnInvoice.Text = "Invoice Details";
            this.rbtnInvoice.UseVisualStyleBackColor = true;
            this.rbtnInvoice.CheckedChanged += new System.EventHandler(this.rbtnInvoice_CheckedChanged);
            // 
            // rbtnPay
            // 
            this.rbtnPay.AutoSize = true;
            this.rbtnPay.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.rbtnPay.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rbtnPay.Location = new System.Drawing.Point(668, 362);
            this.rbtnPay.Name = "rbtnPay";
            this.rbtnPay.Size = new System.Drawing.Size(111, 20);
            this.rbtnPay.TabIndex = 22;
            this.rbtnPay.TabStop = true;
            this.rbtnPay.Text = "Payment Details";
            this.rbtnPay.UseVisualStyleBackColor = true;
            this.rbtnPay.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // grpBoxInvoice
            // 
            this.grpBoxInvoice.BackColor = System.Drawing.Color.Transparent;
            this.grpBoxInvoice.Controls.Add(this.cmbInvoiceID);
            this.grpBoxInvoice.Controls.Add(this.txtInvoiceDate);
            this.grpBoxInvoice.Controls.Add(this.label11);
            this.grpBoxInvoice.Controls.Add(this.txtTotAmonut);
            this.grpBoxInvoice.Controls.Add(this.txtBalance);
            this.grpBoxInvoice.Controls.Add(this.label12);
            this.grpBoxInvoice.Controls.Add(this.label14);
            this.grpBoxInvoice.Controls.Add(this.label15);
            this.grpBoxInvoice.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.grpBoxInvoice.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.grpBoxInvoice.Location = new System.Drawing.Point(334, 382);
            this.grpBoxInvoice.Name = "grpBoxInvoice";
            this.grpBoxInvoice.Size = new System.Drawing.Size(315, 245);
            this.grpBoxInvoice.TabIndex = 23;
            this.grpBoxInvoice.TabStop = false;
            this.grpBoxInvoice.Text = "Invoice Details";
            // 
            // cmbInvoiceID
            // 
            this.cmbInvoiceID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbInvoiceID.FormattingEnabled = true;
            this.cmbInvoiceID.Location = new System.Drawing.Point(111, 33);
            this.cmbInvoiceID.Name = "cmbInvoiceID";
            this.cmbInvoiceID.Size = new System.Drawing.Size(180, 24);
            this.cmbInvoiceID.TabIndex = 7;
            this.cmbInvoiceID.SelectedIndexChanged += new System.EventHandler(this.cmbInvoiceID_SelectedIndexChanged);
            this.cmbInvoiceID.TextChanged += new System.EventHandler(this.cmbInvoiceID_TextChanged);
            // 
            // txtInvoiceDate
            // 
            this.txtInvoiceDate.Location = new System.Drawing.Point(111, 76);
            this.txtInvoiceDate.Name = "txtInvoiceDate";
            this.txtInvoiceDate.Size = new System.Drawing.Size(180, 23);
            this.txtInvoiceDate.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(27, 37);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 16);
            this.label11.TabIndex = 12;
            this.label11.Text = "Invoice ID";
            // 
            // txtTotAmonut
            // 
            this.txtTotAmonut.Location = new System.Drawing.Point(111, 163);
            this.txtTotAmonut.Name = "txtTotAmonut";
            this.txtTotAmonut.Size = new System.Drawing.Size(180, 23);
            this.txtTotAmonut.TabIndex = 10;
            this.txtTotAmonut.TextChanged += new System.EventHandler(this.txtTotAmonut_TextChanged);
            // 
            // txtBalance
            // 
            this.txtBalance.Location = new System.Drawing.Point(111, 116);
            this.txtBalance.Name = "txtBalance";
            this.txtBalance.Size = new System.Drawing.Size(180, 23);
            this.txtBalance.TabIndex = 9;
            this.txtBalance.TextChanged += new System.EventHandler(this.txtBalance_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(27, 166);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 16);
            this.label12.TabIndex = 6;
            this.label12.Text = "Amount";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(27, 81);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(32, 16);
            this.label14.TabIndex = 3;
            this.label14.Text = "Date";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(27, 119);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 16);
            this.label15.TabIndex = 2;
            this.label15.Text = "Balance";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblLogout);
            this.panel1.Controls.Add(this.lblTime);
            this.panel1.Controls.Add(this.lblUserLevel);
            this.panel1.Controls.Add(this.lblUserName);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.btnHome);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(992, 80);
            this.panel1.TabIndex = 100;
            // 
            // lblLogout
            // 
            this.lblLogout.AutoSize = true;
            this.lblLogout.Font = new System.Drawing.Font("Microsoft Tai Le", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogout.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblLogout.Location = new System.Drawing.Point(898, 38);
            this.lblLogout.Name = "lblLogout";
            this.lblLogout.Size = new System.Drawing.Size(81, 29);
            this.lblLogout.TabIndex = 101;
            this.lblLogout.TabStop = true;
            this.lblLogout.Text = "Logout";
            this.lblLogout.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblLogout_LinkClicked);
            // 
            // lblTime
            // 
            this.lblTime.BackColor = System.Drawing.Color.Transparent;
            this.lblTime.Font = new System.Drawing.Font("Microsoft JhengHei", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTime.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTime.Location = new System.Drawing.Point(557, 2);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(334, 46);
            this.lblTime.TabIndex = 52;
            this.lblTime.Text = "Time";
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUserLevel
            // 
            this.lblUserLevel.AutoSize = true;
            this.lblUserLevel.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserLevel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblUserLevel.Location = new System.Drawing.Point(560, 48);
            this.lblUserLevel.Name = "lblUserLevel";
            this.lblUserLevel.Size = new System.Drawing.Size(76, 19);
            this.lblUserLevel.TabIndex = 48;
            this.lblUserLevel.Text = "User Level";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblUserName.Location = new System.Drawing.Point(663, 48);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(82, 19);
            this.lblUserName.TabIndex = 49;
            this.lblUserName.Text = "User Name";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Tai Le", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label16.Location = new System.Drawing.Point(112, 48);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(417, 18);
            this.label16.TabIndex = 34;
            this.label16.Text = "Backup and Restore Database , Select a Skin and Background Color";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Tai Le", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label17.Location = new System.Drawing.Point(110, 18);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(146, 29);
            this.label17.TabIndex = 33;
            this.label17.Text = "Payments Tile";
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.Color.LimeGreen;
            this.btnHome.FlatAppearance.BorderSize = 0;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnHome.Font = new System.Drawing.Font("Microsoft Tai Le", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHome.Location = new System.Drawing.Point(4, 3);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(100, 68);
            this.btnHome.TabIndex = 0;
            this.btnHome.Text = "Home";
            this.btnHome.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // btnPay
            // 
            this.btnPay.BackColor = System.Drawing.Color.Green;
            this.btnPay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPay.Font = new System.Drawing.Font("Microsoft Tai Le", 10F);
            this.btnPay.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnPay.Location = new System.Drawing.Point(473, 642);
            this.btnPay.Name = "btnPay";
            this.btnPay.Size = new System.Drawing.Size(120, 32);
            this.btnPay.TabIndex = 15;
            this.btnPay.Text = "Pay";
            this.btnPay.UseVisualStyleBackColor = false;
            this.btnPay.Click += new System.EventHandler(this.btnPay_Click);
            // 
            // btnPayEdit
            // 
            this.btnPayEdit.BackColor = System.Drawing.Color.MidnightBlue;
            this.btnPayEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPayEdit.Font = new System.Drawing.Font("Microsoft Tai Le", 10F);
            this.btnPayEdit.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnPayEdit.Location = new System.Drawing.Point(599, 642);
            this.btnPayEdit.Name = "btnPayEdit";
            this.btnPayEdit.Size = new System.Drawing.Size(120, 32);
            this.btnPayEdit.TabIndex = 16;
            this.btnPayEdit.Text = "Edit Payment";
            this.btnPayEdit.UseVisualStyleBackColor = false;
            this.btnPayEdit.Click += new System.EventHandler(this.btnPayEdit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Tai Le", 10F);
            this.btnDelete.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnDelete.Location = new System.Drawing.Point(725, 642);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(120, 32);
            this.btnDelete.TabIndex = 17;
            this.btnDelete.Text = "Delete Payments";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.Orange;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Tai Le", 10F);
            this.btnClear.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnClear.Location = new System.Drawing.Point(851, 642);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(120, 32);
            this.btnClear.TabIndex = 18;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.radButton2_Click);
            // 
            // Payments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(992, 691);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnPayEdit);
            this.Controls.Add(this.btnPay);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.grpBoxInvoice);
            this.Controls.Add(this.rbtnPay);
            this.Controls.Add(this.rbtnInvoice);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.grpBoxPaymnts);
            this.Controls.Add(this.gridPayments);
            this.Font = new System.Drawing.Font("Microsoft Tai Le", 12F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "Payments";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Payments";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.Payments_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridPayments)).EndInit();
            this.grpBoxPaymnts.ResumeLayout(false);
            this.grpBoxPaymnts.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.grpBoxInvoice.ResumeLayout(false);
            this.grpBoxInvoice.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView gridPayments;
        private System.Windows.Forms.GroupBox grpBoxPaymnts;
        private System.Windows.Forms.ComboBox cmbPaymentType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBoxAmount;
        private System.Windows.Forms.TextBox txtBoxId;
        private System.Windows.Forms.DateTimePicker datePickerDate;
        private System.Windows.Forms.TextBox txtBoxNic;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtFname;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtMname;
        private System.Windows.Forms.TextBox txtLname;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RadioButton rbtnInvoice;
        private System.Windows.Forms.RadioButton rbtnPay;
        private System.Windows.Forms.GroupBox grpBoxInvoice;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtTotAmonut;
        private System.Windows.Forms.TextBox txtBalance;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtInvoiceDate;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtBoxCusId;
        private System.Windows.Forms.ComboBox cmbInvoiceID;
        private Telerik.WinControls.Themes.Office2010Theme office2010Theme1;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblUserLevel;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Button btnPay;
        private System.Windows.Forms.Button btnPayEdit;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.LinkLabel lblLogout;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007Black;
        private Telerik.WinControls.Themes.VistaTheme vista;
        private Telerik.WinControls.Themes.Office2010Theme office2010;
        private Telerik.WinControls.Themes.TelerikTheme telerik;
        private Telerik.WinControls.Themes.AquaTheme aqua;
        private Telerik.WinControls.Themes.DesertTheme desert;
        private Telerik.WinControls.Themes.BreezeTheme breeze;
        private Telerik.WinControls.Themes.Office2007SilverTheme office2007SilverTheme1;
        private Telerik.WinControls.Themes.Windows7Theme windows7Theme1;
    }
}

