using System.Windows.Forms;
using System.IO;
using System;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Linq;
using System.Windows;
using System.Drawing;
using System.Windows.Forms;
using System.Management;
using Telerik.WinControls;
using System.Xml;
using HotelManagementIcom.View;
using Microsoft.SqlServer.Server;
using System.Text.RegularExpressions;
using HotelManagementIcom.Model;


using System.Net;
using HotelManagementIcom.Controller;

namespace HotelManagementIcom.View
{
    public partial class Settings : Telerik.WinControls.UI.RadForm
    {
      //  private static Server srvSql;
      
        private Stream requestStream = null;

        SettingsControl set = new SettingsControl();

        hotelmsEntities1 hms = new hotelmsEntities1();
        Server srv;
        ServerConnection conn;

        static string startPath = System.IO.Directory.GetCurrentDirectory();
        static string[] lines = Regex.Split(startPath, "bin");
        public static string xmlPath = lines[0] + @"settings.xml"; // The Path to the settings.Xml file //
        public static string ftpPath = lines[0] + @"ftpDet.xml";  // The Path to the ftpDet.Xml file //
        public static string logPath = lines[0] + @"backuplog.xml"; // The Path to the backuplog.Xml file //
        public static string autoBackupPath = lines[0] + @"autoBackupPath.xml"; // The Path to the backuplog.Xml file //

        static string datetime;
        static string selectedRow;
        
        public Settings()
        {
             InitializeComponent();
             conn = new ServerConnection(@"DDART-MAINFRAME\SQLEXPRESS", "sa", "123");
             srv = new Server(conn);
             cmbThemeSelect.SelectedItem = breeze;

             timer.Tick += new EventHandler(timer_Tick);
             timer.Interval = 1000;
             timer.Start();

             lblUserName.Text = new LoggedUser().UserName1;
             // lblUserName.Text = LoggedUser.UserName;
             lblUserLevel.Text = new LoggedUser().UserLevel1;
             
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            try
            {
                gridLoad();
                
                this.ThemeName = set.getTheme();
                this.BackColor = System.Drawing.Color.FromName(set.getColor());

                //panel1.BackColor = System.Drawing.Color.FromName(colorName);
                //groupBox1.BackColor = System.Drawing.Color.FromName(colorName);
                //groupBox2.BackColor = System.Drawing.Color.FromName(colorName);
                //groupBox3.BackColor = System.Drawing.Color.FromName(colorName);


                FileStream READER = new FileStream(autoBackupPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //Set up the filestream (READER) //
                System.Xml.XmlDocument settings = new System.Xml.XmlDocument();// Set up the XmlDocument //
                settings.Load(READER); //Load the data from the file into the XmlDocument  //
                System.Xml.XmlNodeList NodeList = settings.GetElementsByTagName("autoBackupDet"); // Create a list of the nodes in the xml file //

                cmbMonth.Text = NodeList[0].FirstChild.ChildNodes[0].InnerText.ToString();
                cmbTime.Text = NodeList[0].FirstChild.ChildNodes[1].InnerText.ToString();

                setTheme();
            }
            catch (Exception eq)
            {
                MessageBox.Show(eq.Message);
            }
        }

        [STAThread]
        private void gridLoad()
        {
            if (File.Exists(logPath))
            {
                DataSet XMLDataSet;
                string FilePath = logPath;
                XMLDataSet = new DataSet();

                //Read the contents of the XML file into the DataSet
                XMLDataSet.ReadXml(FilePath);
                grdBackup.DataSource = XMLDataSet.Tables[0].DefaultView;
                grdBackup.ForeColor = System.Drawing.Color.Black;
            }
        }
        private void timer_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToString();
            
        }
        private void backup_Click(object sender, EventArgs e)
        {
            backup();
        }

        [STAThread]
        public void backup()
        {
            try
            {
               datetime = DateTime.Now.ToString().Replace("/", "_").Replace(":", ".");               
               SaveFileDialog dlg = new SaveFileDialog();
               
               dlg.FileName = datetime; // Default file name
               dlg.DefaultExt = ".bak"; // Default file extension
        
               dlg.Filter = "Backup Files (.bak)|*.bak"; // Filter files by extension

               DialogResult ftpConfirmResult=MessageBox.Show("Where do you want to save your backup file ? Select 'Yes' to save it in a Remote Server or Select 'No' to save it in this machine","Backup file location",
                   MessageBoxButtons.YesNoCancel,
                   MessageBoxIcon.Question,
		           MessageBoxDefaultButton.Button1);

               if (ftpConfirmResult==DialogResult.No)
               {
                   dlg.ShowDialog();
                   string fname=dlg.FileName;
                   this.Cursor = Cursors.WaitCursor;
                   if (backupFileCreation(fname))
                   {
                       addToXml(datetime);
                       this.Cursor = Cursors.Default;
                       MessageBox.Show("Back up creation Succesful", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
                       gridLoad();
                   }
                   else 
                   {
                       this.Cursor = Cursors.Default;
                       MessageBox.Show("Back up creation Failed", "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Information);
                       gridLoad();

                   }
               }
               else if(ftpConfirmResult==DialogResult.Yes)
               {
                   this.Cursor = Cursors.WaitCursor;
                   Boolean success = saveToFtp(datetime);  
                   if(success)
                   {
                       addToXml(datetime);
                       this.Cursor = Cursors.Default;
                       MessageBox.Show("Back up creation is Successful", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
                       gridLoad();

                   }
                   else
                   {
                       this.Cursor = Cursors.Default;
                       MessageBox.Show("Back up creation is Failed", "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Information);
                       gridLoad();

                   }
               }

            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
                // this.prgBackup.Value = 0;
            }
        }

        [STAThread]
        public bool saveToFtp(string datetime)
        {
            
            if (File.Exists(ftpPath))
            {
                string startPath = System.IO.Directory.GetCurrentDirectory();
                string[] lines = Regex.Split(startPath, "bin");
                string fileName = lines[0] + datetime + @".bak";

                if (backupFileCreation(fileName))
                {
                    if (uploadToFtp(fileName))
                        return true;
                    else
                        return false;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                DialogResult ftpDetResult = MessageBox.Show("You haven't entered the ftp details yet.Please enter the ftp URL,User Name and Password. Click 'OK' to enter ftp Details or Click 'Cancel' to quit this procedure.", "FTP Details Missing",
                   MessageBoxButtons.OKCancel,
                   MessageBoxIcon.Asterisk,
                   MessageBoxDefaultButton.Button1);

                if (ftpDetResult == DialogResult.OK)
                {
                    ftpDetails ftp = new ftpDetails();
                    ftp.Show();
                   // saveToFtp(datetime);
                }
                else if (ftpDetResult == DialogResult.Cancel)
                {
                    return false;
                }
                return false;
            }
        }

        [STAThread]
        public void addToXml(string fname)
        {
            int number;
            if (File.Exists(logPath))
            {
                XmlTextReader reader = new XmlTextReader(logPath);
                XmlDocument doc = new XmlDocument();
                doc.Load(reader);
                reader.Close();

                
                XmlNode num;
                XmlElement root = doc.DocumentElement;
                Console.WriteLine(fname);
                num = root.SelectSingleNode("/backups").LastChild.FirstChild;
                number = Int32.Parse(num.InnerText.ToString())+1;

                XmlNode currNode;

                XmlDocumentFragment docFrag = doc.CreateDocumentFragment();

                docFrag.InnerXml = "<backup>"+
                                   "<Number>" + number + "</Number>" +
                                   "<Name>"+ fname + "</Name>"+                    
                                   "</backup>";

                // insert the availability node into the document 
                currNode = doc.DocumentElement;
                currNode.InsertAfter(docFrag, currNode.LastChild);
                //save the output to a file 
                doc.Save(logPath);
                this.DialogResult = DialogResult.OK;
            }
            else 
            {
                using (XmlWriter writer = XmlWriter.Create(logPath))
                {

                    writer.WriteStartDocument();

                    writer.WriteStartElement("backups");

                    writer.WriteStartElement("backup");

                    writer.WriteElementString("Number", "1");
                    writer.WriteElementString("Name", fname);

                    writer.WriteEndElement();

                    writer.WriteEndElement();

                    writer.WriteEndDocument();
                }
            }
        }

        [STAThread]
        public bool backupFileCreation(string filename)
        {
            Console.WriteLine(filename);

            // Create a new backup operation
            Backup bkpDatabase = new Backup();

            // Set the backup type to a database backup
            bkpDatabase.Action = BackupActionType.Database;

            // Set the database that we want to perform a backup on
            bkpDatabase.Database = "hotelms";

            // Set the backup device to a file
            BackupDeviceItem bkpDevice = new BackupDeviceItem(filename, DeviceType.File);

            // Add the backup device to the backup
            bkpDatabase.Devices.Add(bkpDevice);

            //prgBackup.Value = 0;
           // prgBackup.Maximum = 100;
            //prgBackup.Value = 10;

            bkpDatabase.PercentCompleteNotification = 10;
           // bkpDatabase.PercentComplete += new PercentCompleteEventHandler(ProgressEventHandler);

            // Perform the backup
            bkpDatabase.SqlBackup(srv);

            if (File.Exists(filename))
            {
                return true;
            }
            return false;
        }

        public bool uploadToFtp(string filename)
        {
            try
            {
                //filename = @"C:\Users\DDArt\Desktop\Backup.txt";
                FileStream READER = new FileStream(ftpPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //Set up the filestream (READER) //
                System.Xml.XmlDocument ftpDetails = new System.Xml.XmlDocument();// Set up the XmlDocument //
                ftpDetails.Load(READER); //Load the data from the file into the XmlDocument  //
                System.Xml.XmlNodeList NodeList = ftpDetails.GetElementsByTagName("ftpDetails"); // Create a list of the nodes in the xml file //

                String ftpserver = NodeList[0].FirstChild.ChildNodes[0].InnerText.ToString();
                String username = NodeList[0].FirstChild.ChildNodes[1].InnerText.ToString();
                String password = NodeList[0].FirstChild.ChildNodes[2].InnerText.ToString();
                 

                //using (FtpConnection ftp = new FtpConnection("yourslguide.webuda.com", "a3942727", "123abc"))

                // Get the object used to communicate with the server.
                String requesturl="ftp://"+ftpserver+"/"+datetime+".bak";
                Console.WriteLine(requesturl);

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(requesturl);
                request.Method = WebRequestMethods.Ftp.UploadFile;

                // This example assumes the FTP site uses anonymous logon.
                request.Credentials = new NetworkCredential(username, password);

                // Copy the contents of the file to the request stream.
                StreamReader sourceStream = new StreamReader(filename);
                byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                sourceStream.Close();
                request.ContentLength = fileContents.Length;

                requestStream = request.GetRequestStream();
                this.Cursor = Cursors.WaitCursor;
                requestStream.Write(fileContents, 0, fileContents.Length);

                this.Cursor = Cursors.Default;
                Console.WriteLine("Upload Success!");
               // MessageBox.Show("Backup file succefuly uploaded to ftp server", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
                requestStream.Close();

                //FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                
                //Console.WriteLine("Upload File Complete, status {0}", response.StatusDescription);
                
                //response.Close();

                return true;
            }
            catch (System.Net.WebException webExc) 
            {
                MessageBox.Show("Problem with uploading backup file.Either the FTP account details are incorrect or your internet connection is down.","Sorry",MessageBoxButtons.OK,MessageBoxIcon.Error);
                return false;                
            }
            return false;
        }

        private void logHistory_click(object sender, EventArgs e)
        {
            LogHistory log = new LogHistory();
            log.Show();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                using (XmlWriter writer = XmlWriter.Create(xmlPath))
                {
                    
                    writer.WriteStartDocument();

                    writer.WriteStartElement("themecolor");

                    writer.WriteStartElement("theme_color");

                    writer.WriteElementString("theme", this.ThemeName.ToString());
                    
                    writer.WriteElementString("color", this.BackColor.Name.ToString());

                    writer.WriteEndElement();

                    writer.WriteEndElement();

                    writer.WriteEndDocument();
                }

                if (File.Exists(xmlPath))
                {
                    MessageBox.Show("Theme and Background Color succesfully applied", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else 
                {
                    MessageBox.Show("Theme and Background Color apllying failed", "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ef) 
            {
                MessageBox.Show(ef.Message);
            }
        }

        private void theamChanged(object sender, EventArgs e)
        {
            string theme=null;
            theme = cmbThemeSelect.SelectedItem.ToString();

            switch (theme)
            {
                case "office2007Black":
                     this.ThemeName = "office2007Black";
                     this.Refresh();
                    break;
                case "office2007Silver":
                    this.ThemeName = "office2007Silver";
                     this.Refresh();
                    break;
                case "vista":
                    this.ThemeName = "vista";
                     this.Refresh();
                    break;
                case "windows7":
                    this.ThemeName = "windows7";
                     this.Refresh();
                    break;
                case "office2010":
                    this.ThemeName = "office2010";
                     this.Refresh();
                    break;
                case "telerik":
                    this.ThemeName = "telerik";
                     this.Refresh();
                    break;
                case "aqua":
                    this.ThemeName = "aqua";
                     this.Refresh();
                    break;
                case "desert":
                    this.ThemeName = "desert";
                     this.Refresh();
                    break;
                case "breeze":
                    this.ThemeName = "breeze";
                     this.Refresh();
                    break;
            }
        }

        private void Settings_ThemeNameChanged(object source, ThemeNameChangedEventArgs args)
        {
            this.Refresh();
        }

        
         public void setTheme()
        {
            try
            {
                switch (set.getTheme())
                {
                    case "office2007Black":
                        cmbThemeSelect.SelectedText= "Office 2007 Black";
                        break;
                    case "office2007Silver":
                        cmbThemeSelect.SelectedText = "Office 2007 Silver";
                        break;
                    case "vista":
                        cmbThemeSelect.SelectedText = "vista";
                        break;
                    case "windows7":
                        cmbThemeSelect.SelectedText = "windows7";
                        break;
                    case "office2010":
                        cmbThemeSelect.SelectedText = "office2010";
                        break;
                    case "telerik":
                        cmbThemeSelect.SelectedText = "telerik";
                        break;
                    case "aqua":
                        cmbThemeSelect.SelectedText = "aqua";
                        break;
                    case "desert":
                        cmbThemeSelect.SelectedText = "desert";
                        break;
                    case "breeze":
                        cmbThemeSelect.SelectedText = "breeze";
                        break;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void Cancel_Clicked(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnRestore_Click(object sender, EventArgs e)
        {
            Restore();
        }

        public void Restore()
        {
            try
            {
                Restore res = new Restore();

            DialogResult ftpConfirmResult = MessageBox.Show("Where do you want to save your backup file ? Select 'Yes' to save it in a Remote Server or Select 'No' to save it in this machine", "Backup file location",
                   MessageBoxButtons.YesNoCancel,
                   MessageBoxIcon.Question,
                   MessageBoxDefaultButton.Button1);

            if (ftpConfirmResult == DialogResult.No)
            {
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.Multiselect = false;
                dlg.ShowDialog();
                string fileName = dlg.FileName;
                string databaseName = "hotelms";

                res.Database = databaseName;
                res.Action = RestoreActionType.Database;
                res.Devices.AddDevice(fileName, DeviceType.File);

                res.PercentCompleteNotification = 10;
                res.ReplaceDatabase = true;
                // res.PercentComplete += new PercentCompleteEventHandler(ProgressEventHandler);
                res.SqlRestore(srv);

                MessageBox.Show("Restore of " + databaseName +
                " Complete!", "Restore", MessageBoxButtons.OK, MessageBoxIcon.Information);
            
            }
            else if (ftpConfirmResult == DialogResult.Yes)
            {
                FtpWebRequest reqFTP;
                string databaseName = "hotelms";

                //Downloading backup file 

                FileStream READER = new FileStream(ftpPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //Set up the filestream (READER) //
                System.Xml.XmlDocument ftpDetails = new System.Xml.XmlDocument();// Set up the XmlDocument //
                ftpDetails.Load(READER); //Load the data from the file into the XmlDocument  //
                System.Xml.XmlNodeList NodeList = ftpDetails.GetElementsByTagName("ftpDetails"); // Create a list of the nodes in the xml file //

                String ftpserver = NodeList[0].FirstChild.ChildNodes[0].InnerText.ToString();
                String username = NodeList[0].FirstChild.ChildNodes[1].InnerText.ToString();
                String password = NodeList[0].FirstChild.ChildNodes[2].InnerText.ToString();

            
                string filePath = "D:" + "\\" + selectedRow + ".bak";
                string fileName = selectedRow + ".bak";

                FileStream outputStream = new FileStream(filePath, FileMode.Create);

                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + ftpserver + "/" + fileName));
                reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
                reqFTP.UseBinary = true;
                reqFTP.Credentials = new NetworkCredential(username, password);
                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                Stream ftpStream = response.GetResponseStream();
                long cl = response.ContentLength;
                int bufferSize = 2048;
                int readCount;
                byte[] buffer = new byte[bufferSize];

                readCount = ftpStream.Read(buffer, 0, bufferSize);
                while (readCount > 0)
                {
                    outputStream.Write(buffer, 0, readCount);
                    readCount = ftpStream.Read(buffer, 0, bufferSize);
                }

                ftpStream.Close();
                outputStream.Close();
                response.Close();

               //Restoring downloaded file

                res.Database = databaseName;
                res.Action = RestoreActionType.Database;
                res.Devices.AddDevice(filePath, DeviceType.File);

                //res.PercentCompleteNotification = 10;
                res.ReplaceDatabase = true;
                // res.PercentComplete += new PercentCompleteEventHandler(ProgressEventHandler);
                res.SqlRestore(srv);

                MessageBox.Show("Restore of " + databaseName +
                " Complete!", "Restore", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Restoring Failed","Sorry",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
                //this.prgBackup.Value = 0;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
        }

        

        private void chkAutoCheckChanged(object sender, EventArgs e)
        {
            if (chkboxAuto.Checked)
            {
                cmbMonth.Enabled = true;
                cmbMonth.BackColor = Color.White;
                cmbTime.Enabled = true;
                cmbTime.BackColor = Color.White;
            }
            else
            {
                cmbMonth.Enabled = false;
                cmbMonth.BackColor = Color.Silver;
                cmbTime.Enabled = false;
                cmbTime.BackColor = Color.Silver;
            }
        }

        private void ftp_details_Click(object sender, EventArgs e)
        {
            ftpDetails ftp = new ftpDetails();
            ftp.Show();
        }

        private void grdBackup_CellContentClick(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            selectedRow=grdBackup.Rows[e.RowIndex].Cells[1].Value.ToString();
            Console.WriteLine(selectedRow);
        }

        private void grdBackup_RowHeaderMouseClick(object sender, System.Windows.Forms.DataGridViewCellMouseEventArgs e)
        {
            selectedRow = grdBackup.Rows[e.RowIndex].Cells[1].Value.ToString();
            Console.WriteLine(selectedRow);
        }

        [STAThread]
        public void checkDate()
        {
            FileStream READER = new FileStream(autoBackupPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //Set up the filestream (READER) //
            System.Xml.XmlDocument settings = new System.Xml.XmlDocument();// Set up the XmlDocument //
            settings.Load(READER); //Load the data from the file into the XmlDocument  //
            System.Xml.XmlNodeList NodeList = settings.GetElementsByTagName("autoBackupDet"); // Create a list of the nodes in the xml file //

            string selectedDate = NodeList[0].FirstChild.ChildNodes[0].InnerText.ToString();
            string selectedTime = NodeList[0].FirstChild.ChildNodes[1].InnerText.ToString();

            Console.WriteLine(selectedDate+"  "+selectedTime);
            try
            {          
                while (true)
                {

                    String[] tokens = System.DateTime.Now.ToString().Split(' ');

                    String date = tokens[0].ToString();
                    String[] datetoken = date.Split('/');
                    date = datetoken[1];

                    //Console.WriteLine(date);

                    String time = tokens[1].ToString();
                    String[] timetoken = time.Split(':');

                    if (tokens[2].ToString() == "PM")
                    {
                        timetoken[0] = (Int32.Parse(timetoken[0]) + 12).ToString();
                    }
                    
                    time = timetoken[0] + ":" + timetoken[1];
                    //time = "13:00";
                    //Console.WriteLine(time);

                    if (time == selectedTime && date == selectedDate)
                    {
                        //autoBackupToFtp();
                        autoBackupToLocal();
                    }
                    
                }
            }
            catch (Exception)
            {

            }


        }

        private void autoBackupToLocal()
        {
            datetime = DateTime.Now.ToString().Replace("/", "_").Replace(":", ".");    

            DialogResult autoBackupResult = MessageBox.Show("System is about to backup the database automatically. Do you want to backup the database now ?", "Automatic Backup confirmation",
               MessageBoxButtons.YesNo,
               MessageBoxIcon.Question,
               MessageBoxDefaultButton.Button1);

            if (autoBackupResult == DialogResult.Yes)
            {
                
                string fname = @"E:\"+datetime+".bak";
                this.Cursor = Cursors.WaitCursor;
                if (backupFileCreation(fname))
                {
                    addToXml(datetime);
                    this.Cursor = Cursors.Default;
                    MessageBox.Show("Back up creation Succesful", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    gridLoad();
                }
                else
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show("Back up creation Failed", "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    gridLoad();

                }
            }
            
        }

        private void autoBackupToFtp()
        {

            DialogResult autoBackupResult = MessageBox.Show("System is about to backup the database automatically. Do you want to backup the database now ?", "Automatic Backup confirmation",
            MessageBoxButtons.YesNo,
            MessageBoxIcon.Question,
            MessageBoxDefaultButton.Button1);

            if (autoBackupResult == DialogResult.Yes)
            {
                this.Cursor = Cursors.WaitCursor;
                Boolean success = saveToFtp(datetime);
                if (success)
                {
                    addToXml(datetime);
                    this.Cursor = Cursors.Default;
                    MessageBox.Show("Back up creation is Successful", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    gridLoad();

                }
                else
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show("Back up creation is Failed", "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    gridLoad();

                }
            }
        }

       
        public void autoBackupDetChanged() 
        {
            using (XmlWriter writer = XmlWriter.Create(autoBackupPath))
            {

                writer.WriteStartDocument();

                writer.WriteStartElement("autoBackupDet");

                writer.WriteStartElement("autobackup");

                writer.WriteElementString("date", cmbMonth.Text.ToString());

                writer.WriteElementString("time", cmbTime.Text.ToString());

                writer.WriteEndElement();

                writer.WriteEndElement();

                writer.WriteEndDocument();
            }
        }

        private void cmbMonth_SelectedValueChanged(object sender, System.EventArgs e)
        {
            autoBackupDetChanged();
        }

        private void cmbTime_SelectedValueChanged(object sender, System.EventArgs e)
        {
            autoBackupDetChanged();
        }

        private void Settings_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            this.Dispose();
        }

        private void btnChekOut_Click(object sender, System.EventArgs e)
        {
            Home h = new Home();
            h.Show();
            //this.Dispose();
        }

        private void button4_Click(object sender, System.EventArgs e)
        {
            HotelDetails h = new HotelDetails();
            h.Show();
        }

        private void lblLogout_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
        {
            string logID = new LoggedUser().LogHisId.ToString();
            String[] tokens3 = System.DateTime.Today.ToString().Split(' ');
            String onlyDate = tokens3[1];

            //log history record
            hotelmsEntities1 hmsLog = new hotelmsEntities1();
            logHistory log = hmsLog.logHistories.Where(iii => iii.logHistoryId == logID.Trim()).Single();
            log.logHistoryId = logID.ToString().Trim();
            log.logOutTime = System.DateTime.Now;
            hmsLog.SaveChanges();

            //clearing the detils 
            LoggedUser lu = new LoggedUser();
            lu.UserName1 = "";
            lu.UserLevel1 = "";
            lu.LogHisId = "";
            Login login = new Login();
            login.Show();
            this.Dispose();
        }

        private void button3_Click_1(object sender, System.EventArgs e)
        {
            GallerySettings gallerySet = new GallerySettings();
            gallerySet.Show();
            
        }

        private void btnColorSelect_Click(object sender, System.EventArgs e)
        {
            int red=0, green=0, blue=0;
            

            clrDlgBackground.ShowDialog();
            Console.WriteLine(clrDlgBackground.Color.ToString());
            if (clrDlgBackground.Color.ToString().Length < 16)
            {
                this.BackColor = clrDlgBackground.Color;
                
            }
            else 
            {
                String colorString =clrDlgBackground.Color.ToString();
                String [] redArr= Regex.Split(colorString, "R=");
                redArr = Regex.Split(redArr[1], ",");
                red = Int32.Parse(redArr[0]);

                String[] greenArr = Regex.Split(colorString, "G=");
                greenArr = Regex.Split(greenArr[1], ",");
                green = Int32.Parse(greenArr[0]);

                String[] blueArr = Regex.Split(colorString, "B=");
                blueArr = Regex.Split(blueArr[1], "]");
                blue = Int32.Parse(blueArr[0]);

                this.BackColor = System.Drawing.Color.FromArgb(red, green, blue);
            }
            this.Refresh();
        }

        private void btnHome_Click(object sender, System.EventArgs e)
        {
            Home h = new Home();
            h.Show();
            this.Dispose();
        }
        
    }
}
 