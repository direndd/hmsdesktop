namespace HotelManagementIcom.View
{
    partial class Checkout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Checkout));
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.grdCheckout = new System.Windows.Forms.DataGridView();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnCheckout = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblLogout = new System.Windows.Forms.LinkLabel();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblUserLevel = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnHome = new System.Windows.Forms.Button();
            this.txtBoxResId = new System.Windows.Forms.TextBox();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.txtBoxPassPort = new System.Windows.Forms.TextBox();
            this.txtBoxCustName = new System.Windows.Forms.TextBox();
            this.txtBoxCusId = new System.Windows.Forms.TextBox();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.cmbCusNic = new System.Windows.Forms.ComboBox();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel21 = new Telerik.WinControls.UI.RadLabel();
            this.txtDisAmount = new System.Windows.Forms.TextBox();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.txtBalance = new System.Windows.Forms.TextBox();
            this.txtPaymentID = new System.Windows.Forms.TextBox();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPayingAmt = new System.Windows.Forms.TextBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.txtInvoiceID = new System.Windows.Forms.TextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.txtTotAmount = new System.Windows.Forms.TextBox();
            this.txtBoxdiscount = new System.Windows.Forms.TextBox();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.cmbpaymentType = new System.Windows.Forms.ComboBox();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.office2007Black = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.vista = new Telerik.WinControls.Themes.VistaTheme();
            this.office2010 = new Telerik.WinControls.Themes.Office2010Theme();
            this.telerik = new Telerik.WinControls.Themes.TelerikTheme();
            this.aqua = new Telerik.WinControls.Themes.AquaTheme();
            this.desert = new Telerik.WinControls.Themes.DesertTheme();
            this.breeze = new Telerik.WinControls.Themes.BreezeTheme();
            this.office2007SilverTheme1 = new Telerik.WinControls.Themes.Office2007SilverTheme();
            this.windows7Theme1 = new Telerik.WinControls.Themes.Windows7Theme();
            ((System.ComponentModel.ISupportInitialize)(this.grdCheckout)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // grdCheckout
            // 
            this.grdCheckout.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdCheckout.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCheckout.Location = new System.Drawing.Point(12, 86);
            this.grdCheckout.Name = "grdCheckout";
            this.grdCheckout.Size = new System.Drawing.Size(968, 267);
            this.grdCheckout.TabIndex = 89;
            this.grdCheckout.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdReservations_CellContentClick);
            this.grdCheckout.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.grdCheckout_RowHeaderMouseClick);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Tai Le", 10F);
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCancel.Location = new System.Drawing.Point(831, 638);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(140, 32);
            this.btnCancel.TabIndex = 16;
            this.btnCancel.Text = "Cancel Reservation";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.button7_Click);
            // 
            // btnCheckout
            // 
            this.btnCheckout.BackColor = System.Drawing.Color.Green;
            this.btnCheckout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCheckout.Font = new System.Drawing.Font("Microsoft Tai Le", 10F);
            this.btnCheckout.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCheckout.Location = new System.Drawing.Point(579, 638);
            this.btnCheckout.Name = "btnCheckout";
            this.btnCheckout.Size = new System.Drawing.Size(120, 32);
            this.btnCheckout.TabIndex = 14;
            this.btnCheckout.Text = "Checkout";
            this.btnCheckout.UseVisualStyleBackColor = false;
            this.btnCheckout.Click += new System.EventHandler(this.btnCheckout_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.MidnightBlue;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Tai Le", 10F);
            this.btnUpdate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnUpdate.Location = new System.Drawing.Point(705, 638);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(120, 32);
            this.btnUpdate.TabIndex = 15;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblLogout);
            this.panel1.Controls.Add(this.lblTime);
            this.panel1.Controls.Add(this.lblUserLevel);
            this.panel1.Controls.Add(this.lblUserName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.btnHome);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(992, 80);
            this.panel1.TabIndex = 95;
            // 
            // lblLogout
            // 
            this.lblLogout.AutoSize = true;
            this.lblLogout.Font = new System.Drawing.Font("Microsoft Tai Le", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogout.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblLogout.Location = new System.Drawing.Point(898, 37);
            this.lblLogout.Name = "lblLogout";
            this.lblLogout.Size = new System.Drawing.Size(81, 29);
            this.lblLogout.TabIndex = 101;
            this.lblLogout.TabStop = true;
            this.lblLogout.Text = "Logout";
            this.lblLogout.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblLogout_LinkClicked);
            this.lblLogout.Click += new System.EventHandler(this.lblLogout_Click);
            // 
            // lblTime
            // 
            this.lblTime.BackColor = System.Drawing.Color.Transparent;
            this.lblTime.Font = new System.Drawing.Font("Microsoft JhengHei", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTime.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTime.Location = new System.Drawing.Point(557, 2);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(334, 46);
            this.lblTime.TabIndex = 52;
            this.lblTime.Text = "Time";
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUserLevel
            // 
            this.lblUserLevel.AutoSize = true;
            this.lblUserLevel.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserLevel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblUserLevel.Location = new System.Drawing.Point(560, 48);
            this.lblUserLevel.Name = "lblUserLevel";
            this.lblUserLevel.Size = new System.Drawing.Size(76, 19);
            this.lblUserLevel.TabIndex = 48;
            this.lblUserLevel.Text = "User Level";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblUserName.Location = new System.Drawing.Point(663, 48);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(82, 19);
            this.lblUserName.TabIndex = 49;
            this.lblUserName.Text = "User Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Tai Le", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(112, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 18);
            this.label1.TabIndex = 34;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Tai Le", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(110, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(144, 29);
            this.label4.TabIndex = 33;
            this.label4.Text = "Checkout Tile";
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.Color.LimeGreen;
            this.btnHome.FlatAppearance.BorderSize = 0;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnHome.Font = new System.Drawing.Font("Microsoft Tai Le", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHome.Location = new System.Drawing.Point(4, 3);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(100, 68);
            this.btnHome.TabIndex = 0;
            this.btnHome.Text = "Home";
            this.btnHome.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // txtBoxResId
            // 
            this.txtBoxResId.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.txtBoxResId.Location = new System.Drawing.Point(112, 25);
            this.txtBoxResId.Name = "txtBoxResId";
            this.txtBoxResId.Size = new System.Drawing.Size(185, 23);
            this.txtBoxResId.TabIndex = 1;
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.radLabel10.Location = new System.Drawing.Point(15, 29);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(85, 19);
            this.radLabel10.TabIndex = 110;
            this.radLabel10.Text = "Reservation ID";
            // 
            // txtBoxPassPort
            // 
            this.txtBoxPassPort.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.txtBoxPassPort.Location = new System.Drawing.Point(112, 120);
            this.txtBoxPassPort.Name = "txtBoxPassPort";
            this.txtBoxPassPort.Size = new System.Drawing.Size(185, 23);
            this.txtBoxPassPort.TabIndex = 3;
            // 
            // txtBoxCustName
            // 
            this.txtBoxCustName.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.txtBoxCustName.Location = new System.Drawing.Point(112, 214);
            this.txtBoxCustName.Name = "txtBoxCustName";
            this.txtBoxCustName.Size = new System.Drawing.Size(185, 23);
            this.txtBoxCustName.TabIndex = 5;
            // 
            // txtBoxCusId
            // 
            this.txtBoxCusId.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.txtBoxCusId.Location = new System.Drawing.Point(112, 167);
            this.txtBoxCusId.Name = "txtBoxCusId";
            this.txtBoxCusId.Size = new System.Drawing.Size(185, 23);
            this.txtBoxCusId.TabIndex = 4;
            // 
            // radLabel16
            // 
            this.radLabel16.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.radLabel16.Location = new System.Drawing.Point(15, 123);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(53, 19);
            this.radLabel16.TabIndex = 108;
            this.radLabel16.Text = "Passport";
            // 
            // cmbCusNic
            // 
            this.cmbCusNic.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbCusNic.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.cmbCusNic.FormattingEnabled = true;
            this.cmbCusNic.Location = new System.Drawing.Point(112, 72);
            this.cmbCusNic.Name = "cmbCusNic";
            this.cmbCusNic.Size = new System.Drawing.Size(185, 24);
            this.cmbCusNic.TabIndex = 2;
            // 
            // radLabel19
            // 
            this.radLabel19.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.radLabel19.Location = new System.Drawing.Point(15, 76);
            this.radLabel19.Name = "radLabel19";
            this.radLabel19.Size = new System.Drawing.Size(33, 19);
            this.radLabel19.TabIndex = 106;
            this.radLabel19.Text = "N I C";
            // 
            // radLabel20
            // 
            this.radLabel20.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.radLabel20.Location = new System.Drawing.Point(15, 215);
            this.radLabel20.Name = "radLabel20";
            this.radLabel20.Size = new System.Drawing.Size(74, 19);
            this.radLabel20.TabIndex = 105;
            this.radLabel20.Text = "Guest Name";
            // 
            // radLabel21
            // 
            this.radLabel21.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.radLabel21.Location = new System.Drawing.Point(15, 171);
            this.radLabel21.Name = "radLabel21";
            this.radLabel21.Size = new System.Drawing.Size(58, 19);
            this.radLabel21.TabIndex = 104;
            this.radLabel21.Text = "Guest ID*";
            // 
            // txtDisAmount
            // 
            this.txtDisAmount.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.txtDisAmount.Location = new System.Drawing.Point(118, 122);
            this.txtDisAmount.MinimumSize = new System.Drawing.Size(165, 20);
            this.txtDisAmount.Name = "txtDisAmount";
            this.txtDisAmount.Size = new System.Drawing.Size(170, 23);
            this.txtDisAmount.TabIndex = 11;
            this.txtDisAmount.TextChanged += new System.EventHandler(this.txtDisAmount_TextChanged_1);
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.radLabel8.Location = new System.Drawing.Point(6, 126);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(116, 19);
            this.radLabel8.TabIndex = 122;
            this.radLabel8.Text = "Discounted Amount";
            // 
            // txtBalance
            // 
            this.txtBalance.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.txtBalance.Location = new System.Drawing.Point(118, 212);
            this.txtBalance.Name = "txtBalance";
            this.txtBalance.Size = new System.Drawing.Size(170, 23);
            this.txtBalance.TabIndex = 13;
            this.txtBalance.TextChanged += new System.EventHandler(this.txtBalance_TextChanged_1);
            // 
            // txtPaymentID
            // 
            this.txtPaymentID.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.txtPaymentID.Location = new System.Drawing.Point(110, 77);
            this.txtPaymentID.Name = "txtPaymentID";
            this.txtPaymentID.Size = new System.Drawing.Size(185, 23);
            this.txtPaymentID.TabIndex = 7;
            // 
            // radLabel11
            // 
            this.radLabel11.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.radLabel11.Location = new System.Drawing.Point(18, 78);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(68, 19);
            this.radLabel11.TabIndex = 126;
            this.radLabel11.Text = "Payment Id";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.label5.Location = new System.Drawing.Point(6, 215);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 16);
            this.label5.TabIndex = 125;
            this.label5.Text = "Balance";
            // 
            // txtPayingAmt
            // 
            this.txtPayingAmt.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.txtPayingAmt.Location = new System.Drawing.Point(118, 167);
            this.txtPayingAmt.Name = "txtPayingAmt";
            this.txtPayingAmt.Size = new System.Drawing.Size(170, 23);
            this.txtPayingAmt.TabIndex = 12;
            this.txtPayingAmt.TextChanged += new System.EventHandler(this.txtPayingAmt_TextChanged_1);
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.radLabel7.Location = new System.Drawing.Point(6, 174);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(88, 19);
            this.radLabel7.TabIndex = 124;
            this.radLabel7.Text = "Amount to Pay";
            // 
            // txtInvoiceID
            // 
            this.txtInvoiceID.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.txtInvoiceID.Location = new System.Drawing.Point(110, 31);
            this.txtInvoiceID.Name = "txtInvoiceID";
            this.txtInvoiceID.Size = new System.Drawing.Size(185, 23);
            this.txtInvoiceID.TabIndex = 6;
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.radLabel2.Location = new System.Drawing.Point(18, 31);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(59, 19);
            this.radLabel2.TabIndex = 123;
            this.radLabel2.Text = "Invoice Id";
            // 
            // txtTotAmount
            // 
            this.txtTotAmount.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.txtTotAmount.Location = new System.Drawing.Point(110, 120);
            this.txtTotAmount.Name = "txtTotAmount";
            this.txtTotAmount.Size = new System.Drawing.Size(170, 23);
            this.txtTotAmount.TabIndex = 8;
            this.txtTotAmount.TextChanged += new System.EventHandler(this.txtTotAmount_TextChanged);
            // 
            // txtBoxdiscount
            // 
            this.txtBoxdiscount.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.txtBoxdiscount.Location = new System.Drawing.Point(118, 77);
            this.txtBoxdiscount.MinimumSize = new System.Drawing.Size(165, 20);
            this.txtBoxdiscount.Name = "txtBoxdiscount";
            this.txtBoxdiscount.Size = new System.Drawing.Size(170, 23);
            this.txtBoxdiscount.TabIndex = 10;
            this.txtBoxdiscount.TextChanged += new System.EventHandler(this.txtBoxdiscount_TextChanged_1);
            // 
            // radLabel13
            // 
            this.radLabel13.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.radLabel13.Location = new System.Drawing.Point(18, 126);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(81, 19);
            this.radLabel13.TabIndex = 121;
            this.radLabel13.Text = "Total Amount";
            // 
            // radLabel15
            // 
            this.radLabel15.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.radLabel15.Location = new System.Drawing.Point(6, 78);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(60, 19);
            this.radLabel15.TabIndex = 120;
            this.radLabel15.Text = "Discounts";
            // 
            // cmbpaymentType
            // 
            this.cmbpaymentType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbpaymentType.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.cmbpaymentType.FormattingEnabled = true;
            this.cmbpaymentType.Items.AddRange(new object[] {
            "Cheque",
            "Cash",
            "Credit Card",
            "Credit"});
            this.cmbpaymentType.Location = new System.Drawing.Point(118, 31);
            this.cmbpaymentType.Name = "cmbpaymentType";
            this.cmbpaymentType.Size = new System.Drawing.Size(185, 24);
            this.cmbpaymentType.TabIndex = 9;
            // 
            // radLabel17
            // 
            this.radLabel17.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.radLabel17.Location = new System.Drawing.Point(6, 35);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(81, 19);
            this.radLabel17.TabIndex = 119;
            this.radLabel17.Text = "Payment type";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radLabel19);
            this.groupBox1.Controls.Add(this.radLabel21);
            this.groupBox1.Controls.Add(this.radLabel20);
            this.groupBox1.Controls.Add(this.cmbCusNic);
            this.groupBox1.Controls.Add(this.radLabel16);
            this.groupBox1.Controls.Add(this.txtBoxCusId);
            this.groupBox1.Controls.Add(this.txtBoxCustName);
            this.groupBox1.Controls.Add(this.txtBoxPassPort);
            this.groupBox1.Controls.Add(this.radLabel10);
            this.groupBox1.Controls.Add(this.txtBoxResId);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Location = new System.Drawing.Point(12, 359);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(315, 265);
            this.groupBox1.TabIndex = 127;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Guest Details";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radLabel11);
            this.groupBox2.Controls.Add(this.radLabel13);
            this.groupBox2.Controls.Add(this.txtTotAmount);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.radLabel2);
            this.groupBox2.Controls.Add(this.txtInvoiceID);
            this.groupBox2.Controls.Add(this.txtPaymentID);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox2.Location = new System.Drawing.Point(340, 359);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(315, 265);
            this.groupBox2.TabIndex = 128;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Invoice Details";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(286, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 16);
            this.label2.TabIndex = 127;
            this.label2.Text = "$";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.radLabel17);
            this.groupBox3.Controls.Add(this.cmbpaymentType);
            this.groupBox3.Controls.Add(this.radLabel15);
            this.groupBox3.Controls.Add(this.txtDisAmount);
            this.groupBox3.Controls.Add(this.txtBoxdiscount);
            this.groupBox3.Controls.Add(this.radLabel8);
            this.groupBox3.Controls.Add(this.radLabel7);
            this.groupBox3.Controls.Add(this.txtBalance);
            this.groupBox3.Controls.Add(this.txtPayingAmt);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox3.Location = new System.Drawing.Point(668, 359);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(315, 265);
            this.groupBox3.TabIndex = 129;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Payment Details";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(291, 123);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(14, 16);
            this.label8.TabIndex = 130;
            this.label8.Text = "$";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(291, 170);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 16);
            this.label6.TabIndex = 129;
            this.label6.Text = "$";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(291, 217);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 16);
            this.label3.TabIndex = 128;
            this.label3.Text = "$";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(291, 80);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 16);
            this.label7.TabIndex = 126;
            this.label7.Text = "%";
            // 
            // Checkout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(992, 686);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.grdCheckout);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnCheckout);
            this.Controls.Add(this.btnUpdate);
            this.Font = new System.Drawing.Font("Microsoft Tai Le", 12F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1000, 720);
            this.MinimumSize = new System.Drawing.Size(1000, 720);
            this.Name = "Checkout";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(1000, 720);
            this.RootElement.MinSize = new System.Drawing.Size(1000, 720);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Checkout";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.Checkout_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdCheckout)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private System.Windows.Forms.DataGridView grdCheckout;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnCheckout;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblUserLevel;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.TextBox txtBoxResId;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private System.Windows.Forms.TextBox txtBoxPassPort;
        private System.Windows.Forms.TextBox txtBoxCustName;
        private System.Windows.Forms.TextBox txtBoxCusId;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private System.Windows.Forms.ComboBox cmbCusNic;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadLabel radLabel20;
        private Telerik.WinControls.UI.RadLabel radLabel21;
        private System.Windows.Forms.TextBox txtDisAmount;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private System.Windows.Forms.TextBox txtBalance;
        private System.Windows.Forms.TextBox txtPaymentID;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPayingAmt;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private System.Windows.Forms.TextBox txtInvoiceID;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private System.Windows.Forms.TextBox txtTotAmount;
        private System.Windows.Forms.TextBox txtBoxdiscount;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        public System.Windows.Forms.ComboBox cmbpaymentType;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.LinkLabel lblLogout;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007Black;
        private Telerik.WinControls.Themes.VistaTheme vista;
        private Telerik.WinControls.Themes.Office2010Theme office2010;
        private Telerik.WinControls.Themes.TelerikTheme telerik;
        private Telerik.WinControls.Themes.AquaTheme aqua;
        private Telerik.WinControls.Themes.DesertTheme desert;
        private Telerik.WinControls.Themes.BreezeTheme breeze;
        private Telerik.WinControls.Themes.Office2007SilverTheme office2007SilverTheme1;
        private Telerik.WinControls.Themes.Windows7Theme windows7Theme1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;

    }
}

