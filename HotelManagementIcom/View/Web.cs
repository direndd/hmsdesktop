using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using System.IO;
using HotelManagementIcom.Model;

namespace HotelManagementIcom.View
{
    public partial class Web : Telerik.WinControls.UI.RadForm
    {
        hotelmsEntities1 hms = new hotelmsEntities1();
        string imageUrl;
        public Web()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string Chosen_File = "";

                openFileDialog1.Title = "Insert an Image";
                openFileDialog1.FileName = "";
                openFileDialog1.Filter = "JPEG Images|*.jpg|GIF Images|*.gif|PNG Images|*.png";

                if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                {
                    MessageBox.Show("Operation Failed");
                }
                else
                {
                    Chosen_File = openFileDialog1.FileName;
                    imageUrl = openFileDialog1.FileName;
                    //MessageBox.Show(Chosen_File);
                    this.pic3Image.Image = Image.FromFile(Chosen_File);
                }
            }
            catch (Exception ee)
            {
                
              
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //if (this.pic3Image.Image.Equals(null) )
            //{
               
                try            
                {
            if (rbtFacilities.Checked)
            {
                facilitiesWeb rmt = new facilitiesWeb();
               
                rmt.title = txtTitle.Text;
                rmt.message = txtDescription.Text;

                rmt.picture = imageToByteArray(this.pic3Image.Image);
                hms.AddTofacilitiesWebs(rmt);
                hms.SaveChanges();
                MessageBox.Show("added successfully", "successful");
            }
            if (rbtNews.Checked)
            {
                NewsFeed rmt = new NewsFeed();
                rmt.title = txtTitle.Text;
                rmt.message = txtDescription.Text;                
                hms.AddToNewsFeeds(rmt);
                hms.SaveChanges();
                MessageBox.Show("added successfully", "successful");
            }
            if (rbtspecialOffers.Checked)
            {
                specialoffer rmt = new specialoffer();
                rmt.title = txtTitle.Text;
                rmt.message = txtDescription.Text;

                rmt.picture = imageToByteArray(this.pic3Image.Image);
                hms.AddTospecialoffers(rmt);
                hms.SaveChanges();
                MessageBox.Show("added successfully", "successful");
            }
            else {
                MessageBox.Show("Please check your selection", "Error");
            }
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);

                Console.WriteLine("sdasdasd");
                Console.WriteLine(ee.Message);
            } 
            //}
            
            //else{
            //    MessageBox.Show("Please select what you want to do");
            //}
           // this.pic3Image.Image = null;
        }

        public byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                return ms.ToArray();
            }
            catch (Exception)
            {
                
                return null;
            }
        }

        private void rbtNews_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
