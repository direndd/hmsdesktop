using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using HotelManagementIcom.Model;
using System.Linq;
using HotelManagementIcom.View;
using System.Collections;
using HotelManagementIcom.Controller;

namespace HotelManagementIcom
{
    public partial class Login : Telerik.WinControls.UI.RadForm
    {
        
        public Login()
        {
            InitializeComponent();
        }

       
        private void txtBoxUname_TextChanged(object sender, EventArgs e)
        {
            hotelmsEntities1 hms1 = new hotelmsEntities1();
            user user2 = new user();
            StringBuilder priviledge = new StringBuilder();
            StringBuilder userName = new StringBuilder();
            try
            {
                IEnumerable<user> c = hms1.users.Where(i => i.userName.Trim() == txtBoxUname.Text.Trim());
                foreach (user s in c)
                {
                    priviledge.Append(s.userLevel + Environment.NewLine);
                    userName.Append(s.userName + Environment.NewLine);
                    lblUserLevel.Text = priviledge.ToString();

                }
            }
            catch (Exception)
            {
                
            }
        }

       
        //private void btnLogin_Click_1(object sender, EventArgs e)
        //{
           

        //}

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void btnCancle_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            hotelmsEntities1 hms1 = new hotelmsEntities1();
            user user2 = new user();
            StringBuilder priviledge = new StringBuilder();
            StringBuilder userName = new StringBuilder();
            StringBuilder userID = new StringBuilder();
            hotelmsEntities1 hms = new hotelmsEntities1();

            string logID;
            int logIdSubString;
            int logMaxID = 0;
            string logIDChk=null;
            try
            {
                //get the max Customer Id
                var log = hms.logHistories.Select(i => new { i.logHistoryId });
                foreach (var item in log)
                {
                    if (item.logHistoryId != string.Empty)
                    {
                        logID = item.logHistoryId.Substring(3);
                        logIdSubString = Int16.Parse(logID);
                        if (logMaxID < logIdSubString)
                        {
                            logMaxID = logIdSubString;
                        }
                    }
                    else
                    {
                        logMaxID = 0;
                    }

                }
                logMaxID++;
                logID = "log" + logMaxID.ToString();
                logIDChk = logID;

            }
            catch (Exception ewww)
            {
                
            }


            int count = (from usr1 in hms1.users where usr1.userName == txtBoxUname.Text.Trim() && usr1.pwd == txtBoxPass.Text.Trim() select usr1).Count();
            if (count >= 1)
            {
                IEnumerable<user> c = hms1.users.Where(i => i.userName == txtBoxUname.Text.Trim());
                //  MessageBox.Show(c.ToString());
                foreach (user s in c)
                {
                    //   MessageBox.Show(s.ToString());
                    priviledge.Append(s.userLevel + Environment.NewLine);
                    userName.Append(s.userName + Environment.NewLine);
                    userID.Append(s.userId + Environment.NewLine);
                }
                LoggedUser lu = new LoggedUser();
                //lu.UserName1 = txtBoxUname.Text.Trim();
                //lu.UserLevel1 = lblUserLevel.Text.ToString().Trim();
                //lu.LogHisId = logIDChk.ToString().Trim();
                //lu.LogUser = userID.ToString().Trim();

                hotelmsEntities1 hmsLog = new hotelmsEntities1();
                logHistory log = new logHistory();
                log.logHistoryId = logIDChk.ToString().Trim();
                log.logInTime = System.DateTime.Now;
                log.userId = userID.ToString().Trim();
                hmsLog.AddTologHistories(log);
                hmsLog.SaveChanges();

                this.Visible = false;
                Home home = new Home();
               home.Show();
                //Users u = new Users();
                //u.Show();
                //LogHistory l = new LogHistory();
                //l.Show();
                //Customer c1 = new Customer();
                //c1.Show();
            }
            else
            {

                //Customer c = new Customer();
                //c.Show();
                Users u = new Users();
                u.Show();
                //LogHistory l = new LogHistory();
                //l.Show();
              //  MessageBox.Show("Invalid User");
              //  txtBoxPass.Text = null;


            }

        }

        private void txtBoxPass_Enter(object sender, EventArgs e)
        {
        
        }

        private void txtBoxPass_TextChanged(object sender, EventArgs e)
        {

        }

             
    }
}
