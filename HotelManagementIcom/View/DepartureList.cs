using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using HotelManagementIcom.DataAccess;
using HotelManagementIcom.Reports;
using HotelManagementIcom.Controller;

namespace HotelManagementIcom.View
{
    public partial class DepartureList : Telerik.WinControls.UI.RadForm
    {
        DataController data = new DataController();

        public DepartureList()
        {
            InitializeComponent();
        }

        private void DepartureList_Load(object sender, EventArgs e)
        {
            SettingsControl set = new SettingsControl();
            this.ThemeName = set.getTheme();
            this.BackColor = System.Drawing.Color.FromName(set.getColor());

            dtPickerToday.Enabled = false;

            dtPickerFrom.Enabled = false;
            dtPickerTo.Enabled = false;
        }

        private void rdbToday_CheckedChanged(object sender, EventArgs e)
        {
            dtPickerToday.Enabled = true;

            dtPickerFrom.Enabled = false;
            dtPickerTo.Enabled = false;
        }

        private void rdbFrom_CheckedChanged(object sender, EventArgs e)
        {

            dtPickerToday.Enabled = false;

            dtPickerFrom.Enabled = true;
            dtPickerTo.Enabled = true;

            if (this.rdbFrom.Checked)
            {
                this.dtPickerTo.MinDate = this.dtPickerFrom.Value;
            }
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (this.rdbToday.Checked)
            {
                string endingDate = data.FormatDate(this.dtPickerToday);
                HotelManagementIcom.Reports.DepartureListReport1 rpt = new Reports.DepartureListReport1(endingDate);
                rpt.Show();
            }
            else if (this.rdbFrom.Checked)
            {
                string startingDate = data.FormatDate(this.dtPickerFrom);
                string endingDate = data.FormatDate(this.dtPickerTo);
                DeaprtureListReport2 rpt = new DeaprtureListReport2(startingDate, endingDate);
                rpt.Show();
            }

        }

        private void dtPickerFrom_ValueChanged(object sender, EventArgs e)
        {
            this.dtPickerTo.MinDate = this.dtPickerFrom.Value;
        }
    }
}
