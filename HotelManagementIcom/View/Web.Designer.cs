namespace HotelManagementIcom.View
{
    partial class Web
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.pic3Image = new System.Windows.Forms.PictureBox();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.rbtspecialOffers = new System.Windows.Forms.RadioButton();
            this.rbtNews = new System.Windows.Forms.RadioButton();
            this.rbtFacilities = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pic3Image)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.LimeGreen;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.Snow;
            this.button1.Location = new System.Drawing.Point(566, 276);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(170, 41);
            this.button1.TabIndex = 0;
            this.button1.Text = "Browse";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Blue;
            this.button2.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.Color.Snow;
            this.button2.Location = new System.Drawing.Point(541, 369);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(195, 44);
            this.button2.TabIndex = 1;
            this.button2.Text = "Post";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // pic3Image
            // 
            this.pic3Image.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pic3Image.Location = new System.Drawing.Point(486, 31);
            this.pic3Image.Name = "pic3Image";
            this.pic3Image.Size = new System.Drawing.Size(282, 230);
            this.pic3Image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic3Image.TabIndex = 13;
            this.pic3Image.TabStop = false;
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(139, 53);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(263, 20);
            this.txtTitle.TabIndex = 14;
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(139, 105);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(263, 152);
            this.txtDescription.TabIndex = 15;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // rbtspecialOffers
            // 
            this.rbtspecialOffers.AutoSize = true;
            this.rbtspecialOffers.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.rbtspecialOffers.ForeColor = System.Drawing.Color.White;
            this.rbtspecialOffers.Location = new System.Drawing.Point(17, 19);
            this.rbtspecialOffers.Name = "rbtspecialOffers";
            this.rbtspecialOffers.Size = new System.Drawing.Size(94, 17);
            this.rbtspecialOffers.TabIndex = 16;
            this.rbtspecialOffers.TabStop = true;
            this.rbtspecialOffers.Text = "Special offers";
            this.rbtspecialOffers.UseVisualStyleBackColor = true;
            // 
            // rbtNews
            // 
            this.rbtNews.AutoSize = true;
            this.rbtNews.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.rbtNews.ForeColor = System.Drawing.Color.White;
            this.rbtNews.Location = new System.Drawing.Point(159, 19);
            this.rbtNews.Name = "rbtNews";
            this.rbtNews.Size = new System.Drawing.Size(54, 17);
            this.rbtNews.TabIndex = 17;
            this.rbtNews.TabStop = true;
            this.rbtNews.Text = "News";
            this.rbtNews.UseVisualStyleBackColor = true;
            this.rbtNews.CheckedChanged += new System.EventHandler(this.rbtNews_CheckedChanged);
            // 
            // rbtFacilities
            // 
            this.rbtFacilities.AutoSize = true;
            this.rbtFacilities.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.rbtFacilities.ForeColor = System.Drawing.Color.White;
            this.rbtFacilities.Location = new System.Drawing.Point(261, 19);
            this.rbtFacilities.Name = "rbtFacilities";
            this.rbtFacilities.Size = new System.Drawing.Size(69, 17);
            this.rbtFacilities.TabIndex = 18;
            this.rbtFacilities.TabStop = true;
            this.rbtFacilities.Text = "Facilities";
            this.rbtFacilities.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(29, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Title";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(29, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Message";
            // 
            // panel1
            // 
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.rbtspecialOffers);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtTitle);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtDescription);
            this.panel1.Controls.Add(this.rbtFacilities);
            this.panel1.Controls.Add(this.rbtNews);
            this.panel1.Location = new System.Drawing.Point(21, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(422, 286);
            this.panel1.TabIndex = 21;
            this.panel1.Tag = "";
            // 
            // Web
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(792, 435);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pic3Image);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Web";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Tag = "";
            this.Text = "Web Admin";
            this.ThemeName = "Office2007Black";
            ((System.ComponentModel.ISupportInitialize)(this.pic3Image)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pic3Image;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.RadioButton rbtspecialOffers;
        private System.Windows.Forms.RadioButton rbtNews;
        private System.Windows.Forms.RadioButton rbtFacilities;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private System.Windows.Forms.Panel panel1;
    }
}

