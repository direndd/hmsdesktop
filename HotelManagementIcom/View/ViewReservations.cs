using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using HotelManagementIcom.Model;
using System.Collections;
using System.Linq;
using HotelManagementIcom.Controller;

namespace HotelManagementIcom.View
{
    public partial class ViewReservations : Telerik.WinControls.UI.RadForm
    {
        hotelmsEntities1 hms = new hotelmsEntities1();
        //user user1 = new user();
        public ViewReservations()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            
        }

        private void ViewReservations_Load(object sender, EventArgs e)
        {
            SettingsControl set = new SettingsControl();
            this.ThemeName = set.getTheme();
            this.BackColor = System.Drawing.Color.FromName(set.getColor());

            hotelmsEntities1 hms = new hotelmsEntities1();
            dataGridViewReservations.AutoGenerateColumns = true;
            IEnumerable<roomre> bookedroms = hms.roomres;
            //projecting the fields
            var allReservations = hms.roomres.Select(i => new { i.reservationId, i.room.roomNo, i.reservation.customer.nic, i.reservation.customer.fName, last_Nname = i.reservation.customer.lName, i.startDate, i.endDate, i.noOfAdults, i.noOfChildren, i.reservation.invoiceId, i.reservation.invoice.total, i.reservation.invoice.balance });
            dataGridViewReservations.DataSource = allReservations;
            
  

        }

        private void resGrid_Click(object sender, EventArgs e)
        {
            
        }

        private void resGrid_SelectionChanged(object sender, EventArgs e)
        {
           
        }

        private void cmbCusNicS_SelectedIndexChanged(object sender, EventArgs e)
        {


            var list = from cust2 in hms.customers where cust2.nic == txtBoxNic.Text select cust2;
            StringBuilder name = new StringBuilder();
            StringBuilder passport = new StringBuilder();
            StringBuilder nic = new StringBuilder();

            foreach (customer s in list)
            {

                name.Append(s.fName + Environment.NewLine);
                nic.Append(s.cusId + Environment.NewLine);
                passport.Append(s.passport + Environment.NewLine);
            }

            //cmbCusNicS.Text = nic.ToString();
            txtBoxCustNameS.Text = name.ToString();
            txtBoxPassPortS.Text = passport.ToString();
        }

        private void txtBoxCustNameS_TextChanged(object sender, EventArgs e)
        {


            try
            {
                   hotelmsEntities1 hms2 = new hotelmsEntities1();
            dataGridViewReservations.AutoGenerateColumns = true;
            IEnumerable<roomre> bookedroms = hms2.roomres;
            //projecting the fields
            var allReservations = hms2.roomres.Where(ii => ii.reservation.customer.fName == txtBoxCustNameS.Text).Select(i => new { i.reservationId, i.room.roomNo, i.reservation.customer.nic, i.reservation.customer.fName, last_Nname = i.reservation.customer.lName, i.startDate, i.endDate, i.noOfAdults, i.noOfChildren, i.reservation.invoiceId, i.reservation.invoice.total, i.reservation.invoice.balance });
            dataGridViewReservations.DataSource = allReservations;
            }
            catch (Exception)
            {
                
             
            }

        
        }

        private void vtnExitS_Click(object sender, EventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void txtBoxNic_TextChanged(object sender, EventArgs e)
        {      var list = from cust2 in hms.customers where cust2.nic == txtBoxNic.Text select cust2;
            StringBuilder name = new StringBuilder();
            StringBuilder passport = new StringBuilder();
            StringBuilder nic = new StringBuilder();

            foreach (customer s in list)
            {

                name.Append(s.fName + Environment.NewLine);
                nic.Append(s.cusId + Environment.NewLine);
                passport.Append(s.passport + Environment.NewLine);
            }

            //cmbCusNicS.Text = nic.ToString();
            txtBoxCustNameS.Text = name.ToString();
            txtBoxPassPortS.Text = passport.ToString();
        

            try
            {
                hotelmsEntities1 hms2 = new hotelmsEntities1();
                dataGridViewReservations.AutoGenerateColumns = true;
                IEnumerable<roomre> bookedroms = hms2.roomres;
                //projecting the fields
                var allReservations = hms2.roomres.Where(ii => ii.reservation.customer.nic == txtBoxNic.Text).Select(i => new { i.reservationId, i.room.roomNo, i.reservation.customer.fName, last_Nname = i.reservation.customer.lName, i.startDate, i.endDate, i.noOfAdults, i.noOfChildren, i.reservation.invoiceId, i.reservation.invoice.total, i.reservation.invoice.balance });
                dataGridViewReservations.DataSource = allReservations;
            }
            catch (Exception)
            {


            }

        }

        private void txtBoxPassPortS_TextChanged(object sender, EventArgs e)
        {
   
            //txtBoxPassPortS.Text = "";
            try
            {
                hotelmsEntities1 hms2 = new hotelmsEntities1();
                dataGridViewReservations.AutoGenerateColumns = true;
                IEnumerable<roomre> bookedroms = hms2.roomres;
                //projecting the fields
                var allReservations = hms2.roomres.Where(ii => ii.reservation.customer.passport == txtBoxPassPortS.Text).Select(i => new { i.reservationId, i.room.roomNo, i.reservation.customer.fName, last_Nname = i.reservation.customer.lName, i.startDate, i.endDate, i.noOfAdults, i.noOfChildren, i.reservation.invoiceId, i.reservation.invoice.total, i.reservation.invoice.balance });
                dataGridViewReservations.DataSource = allReservations;
            }
            catch (Exception)
            {


            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            hotelmsEntities1 hms = new hotelmsEntities1();
            dataGridViewReservations.AutoGenerateColumns = true;
            IEnumerable<roomre> bookedroms = hms.roomres;
            //projecting the fields
            var allReservations = hms.roomres.Select(i => new { i.reservationId, i.room.roomNo, i.reservation.customer.nic, i.reservation.customer.fName, last_Nname = i.reservation.customer.lName, i.startDate, i.endDate, i.noOfAdults, i.noOfChildren, i.reservation.invoiceId, i.reservation.invoice.total, i.reservation.invoice.balance });
            dataGridViewReservations.DataSource = allReservations;
            
        }

        private void dataGridViewReservations_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                string id = dataGridViewReservations.Rows[e.RowIndex].Cells[2].Value.ToString();
                                  
                    txtBoxNic.Text = id;          
                
            }

            catch (Exception)
            {



            }
 
        }
    }
}
