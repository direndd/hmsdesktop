using HotelManagementIcom.Controller;
namespace HotelManagementIcom.View
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.radDesktopAlert1 = new Telerik.WinControls.UI.RadDesktopAlert(this.components);
            this.lblHotelName = new System.Windows.Forms.Label();
            this.fdlgRead = new System.Windows.Forms.OpenFileDialog();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.lblContact = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblWeb = new System.Windows.Forms.Label();
            this.picBoxLogo = new System.Windows.Forms.PictureBox();
            this.lblTip = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.btnRoomMgmt = new System.Windows.Forms.Button();
            this.btnUserMgmt = new System.Windows.Forms.Button();
            this.btnRoomServ = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.btnPayments = new System.Windows.Forms.Button();
            this.btnChekOut = new System.Windows.Forms.Button();
            this.btnReservation = new System.Windows.Forms.Button();
            this.btnViewRes = new System.Windows.Forms.Button();
            this.btnLogHistory = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblUserLevel = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblLogout = new System.Windows.Forms.LinkLabel();
            this.lblUserName = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.office2007BlackTheme2 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.btnDepartureLst = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnArrivalLst = new System.Windows.Forms.Button();
            this.btnGuestMgmt = new System.Windows.Forms.Button();
            this.btnBookingLst = new System.Windows.Forms.Button();
            this.radDesktopAlert2 = new Telerik.WinControls.UI.RadDesktopAlert(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel7 = new System.Windows.Forms.Panel();
            this.office2007Black = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.vista = new Telerik.WinControls.Themes.VistaTheme();
            this.office2010 = new Telerik.WinControls.Themes.Office2010Theme();
            this.telerik = new Telerik.WinControls.Themes.TelerikTheme();
            this.aqua = new Telerik.WinControls.Themes.AquaTheme();
            this.desert = new Telerik.WinControls.Themes.DesertTheme();
            this.breeze = new Telerik.WinControls.Themes.BreezeTheme();
            this.office2007SilverTheme1 = new Telerik.WinControls.Themes.Office2007SilverTheme();
            this.windows7Theme1 = new Telerik.WinControls.Themes.Windows7Theme();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogo)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radDesktopAlert1
            // 
            this.radDesktopAlert1.ContentImage = null;
            this.radDesktopAlert1.PlaySound = false;
            this.radDesktopAlert1.SoundToPlay = null;
            this.radDesktopAlert1.ThemeName = null;
            // 
            // lblHotelName
            // 
            this.lblHotelName.AutoSize = true;
            this.lblHotelName.BackColor = System.Drawing.Color.Transparent;
            this.lblHotelName.Font = new System.Drawing.Font("Microsoft NeoGothic", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHotelName.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblHotelName.Location = new System.Drawing.Point(171, 8);
            this.lblHotelName.Name = "lblHotelName";
            this.lblHotelName.Size = new System.Drawing.Size(214, 49);
            this.lblHotelName.TabIndex = 5;
            this.lblHotelName.Text = "Hotel Name";
            // 
            // fdlgRead
            // 
            this.fdlgRead.FileName = "openFileDialog1";
            // 
            // lblContact
            // 
            this.lblContact.AutoSize = true;
            this.lblContact.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContact.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblContact.Location = new System.Drawing.Point(522, 85);
            this.lblContact.Name = "lblContact";
            this.lblContact.Size = new System.Drawing.Size(89, 19);
            this.lblContact.TabIndex = 46;
            this.lblContact.Text = "0112823510";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblEmail.Location = new System.Drawing.Point(176, 85);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(46, 19);
            this.lblEmail.TabIndex = 45;
            this.lblEmail.Text = "Email";
            // 
            // lblWeb
            // 
            this.lblWeb.AutoSize = true;
            this.lblWeb.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeb.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblWeb.Location = new System.Drawing.Point(176, 57);
            this.lblWeb.Name = "lblWeb";
            this.lblWeb.Size = new System.Drawing.Size(139, 19);
            this.lblWeb.TabIndex = 44;
            this.lblWeb.Text = "www.hotelweb.com";
            // 
            // picBoxLogo
            // 
            this.picBoxLogo.Image = ((System.Drawing.Image)(resources.GetObject("picBoxLogo.Image")));
            this.picBoxLogo.Location = new System.Drawing.Point(12, 9);
            this.picBoxLogo.Name = "picBoxLogo";
            this.picBoxLogo.Size = new System.Drawing.Size(139, 101);
            this.picBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxLogo.TabIndex = 4;
            this.picBoxLogo.TabStop = false;
            // 
            // lblTip
            // 
            this.lblTip.AutoSize = true;
            this.lblTip.Font = new System.Drawing.Font("Franklin Gothic Book", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTip.ForeColor = System.Drawing.SystemColors.Info;
            this.lblTip.Location = new System.Drawing.Point(15, 383);
            this.lblTip.MaximumSize = new System.Drawing.Size(300, 0);
            this.lblTip.Name = "lblTip";
            this.lblTip.Size = new System.Drawing.Size(257, 42);
            this.lblTip.TabIndex = 41;
            this.lblTip.Text = "Create and Manage Reservations for Guests";
            this.lblTip.Visible = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.btnRoomMgmt);
            this.panel3.Controls.Add(this.btnUserMgmt);
            this.panel3.Controls.Add(this.btnRoomServ);
            this.panel3.Controls.Add(this.btnSettings);
            this.panel3.Location = new System.Drawing.Point(660, 136);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(300, 432);
            this.panel3.TabIndex = 48;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe WP SemiLight", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(11, 2);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(186, 36);
            this.label5.TabIndex = 47;
            this.label5.Text = "Administration";
            // 
            // btnRoomMgmt
            // 
            this.btnRoomMgmt.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnRoomMgmt.FlatAppearance.BorderSize = 0;
            this.btnRoomMgmt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRoomMgmt.Font = new System.Drawing.Font("Microsoft NeoGothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRoomMgmt.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnRoomMgmt.Location = new System.Drawing.Point(17, 41);
            this.btnRoomMgmt.Name = "btnRoomMgmt";
            this.btnRoomMgmt.Size = new System.Drawing.Size(120, 120);
            this.btnRoomMgmt.TabIndex = 10;
            this.btnRoomMgmt.Text = "Room Management";
            this.btnRoomMgmt.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnRoomMgmt.UseVisualStyleBackColor = false;
            this.btnRoomMgmt.Click += new System.EventHandler(this.btnRoomMgmt_Click);
            // 
            // btnUserMgmt
            // 
            this.btnUserMgmt.BackColor = System.Drawing.Color.Orange;
            this.btnUserMgmt.FlatAppearance.BorderSize = 0;
            this.btnUserMgmt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUserMgmt.Font = new System.Drawing.Font("Microsoft NeoGothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUserMgmt.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnUserMgmt.Location = new System.Drawing.Point(143, 41);
            this.btnUserMgmt.Name = "btnUserMgmt";
            this.btnUserMgmt.Size = new System.Drawing.Size(120, 120);
            this.btnUserMgmt.TabIndex = 11;
            this.btnUserMgmt.Text = "User Management";
            this.btnUserMgmt.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnUserMgmt.UseVisualStyleBackColor = false;
            this.btnUserMgmt.Click += new System.EventHandler(this.btnUserMgmt_Click);
            // 
            // btnRoomServ
            // 
            this.btnRoomServ.BackColor = System.Drawing.Color.DarkGreen;
            this.btnRoomServ.FlatAppearance.BorderSize = 0;
            this.btnRoomServ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRoomServ.Font = new System.Drawing.Font("Microsoft NeoGothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRoomServ.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnRoomServ.Location = new System.Drawing.Point(17, 167);
            this.btnRoomServ.Name = "btnRoomServ";
            this.btnRoomServ.Size = new System.Drawing.Size(120, 120);
            this.btnRoomServ.TabIndex = 12;
            this.btnRoomServ.Text = "Room Services";
            this.btnRoomServ.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnRoomServ.UseVisualStyleBackColor = false;
            this.btnRoomServ.Click += new System.EventHandler(this.btnRoomServ_Click);
            // 
            // btnSettings
            // 
            this.btnSettings.BackColor = System.Drawing.Color.Crimson;
            this.btnSettings.FlatAppearance.BorderSize = 0;
            this.btnSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSettings.Font = new System.Drawing.Font("Microsoft NeoGothic", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSettings.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSettings.Location = new System.Drawing.Point(17, 293);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(246, 125);
            this.btnSettings.TabIndex = 13;
            this.btnSettings.Text = "Settings";
            this.btnSettings.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnSettings.UseVisualStyleBackColor = false;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.btnPayments);
            this.panel1.Controls.Add(this.btnChekOut);
            this.panel1.Controls.Add(this.btnReservation);
            this.panel1.Controls.Add(this.btnViewRes);
            this.panel1.Location = new System.Drawing.Point(12, 135);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(300, 432);
            this.panel1.TabIndex = 46;
            this.panel1.Tag = "Reservation";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe WP SemiLight", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(9, -2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(153, 36);
            this.label3.TabIndex = 44;
            this.label3.Text = "Reservation";
            // 
            // btnPayments
            // 
            this.btnPayments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnPayments.FlatAppearance.BorderSize = 0;
            this.btnPayments.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPayments.Font = new System.Drawing.Font("Microsoft NeoGothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPayments.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnPayments.Location = new System.Drawing.Point(15, 165);
            this.btnPayments.Name = "btnPayments";
            this.btnPayments.Size = new System.Drawing.Size(120, 120);
            this.btnPayments.TabIndex = 2;
            this.btnPayments.Text = "Payments";
            this.btnPayments.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnPayments.UseVisualStyleBackColor = false;
            this.btnPayments.Click += new System.EventHandler(this.btnPayments_Click);
            // 
            // btnChekOut
            // 
            this.btnChekOut.BackColor = System.Drawing.Color.Green;
            this.btnChekOut.FlatAppearance.BorderSize = 0;
            this.btnChekOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChekOut.Font = new System.Drawing.Font("Microsoft NeoGothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChekOut.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnChekOut.Location = new System.Drawing.Point(141, 165);
            this.btnChekOut.Name = "btnChekOut";
            this.btnChekOut.Size = new System.Drawing.Size(120, 120);
            this.btnChekOut.TabIndex = 3;
            this.btnChekOut.Text = "Checkout";
            this.btnChekOut.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnChekOut.UseVisualStyleBackColor = false;
            this.btnChekOut.Click += new System.EventHandler(this.btnChekOut_Click);
            // 
            // btnReservation
            // 
            this.btnReservation.BackColor = System.Drawing.Color.Purple;
            this.btnReservation.FlatAppearance.BorderSize = 0;
            this.btnReservation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReservation.Font = new System.Drawing.Font("Microsoft NeoGothic", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReservation.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnReservation.Location = new System.Drawing.Point(15, 34);
            this.btnReservation.Name = "btnReservation";
            this.btnReservation.Size = new System.Drawing.Size(246, 125);
            this.btnReservation.TabIndex = 1;
            this.btnReservation.Text = "Reservation";
            this.btnReservation.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnReservation.UseVisualStyleBackColor = false;
            this.btnReservation.Click += new System.EventHandler(this.btnReservation_Click);
            // 
            // btnViewRes
            // 
            this.btnViewRes.BackColor = System.Drawing.Color.SlateBlue;
            this.btnViewRes.FlatAppearance.BorderSize = 0;
            this.btnViewRes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewRes.Font = new System.Drawing.Font("Microsoft NeoGothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewRes.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnViewRes.Location = new System.Drawing.Point(15, 291);
            this.btnViewRes.Name = "btnViewRes";
            this.btnViewRes.Size = new System.Drawing.Size(120, 120);
            this.btnViewRes.TabIndex = 4;
            this.btnViewRes.Text = "View Resevations";
            this.btnViewRes.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnViewRes.UseVisualStyleBackColor = false;
            this.btnViewRes.Click += new System.EventHandler(this.btnViewRes_Click);
            // 
            // btnLogHistory
            // 
            this.btnLogHistory.BackColor = System.Drawing.Color.DimGray;
            this.btnLogHistory.FlatAppearance.BorderSize = 0;
            this.btnLogHistory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogHistory.Font = new System.Drawing.Font("Microsoft NeoGothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogHistory.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnLogHistory.Location = new System.Drawing.Point(143, 296);
            this.btnLogHistory.Name = "btnLogHistory";
            this.btnLogHistory.Size = new System.Drawing.Size(120, 120);
            this.btnLogHistory.TabIndex = 9;
            this.btnLogHistory.Text = "Log History";
            this.btnLogHistory.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnLogHistory.UseVisualStyleBackColor = false;
            this.btnLogHistory.Click += new System.EventHandler(this.btnLogHistory_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Azure;
            this.panel5.Location = new System.Drawing.Point(324, 151);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(2, 390);
            this.panel5.TabIndex = 50;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.Controls.Add(this.picBoxLogo);
            this.panel4.Controls.Add(this.lblContact);
            this.panel4.Controls.Add(this.lblUserLevel);
            this.panel4.Controls.Add(this.lblEmail);
            this.panel4.Controls.Add(this.lblTime);
            this.panel4.Controls.Add(this.lblWeb);
            this.panel4.Controls.Add(this.lblLogout);
            this.panel4.Controls.Add(this.lblUserName);
            this.panel4.Controls.Add(this.lblHotelName);
            this.panel4.Font = new System.Drawing.Font("AR DECODE", 8.25F);
            this.panel4.Location = new System.Drawing.Point(0, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(972, 115);
            this.panel4.TabIndex = 49;
            // 
            // lblUserLevel
            // 
            this.lblUserLevel.AutoSize = true;
            this.lblUserLevel.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserLevel.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblUserLevel.Location = new System.Drawing.Point(639, 85);
            this.lblUserLevel.Name = "lblUserLevel";
            this.lblUserLevel.Size = new System.Drawing.Size(76, 19);
            this.lblUserLevel.TabIndex = 44;
            this.lblUserLevel.Text = "User Level";
            // 
            // lblTime
            // 
            this.lblTime.BackColor = System.Drawing.Color.Transparent;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Tai Le", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblTime.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTime.Location = new System.Drawing.Point(626, 1);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(334, 62);
            this.lblTime.TabIndex = 43;
            this.lblTime.Text = "Time";
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLogout
            // 
            this.lblLogout.AutoSize = true;
            this.lblLogout.Font = new System.Drawing.Font("Microsoft Tai Le", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogout.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblLogout.Location = new System.Drawing.Point(879, 75);
            this.lblLogout.Name = "lblLogout";
            this.lblLogout.Size = new System.Drawing.Size(81, 29);
            this.lblLogout.TabIndex = 100;
            this.lblLogout.TabStop = true;
            this.lblLogout.Text = "Logout";
            this.lblLogout.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblLogout_LinkClicked);
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblUserName.Location = new System.Drawing.Point(736, 85);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(82, 19);
            this.lblUserName.TabIndex = 45;
            this.lblUserName.Text = "User Name";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Azure;
            this.panel6.Location = new System.Drawing.Point(648, 151);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(2, 390);
            this.panel6.TabIndex = 51;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Segoe WP SemiLight", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label10.Location = new System.Drawing.Point(11, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 36);
            this.label10.TabIndex = 45;
            this.label10.Text = "Reports";
            // 
            // btnDepartureLst
            // 
            this.btnDepartureLst.BackColor = System.Drawing.Color.Firebrick;
            this.btnDepartureLst.FlatAppearance.BorderSize = 0;
            this.btnDepartureLst.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDepartureLst.Font = new System.Drawing.Font("Microsoft NeoGothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDepartureLst.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDepartureLst.Location = new System.Drawing.Point(143, 39);
            this.btnDepartureLst.Name = "btnDepartureLst";
            this.btnDepartureLst.Size = new System.Drawing.Size(120, 120);
            this.btnDepartureLst.TabIndex = 6;
            this.btnDepartureLst.Text = "Departure List";
            this.btnDepartureLst.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnDepartureLst.UseVisualStyleBackColor = false;
            this.btnDepartureLst.Click += new System.EventHandler(this.btnDepartureLst_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.btnLogHistory);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.btnDepartureLst);
            this.panel2.Controls.Add(this.btnArrivalLst);
            this.panel2.Controls.Add(this.btnGuestMgmt);
            this.panel2.Controls.Add(this.btnBookingLst);
            this.panel2.Location = new System.Drawing.Point(338, 135);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(300, 432);
            this.panel2.TabIndex = 47;
            // 
            // btnArrivalLst
            // 
            this.btnArrivalLst.BackColor = System.Drawing.Color.Teal;
            this.btnArrivalLst.FlatAppearance.BorderSize = 0;
            this.btnArrivalLst.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnArrivalLst.Font = new System.Drawing.Font("Microsoft NeoGothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnArrivalLst.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnArrivalLst.Location = new System.Drawing.Point(17, 39);
            this.btnArrivalLst.Name = "btnArrivalLst";
            this.btnArrivalLst.Size = new System.Drawing.Size(120, 120);
            this.btnArrivalLst.TabIndex = 5;
            this.btnArrivalLst.Text = "Arrival List";
            this.btnArrivalLst.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnArrivalLst.UseVisualStyleBackColor = false;
            this.btnArrivalLst.Click += new System.EventHandler(this.btnArrivalLst_Click);
            // 
            // btnGuestMgmt
            // 
            this.btnGuestMgmt.BackColor = System.Drawing.Color.OrangeRed;
            this.btnGuestMgmt.FlatAppearance.BorderSize = 0;
            this.btnGuestMgmt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuestMgmt.Font = new System.Drawing.Font("Microsoft NeoGothic", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuestMgmt.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnGuestMgmt.Location = new System.Drawing.Point(17, 165);
            this.btnGuestMgmt.Name = "btnGuestMgmt";
            this.btnGuestMgmt.Size = new System.Drawing.Size(246, 125);
            this.btnGuestMgmt.TabIndex = 7;
            this.btnGuestMgmt.Text = "Guest Management";
            this.btnGuestMgmt.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnGuestMgmt.UseVisualStyleBackColor = false;
            this.btnGuestMgmt.Click += new System.EventHandler(this.btnGuestMgmt_Click);
            // 
            // btnBookingLst
            // 
            this.btnBookingLst.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnBookingLst.FlatAppearance.BorderSize = 0;
            this.btnBookingLst.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBookingLst.Font = new System.Drawing.Font("Microsoft NeoGothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBookingLst.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnBookingLst.Location = new System.Drawing.Point(17, 296);
            this.btnBookingLst.Name = "btnBookingLst";
            this.btnBookingLst.Size = new System.Drawing.Size(120, 120);
            this.btnBookingLst.TabIndex = 8;
            this.btnBookingLst.Text = "Booking List";
            this.btnBookingLst.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnBookingLst.UseVisualStyleBackColor = false;
            this.btnBookingLst.Click += new System.EventHandler(this.btnBookingLst_Click);
            // 
            // radDesktopAlert2
            // 
            this.radDesktopAlert2.ContentImage = null;
            this.radDesktopAlert2.PlaySound = false;
            this.radDesktopAlert2.SoundToPlay = null;
            this.radDesktopAlert2.ThemeName = null;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Azure;
            this.panel7.Location = new System.Drawing.Point(22, 123);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(940, 2);
            this.panel7.TabIndex = 52;
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(972, 581);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.lblTip);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("Microsoft Tai Le", 12F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(980, 615);
            this.MinimumSize = new System.Drawing.Size(980, 615);
            this.Name = "Home";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(980, 615);
            this.RootElement.MinSize = new System.Drawing.Size(980, 615);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Icom Casa HMS";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.Home_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogo)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.UI.RadDesktopAlert radDesktopAlert1;
        private System.Windows.Forms.PictureBox picBoxLogo;
        private System.Windows.Forms.Label lblHotelName;
        private System.Windows.Forms.OpenFileDialog fdlgRead;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label lblContact;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblWeb;
        private System.Windows.Forms.Label lblTip;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnRoomMgmt;
        private System.Windows.Forms.Button btnUserMgmt;
        private System.Windows.Forms.Button btnRoomServ;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnPayments;
        private System.Windows.Forms.Button btnChekOut;
        private System.Windows.Forms.Button btnReservation;
        private System.Windows.Forms.Button btnViewRes;
        private System.Windows.Forms.Button btnLogHistory;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblUserLevel;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.LinkLabel lblLogout;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label10;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme2;
        private System.Windows.Forms.Button btnDepartureLst;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnArrivalLst;
        private System.Windows.Forms.Button btnGuestMgmt;
        private System.Windows.Forms.Button btnBookingLst;
        private Telerik.WinControls.UI.RadDesktopAlert radDesktopAlert2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel7;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007Black;
        private Telerik.WinControls.Themes.VistaTheme vista;
        private Telerik.WinControls.Themes.Office2010Theme office2010;
        private Telerik.WinControls.Themes.TelerikTheme telerik;
        private Telerik.WinControls.Themes.AquaTheme aqua;
        private Telerik.WinControls.Themes.DesertTheme desert;
        private Telerik.WinControls.Themes.BreezeTheme breeze;
        private Telerik.WinControls.Themes.Office2007SilverTheme office2007SilverTheme1;
        private Telerik.WinControls.Themes.Windows7Theme windows7Theme1;

    }
}

