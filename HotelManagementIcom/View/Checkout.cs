using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using HotelManagementIcom.Controller;
using HotelManagementIcom.Model;
using System.Linq;

namespace HotelManagementIcom.View
{
    public partial class Checkout : Telerik.WinControls.UI.RadForm
    {

        public string PaymentIDChk;
        
        hotelmsEntities1 hms = new hotelmsEntities1();
        public Checkout()
        {
            InitializeComponent();
            loadGrdChekout(hms);

            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = 1000;
            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            string[] Time = DateTime.Now.ToString().Split(' ');
            lblTime.Text = Time[1] + " " + Time[2];
        }

        private void grdReservations_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }      
        private void txtBoxResId_TextChanged(object sender, EventArgs e)
        {
            clearTextFields();
            fillTextFields(txtBoxResId.Text.Trim());
            
        }

        private void clearTextFields()
        {
            txtBoxCusId.Text = "";
            txtBalance.Text = "";
            txtBoxCustName.Text = "";
            cmbCusNic.Text = "";
            cmbpaymentType.Text = "";
            txtBoxPassPort.Text = "";
            txtInvoiceID.Text = "";
            txtPayingAmt.Text = "";
            txtPaymentID.Text = "";
            txtTotAmount.Text = "";
            txtBoxdiscount.Text = "";
            txtDisAmount.Text = "";
        }

    
        private void fillTextFields(string resId)
        {
            hotelmsEntities1 hms = new hotelmsEntities1();
            var list = from res in hms.reservations where res.reservationId == resId select res;
            StringBuilder nic = new StringBuilder();
            StringBuilder name = new StringBuilder();
            StringBuilder id = new StringBuilder();
            StringBuilder passport = new StringBuilder();
            StringBuilder gender = new StringBuilder();
            StringBuilder Nationalty = new StringBuilder();

            StringBuilder chkInDate = new StringBuilder();
            StringBuilder chkOutDate = new StringBuilder();
            StringBuilder roomType = new StringBuilder();
            StringBuilder adults = new StringBuilder();
            StringBuilder children = new StringBuilder();

            StringBuilder InvoiceId = new StringBuilder();
            StringBuilder balance = new StringBuilder();
            //StringBuilder roomType = new StringBuilder();
            //StringBuilder adults = new StringBuilder();
            //StringBuilder children = new StringBuilder();

            try
            {
                //Customer details filling     

                foreach (reservation s in list)
                {
                    nic.Append(s.customer.nic + Environment.NewLine);
                    name.Append(s.customer.fName + Environment.NewLine);
                    id.Append(s.customer.cusId + Environment.NewLine);
                    passport.Append(s.customer.passport + Environment.NewLine);
                    gender.Append(s.customer.gender + Environment.NewLine);
                    Nationalty.Append(s.customer.nationalty + Environment.NewLine);

                    //chkInDate.Append(s.roomres.s + Environment.NewLine);
                    //chkOutDate.Append(s.gender + Environment.NewLine);
                    //roomType.Append(s.gender + Environment.NewLine);
                    //adults.Append(s.gender + Environment.NewLine);
                    //children.Append(s.gender + Environment.NewLine);

                    InvoiceId.Append(s.invoice.invoiceId + Environment.NewLine);
                    balance.Append(s.invoice.balance + Environment.NewLine);


                    //setting details 
                    txtBoxCusId.Text = id.ToString();
                    txtBoxCustName.Text = Name.ToString();
                    txtBoxPassPort.Text = passport.ToString();
                    cmbCusNic.Text = nic.ToString();
                    

                    
                    //datePickerCIn.Text = all.FirstOrDefault().startDate.ToString();
                    //datePickerCOut.Text = all.FirstOrDefault().endDate.ToString();
                    //cmbRoomType.Text = all.FirstOrDefault().roomType.ToString();
                    //txtBoxAdults.Text = all.FirstOrDefault().noOfAdults.ToString();
                    //txtBoxChildren.Text = all.FirstOrDefault().noOfChildren.ToString();

                    txtInvoiceID.Text = InvoiceId.ToString();
                    txtTotAmount.Text = balance.ToString();

                }
            }
            catch (Exception)
            {


            }
        }
        

        private void Checkout_Load(object sender, EventArgs e)
        {
            SettingsControl set = new SettingsControl();
            this.ThemeName = set.getTheme();
            this.BackColor = System.Drawing.Color.FromName(set.getColor());

            hotelmsEntities1 hmsUpdate = new hotelmsEntities1();
            loadGrdChekout(hmsUpdate);
        }

        private void loadGrdChekout(hotelmsEntities1 hms)
        {
            try
            {
                grdCheckout.AutoGenerateColumns = true;
                IEnumerable<roomre> bookedroms = hms.roomres.Where(i => i.cancel == false).Where(i => i.reservation.invoice.balance!=0);
                //projecting the fields
                var allReservations =
                    hms.roomres.Where(i => i.cancel == false).Where(i => i.reservation.invoice.balance!=0).Select(i => new
                    {
                        Reservation_ID=i.reservationId,
                        Room_No=i.room.roomNo,
                        Guest_First_Name=i.reservation.customer.fName,
                        Guest_Last_Name= i.reservation.customer.lName,
                        Start_Date=i.startDate,
                        End_Date=i.endDate,
                        No_Of_Adults=i.noOfAdults,
                        No_Of_Childs=i.noOfChildren,
                        Invoice_Id=i.reservation.invoiceId,
                        Total_Amount=i.reservation.invoice.total,
                        Balance=i.reservation.invoice.balance
                    });

                grdCheckout.DataSource = allReservations;
            }
            catch (Exception)
            {
                
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {    
            try
            {
            DialogResult result = MessageBox.Show("Are you sure you want to Cancel the Reservation ?", "Confirm Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {

                //Cancel Reservation
                roomre x = (from roo in hms.roomres where roo.reservationId == txtBoxResId.Text.Trim() select roo).First();

                x.cancel = false;

                hms.SaveChanges();

                hotelmsEntities1 hmsUpdate = new hotelmsEntities1();
                loadGrdChekout(hmsUpdate);

            }
            }
            catch (Exception)
            {
                
            }
        }

        private void btnCheckout_Click(object sender, EventArgs e)
        {
            try
            {
                string invoiceId = txtInvoiceID.Text.Trim();
                //Updating the record in Invoice Table
                invoice invo = (from inv in hms.invoices where inv.invoiceId == invoiceId select inv).First();
                invo.balance = Convert.ToDouble(txtBalance.Text);

            //Updating the record in Customer Table
              string custIDd=  txtBoxCusId.Text.Trim();
              customer custo = (from cus in hms.customers where cus.cusId == custIDd select cus).First();
            custo.checkInstatus = false;

           // roomre roore = (from roo in hms.roomres where roo.reservationId == txtBoxResId.Text.Trim() select roo).First();
           // roore.cancel = true;

            hms.SaveChanges();

            payment pay = new payment();
      
            //inserting a record to payment table
            string payIdd = Pay();
            pay.paymentId = payIdd;
            pay.paymentDate = System.DateTime.Today;
            pay.Amount = Convert.ToDouble(txtPayingAmt.Text);
            pay.paymentType = cmbpaymentType.Text;
            pay.userId = new LoggedUser().LogUser;
            pay.cusId = txtBoxCusId.Text.Trim();
            pay.invoiceId = txtInvoiceID.Text.Trim();
            hms.AddTopayments(pay);

           
            int statusPayment = hms.SaveChanges();

            if (statusPayment > 0)
            {
                MessageBox.Show("Payment Added", "Successful");
            }
            else 
            {
                MessageBox.Show("Payment unsuccesfull!", "Sorry");
            }
            hotelmsEntities1 hmsUpdate = new hotelmsEntities1();
            loadGrdChekout(hmsUpdate);

            clearTextFields();
            }
            catch (Exception)
            {
                 MessageBox.Show("Payment unsuccesfull!", "Sorry"); 
            }
        }

        private void txtPayingAmt_TextChanged(object sender, EventArgs e)
        {
            try
            {
                new Validation().validateTextDouble(sender, e);

                double totalAmount = Convert.ToDouble(txtTotAmount.Text.Trim());
            
            if (txtBoxdiscount.Text == "")
            {
                txtBalance.Text = Convert.ToString(totalAmount - Convert.ToDouble(txtPayingAmt.Text.Trim()));
            }
            else 
            {
                txtBalance.Text = Convert.ToString(Convert.ToDouble(txtDisAmount.Text.Trim()) - Convert.ToDouble(txtPayingAmt.Text.Trim()));
            }
            }
            catch (Exception)
            {
               
            } 
        }

        private void txtBoxdiscount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                new Validation().validateTextDouble(sender,e);

                double totalAmount = Convert.ToDouble(txtTotAmount.Text.Trim());
                double discount=Convert.ToDouble(txtBoxdiscount.Text.Trim());

                txtDisAmount.Text = Convert.ToString(totalAmount-(discount*totalAmount/100));
            }
            catch (Exception)
            {
                
                
            }
        }

        private void txtPaymentID_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                 string payId;
                int payMaxID = 0;
                int payIdSubString = 0;


                //Generating a new Payment ID by getting the maximum payment ID
                var payID = hms.payments.Select(i => new { i.paymentId });
                foreach (var item in payID)
                {
                    if (item.paymentId != string.Empty)
                    {
                        payId = item.paymentId.Substring(3);
                        payIdSubString = Int16.Parse(payId);
                        if (payMaxID < payIdSubString)
                        {
                            payMaxID = payIdSubString;
                        }
                    }

                    else
                    {
                        payMaxID = 0;
                    }

                }
                payMaxID++;
                payId = "pay" + payMaxID.ToString();
                //PaymentIDChk = payId;
                txtPaymentID.Text = payId;
            }
            catch (Exception)
            {
                
                
            }
        }

        private void grdCheckout_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                txtBoxResId.Text = grdCheckout.Rows[e.RowIndex].Cells[0].Value.ToString();
            }
            catch (Exception)
            {
               
            }
           
        }

        private void txtDisAmount_TextChanged(object sender, EventArgs e)
        {
            new Validation().validateTextDouble(sender, e);
        }

        private void txtBalance_TextChanged(object sender, EventArgs e)
        {
            new Validation().validateTextDouble(sender, e);
        }

        private void txtPaymentID_TextChanged(object sender, EventArgs e)
        {
            clearTextFields();
            fillTextFields(txtBoxResId.Text.Trim());
        }
        public string Pay()
        {
            string payId;
            int payMaxID = 0;
            int payIdSubString = 0;


            //Generating a new Payment ID by getting the maximum payment ID
            var payID = hms.payments.Select(i => new { i.paymentId });
            foreach (var item in payID)
            {
                if (item.paymentId != string.Empty)
                {
                    payId = item.paymentId.Substring(3);
                    payIdSubString = Int16.Parse(payId);
                    if (payMaxID < payIdSubString)
                    {
                        payMaxID = payIdSubString;
                    }
                }

                else
                {
                    payMaxID = 0;
                }

            }
            payMaxID++;
            payId = "pay" + payMaxID.ToString();
            //PaymentIDChk = payId;
            txtPaymentID.Text = payId;
            return payId;
        }

        private void btnChekOut_Click_1(object sender, EventArgs e)
        {
           
        }

        private void lblLogout_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string logID = new LoggedUser().LogHisId.ToString();
            String[] tokens3 = System.DateTime.Today.ToString().Split(' ');
            String onlyDate = tokens3[1];

            //log history record
            hotelmsEntities1 hmsLog = new hotelmsEntities1();
            logHistory log = hmsLog.logHistories.Where(iii => iii.logHistoryId == logID.Trim()).Single();
            log.logHistoryId = logID.ToString().Trim();
            log.logOutTime = System.DateTime.Now;
            hmsLog.SaveChanges();

            //clearing the detils 
            LoggedUser lu = new LoggedUser();
            lu.UserName1 = "";
            lu.UserLevel1 = "";
            lu.LogHisId = "";
            Login login = new Login();
            login.Show();
            this.Dispose();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                //Upadte 
                invoice invo = (from inv in hms.invoices where inv.invoiceId == txtInvoiceID.Text.Trim() select inv).First();

                invo.total = Convert.ToDouble(txtTotAmount.Text);

                hms.SaveChanges();

                hotelmsEntities1 hmsUpdate = new hotelmsEntities1();
                loadGrdChekout(hmsUpdate);
                MessageBox.Show("Update Succesful", "Successful");

            }
            catch (Exception)
            {
                MessageBox.Show("Payment unsuccesfull!", "Sorry"); 

            }
        }

        private void lblLogout_Click(object sender, EventArgs e)
        {
            string logID = new LoggedUser().LogHisId.ToString();
            String[] tokens3 = System.DateTime.Today.ToString().Split(' ');
            String onlyDate = tokens3[1];

            //log history record
            hotelmsEntities1 hmsLog = new hotelmsEntities1();
            logHistory log = hmsLog.logHistories.Where(iii => iii.logHistoryId == logID.Trim()).Single();
            log.logHistoryId = logID.ToString().Trim();
            log.logOutTime = System.DateTime.Now;
            hmsLog.SaveChanges();

            //clearing the detils 
            LoggedUser lu = new LoggedUser();
            lu.UserName1 = "";
            lu.UserLevel1 = "";
            lu.LogHisId = "";
            Login login = new Login();
            login.Show();
            this.Dispose();
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            Home h = new Home();
            h.Show();
            this.Dispose();
        }

        private void txtTotAmount_TextChanged(object sender, EventArgs e)
        {
            validateTextDouble(sender, e);
        }

        //validating to double
        private void validateTextDouble(object sender, EventArgs e)
        {
            Exception X = new Exception();

            TextBox T = (TextBox)sender;

            try
            {
                if (T.Text != "-")
                {
                    double x = double.Parse(T.Text);

                    if (T.Text.Contains(','))
                        throw X;
                }
            }
            catch (Exception)
            {
                try
                {
                    int CursorIndex = T.SelectionStart - 1;
                    T.Text = T.Text.Remove(CursorIndex, 1);

                    //Align Cursor to same index
                    T.SelectionStart = CursorIndex;
                    T.SelectionLength = 0;
                }
                catch (Exception) { }
            }
        }

        private void txtBalance_TextChanged_1(object sender, EventArgs e)
        {
            validateTextDouble(sender, e);
        }

        private void txtPayingAmt_TextChanged_1(object sender, EventArgs e)
        {
            validateTextDouble(sender, e);
        }

        private void txtDisAmount_TextChanged_1(object sender, EventArgs e)
        {
            validateTextDouble(sender, e);
        }

        private void txtBoxdiscount_TextChanged_1(object sender, EventArgs e)
        {
            validateTextDouble(sender, e);
        }

       
      
    }
}
