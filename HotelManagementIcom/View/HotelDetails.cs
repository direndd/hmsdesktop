using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using System.Xml;
using System.Text.RegularExpressions;
using System.IO;
using HotelManagementIcom.Controller;

namespace HotelManagementIcom.View
{
    public partial class HotelDetails : Telerik.WinControls.UI.RadForm
    {
        static string startPath = System.IO.Directory.GetCurrentDirectory();
        static string[] lines = Regex.Split(startPath, "bin");
        static string lpath = lines[0] + @"View\images\logoDefault.png";
        static string rpath = lines[0] + @"View\images\RepDefault.jpg";
        static string xmlPath = lines[0] + @"HotelDetails.xml";  // The Path to the ftpDet.Xml file //

        Validation Validate = new Validation();


        

        public HotelDetails()
        {
            InitializeComponent();
            
            /***
                Reading the HotelDetails.xml file and writing the data taken into text boxes.
             ***/

            
            if (File.Exists(xmlPath))
            {

                FileStream READER = new FileStream(xmlPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //Set up the filestream (READER) //
                System.Xml.XmlDocument HotelDetails = new System.Xml.XmlDocument();// Set up the XmlDocument //
                HotelDetails.Load(READER); //Load the data from the file into the XmlDocument  //
                System.Xml.XmlNodeList NodeList = HotelDetails.GetElementsByTagName("Hotel"); // Create a list of the nodes in the xml file //

                txtHotelName.Text = NodeList[0].FirstChild.ChildNodes[0].InnerText.ToString();
                txtWeb.Text = NodeList[0].FirstChild.ChildNodes[1].InnerText.ToString();
                txtContact.Text = NodeList[0].FirstChild.ChildNodes[2].InnerText.ToString();
                txtEmail.Text = NodeList[0].FirstChild.ChildNodes[3].InnerText.ToString();
                picBoxLogo.Image = Image.FromFile(NodeList[0].FirstChild.ChildNodes[4].InnerText.ToString());
                txtDolorRate.Text = NodeList[0].FirstChild.ChildNodes[5].InnerText.ToString();
                picBoxReports.Image = Image.FromFile(NodeList[0].FirstChild.ChildNodes[6].InnerText.ToString());

            }
            else 
            {
                picBoxLogo.Image = Image.FromFile(lpath);
                picBoxReports.Image = Image.FromFile(rpath);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            /*** 
                Validating the Email Adrreess
             ***/
            if (!Validate.validateEmail(txtEmail.Text)) 
            {
                lblWarning.Visible = true;
                
            }
            /*** 
                Validating the Web Site Url
            ***/
            else if (!Validate.validateWeb(txtWeb.Text))
            {
                lblWeb.Visible = true;
                
            }else{
                /*** 
                 Creating the HotelDetails.xml file and writing Data into it.
                 ***/
            using (XmlWriter writer = XmlWriter.Create(xmlPath))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Hotel");

                
                    writer.WriteStartElement("Hotel");

                    writer.WriteElementString("hotelName", txtHotelName.Text.ToString());
                    writer.WriteElementString("web", txtWeb.Text.ToString());
                    writer.WriteElementString("contact", txtContact.Text.ToString());
                    writer.WriteElementString("email", txtEmail.Text.ToString());
                    writer.WriteElementString("lpath", lpath.ToString());
                    writer.WriteElementString("dolor", txtDolorRate.Text.ToString());
                    writer.WriteElementString("rpath", rpath.ToString());

                    writer.WriteEndElement();
               
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }

            MessageBox.Show("Hotel Details Added Succesfully","Success!");
            Home home = new Home();
            home.Show();
            this.Dispose();
          }
        }

        

        private void btnBrowse_Click(object sender, EventArgs e)
        {

        }

        private void btnBrowse_Click_1(object sender, EventArgs e)
        {

            /*** 
             * Openning File Browser Dialog box and allowing user to select the image.
             * Setting it to Picture Box
             * 
             ***/
            String fileName = "";
            
            fdlgImage.Title = "Select the hotel logo";
            fdlgImage.FileName = "";
            fdlgImage.Filter= "Jpeg Images (*.jpg*)|*.jpg*|PNG Images (*.png*)|*.png*";

            if (fdlgImage.ShowDialog() != DialogResult.Cancel) 
            {
                fileName = fdlgImage.FileName.ToString();

                string startUpPath = System.IO.Directory.GetCurrentDirectory();
                string[] lines = Regex.Split(startUpPath, "bin");

                if (fileName.EndsWith(".jpg") || fileName.EndsWith(".JPEG"))
                {
                    lpath = lines[0] + @"View\images\logo.jpg";
                }
                else
                {
                    lpath = lines[0] + @"View\images\logo.png";
                }


                if (File.Exists(lpath))
                {
                    File.Delete(lpath);
                }

                File.Copy(fileName, lpath);
                picBoxLogo.Image = Image.FromFile(lpath);
            }

           
        }

        private void txtContact_TextChanged(object sender, EventArgs e)
        {
            /*** 
             Validating the Phone Number
             ***/
            new Validation().validateTextInteger(sender, e);
            if (!Validate.validatePhone(txtContact.Text))
            {

                lblContact.Visible = false;
            }
            else
            {
                lblContact.Visible=true;
                lblWarning.Visible = false;
            }
        }

        private void txtEmail_TextChanged(object sender, EventArgs e)
        {
            /*** 
             Validating the Email Address
             ***/
            if (!txtEmail.Text.ToString().Equals(""))
            {
                if (!Validate.validateEmail(txtEmail.Text))
                {
                    lblEmail.Visible = true;
                }
                else
                {
                    lblEmail.Visible = false;
                    lblWarning.Visible = false;
                }
            }

        }

        private void txtWeb_TextChanged(object sender, EventArgs e)
        {
            /*** 
             Validating the Web Site Url
             ***/
        
            if (!Validate.validateWeb(txtWeb.Text))
            {
                lblWeb.Visible = true;

            }
            else
            {
                lblWeb.Visible = false;
                lblWarning.Visible = false;
            }
        }

       
        
        private void btnMainBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                
            String fileName = "";

            fdlgImage.Title = "Select the Reports Image";
            fdlgImage.FileName = "";
            fdlgImage.Filter = "Jpeg Images (*.jpg*)|*.jpg*|PNG Images (*.png*)|*.png* | BMP Images (*.bmp*)|*.bmp* | GIF Images (*.gif*)|*.gif*";

            if (fdlgImage.ShowDialog() != DialogResult.Cancel)
            {
                fileName = fdlgImage.FileName.ToString();

                string startUpPath = System.IO.Directory.GetCurrentDirectory();
                string[] lines = Regex.Split(startUpPath, "bin");

                if (fileName.EndsWith(".jpg") || fileName.EndsWith(".JPEG"))
                {
                    rpath = lines[0] + @"View\images\RepDefault.jpg";
                }
                else if(fileName.EndsWith(".png"))
                {
                    rpath = lines[0] + @"View\images\RepDefault.png";
                }
                else if (fileName.EndsWith(".bmp"))
                {
                    rpath = lines[0] + @"View\images\RepDefault.bmp";
                }
                else if(fileName.EndsWith(".gif"))
                {
                    rpath = lines[0] + @"View\images\RepDefault.gif";
                }

                if (File.Exists(rpath))
                {
                    File.Delete(rpath);
                }

                File.Copy(fileName, rpath);
                picBoxReports.Image = Image.FromFile(rpath);
                
            }

           
            }
            catch (Exception)
            {
              
            }
        }

        private void HotelDetails_Load(object sender, EventArgs e)
        {
            SettingsControl set = new SettingsControl();
            this.ThemeName = set.getTheme();
            this.BackColor = System.Drawing.Color.FromName(set.getColor());

            if (File.Exists(xmlPath)) 
            {
                btnSkip.Enabled = false;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

    
        private void btnSkip_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Please Go to Administration > Hotel Details and enter details of your Hotel", "Note");
            Home home = new Home();
            home.Show();
            this.Visible = false;

        }
    }
}
