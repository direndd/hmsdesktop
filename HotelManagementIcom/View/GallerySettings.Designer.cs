namespace HotelManagementIcom.View
{
    partial class GallerySettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rchTxtLocation5 = new System.Windows.Forms.RichTextBox();
            this.rchTxtLocation6 = new System.Windows.Forms.RichTextBox();
            this.rchTxtLocation12 = new System.Windows.Forms.RichTextBox();
            this.rchTxtLocation11 = new System.Windows.Forms.RichTextBox();
            this.rchTxtLocation10 = new System.Windows.Forms.RichTextBox();
            this.rchTxtLocation9 = new System.Windows.Forms.RichTextBox();
            this.rchTxtLocation8 = new System.Windows.Forms.RichTextBox();
            this.rchTxtLocation7 = new System.Windows.Forms.RichTextBox();
            this.rchTxtLocation4 = new System.Windows.Forms.RichTextBox();
            this.rchTxtLocation3 = new System.Windows.Forms.RichTextBox();
            this.rchTxtLocation2 = new System.Windows.Forms.RichTextBox();
            this.rchTxtLocation1 = new System.Windows.Forms.RichTextBox();
            this.picBoxLocation11 = new System.Windows.Forms.PictureBox();
            this.picBoxLocation1 = new System.Windows.Forms.PictureBox();
            this.picBoxLocation12 = new System.Windows.Forms.PictureBox();
            this.picBoxLocation7 = new System.Windows.Forms.PictureBox();
            this.picBoxLocation10 = new System.Windows.Forms.PictureBox();
            this.picBoxLocation6 = new System.Windows.Forms.PictureBox();
            this.picBoxLocation2 = new System.Windows.Forms.PictureBox();
            this.picBoxLocation3 = new System.Windows.Forms.PictureBox();
            this.picBoxLocation4 = new System.Windows.Forms.PictureBox();
            this.picBoxLocation9 = new System.Windows.Forms.PictureBox();
            this.picBoxLocation5 = new System.Windows.Forms.PictureBox();
            this.picBoxLocation8 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.picBoxSlider7 = new System.Windows.Forms.PictureBox();
            this.picBoxSlider1 = new System.Windows.Forms.PictureBox();
            this.picBoxSlider2 = new System.Windows.Forms.PictureBox();
            this.picBoxSlider5 = new System.Windows.Forms.PictureBox();
            this.picBoxSlider9 = new System.Windows.Forms.PictureBox();
            this.picBoxSlider4 = new System.Windows.Forms.PictureBox();
            this.picBoxSlider3 = new System.Windows.Forms.PictureBox();
            this.picBoxSlider6 = new System.Windows.Forms.PictureBox();
            this.picBoxSlider12 = new System.Windows.Forms.PictureBox();
            this.picBoxSlider10 = new System.Windows.Forms.PictureBox();
            this.picBoxSlider11 = new System.Windows.Forms.PictureBox();
            this.picBoxSlider8 = new System.Windows.Forms.PictureBox();
            this.picBoxEnlarge = new System.Windows.Forms.PictureBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtSitePassword = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSiteUserName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSiteURL = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.richTxtBoxDescription = new System.Windows.Forms.RichTextBox();
            this.fileDialog = new System.Windows.Forms.OpenFileDialog();
            this.office2010 = new Telerik.WinControls.Themes.Office2010Theme();
            this.telerik = new Telerik.WinControls.Themes.TelerikTheme();
            this.aqua = new Telerik.WinControls.Themes.AquaTheme();
            this.desert = new Telerik.WinControls.Themes.DesertTheme();
            this.breeze = new Telerik.WinControls.Themes.BreezeTheme();
            this.office2007SilverTheme1 = new Telerik.WinControls.Themes.Office2007SilverTheme();
            this.windows7Theme1 = new Telerik.WinControls.Themes.Windows7Theme();
            this.vista = new Telerik.WinControls.Themes.VistaTheme();
            this.office2007Black = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.office2007BlackTheme2 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation8)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxEnlarge)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rchTxtLocation5);
            this.groupBox1.Controls.Add(this.rchTxtLocation6);
            this.groupBox1.Controls.Add(this.rchTxtLocation12);
            this.groupBox1.Controls.Add(this.rchTxtLocation11);
            this.groupBox1.Controls.Add(this.rchTxtLocation10);
            this.groupBox1.Controls.Add(this.rchTxtLocation9);
            this.groupBox1.Controls.Add(this.rchTxtLocation8);
            this.groupBox1.Controls.Add(this.rchTxtLocation7);
            this.groupBox1.Controls.Add(this.rchTxtLocation4);
            this.groupBox1.Controls.Add(this.rchTxtLocation3);
            this.groupBox1.Controls.Add(this.rchTxtLocation2);
            this.groupBox1.Controls.Add(this.rchTxtLocation1);
            this.groupBox1.Controls.Add(this.picBoxLocation11);
            this.groupBox1.Controls.Add(this.picBoxLocation1);
            this.groupBox1.Controls.Add(this.picBoxLocation12);
            this.groupBox1.Controls.Add(this.picBoxLocation7);
            this.groupBox1.Controls.Add(this.picBoxLocation10);
            this.groupBox1.Controls.Add(this.picBoxLocation6);
            this.groupBox1.Controls.Add(this.picBoxLocation2);
            this.groupBox1.Controls.Add(this.picBoxLocation3);
            this.groupBox1.Controls.Add(this.picBoxLocation4);
            this.groupBox1.Controls.Add(this.picBoxLocation9);
            this.groupBox1.Controls.Add(this.picBoxLocation5);
            this.groupBox1.Controls.Add(this.picBoxLocation8);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Location = new System.Drawing.Point(12, 321);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(948, 320);
            this.groupBox1.TabIndex = 43;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Locations images and descriptions (Please select only .png files)";
            // 
            // rchTxtLocation5
            // 
            this.rchTxtLocation5.Location = new System.Drawing.Point(630, 121);
            this.rchTxtLocation5.Name = "rchTxtLocation5";
            this.rchTxtLocation5.Size = new System.Drawing.Size(150, 40);
            this.rchTxtLocation5.TabIndex = 11;
            this.rchTxtLocation5.Text = "";
            // 
            // rchTxtLocation6
            // 
            this.rchTxtLocation6.Location = new System.Drawing.Point(786, 121);
            this.rchTxtLocation6.Name = "rchTxtLocation6";
            this.rchTxtLocation6.Size = new System.Drawing.Size(150, 40);
            this.rchTxtLocation6.TabIndex = 12;
            this.rchTxtLocation6.Text = "";
            // 
            // rchTxtLocation12
            // 
            this.rchTxtLocation12.Location = new System.Drawing.Point(786, 271);
            this.rchTxtLocation12.Name = "rchTxtLocation12";
            this.rchTxtLocation12.Size = new System.Drawing.Size(150, 40);
            this.rchTxtLocation12.TabIndex = 19;
            this.rchTxtLocation12.Text = "";
            // 
            // rchTxtLocation11
            // 
            this.rchTxtLocation11.Location = new System.Drawing.Point(630, 271);
            this.rchTxtLocation11.Name = "rchTxtLocation11";
            this.rchTxtLocation11.Size = new System.Drawing.Size(150, 40);
            this.rchTxtLocation11.TabIndex = 18;
            this.rchTxtLocation11.Text = "";
            // 
            // rchTxtLocation10
            // 
            this.rchTxtLocation10.Location = new System.Drawing.Point(474, 271);
            this.rchTxtLocation10.Name = "rchTxtLocation10";
            this.rchTxtLocation10.Size = new System.Drawing.Size(150, 40);
            this.rchTxtLocation10.TabIndex = 17;
            this.rchTxtLocation10.Text = "";
            // 
            // rchTxtLocation9
            // 
            this.rchTxtLocation9.Location = new System.Drawing.Point(318, 271);
            this.rchTxtLocation9.Name = "rchTxtLocation9";
            this.rchTxtLocation9.Size = new System.Drawing.Size(150, 40);
            this.rchTxtLocation9.TabIndex = 16;
            this.rchTxtLocation9.Text = "";
            // 
            // rchTxtLocation8
            // 
            this.rchTxtLocation8.Location = new System.Drawing.Point(162, 271);
            this.rchTxtLocation8.Name = "rchTxtLocation8";
            this.rchTxtLocation8.Size = new System.Drawing.Size(150, 40);
            this.rchTxtLocation8.TabIndex = 15;
            this.rchTxtLocation8.Text = "";
            // 
            // rchTxtLocation7
            // 
            this.rchTxtLocation7.Location = new System.Drawing.Point(6, 271);
            this.rchTxtLocation7.Name = "rchTxtLocation7";
            this.rchTxtLocation7.Size = new System.Drawing.Size(150, 40);
            this.rchTxtLocation7.TabIndex = 14;
            this.rchTxtLocation7.Text = "";
            // 
            // rchTxtLocation4
            // 
            this.rchTxtLocation4.Location = new System.Drawing.Point(474, 121);
            this.rchTxtLocation4.Name = "rchTxtLocation4";
            this.rchTxtLocation4.Size = new System.Drawing.Size(150, 40);
            this.rchTxtLocation4.TabIndex = 10;
            this.rchTxtLocation4.Text = "";
            // 
            // rchTxtLocation3
            // 
            this.rchTxtLocation3.Location = new System.Drawing.Point(318, 121);
            this.rchTxtLocation3.Name = "rchTxtLocation3";
            this.rchTxtLocation3.Size = new System.Drawing.Size(150, 40);
            this.rchTxtLocation3.TabIndex = 9;
            this.rchTxtLocation3.Text = "";
            // 
            // rchTxtLocation2
            // 
            this.rchTxtLocation2.Location = new System.Drawing.Point(162, 121);
            this.rchTxtLocation2.Name = "rchTxtLocation2";
            this.rchTxtLocation2.Size = new System.Drawing.Size(150, 40);
            this.rchTxtLocation2.TabIndex = 8;
            this.rchTxtLocation2.Text = "";
            // 
            // rchTxtLocation1
            // 
            this.rchTxtLocation1.Location = new System.Drawing.Point(6, 121);
            this.rchTxtLocation1.Name = "rchTxtLocation1";
            this.rchTxtLocation1.Size = new System.Drawing.Size(150, 40);
            this.rchTxtLocation1.TabIndex = 7;
            this.rchTxtLocation1.Text = "";
            // 
            // picBoxLocation11
            // 
            this.picBoxLocation11.BackColor = System.Drawing.Color.Black;
            this.picBoxLocation11.Location = new System.Drawing.Point(630, 171);
            this.picBoxLocation11.Name = "picBoxLocation11";
            this.picBoxLocation11.Size = new System.Drawing.Size(150, 94);
            this.picBoxLocation11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxLocation11.TabIndex = 39;
            this.picBoxLocation11.TabStop = false;
            this.picBoxLocation11.DoubleClick += new System.EventHandler(this.picBoxLocation11_DoubleClick);
            this.picBoxLocation11.MouseLeave += new System.EventHandler(this.picBoxLocation11_MouseLeave);
            this.picBoxLocation11.MouseHover += new System.EventHandler(this.picBoxLocation11_MouseHover);
            // 
            // picBoxLocation1
            // 
            this.picBoxLocation1.BackColor = System.Drawing.Color.Black;
            this.picBoxLocation1.Location = new System.Drawing.Point(6, 21);
            this.picBoxLocation1.Name = "picBoxLocation1";
            this.picBoxLocation1.Size = new System.Drawing.Size(150, 94);
            this.picBoxLocation1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxLocation1.TabIndex = 38;
            this.picBoxLocation1.TabStop = false;
            this.picBoxLocation1.DoubleClick += new System.EventHandler(this.picBoxLocation1_DoubleClick);
            this.picBoxLocation1.MouseLeave += new System.EventHandler(this.picBoxLocation1_MouseLeave);
            this.picBoxLocation1.MouseHover += new System.EventHandler(this.picBoxLocation1_MouseHover);
            // 
            // picBoxLocation12
            // 
            this.picBoxLocation12.BackColor = System.Drawing.Color.Black;
            this.picBoxLocation12.Location = new System.Drawing.Point(786, 171);
            this.picBoxLocation12.Name = "picBoxLocation12";
            this.picBoxLocation12.Size = new System.Drawing.Size(150, 94);
            this.picBoxLocation12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxLocation12.TabIndex = 37;
            this.picBoxLocation12.TabStop = false;
            this.picBoxLocation12.DoubleClick += new System.EventHandler(this.picBoxLocation12_DoubleClick);
            this.picBoxLocation12.MouseLeave += new System.EventHandler(this.picBoxLocation12_MouseLeave);
            this.picBoxLocation12.MouseHover += new System.EventHandler(this.picBoxLocation12_MouseHover);
            // 
            // picBoxLocation7
            // 
            this.picBoxLocation7.BackColor = System.Drawing.Color.Black;
            this.picBoxLocation7.Location = new System.Drawing.Point(6, 171);
            this.picBoxLocation7.Name = "picBoxLocation7";
            this.picBoxLocation7.Size = new System.Drawing.Size(150, 94);
            this.picBoxLocation7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxLocation7.TabIndex = 36;
            this.picBoxLocation7.TabStop = false;
            this.picBoxLocation7.DoubleClick += new System.EventHandler(this.picBoxLocation7_DoubleClick);
            this.picBoxLocation7.MouseLeave += new System.EventHandler(this.picBoxLocation7_MouseLeave);
            this.picBoxLocation7.MouseHover += new System.EventHandler(this.picBoxLocation7_MouseHover);
            // 
            // picBoxLocation10
            // 
            this.picBoxLocation10.BackColor = System.Drawing.Color.Black;
            this.picBoxLocation10.Location = new System.Drawing.Point(474, 171);
            this.picBoxLocation10.Name = "picBoxLocation10";
            this.picBoxLocation10.Size = new System.Drawing.Size(150, 94);
            this.picBoxLocation10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxLocation10.TabIndex = 35;
            this.picBoxLocation10.TabStop = false;
            this.picBoxLocation10.DoubleClick += new System.EventHandler(this.picBoxLocation10_DoubleClick);
            this.picBoxLocation10.MouseLeave += new System.EventHandler(this.picBoxLocation10_MouseLeave);
            this.picBoxLocation10.MouseHover += new System.EventHandler(this.picBoxLocation10_MouseHover);
            // 
            // picBoxLocation6
            // 
            this.picBoxLocation6.BackColor = System.Drawing.Color.Black;
            this.picBoxLocation6.Location = new System.Drawing.Point(786, 21);
            this.picBoxLocation6.Name = "picBoxLocation6";
            this.picBoxLocation6.Size = new System.Drawing.Size(150, 94);
            this.picBoxLocation6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxLocation6.TabIndex = 34;
            this.picBoxLocation6.TabStop = false;
            this.picBoxLocation6.DoubleClick += new System.EventHandler(this.picBoxLocation6_DoubleClick);
            this.picBoxLocation6.MouseLeave += new System.EventHandler(this.picBoxLocation6_MouseLeave);
            this.picBoxLocation6.MouseHover += new System.EventHandler(this.picBoxLocation6_MouseHover);
            // 
            // picBoxLocation2
            // 
            this.picBoxLocation2.BackColor = System.Drawing.Color.Black;
            this.picBoxLocation2.Location = new System.Drawing.Point(162, 21);
            this.picBoxLocation2.Name = "picBoxLocation2";
            this.picBoxLocation2.Size = new System.Drawing.Size(150, 94);
            this.picBoxLocation2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxLocation2.TabIndex = 33;
            this.picBoxLocation2.TabStop = false;
            this.picBoxLocation2.DoubleClick += new System.EventHandler(this.picBoxLocation2_DoubleClick);
            this.picBoxLocation2.MouseLeave += new System.EventHandler(this.picBoxLocation2_MouseLeave);
            this.picBoxLocation2.MouseHover += new System.EventHandler(this.picBoxLocation2_MouseHover);
            // 
            // picBoxLocation3
            // 
            this.picBoxLocation3.BackColor = System.Drawing.Color.Black;
            this.picBoxLocation3.Location = new System.Drawing.Point(318, 21);
            this.picBoxLocation3.Name = "picBoxLocation3";
            this.picBoxLocation3.Size = new System.Drawing.Size(150, 94);
            this.picBoxLocation3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxLocation3.TabIndex = 32;
            this.picBoxLocation3.TabStop = false;
            this.picBoxLocation3.DoubleClick += new System.EventHandler(this.picBoxLocation3_DoubleClick);
            this.picBoxLocation3.MouseLeave += new System.EventHandler(this.picBoxLocation3_MouseLeave);
            this.picBoxLocation3.MouseHover += new System.EventHandler(this.picBoxLocation3_MouseHover);
            // 
            // picBoxLocation4
            // 
            this.picBoxLocation4.BackColor = System.Drawing.Color.Black;
            this.picBoxLocation4.Location = new System.Drawing.Point(474, 21);
            this.picBoxLocation4.Name = "picBoxLocation4";
            this.picBoxLocation4.Size = new System.Drawing.Size(150, 94);
            this.picBoxLocation4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxLocation4.TabIndex = 31;
            this.picBoxLocation4.TabStop = false;
            this.picBoxLocation4.DoubleClick += new System.EventHandler(this.picBoxLocation4_DoubleClick);
            this.picBoxLocation4.MouseLeave += new System.EventHandler(this.picBoxLocation4_MouseLeave);
            this.picBoxLocation4.MouseHover += new System.EventHandler(this.picBoxLocation4_MouseHover);
            // 
            // picBoxLocation9
            // 
            this.picBoxLocation9.BackColor = System.Drawing.Color.Black;
            this.picBoxLocation9.Location = new System.Drawing.Point(318, 172);
            this.picBoxLocation9.Name = "picBoxLocation9";
            this.picBoxLocation9.Size = new System.Drawing.Size(150, 94);
            this.picBoxLocation9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxLocation9.TabIndex = 30;
            this.picBoxLocation9.TabStop = false;
            this.picBoxLocation9.DoubleClick += new System.EventHandler(this.picBoxLocation9_DoubleClick);
            this.picBoxLocation9.MouseLeave += new System.EventHandler(this.picBoxLocation9_MouseLeave);
            this.picBoxLocation9.MouseHover += new System.EventHandler(this.picBoxLocation9_MouseHover);
            // 
            // picBoxLocation5
            // 
            this.picBoxLocation5.BackColor = System.Drawing.Color.Black;
            this.picBoxLocation5.Location = new System.Drawing.Point(630, 21);
            this.picBoxLocation5.Name = "picBoxLocation5";
            this.picBoxLocation5.Size = new System.Drawing.Size(150, 94);
            this.picBoxLocation5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxLocation5.TabIndex = 29;
            this.picBoxLocation5.TabStop = false;
            this.picBoxLocation5.DoubleClick += new System.EventHandler(this.picBoxLocation5_DoubleClick);
            this.picBoxLocation5.MouseLeave += new System.EventHandler(this.picBoxLocation5_MouseLeave);
            this.picBoxLocation5.MouseHover += new System.EventHandler(this.picBoxLocation5_MouseHover);
            // 
            // picBoxLocation8
            // 
            this.picBoxLocation8.BackColor = System.Drawing.Color.Black;
            this.picBoxLocation8.Location = new System.Drawing.Point(162, 172);
            this.picBoxLocation8.Name = "picBoxLocation8";
            this.picBoxLocation8.Size = new System.Drawing.Size(150, 94);
            this.picBoxLocation8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxLocation8.TabIndex = 28;
            this.picBoxLocation8.TabStop = false;
            this.picBoxLocation8.DoubleClick += new System.EventHandler(this.picBoxLocation8_DoubleClick);
            this.picBoxLocation8.MouseLeave += new System.EventHandler(this.picBoxLocation8_MouseLeave);
            this.picBoxLocation8.MouseHover += new System.EventHandler(this.picBoxLocation8_MouseHover);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.picBoxSlider7);
            this.groupBox2.Controls.Add(this.picBoxSlider1);
            this.groupBox2.Controls.Add(this.picBoxSlider2);
            this.groupBox2.Controls.Add(this.picBoxSlider5);
            this.groupBox2.Controls.Add(this.picBoxSlider9);
            this.groupBox2.Controls.Add(this.picBoxSlider4);
            this.groupBox2.Controls.Add(this.picBoxSlider3);
            this.groupBox2.Controls.Add(this.picBoxSlider6);
            this.groupBox2.Controls.Add(this.picBoxSlider12);
            this.groupBox2.Controls.Add(this.picBoxSlider10);
            this.groupBox2.Controls.Add(this.picBoxSlider11);
            this.groupBox2.Controls.Add(this.picBoxSlider8);
            this.groupBox2.Controls.Add(this.picBoxEnlarge);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox2.Location = new System.Drawing.Point(12, 73);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(439, 242);
            this.groupBox2.TabIndex = 44;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Main Slider Images";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(389, 16);
            this.label1.TabIndex = 54;
            this.label1.Text = "Click and hold a image to enlarge.   Double click to upload a new image.";
            // 
            // picBoxSlider7
            // 
            this.picBoxSlider7.BackColor = System.Drawing.Color.Navy;
            this.picBoxSlider7.Location = new System.Drawing.Point(222, 104);
            this.picBoxSlider7.Name = "picBoxSlider7";
            this.picBoxSlider7.Size = new System.Drawing.Size(98, 62);
            this.picBoxSlider7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxSlider7.TabIndex = 53;
            this.picBoxSlider7.TabStop = false;
            this.picBoxSlider7.Click += new System.EventHandler(this.picBoxSlider7_DoubleClick);
            this.picBoxSlider7.MouseLeave += new System.EventHandler(this.picBoxSlider7_MouseLeave);
            this.picBoxSlider7.MouseHover += new System.EventHandler(this.picBoxSlider7_MouseHover);
            // 
            // picBoxSlider1
            // 
            this.picBoxSlider1.BackColor = System.Drawing.Color.Navy;
            this.picBoxSlider1.Location = new System.Drawing.Point(14, 36);
            this.picBoxSlider1.Name = "picBoxSlider1";
            this.picBoxSlider1.Size = new System.Drawing.Size(98, 62);
            this.picBoxSlider1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxSlider1.TabIndex = 52;
            this.picBoxSlider1.TabStop = false;
            this.picBoxSlider1.DoubleClick += new System.EventHandler(this.picBoxSlider1_DoubleClick);
            this.picBoxSlider1.MouseLeave += new System.EventHandler(this.picBoxSlider1_MouseLeave);
            this.picBoxSlider1.MouseHover += new System.EventHandler(this.picBoxSlider1_MouseHover);
            // 
            // picBoxSlider2
            // 
            this.picBoxSlider2.BackColor = System.Drawing.Color.Navy;
            this.picBoxSlider2.InitialImage = null;
            this.picBoxSlider2.Location = new System.Drawing.Point(118, 36);
            this.picBoxSlider2.Name = "picBoxSlider2";
            this.picBoxSlider2.Size = new System.Drawing.Size(98, 62);
            this.picBoxSlider2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxSlider2.TabIndex = 51;
            this.picBoxSlider2.TabStop = false;
            this.picBoxSlider2.DoubleClick += new System.EventHandler(this.picBoxSlider2_DoubleClick);
            this.picBoxSlider2.MouseLeave += new System.EventHandler(this.picBoxSlider2_MouseLeave);
            this.picBoxSlider2.MouseHover += new System.EventHandler(this.picBoxSlider2_MouseHover);
            // 
            // picBoxSlider5
            // 
            this.picBoxSlider5.BackColor = System.Drawing.Color.Navy;
            this.picBoxSlider5.Location = new System.Drawing.Point(14, 104);
            this.picBoxSlider5.Name = "picBoxSlider5";
            this.picBoxSlider5.Size = new System.Drawing.Size(98, 62);
            this.picBoxSlider5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxSlider5.TabIndex = 50;
            this.picBoxSlider5.TabStop = false;
            this.picBoxSlider5.Click += new System.EventHandler(this.picBoxSlider5_DoubleClick);
            this.picBoxSlider5.MouseLeave += new System.EventHandler(this.picBoxSlider5_MouseLeave);
            this.picBoxSlider5.MouseHover += new System.EventHandler(this.picBoxSlider5_MouseHover);
            // 
            // picBoxSlider9
            // 
            this.picBoxSlider9.BackColor = System.Drawing.Color.Navy;
            this.picBoxSlider9.Location = new System.Drawing.Point(14, 172);
            this.picBoxSlider9.Name = "picBoxSlider9";
            this.picBoxSlider9.Size = new System.Drawing.Size(98, 62);
            this.picBoxSlider9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxSlider9.TabIndex = 49;
            this.picBoxSlider9.TabStop = false;
            this.picBoxSlider9.Click += new System.EventHandler(this.picBoxSlider9_DoubleClick);
            this.picBoxSlider9.MouseLeave += new System.EventHandler(this.picBoxSlider9_MouseLeave);
            this.picBoxSlider9.MouseHover += new System.EventHandler(this.picBoxSlider9_MouseHover);
            // 
            // picBoxSlider4
            // 
            this.picBoxSlider4.BackColor = System.Drawing.Color.Navy;
            this.picBoxSlider4.Location = new System.Drawing.Point(326, 36);
            this.picBoxSlider4.Name = "picBoxSlider4";
            this.picBoxSlider4.Size = new System.Drawing.Size(98, 62);
            this.picBoxSlider4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxSlider4.TabIndex = 48;
            this.picBoxSlider4.TabStop = false;
            this.picBoxSlider4.Click += new System.EventHandler(this.picBoxSlider4_DoubleClick);
            this.picBoxSlider4.MouseLeave += new System.EventHandler(this.picBoxSlider4_MouseLeave);
            this.picBoxSlider4.MouseHover += new System.EventHandler(this.picBoxSlider4_MouseHover);
            // 
            // picBoxSlider3
            // 
            this.picBoxSlider3.BackColor = System.Drawing.Color.Navy;
            this.picBoxSlider3.Location = new System.Drawing.Point(222, 36);
            this.picBoxSlider3.Name = "picBoxSlider3";
            this.picBoxSlider3.Size = new System.Drawing.Size(98, 62);
            this.picBoxSlider3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxSlider3.TabIndex = 47;
            this.picBoxSlider3.TabStop = false;
            this.picBoxSlider3.Click += new System.EventHandler(this.picBoxSlider3_DoubleClick);
            this.picBoxSlider3.MouseLeave += new System.EventHandler(this.picBoxSlider3_MouseLeave);
            this.picBoxSlider3.MouseHover += new System.EventHandler(this.picBoxSlider3_MouseHover);
            // 
            // picBoxSlider6
            // 
            this.picBoxSlider6.BackColor = System.Drawing.Color.Navy;
            this.picBoxSlider6.Location = new System.Drawing.Point(118, 104);
            this.picBoxSlider6.Name = "picBoxSlider6";
            this.picBoxSlider6.Size = new System.Drawing.Size(98, 62);
            this.picBoxSlider6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxSlider6.TabIndex = 46;
            this.picBoxSlider6.TabStop = false;
            this.picBoxSlider6.Click += new System.EventHandler(this.picBoxSlider6_DoubleClick);
            this.picBoxSlider6.MouseLeave += new System.EventHandler(this.picBoxSlider6_MouseLeave);
            this.picBoxSlider6.MouseHover += new System.EventHandler(this.picBoxSlider6_MouseHover);
            // 
            // picBoxSlider12
            // 
            this.picBoxSlider12.BackColor = System.Drawing.Color.Navy;
            this.picBoxSlider12.Location = new System.Drawing.Point(326, 172);
            this.picBoxSlider12.Name = "picBoxSlider12";
            this.picBoxSlider12.Size = new System.Drawing.Size(98, 62);
            this.picBoxSlider12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxSlider12.TabIndex = 45;
            this.picBoxSlider12.TabStop = false;
            this.picBoxSlider12.Click += new System.EventHandler(this.picBoxSlider12_DoubleClick);
            this.picBoxSlider12.MouseLeave += new System.EventHandler(this.picBoxSlider12_MouseLeave);
            this.picBoxSlider12.MouseHover += new System.EventHandler(this.picBoxSlider12_MouseHover);
            // 
            // picBoxSlider10
            // 
            this.picBoxSlider10.BackColor = System.Drawing.Color.Navy;
            this.picBoxSlider10.Location = new System.Drawing.Point(118, 172);
            this.picBoxSlider10.Name = "picBoxSlider10";
            this.picBoxSlider10.Size = new System.Drawing.Size(98, 62);
            this.picBoxSlider10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxSlider10.TabIndex = 44;
            this.picBoxSlider10.TabStop = false;
            this.picBoxSlider10.Click += new System.EventHandler(this.picBoxSlider10_DoubleClick);
            this.picBoxSlider10.MouseLeave += new System.EventHandler(this.picBoxSlider10_MouseLeave);
            this.picBoxSlider10.MouseHover += new System.EventHandler(this.picBoxSlider10_MouseHover);
            // 
            // picBoxSlider11
            // 
            this.picBoxSlider11.BackColor = System.Drawing.Color.Navy;
            this.picBoxSlider11.Location = new System.Drawing.Point(222, 172);
            this.picBoxSlider11.Name = "picBoxSlider11";
            this.picBoxSlider11.Size = new System.Drawing.Size(98, 62);
            this.picBoxSlider11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxSlider11.TabIndex = 43;
            this.picBoxSlider11.TabStop = false;
            this.picBoxSlider11.Click += new System.EventHandler(this.picBoxSlider11_DoubleClick);
            this.picBoxSlider11.MouseLeave += new System.EventHandler(this.picBoxSlider11_MouseLeave);
            this.picBoxSlider11.MouseHover += new System.EventHandler(this.picBoxSlider11_MouseHover);
            // 
            // picBoxSlider8
            // 
            this.picBoxSlider8.BackColor = System.Drawing.Color.Navy;
            this.picBoxSlider8.Location = new System.Drawing.Point(326, 104);
            this.picBoxSlider8.Name = "picBoxSlider8";
            this.picBoxSlider8.Size = new System.Drawing.Size(98, 62);
            this.picBoxSlider8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxSlider8.TabIndex = 42;
            this.picBoxSlider8.TabStop = false;
            this.picBoxSlider8.Click += new System.EventHandler(this.picBoxSlider8_DoubleClick);
            this.picBoxSlider8.MouseLeave += new System.EventHandler(this.picBoxSlider8_MouseLeave);
            this.picBoxSlider8.MouseHover += new System.EventHandler(this.picBoxSlider8_MouseHover);
            // 
            // picBoxEnlarge
            // 
            this.picBoxEnlarge.BackColor = System.Drawing.Color.Black;
            this.picBoxEnlarge.Location = new System.Drawing.Point(14, 36);
            this.picBoxEnlarge.Name = "picBoxEnlarge";
            this.picBoxEnlarge.Size = new System.Drawing.Size(410, 200);
            this.picBoxEnlarge.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxEnlarge.TabIndex = 55;
            this.picBoxEnlarge.TabStop = false;
            this.picBoxEnlarge.Visible = false;
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Orange;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Tai Le", 10F);
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCancel.Location = new System.Drawing.Point(828, 647);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(120, 32);
            this.btnCancel.TabIndex = 21;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnApply
            // 
            this.btnApply.BackColor = System.Drawing.Color.Green;
            this.btnApply.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApply.Font = new System.Drawing.Font("Microsoft Tai Le", 10F);
            this.btnApply.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnApply.Location = new System.Drawing.Point(702, 647);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(120, 32);
            this.btnApply.TabIndex = 20;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = false;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtSitePassword);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.txtSiteUserName);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.txtSiteURL);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox4.Location = new System.Drawing.Point(12, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(949, 55);
            this.groupBox4.TabIndex = 56;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Site Details";
            // 
            // txtSitePassword
            // 
            this.txtSitePassword.Location = new System.Drawing.Point(709, 19);
            this.txtSitePassword.Name = "txtSitePassword";
            this.txtSitePassword.PasswordChar = '�';
            this.txtSitePassword.Size = new System.Drawing.Size(200, 23);
            this.txtSitePassword.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(645, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "Password";
            // 
            // txtSiteUserName
            // 
            this.txtSiteUserName.Location = new System.Drawing.Point(411, 19);
            this.txtSiteUserName.Name = "txtSiteUserName";
            this.txtSiteUserName.Size = new System.Drawing.Size(200, 23);
            this.txtSiteUserName.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(339, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "User Name";
            // 
            // txtSiteURL
            // 
            this.txtSiteURL.Location = new System.Drawing.Point(117, 19);
            this.txtSiteURL.Name = "txtSiteURL";
            this.txtSiteURL.Size = new System.Drawing.Size(200, 23);
            this.txtSiteURL.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Site URL/IP  http://";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.richTxtBoxDescription);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox3.Location = new System.Drawing.Point(457, 73);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(503, 242);
            this.groupBox3.TabIndex = 57;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Page Description";
            // 
            // richTxtBoxDescription
            // 
            this.richTxtBoxDescription.Font = new System.Drawing.Font("Microsoft New Tai Lue", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTxtBoxDescription.Location = new System.Drawing.Point(15, 22);
            this.richTxtBoxDescription.Name = "richTxtBoxDescription";
            this.richTxtBoxDescription.Size = new System.Drawing.Size(476, 203);
            this.richTxtBoxDescription.TabIndex = 4;
            this.richTxtBoxDescription.Text = "";
            // 
            // fileDialog
            // 
            this.fileDialog.FileName = "openFileDialog1";
            // 
            // GallerySettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(972, 692);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Tai Le", 12F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "GallerySettings";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gallery Page Settings";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.GallerySettings_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLocation8)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSlider8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxEnlarge)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox rchTxtLocation5;
        private System.Windows.Forms.RichTextBox rchTxtLocation6;
        private System.Windows.Forms.RichTextBox rchTxtLocation12;
        private System.Windows.Forms.RichTextBox rchTxtLocation11;
        private System.Windows.Forms.RichTextBox rchTxtLocation10;
        private System.Windows.Forms.RichTextBox rchTxtLocation9;
        private System.Windows.Forms.RichTextBox rchTxtLocation8;
        private System.Windows.Forms.RichTextBox rchTxtLocation7;
        private System.Windows.Forms.RichTextBox rchTxtLocation4;
        private System.Windows.Forms.RichTextBox rchTxtLocation3;
        private System.Windows.Forms.RichTextBox rchTxtLocation2;
        private System.Windows.Forms.RichTextBox rchTxtLocation1;
        private System.Windows.Forms.PictureBox picBoxLocation11;
        private System.Windows.Forms.PictureBox picBoxLocation1;
        private System.Windows.Forms.PictureBox picBoxLocation12;
        private System.Windows.Forms.PictureBox picBoxLocation7;
        private System.Windows.Forms.PictureBox picBoxLocation10;
        private System.Windows.Forms.PictureBox picBoxLocation6;
        private System.Windows.Forms.PictureBox picBoxLocation2;
        private System.Windows.Forms.PictureBox picBoxLocation3;
        private System.Windows.Forms.PictureBox picBoxLocation4;
        private System.Windows.Forms.PictureBox picBoxLocation9;
        private System.Windows.Forms.PictureBox picBoxLocation5;
        private System.Windows.Forms.PictureBox picBoxLocation8;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox picBoxSlider7;
        private System.Windows.Forms.PictureBox picBoxSlider1;
        private System.Windows.Forms.PictureBox picBoxSlider2;
        private System.Windows.Forms.PictureBox picBoxSlider5;
        private System.Windows.Forms.PictureBox picBoxSlider9;
        private System.Windows.Forms.PictureBox picBoxSlider4;
        private System.Windows.Forms.PictureBox picBoxSlider3;
        private System.Windows.Forms.PictureBox picBoxSlider6;
        private System.Windows.Forms.PictureBox picBoxSlider12;
        private System.Windows.Forms.PictureBox picBoxSlider10;
        private System.Windows.Forms.PictureBox picBoxSlider11;
        private System.Windows.Forms.PictureBox picBoxSlider8;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtSiteURL;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RichTextBox richTxtBoxDescription;
        private System.Windows.Forms.PictureBox picBoxEnlarge;
        private System.Windows.Forms.TextBox txtSitePassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSiteUserName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.OpenFileDialog fileDialog;
        private Telerik.WinControls.Themes.Office2010Theme office2010;
        private Telerik.WinControls.Themes.TelerikTheme telerik;
        private Telerik.WinControls.Themes.AquaTheme aqua;
        private Telerik.WinControls.Themes.DesertTheme desert;
        private Telerik.WinControls.Themes.BreezeTheme breeze;
        private Telerik.WinControls.Themes.Office2007SilverTheme office2007SilverTheme1;
        private Telerik.WinControls.Themes.Windows7Theme windows7Theme1;
        private Telerik.WinControls.Themes.VistaTheme vista;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007Black;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme2;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
    }
}

