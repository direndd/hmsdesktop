using System;
using System.Collections ;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using System.IO;
using HotelManagementIcom.Controller;
using System.Threading;
using HotelManagementIcom.Model;

using System.Linq;
using System.Text.RegularExpressions;
 

namespace HotelManagementIcom.View
{
    public partial class Home : Telerik.WinControls.UI.RadForm
    {
        static string startPath = System.IO.Directory.GetCurrentDirectory();
        static string[] lines = Regex.Split(startPath, "bin");
        static string lpath = lines[0] + @"View\images\logoDefault.png";
        
        static string xmlPath = lines[0] + @"HotelDetails.xml";  // The Path to the ftpDet.Xml file //
     
        public Home()
        {
            InitializeComponent();

            new Login().Dispose();

            
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = 1000;
            timer.Start();

            lblUserName.Text = new LoggedUser().UserName1;
            lblUserLevel.Text = new LoggedUser().UserLevel1;

            //Reading the xml and setting the hotel details
            try
            {
                // The Path to the .Xml file //
                if (File.Exists(xmlPath))
                {
                    FileStream READER = new FileStream(xmlPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //Set up the filestream (READER) //
                    System.Xml.XmlDocument HotelDetails = new System.Xml.XmlDocument();// Set up the XmlDocument //
                    HotelDetails.Load(READER); //Load the data from the file into the XmlDocument  //
                    System.Xml.XmlNodeList NodeList = HotelDetails.GetElementsByTagName("Hotel"); // Create a list of the nodes in the xml file //

                    lblHotelName.Text = NodeList[0].FirstChild.ChildNodes[0].InnerText.ToString();
                    lblWeb.Text = NodeList[0].FirstChild.ChildNodes[1].InnerText.ToString();
                    lblContact.Text = NodeList[0].FirstChild.ChildNodes[2].InnerText.ToString();
                    lblEmail.Text = NodeList[0].FirstChild.ChildNodes[3].InnerText.ToString();

                    picBoxLogo.Image = Image.FromFile(NodeList[0].FirstChild.ChildNodes[4].InnerText.ToString());
                    

                    lblContact.Visible = true;
                    lblEmail.Visible = true;
                    lblWeb.Visible = true;

                }
                else
                {
                    lblHotelName.Text = "Hotel Name";
                    lblWeb.Text = "http://www.hotelweb.com";
                    lblEmail.Text = "hotelemail@gmail.com";
                    lblContact.Text = "011 2 *** ***";
                    picBoxLogo.Image = Image.FromFile(lpath);
                  
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Please Go to Administration > Hotel Details and enter details of your Hotel", "Sorry!");
            }
        }


        private void Home_Load(object sender, EventArgs e)
        {
            SettingsControl set = new SettingsControl();
            this.ThemeName = set.getTheme();
            this.BackColor = System.Drawing.Color.FromName(set.getColor());

            WishThread wthrd = new WishThread();
            Thread oThread = new Thread(new ThreadStart(wthrd.checkDate));
            oThread.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            string []Time= DateTime.Now.ToString().Split(' ');        
            lblTime.Text = Time[1]+" "+Time[2];       
         
        }


        private void linkLabel1_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            
        }

        private void btnReservation_Click(object sender, EventArgs e)
        {
            Reservations reservations = new Reservations();
            reservations.Show();
            this.Dispose();
        }

        private void btnPayments_Click(object sender, EventArgs e)
        {
            Payments payments = new Payments();
            payments.Show();
            this.Dispose();
        }

        private void btnChekOut_Click(object sender, EventArgs e)
        {
            Checkout checkout = new Checkout();
            checkout.Show();
            this.Dispose();
        }

        private void btnViewRes_Click(object sender, EventArgs e)
        {
            ViewReservations viewres = new ViewReservations();
            viewres.Show();
            this.Dispose();
        }

        private void btnArrivalLst_Click(object sender, EventArgs e)
        {
            ArrivalList arrival = new ArrivalList();
            arrival.Show();
           // this.Dispose();
        }

        private void btnDepartureLst_Click(object sender, EventArgs e)
        {
            DepartureList departure = new DepartureList();
            departure.Show();
           // this.Dispose();
        }

        private void btnGuestMgmt_Click(object sender, EventArgs e)
        {
            Customer guest = new Customer();
            guest.Show();
            this.Dispose();
        }

        private void btnBookingLst_Click(object sender, EventArgs e)
        {
            BookingList book = new BookingList();
            book.Show();
           // this.Dispose();
        }

        private void btnLogHistory_Click(object sender, EventArgs e)
        {
            LogHistory log = new LogHistory();
            log.Show();
            this.Dispose();
        }

        private void btnRoomMgmt_Click(object sender, EventArgs e)
        {
            ManageRooms room = new ManageRooms();
            room.Show();
            this.Dispose();
        }

        private void btnUserMgmt_Click(object sender, EventArgs e)
        {
            Users user = new Users();
            user.Show();
            this.Dispose();
        }

        private void btnRoomServ_Click(object sender, EventArgs e)
        {
            RoomService roomserv = new RoomService();
            roomserv.Show();
            this.Dispose();
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            Settings settings = new Settings();
            settings.Show();
            this.Dispose();
        }

        private void lblLogout_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
        {
            string logID = new LoggedUser().LogHisId.ToString();
            String[] tokens3 = System.DateTime.Today.ToString().Split(' ');
            String onlyDate = tokens3[1];

            //log history record
            hotelmsEntities1 hmsLog = new hotelmsEntities1();
            logHistory log = hmsLog.logHistories.Where(iii => iii.logHistoryId == logID.Trim()).Single();
            log.logHistoryId = logID.ToString().Trim();
            log.logOutTime = System.DateTime.Now;
            hmsLog.SaveChanges();

            //clearing the detils 
            LoggedUser lu = new LoggedUser();
            lu.UserName1 = "";
            lu.UserLevel1 = "";
            lu.LogHisId = "";
            Login login = new Login();
            login.Show();
            this.Dispose();
        }

       
    }  
    
}
