
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 03/29/2012 13:11:05
-- Generated from EDMX file: E:\C\HotelManagementIcom\HotelManagementIcom\HmsModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [hotelms];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_fk1_bar]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[bars] DROP CONSTRAINT [FK_fk1_bar];
GO
IF OBJECT_ID(N'[dbo].[FK_fk1_customer_phone]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[customer_phone] DROP CONSTRAINT [FK_fk1_customer_phone];
GO
IF OBJECT_ID(N'[dbo].[FK_fk1_reservation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[reservations] DROP CONSTRAINT [FK_fk1_reservation];
GO
IF OBJECT_ID(N'[dbo].[FK_fk1_dining_table]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[dining_table] DROP CONSTRAINT [FK_fk1_dining_table];
GO
IF OBJECT_ID(N'[dbo].[FK_fk1_pool]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[swimmingPools] DROP CONSTRAINT [FK_fk1_pool];
GO
IF OBJECT_ID(N'[dbo].[FK_fk1_reception_hall]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[reception_hall] DROP CONSTRAINT [FK_fk1_reception_hall];
GO
IF OBJECT_ID(N'[dbo].[FK_fk1_room]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[rooms] DROP CONSTRAINT [FK_fk1_room];
GO
IF OBJECT_ID(N'[dbo].[FK_fk2_reservation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[reservations] DROP CONSTRAINT [FK_fk2_reservation];
GO
IF OBJECT_ID(N'[dbo].[FK_fk1_users]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[logHistories] DROP CONSTRAINT [FK_fk1_users];
GO
IF OBJECT_ID(N'[dbo].[FK_reservation_payment_reservation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[reservation_payment] DROP CONSTRAINT [FK_reservation_payment_reservation];
GO
IF OBJECT_ID(N'[dbo].[FK_reservation_payment_payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[reservation_payment] DROP CONSTRAINT [FK_reservation_payment_payment];
GO
IF OBJECT_ID(N'[dbo].[FK_reservationuser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[users] DROP CONSTRAINT [FK_reservationuser];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[bars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[bars];
GO
IF OBJECT_ID(N'[dbo].[customers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[customers];
GO
IF OBJECT_ID(N'[dbo].[customer_phone]', 'U') IS NOT NULL
    DROP TABLE [dbo].[customer_phone];
GO
IF OBJECT_ID(N'[dbo].[dining_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[dining_table];
GO
IF OBJECT_ID(N'[dbo].[facilities]', 'U') IS NOT NULL
    DROP TABLE [dbo].[facilities];
GO
IF OBJECT_ID(N'[dbo].[logHistories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[logHistories];
GO
IF OBJECT_ID(N'[dbo].[payments]', 'U') IS NOT NULL
    DROP TABLE [dbo].[payments];
GO
IF OBJECT_ID(N'[dbo].[reception_hall]', 'U') IS NOT NULL
    DROP TABLE [dbo].[reception_hall];
GO
IF OBJECT_ID(N'[dbo].[reservations]', 'U') IS NOT NULL
    DROP TABLE [dbo].[reservations];
GO
IF OBJECT_ID(N'[dbo].[rooms]', 'U') IS NOT NULL
    DROP TABLE [dbo].[rooms];
GO
IF OBJECT_ID(N'[dbo].[swimmingPools]', 'U') IS NOT NULL
    DROP TABLE [dbo].[swimmingPools];
GO
IF OBJECT_ID(N'[dbo].[sysdiagrams]', 'U') IS NOT NULL
    DROP TABLE [dbo].[sysdiagrams];
GO
IF OBJECT_ID(N'[dbo].[users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[users];
GO
IF OBJECT_ID(N'[dbo].[reservation_payment]', 'U') IS NOT NULL
    DROP TABLE [dbo].[reservation_payment];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'bars'
CREATE TABLE [dbo].[bars] (
    [barId] varchar(50)  NOT NULL,
    [noOfPeople] int  NULL
);
GO

-- Creating table 'customers'
CREATE TABLE [dbo].[customers] (
    [cusId] varchar(50)  NOT NULL,
    [nic] varchar(50)  NULL,
    [fName] varchar(50)  NULL,
    [mName] varchar(50)  NULL,
    [lName] varchar(50)  NULL,
    [passport] varchar(50)  NULL,
    [num] varchar(50)  NULL,
    [street] varchar(50)  NULL,
    [city] varchar(50)  NULL,
    [town] varchar(50)  NULL,
    [email] varchar(200)  NULL
);
GO

-- Creating table 'customer_phone'
CREATE TABLE [dbo].[customer_phone] (
    [cusId] varchar(50)  NOT NULL,
    [phone] varchar(50)  NOT NULL
);
GO

-- Creating table 'dining_table'
CREATE TABLE [dbo].[dining_table] (
    [diningTableId] varchar(50)  NOT NULL,
    [noOfseats] int  NULL,
    [dinner] bit  NULL,
    [supper] bit  NULL,
    [lunch] bit  NULL
);
GO

-- Creating table 'facilities'
CREATE TABLE [dbo].[facilities] (
    [facilityId] varchar(50)  NOT NULL,
    [name] varchar(50)  NULL,
    [price] float  NULL,
    [availability] bit  NULL,
    [description] varchar(200)  NULL,
    [picture] varbinary(max)  NULL
);
GO

-- Creating table 'logHistories'
CREATE TABLE [dbo].[logHistories] (
    [logHistoryId] varchar(50)  NOT NULL,
    [logDate] datetime  NULL,
    [logInTime] time  NULL,
    [logOutTime] time  NULL,
    [userId] varchar(50)  NULL
);
GO

-- Creating table 'payments'
CREATE TABLE [dbo].[payments] (
    [paymentId] varchar(50)  NOT NULL,
    [amount] float  NULL,
    [paidDate] datetime  NULL
);
GO

-- Creating table 'reception_hall'
CREATE TABLE [dbo].[reception_hall] (
    [receptionHallId] varchar(50)  NOT NULL,
    [noOfPeople] int  NULL,
    [barAvailability] bit  NULL
);
GO

-- Creating table 'reservations'
CREATE TABLE [dbo].[reservations] (
    [reservationId] varchar(50)  NOT NULL,
    [noOfAdults] int  NULL,
    [noOfChildren] int  NULL,
    [startDate] datetime  NULL,
    [endDate] datetime  NULL,
    [startTime] time  NULL,
    [endTime] time  NULL,
    [cusId] varchar(50)  NULL,
    [totalPay] float  NULL,
    [balancePay] float  NULL,
    [facilityId] varchar(50)  NULL
);
GO

-- Creating table 'rooms'
CREATE TABLE [dbo].[rooms] (
    [roomId] varchar(50)  NOT NULL,
    [roomNo] varchar(50)  NULL,
    [noOfAdults] int  NULL,
    [noOfChildren] int  NULL,
    [acNonAc] bit  NULL,
    [hotWater] bit  NULL,
    [internet] bit  NULL,
    [tv] bit  NULL
);
GO

-- Creating table 'swimmingPools'
CREATE TABLE [dbo].[swimmingPools] (
    [poolId] varchar(50)  NOT NULL
);
GO

-- Creating table 'sysdiagrams'
CREATE TABLE [dbo].[sysdiagrams] (
    [name] nvarchar(128)  NOT NULL,
    [principal_id] int  NOT NULL,
    [diagram_id] int IDENTITY(1,1) NOT NULL,
    [version] int  NULL,
    [definition] varbinary(max)  NULL
);
GO

-- Creating table 'users'
CREATE TABLE [dbo].[users] (
    [userId] varchar(50)  NOT NULL,
    [fname] varchar(100)  NULL,
    [mname] varchar(100)  NULL,
    [lname] varchar(100)  NULL,
    [num] varchar(50)  NULL,
    [street] varchar(50)  NULL,
    [city] varchar(50)  NULL,
    [mobilePhone] varchar(50)  NULL,
    [landPhone] varchar(50)  NULL,
    [userLevel] varchar(50)  NULL,
    [userName] varchar(50)  NULL,
    [pwd] varchar(50)  NULL,
    [reservation_reservationId] varchar(50)  NOT NULL
);
GO

-- Creating table 'reservation_payment'
CREATE TABLE [dbo].[reservation_payment] (
    [reservations_reservationId] varchar(50)  NOT NULL,
    [payments_paymentId] varchar(50)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [barId] in table 'bars'
ALTER TABLE [dbo].[bars]
ADD CONSTRAINT [PK_bars]
    PRIMARY KEY CLUSTERED ([barId] ASC);
GO

-- Creating primary key on [cusId] in table 'customers'
ALTER TABLE [dbo].[customers]
ADD CONSTRAINT [PK_customers]
    PRIMARY KEY CLUSTERED ([cusId] ASC);
GO

-- Creating primary key on [cusId], [phone] in table 'customer_phone'
ALTER TABLE [dbo].[customer_phone]
ADD CONSTRAINT [PK_customer_phone]
    PRIMARY KEY CLUSTERED ([cusId], [phone] ASC);
GO

-- Creating primary key on [diningTableId] in table 'dining_table'
ALTER TABLE [dbo].[dining_table]
ADD CONSTRAINT [PK_dining_table]
    PRIMARY KEY CLUSTERED ([diningTableId] ASC);
GO

-- Creating primary key on [facilityId] in table 'facilities'
ALTER TABLE [dbo].[facilities]
ADD CONSTRAINT [PK_facilities]
    PRIMARY KEY CLUSTERED ([facilityId] ASC);
GO

-- Creating primary key on [logHistoryId] in table 'logHistories'
ALTER TABLE [dbo].[logHistories]
ADD CONSTRAINT [PK_logHistories]
    PRIMARY KEY CLUSTERED ([logHistoryId] ASC);
GO

-- Creating primary key on [paymentId] in table 'payments'
ALTER TABLE [dbo].[payments]
ADD CONSTRAINT [PK_payments]
    PRIMARY KEY CLUSTERED ([paymentId] ASC);
GO

-- Creating primary key on [receptionHallId] in table 'reception_hall'
ALTER TABLE [dbo].[reception_hall]
ADD CONSTRAINT [PK_reception_hall]
    PRIMARY KEY CLUSTERED ([receptionHallId] ASC);
GO

-- Creating primary key on [reservationId] in table 'reservations'
ALTER TABLE [dbo].[reservations]
ADD CONSTRAINT [PK_reservations]
    PRIMARY KEY CLUSTERED ([reservationId] ASC);
GO

-- Creating primary key on [roomId] in table 'rooms'
ALTER TABLE [dbo].[rooms]
ADD CONSTRAINT [PK_rooms]
    PRIMARY KEY CLUSTERED ([roomId] ASC);
GO

-- Creating primary key on [poolId] in table 'swimmingPools'
ALTER TABLE [dbo].[swimmingPools]
ADD CONSTRAINT [PK_swimmingPools]
    PRIMARY KEY CLUSTERED ([poolId] ASC);
GO

-- Creating primary key on [diagram_id] in table 'sysdiagrams'
ALTER TABLE [dbo].[sysdiagrams]
ADD CONSTRAINT [PK_sysdiagrams]
    PRIMARY KEY CLUSTERED ([diagram_id] ASC);
GO

-- Creating primary key on [userId] in table 'users'
ALTER TABLE [dbo].[users]
ADD CONSTRAINT [PK_users]
    PRIMARY KEY CLUSTERED ([userId] ASC);
GO

-- Creating primary key on [reservations_reservationId], [payments_paymentId] in table 'reservation_payment'
ALTER TABLE [dbo].[reservation_payment]
ADD CONSTRAINT [PK_reservation_payment]
    PRIMARY KEY NONCLUSTERED ([reservations_reservationId], [payments_paymentId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [barId] in table 'bars'
ALTER TABLE [dbo].[bars]
ADD CONSTRAINT [FK_fk1_bar]
    FOREIGN KEY ([barId])
    REFERENCES [dbo].[facilities]
        ([facilityId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [cusId] in table 'customer_phone'
ALTER TABLE [dbo].[customer_phone]
ADD CONSTRAINT [FK_fk1_customer_phone]
    FOREIGN KEY ([cusId])
    REFERENCES [dbo].[customers]
        ([cusId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [cusId] in table 'reservations'
ALTER TABLE [dbo].[reservations]
ADD CONSTRAINT [FK_fk1_reservation]
    FOREIGN KEY ([cusId])
    REFERENCES [dbo].[customers]
        ([cusId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_fk1_reservation'
CREATE INDEX [IX_FK_fk1_reservation]
ON [dbo].[reservations]
    ([cusId]);
GO

-- Creating foreign key on [diningTableId] in table 'dining_table'
ALTER TABLE [dbo].[dining_table]
ADD CONSTRAINT [FK_fk1_dining_table]
    FOREIGN KEY ([diningTableId])
    REFERENCES [dbo].[facilities]
        ([facilityId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [poolId] in table 'swimmingPools'
ALTER TABLE [dbo].[swimmingPools]
ADD CONSTRAINT [FK_fk1_pool]
    FOREIGN KEY ([poolId])
    REFERENCES [dbo].[facilities]
        ([facilityId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [receptionHallId] in table 'reception_hall'
ALTER TABLE [dbo].[reception_hall]
ADD CONSTRAINT [FK_fk1_reception_hall]
    FOREIGN KEY ([receptionHallId])
    REFERENCES [dbo].[facilities]
        ([facilityId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [roomId] in table 'rooms'
ALTER TABLE [dbo].[rooms]
ADD CONSTRAINT [FK_fk1_room]
    FOREIGN KEY ([roomId])
    REFERENCES [dbo].[facilities]
        ([facilityId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [facilityId] in table 'reservations'
ALTER TABLE [dbo].[reservations]
ADD CONSTRAINT [FK_fk2_reservation]
    FOREIGN KEY ([facilityId])
    REFERENCES [dbo].[facilities]
        ([facilityId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_fk2_reservation'
CREATE INDEX [IX_FK_fk2_reservation]
ON [dbo].[reservations]
    ([facilityId]);
GO

-- Creating foreign key on [logHistoryId] in table 'logHistories'
ALTER TABLE [dbo].[logHistories]
ADD CONSTRAINT [FK_fk1_users]
    FOREIGN KEY ([logHistoryId])
    REFERENCES [dbo].[logHistories]
        ([logHistoryId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [reservations_reservationId] in table 'reservation_payment'
ALTER TABLE [dbo].[reservation_payment]
ADD CONSTRAINT [FK_reservation_payment_reservation]
    FOREIGN KEY ([reservations_reservationId])
    REFERENCES [dbo].[reservations]
        ([reservationId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [payments_paymentId] in table 'reservation_payment'
ALTER TABLE [dbo].[reservation_payment]
ADD CONSTRAINT [FK_reservation_payment_payment]
    FOREIGN KEY ([payments_paymentId])
    REFERENCES [dbo].[payments]
        ([paymentId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_reservation_payment_payment'
CREATE INDEX [IX_FK_reservation_payment_payment]
ON [dbo].[reservation_payment]
    ([payments_paymentId]);
GO

-- Creating foreign key on [reservation_reservationId] in table 'users'
ALTER TABLE [dbo].[users]
ADD CONSTRAINT [FK_reservationuser]
    FOREIGN KEY ([reservation_reservationId])
    REFERENCES [dbo].[reservations]
        ([reservationId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_reservationuser'
CREATE INDEX [IX_FK_reservationuser]
ON [dbo].[users]
    ([reservation_reservationId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------