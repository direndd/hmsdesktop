﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HotelManagementIcom.Controller;
using System.IO;
using System.Drawing;


namespace HotelManagementIcom.DataAccess
{
    public class DataController
    {
        DBDataContext data = new DBDataContext();
       
        /*--------------------------------------------------------------------------------------
         * Below methods are mainly used in manage rooms interface and add roomtype interface
         Coding by Manesha Gunathillake- IT 10 1584 32
         ----------------------------------------------------------------------------------------*/


        //Filling Room Type Combo box
        //used in Manage RoomTypes,Add RoomTypes
        public void fillRoomType(ComboBox cmb) 
        {
            
            cmb.Items.Clear();
            IEnumerable<roomType> roomTypes = data.roomTypes.ToList();
            var roomType = roomTypes.Select(i => new { i.typeName }).OrderBy(x => x.typeName);
            foreach (var rmt in roomType)
            {

                cmb.Items.Add(rmt.typeName);
                cmb.DisplayMember = "roomType";
                cmb.ValueMember = "roomType";
                cmb.SelectedIndex = 0;
            }
            
            
        }
        
        public void roomTypeTableFilll(DataGridView dg)
        {
            
            //Filling Room Type Data Grid View
            dg.AutoGenerateColumns = true;
            dg.DataSource = data.roomTypes.Select(i => new { Room_Type = i.typeName, Number_Of_Adults = i.noOfAdults, Number_Of_Children = i.noOfChildren, Full_Board_Price = i.price, Half_Board_Price = i.priceHalf }).OrderBy(x => x.Room_Type);
            
        }

        public void logHistorySave(String logID)
        {
            
            //saving the log history record

            logHistory log = data.logHistories.Where(iii => iii.logHistoryId == logID.Trim()).Single();
            log.logHistoryId = logID.ToString().Trim();
            log.logOutTime = System.DateTime.Now;
            try
            {
                data.SubmitChanges();
                MessageBox.Show("Logging Record Saved in The Databse", " Logout ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception e1)
            {
                var msg = e1.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            

        }
        public List<roomType> searchRoomType(String rm) 
        {
            //Get the details of Room Type according to the selected item in Room Type Combobox
            
            List<roomType> roomList = new List<roomType>();
            roomList = (from d in data.roomTypes
                        where d.typeName == rm
                        select d).ToList();
            return roomList;
            
            

        }
        public byte[] imageToByteArray(Image imageIn)
        {
            
            //Convert the image in to a byte array
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
            
        }

        public void addNewRoomType(string name,int adults,int chil,double full,double half,byte[] pic) 
        {
            
            //Saving a new room type record in to the database
            roomType addrmt = new roomType();
            addrmt.typeName = name;
            addrmt.noOfAdults = adults;
            addrmt.noOfChildren = chil;
            addrmt.price = full;
            addrmt.priceHalf = half;
            addrmt.picture = pic;

            data.roomTypes.InsertOnSubmit(addrmt);
            try
            {
                data.SubmitChanges();
                MessageBox.Show("Room Type " + name + " Added", " Insertion Successful ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
                
            catch (Exception e2)
            {
                var msg = e2.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void deleteRoomType(string name) 
        {
            //deleting a room type from the database
            roomType delrmt = (from d in data.roomTypes
                               where d.typeName == name
                               select d).SingleOrDefault();

            data.roomTypes.DeleteOnSubmit(delrmt);
            try
            {
                data.SubmitChanges();
                MessageBox.Show("Room Type " + name + " Deleted", " Deletion Successful ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception e3)
            {
                var msg = e3.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        public Image getPicture(string key)
        {
            //retreiving roomtype image from the database
            try
            {
                byte[] picture = (from d in data.roomTypes
                                  where d.typeName == key
                                  select (d.picture.ToArray())).Single();

                MemoryStream ms = new MemoryStream(picture);
                Image returnImage = Image.FromStream(ms);
                return returnImage;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void updateRoomType(string name, int adults, int chil, double full, double half, byte[] pic) 
        {
            
            //updating roomtype table
            
            roomType uprmt = (from d in data.roomTypes
                              where d.typeName == name
                              select d).SingleOrDefault();

            uprmt.typeName = name;
            uprmt.noOfAdults = adults;
            uprmt.noOfChildren = chil;
            uprmt.price = full;
            uprmt.priceHalf = half;
            uprmt.picture = pic;
            try
            {
                data.SubmitChanges();
                MessageBox.Show("Room Type " + name + " Updated", " Updating Successful ", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception e4)
            {
                var msg = e4.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            

        }

        //search room type when user write on the combobox
        public List<roomType> getRoomTypes(String type)
        {
            var searmt = (from d in data.roomTypes
                          where d.typeName.StartsWith(type)
                          select d);

            List<roomType> list = new List<roomType>();
            list = searmt.ToList();
            return list;


        }

        /*---------------------------------------------------------------------------------
         * Below methods are mainly used in manage room interface and add rooms interfaces
         Coding by Manesha Gunathillake- IT 10 1584 32
         -----------------------------------------------------------------------------------*/
        public void fillRoomNo(ComboBox cmb)
        {
            
            cmb.Items.Clear();
            IEnumerable<room> rooms = data.rooms.ToList();
            var roomNo = rooms.Select(i => new { i.roomId, i.roomNo }).OrderBy(x => x.roomNo);
            foreach (var rmt in roomNo)
            {

                cmb.Items.Add(rmt.roomNo);
                cmb.DisplayMember = "roomNo";
                cmb.ValueMember = "roomId";
                cmb.SelectedIndex = 0;
            }
                 
        }

        public void roomTableFilll(DataGridView dg)
        {
            //Filling Room Type Data Grid View
            dg.AutoGenerateColumns = true;
            dg.DataSource = data.rooms.Select(i => new { Room_No = i.roomNo, Room_Type = i.roomType, Floor_No = i.floorNo, Description = i.descrip, Availability = i.avalability, Number_Of_Adults = i.noOfAdults, Number_Of_Children = i.noOfChildren, Full_Board_Price = i.price, Half_Board_Price = i.priceHalf }).OrderBy(x => x.Room_No);
            
        }

        public List<room> searchRoom(String rm)
        {
            //Get the details of Rooms according to the selected item in Room No Combobox
            List<room> roomList = new List<room>();
            roomList = (from d in data.rooms
                        where d.roomNo == rm
                        select d).ToList();
            return roomList;


        }

        //search room No when user write on the combobox
        public List<room> getRooms(String no)
        {
            var searm = (from d in data.rooms
                         where d.roomNo.StartsWith(no)
                         select d);

            List<room> list = new List<room>();
            list = searm.ToList();
            return list;


        }

        public void checkAvailability(ComboBox cmb)
        {
           

            cmb.Items.Add("Available");
            cmb.Items.Add("Not Avilable");
            cmb.SelectedIndex = 0;
            
            
        }
        public string generateId()
        {
            var roomIds = data.rooms.Select(i => new { i.roomId });
            int MaxRId = 1;
            foreach (var item in roomIds)
            {
                if (item.roomId != string.Empty)
                {
                    string rId = item.roomId.Substring(3);
                    int rIdSubString = Int16.Parse(rId);
                    if (MaxRId < rIdSubString)
                    { MaxRId = rIdSubString; }
                }
                else
                { MaxRId = 1; }
            }
            MaxRId++;
            
            string newRid = "roo" + MaxRId.ToString();
            return newRid;
        }
        public void addNewRoom(string no,string type,string floor,string des,string aval)
        {
            
            //Saving a new room type record in to the database
            room addrm = new room();
            addrm.roomId = generateId();
            addrm.roomNo = no;
            addrm.roomType = type;
            addrm.floorNo = floor;
            addrm.descrip = des;

            if (aval.Equals("Available"))
            {
                addrm.avalability = true;
            }
            else
            {
                addrm.avalability = false;
            }

            roomType rm2 = data.roomTypes.Where(f => f.typeName == type).First();

            int adults = Convert.ToInt32(rm2.noOfAdults);
            int children = Convert.ToInt32(rm2.noOfChildren);
            float price = Convert.ToInt32(rm2.price);
            float halfPrice = Convert.ToInt32(rm2.priceHalf);

            addrm.noOfAdults = adults;
            addrm.noOfChildren = children;
            addrm.price = price;
            addrm.priceHalf = halfPrice;

            data.rooms.InsertOnSubmit(addrm);
            try
            {
                data.SubmitChanges();
                MessageBox.Show("Room No " + no + " Added", " Insertion Successful ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception e5)
            {
                var msg = e5.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        public void updateRoom(string no, string type, string floor, string des, string aval)
        {
            
            //updating room table

            room uprm = (from d in data.rooms
                         where d.roomNo == no
                         select d).SingleOrDefault();
            roomType rm2 = data.roomTypes.Where(f => f.typeName == type).First();

            int adults = Convert.ToInt32(rm2.noOfAdults);
            int children = Convert.ToInt32(rm2.noOfChildren);
            float price = Convert.ToInt32(rm2.price);
            float halfPrice = Convert.ToInt32(rm2.priceHalf);

            uprm.roomType = type;
            uprm.floorNo = floor;
            uprm.descrip = des;
            if (aval.Equals("Available"))
            {
                uprm.avalability = true;
            }
            else
            {
                uprm.avalability = false;
            }


            uprm.noOfAdults = adults;
            uprm.noOfChildren = children;
            uprm.price = price;
            uprm.priceHalf = halfPrice;
            try
            {
                data.SubmitChanges();
                MessageBox.Show("Room No " + no + " Updated", " Updating Successful ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception e6)
            {
                var msg = e6.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        public void deleteRoom(string no)
        {
           
            //deleting a room type from the database
            room delrm = (from d in data.rooms
                          where d.roomNo == no
                          select d).SingleOrDefault();

            data.rooms.DeleteOnSubmit(delrm);
            try
            {
                data.SubmitChanges();
                MessageBox.Show("Room No " + no + " Deleted", " Deletion Successful ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception e7)
            {
                var msg = e7.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /*---------------------------------------------------------------------------------
        * Below methods are mainly used in Reporting
        Coding by Manesha Gunathillake- IT 10 1584 32
        -----------------------------------------------------------------------------------*/

        public string FormatDate(DateTimePicker dtp)
        {
            string year, month, day;

            year = dtp.Value.Year.ToString();
            month = dtp.Value.Month.ToString();
            day = dtp.Value.Day.ToString();

            if (month.Length == 1)
            {
                month = "0" + month;
            }

            if (day.Length == 1)
            {
                day = "0" + day;
            }

            return (year + "-" + month + "-" + day);
        }

        /*--------------------------------------------------------------------------------------
         * Below methods are mainly used in manage room facilities interface 
         Coding by Manesha Gunathillake- IT 10 1584 32
         ----------------------------------------------------------------------------------------*/
        public string generateFacilityId()
        {
            var facIds = data.facilitiys.Select(i => new { i.facilityId });
            int MaxfId = 1;
            foreach (var item in facIds)
            {
                if (item.facilityId != string.Empty)
                {
                    string fId = item.facilityId.Substring(3);
                    int fIdSubString = Int16.Parse(fId);
                    if (MaxfId < fIdSubString)
                    { MaxfId = fIdSubString; }
                }
                else
                { MaxfId = 1; }
            }
            MaxfId++;

            string newRid = "fac" + MaxfId.ToString();
            return newRid;
        }

        public void addNewFacility(string name)
        {

            //Saving a new room type record in to the database
            facilitiy addfac = new facilitiy();
            addfac.facilityId = generateFacilityId();
            addfac.facilityName = name;

            data.facilitiys.InsertOnSubmit(addfac);
            try
            {
                data.SubmitChanges();
                MessageBox.Show("Facility " + name + " Added", " Insertion Successful ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception e8)
            {
                var msg = e8.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        public void hotelFacTableFilll(DataGridView dg)
        {

            //Filling Hotel facilities Data Grid View
            dg.AutoGenerateColumns = true;
            dg.DataSource = data.facilitiys.Select(i => new { Facility_Name = i.facilityName });

        }
        public void deleteFacility(string name)
        {

            //deleting a room type from the database
            facilitiy delfac = (from d in data.facilitiys
                                where d.facilityName == name
                                select d).SingleOrDefault();

            data.facilitiys.DeleteOnSubmit(delfac);
            try
            {
                data.SubmitChanges();
                MessageBox.Show("Facility " + name + " Deleted", " Deletion Successful ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception e9)
            {
                var msg = e9.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public List<facilitiy> getFac(string name)
        {
            var fac = from s in data.facilitiys
                      join h in data.roomfacs
                      on
                      new { FacilityID = s.facilityId }
                      equals
                      new { FacilityID = h.facility }
                      where h.roomType == name
                      select s;


            List<facilitiy> list = new List<facilitiy>();
            list = fac.ToList();
            return list;
        }
        public void deleteRoomfacility(String roomType)
        {
            var delete = from s in data.roomfacs
                         where s.roomType == roomType
                         select s;

            try
            {
                foreach (var d in delete)
                {
                    data.roomfacs.DeleteOnSubmit(d);

                }
            }
            catch (Exception e10)
            {
                var msg = e10.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void assinRoomFacility(string roomType, string facility)
        {
            roomfac assign = new roomfac();
            facilitiy fac = data.facilitiys.Where(f => f.facilityName == facility).First();
            String facid = fac.facilityId;
            assign.facility = facid;
            assign.roomType = roomType;

            data.roomfacs.InsertOnSubmit(assign);
            try
            {
                data.SubmitChanges();

            }
            catch (Exception e11)
            {
                var msg = e11.Message;
                MessageBox.Show(msg, "Error Occured ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
          
           

    }
}
