﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using HotelManagementIcom.View;
using System.Text.RegularExpressions;
using System.IO;

namespace HotelManagementIcom
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        
        [STAThread]
        static void Main() 
        {
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

           Application.Run(new Login());
            //Application.Run(new Home());

          // Application.Run(new Settings());
           //Application.Run(new GallerySettings();
            //Application.Run(new HotelDetails());

        }
    }
}
