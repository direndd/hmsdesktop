﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HotelManagementIcom.Controller
{
    public class LoggedUser
    {
        public static string UserName;
        public static string UserLevel;
        public static string logHisId;
        private static string logUser;

        public string LogUser
        {
            get { return logUser; }
            set { logUser = value; }
        }

        public string LogHisId
        {
            get { return logHisId; }
            set { logHisId = value; }
        }

        public string UserLevel1
        {
            get { return UserLevel; }
            set { UserLevel = value; }
        }

        public string UserName1
        {
            get { return UserName; }
            set { UserName = value; }
        }
    }
}
