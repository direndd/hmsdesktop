﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using HotelManagementIcom.View;

using HotelManagementIcom.Model;


using System.ComponentModel;
using System.Data;
using System.Data.Objects;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls;

namespace HotelManagementIcom.Controller
{
    public class WishThread
    {     
        hotelmsEntities1 hms = new hotelmsEntities1();               
        ArrayList a = new ArrayList();            
        WishesList wish = new WishesList();               
        Customer customerFrmDB = new Customer();

        public void checkDate() {
            try
            {
                  //when the method is called in mid day
            String[] tokens7 = System.DateTime.Now.ToString().Split(' ');
            String onlyDate2 = tokens7[0];
            String[] tokens8 = onlyDate2.Split('/');          
            selectingCust(tokens8[0], tokens8[1]);


            while (true)            
            { 
                 String[] tokens3 = System.DateTime.Now.ToString().Split(' ');            
                String onlyDate = tokens3[0];      
                String[] tokens4 = onlyDate.Split('/');           
                //time            
                String time = tokens3[1];              
                //  if (true)
                if (time == "12:01:00" && "am".Equals(tokens3[2], StringComparison.OrdinalIgnoreCase))
                {  
                    selectingCust(tokens4[0], tokens4[1]);

                }
            }
            }
            catch (Exception)
            {
                
             
            }
            
            
        }


        public void selectingCust(string day,string month){
            hotelmsEntities1 hms1 = new hotelmsEntities1(); 
            IEnumerable<customer> list = hms1.customers;

            try
            {
                foreach (customer s in list)
                {
                    CustomerWish cust = new CustomerWish();
                    StringBuilder custId = new StringBuilder();
                    StringBuilder fname = new StringBuilder();
                    StringBuilder mname = new StringBuilder();
                    StringBuilder lname = new StringBuilder();
                    StringBuilder dob = new StringBuilder();
                    StringBuilder phone = new StringBuilder();
                    StringBuilder email = new StringBuilder();


                    custId.Append(s.cusId + Environment.NewLine);
                    fname.Append(s.fName + Environment.NewLine);
                    mname.Append(s.mName + Environment.NewLine);
                    lname.Append(s.lName + Environment.NewLine);
                    dob.Append(s.dob + Environment.NewLine);
                    phone.Append(s.mobile + Environment.NewLine);
                    email.Append(s.email + Environment.NewLine);


                    // string date = dob.ToString();
                    String[] tokens5 = dob.ToString().Split(' ');
                    String onlyDate1 = tokens5[0];
                    String[] tokens6 = onlyDate1.Split('/');
                    //matching with the customer bdays
                 
                        if (month == tokens6[1] && tokens6[0] == day)
                        {
                            cust.custID = custId.ToString();
                            cust.fname = fname.ToString();
                            cust.mname = mname.ToString();
                            cust.lname = lname.ToString();
                            cust.phone = phone.ToString();
                            cust.email = email.ToString();
                            cust.dob = dob.ToString();
                            a.Add(cust);
                        }
                    



                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
       //calling the sms and email sending methods
            wish.CheckWished(a);
        
        }

    }
}
