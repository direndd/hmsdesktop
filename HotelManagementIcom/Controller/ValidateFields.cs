﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace HotelManagementIcom.Controller
{
    public class ValidateFields
    {
        public bool ValidateEmail(string email)
        {
            string pattern = @"^[a-z][a-z|0-9|]*([_][a-z|0-9]+)*([.][a-z|" +

                @"0-9]+([_][a-z|0-9]+)*)?@[a-z][a-z|0-9|]*\.([a-z]" +

                @"[a-z|0-9]*(\.[a-z][a-z|0-9]*)?)$";

            System.Text.RegularExpressions.Match match=Regex.Match(email, pattern, RegexOptions.IgnoreCase);
            if (match.Success == true || string.Compare(email, "") == 0)
                return true;
            else
                return false;
        }

        public bool ValidateNIC(string email)
        {
            string pattern = @"^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][V|v|X|x]$";

            System.Text.RegularExpressions.Match match =

            Regex.Match(email, pattern, RegexOptions.IgnoreCase);

            if (!match.Success)
            {
                //MessageBox.Show("Invalid Email", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else
            {
                return true;
            }
        }

        public string checkForDigits(string x)
        {
            string sub=null;
            foreach (char c in x)
            {
                if (char.IsDigit(c) == false)
                {
                    MessageBox.Show("Please use numbers only");
                    sub = x.Substring(0, (x.Length - 1));
                    return sub;
                }
                    
            }
            return x;
        }
        public string checkForTenDigits(string x)
        {
            string sub = null;
            foreach (char c in x)
            {
                if (x.Length>10)
                {
                    MessageBox.Show("Mobile number has only 10 digits");
                    sub = x.Substring(0, (x.Length - 1));
                    return sub;
                }
            }
            return x;
        }
        public string checkForLetters(string x)
        {
            string sub = null;
            foreach (char c in x)
            {
                if (char.IsLetter(c) == false)
                {
                    MessageBox.Show("Please use letters only");
                    sub = x.Substring(0, (x.Length - 1));
                    return sub;
                }

            }
            return x;
        }

        
    }
}
