﻿// -----------------------------------------------------------------------
// <copyright file="Settings.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace HotelManagementIcom.Controller
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;
    using System.Text.RegularExpressions;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class SettingsControl
    {
        public string themeName = "office2007Black";
        public string colorName = "Black";

        public SettingsControl()
        {
            string startPath = System.IO.Directory.GetCurrentDirectory();
            string[] lines = Regex.Split(startPath, "bin");
            string xmlPath = lines[0] + @"settings.xml";  // The Path to the .Xml file //
            Console.WriteLine("File Path is" + xmlPath);

            if (File.Exists(xmlPath))
            {

                FileStream READER = new FileStream(xmlPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //Set up the filestream (READER) //
                System.Xml.XmlDocument settings = new System.Xml.XmlDocument();// Set up the XmlDocument //
                settings.Load(READER); //Load the data from the file into the XmlDocument  //
                System.Xml.XmlNodeList NodeList = settings.GetElementsByTagName("themecolor"); // Create a list of the nodes in the xml file //

                themeName = NodeList[0].FirstChild.ChildNodes[0].InnerText.ToString();
                colorName = NodeList[0].FirstChild.ChildNodes[1].InnerText.ToString();
            }
            else
            {
                themeName = "office2007Black";
                colorName = "Black";
            }
        }

        public string getColor() 
        {
            return this.colorName;
        }
        public string getTheme() 
        {
            return this.themeName;
        }


    }
}
