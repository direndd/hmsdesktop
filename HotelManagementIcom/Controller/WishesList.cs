﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Collections;
using System.IO;
using System.Windows.Forms;

namespace HotelManagementIcom.Controller
{
    class WishesList
    {
        public void ReplaceLineByLine(string filePath, string replaceText, string withText)
        {
            StreamReader streamReader = new StreamReader(filePath);
            StreamWriter streamWriter = new StreamWriter(filePath + ".tmp");

            while (!streamReader.EndOfStream)
            {
                string data = streamReader.ReadLine();
                data = data.Replace(replaceText, withText);
                streamWriter.WriteLine(data);
            }

            streamReader.Close();
            streamWriter.Close();
        }
        
        
        ArrayList  custListArray=new ArrayList();
        public void setList(CustomerWish cust)
        {
            try
            {
                //List<CustomerWish> c;

                String[] tokens3 = System.DateTime.Today.ToString().Split(' ');
                String onlyDate = tokens3[0];
                String[] tokens4 = onlyDate.Split('/');


                string name = "BdayWishes" + "_" + Int32.Parse(tokens4[0]) + "_" + Int32.Parse(tokens4[1]) + "_" + Int32.Parse(tokens4[2]) + ".xml";


                string xmlFilePath = System.IO.Directory.GetCurrentDirectory() + "\\bdyList\\" + name;
                Console.WriteLine(xmlFilePath);
                if (!File.Exists(xmlFilePath))
                {
                    using (XmlWriter writer = XmlWriter.Create("bdyList\\" + name))
                    {
                        writer.WriteStartDocument();
                        writer.WriteStartElement("Customer1");
                        writer.WriteStartElement("Customer");

                        writer.WriteElementString("custId", cust.custID.ToString());
                        writer.WriteElementString("custfName", cust.fname.ToString());
                        writer.WriteElementString("custMName", cust.mname.ToString());
                        writer.WriteElementString("custLName", cust.lname.ToString());
                        writer.WriteElementString("Dob", cust.dob.ToString());
                        writer.WriteElementString("contact", cust.phone.ToString());
                        writer.WriteElementString("email", cust.email.ToString());

                        // writer.WriteEndElement();
                        writer.WriteEndElement();
                        writer.WriteEndDocument();

                        writer.Close();
                        ReplaceLineByLine(xmlFilePath, "</Customer1>", " ");
                    }
                }
                else
                {
                    ReplaceLineByLine(xmlFilePath, "</Customer1>", " ");



                    Stream xmlFile = new FileStream(xmlFilePath, FileMode.Append);
                    XmlTextWriter writer = new XmlTextWriter(xmlFile, Encoding.Default);

                    // writer.WriteStartDocument();
                    //  writer.WriteStartElement("Customer");

                    writer.WriteStartElement("Customer");


                    writer.WriteElementString("custId", cust.custID.ToString());
                    writer.WriteElementString("custfName", cust.fname.ToString());
                    writer.WriteElementString("custMName", cust.mname.ToString());
                    writer.WriteElementString("custLName", cust.lname.ToString());
                    writer.WriteElementString("Dob", cust.dob.ToString());
                    writer.WriteElementString("contact", cust.phone.ToString());
                    writer.WriteElementString("email", cust.email.ToString());


                    writer.WriteEndElement();
                    writer.Close();


                }


                using (StreamWriter sw = File.AppendText(xmlFilePath + ".tmp"))
                {

                    sw.WriteLine("</Customer1>");
                    sw.Close();
                }
            }
            catch (Exception)
            {
                
                
            }
           
          
               
        
        
    }
        public void getList()
        {
            try
            {
                String[] tokens3 = System.DateTime.Today.ToString().Split(' ');
                String onlyDate = tokens3[0];
                String[] tokens4 = onlyDate.Split('/');


                string name = "BdayWishes" + "_" + Int32.Parse(tokens4[0]) + "_" + Int32.Parse(tokens4[1]) + "_" + Int32.Parse(tokens4[2]) + ".xml";

                string xmlFilePath = System.IO.Directory.GetCurrentDirectory() + "\\bdyList\\" + name;
                string xmlPath = " bdyList\\" + name + ".tmp"; // The Path to the .Xml file //
                Console.WriteLine("E:\\SLIIT\\3rd year\\1st sem\\SEP\\HMS\\Desktop\\Hasaru 20-5-2012\\HotelManagementIcom\\HotelManagementIcom\\bin\\Debug\bdyList");
                Console.WriteLine("E:\\SLIIT\\3rd year\\1st sem\\SEP\\HMS\\Desktop\\Hasaru 20-5-2012\\HotelManagementIcom\\HotelManagementIcom\\bin\\Debug\\bdyList\\" + name + ".tmp".Trim());
                Console.WriteLine(xmlFilePath + ".tmp");
                if (File.Exists(xmlFilePath + ".tmp"))
                {
                    FileStream READER = new FileStream(xmlFilePath + ".tmp", FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //Set up the filestream (READER) //
                    System.Xml.XmlDocument HotelDetails = new System.Xml.XmlDocument();// Set up the XmlDocument //
                    HotelDetails.Load(READER); //Load the data from the file into the XmlDocument  //
                    System.Xml.XmlNodeList NodeList = HotelDetails.GetElementsByTagName("Customer"); // Create a list of the nodes in the xml file //
                    for (int i = 0; i < NodeList.Count; i++)
                    {
                        CustomerWish c = new CustomerWish();
                        c.custID = NodeList[i].ChildNodes[0].InnerText.ToString();
                        c.fname = NodeList[i].ChildNodes[1].InnerText.ToString();
                        c.mname = NodeList[i].ChildNodes[2].InnerText.ToString();
                        c.lname = NodeList[i].ChildNodes[3].InnerText.ToString();
                        c.phone = NodeList[i].ChildNodes[4].InnerText.ToString();
                        c.email = NodeList[i].ChildNodes[5].InnerText.ToString();
                        c.dob = NodeList[i].ChildNodes[6].InnerText.ToString();
                        //MessageBox.Show(NodeList[i].FirstChild.ChildNodes[1].InnerText.ToString());
                        custListArray.Add(c);
                        // MessageBox.Show(c.custID);
                    }

                    READER.Close();
                }
                else
                {
                    //lblHotelName.Text = "Please Go to Administration > Hotel Details and enter details of your Hotel";
                }
            }
            catch (Exception)
            {
                
              
            }


        }


        public ArrayList getWishedList()
        {
            ArrayList custListArray2 = new ArrayList();
            try
            {
                
                String[] tokens3 = System.DateTime.Today.ToString().Split(' ');
                String onlyDate = tokens3[0];
                String[] tokens4 = onlyDate.Split('/');


                string name = "BdayWishes" + "_" + Int32.Parse(tokens4[0]) + "_" + Int32.Parse(tokens4[1]) + "_" + Int32.Parse(tokens4[2]) + ".xml";

                string xmlFilePath = System.IO.Directory.GetCurrentDirectory() + "\\ bdyList\\" + name;
                string xmlPath = name + ".tmp"; // The Path to the .Xml file //
                if (File.Exists(xmlFilePath + ".tmp"))
                {
                    FileStream READER = new FileStream(xmlFilePath + ".tmp", FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //Set up the filestream (READER) //
                    System.Xml.XmlDocument HotelDetails = new System.Xml.XmlDocument();// Set up the XmlDocument //
                    HotelDetails.Load(READER); //Load the data from the file into the XmlDocument  //
                    System.Xml.XmlNodeList NodeList = HotelDetails.GetElementsByTagName("Customer"); // Create a list of the nodes in the xml file //
                    for (int i = 0; i < NodeList.Count; i++)
                    {
                        CustomerWish c = new CustomerWish();
                        c.custID = NodeList[i].ChildNodes[0].InnerText.ToString();
                        c.fname = NodeList[i].ChildNodes[1].InnerText.ToString();
                        c.mname = NodeList[i].ChildNodes[2].InnerText.ToString();
                        c.lname = NodeList[i].ChildNodes[3].InnerText.ToString();
                        c.phone = NodeList[i].ChildNodes[4].InnerText.ToString();
                        c.email = NodeList[i].ChildNodes[5].InnerText.ToString();
                        c.dob = NodeList[i].ChildNodes[6].InnerText.ToString();
                        //MessageBox.Show(NodeList[i].FirstChild.ChildNodes[1].InnerText.ToString());
                        custListArray2.Add(c);
                        // MessageBox.Show(c.custID);
                    }

                    READER.Close();
                }
                else
                {
                    //lblHotelName.Text = "Please Go to Administration > Hotel Details and enter details of your Hotel";
                }
                
            }
            catch (Exception)
            {
                
                
            }
            return custListArray2;
        }

        public void CheckWished(ArrayList newCustArrayList) {
            try
            {
                           //filling the array with already whished customers
            getList();
            
            //foreach customer we got from the interface
            foreach (CustomerWish bdyCust in newCustArrayList)
            {
                bool exists = false;
                if (custListArray.Count == 0)
                {
                    Console.WriteLine("first time" + bdyCust.custID);
                    //sending emails and sms
                    BdayWishes bdyWsh = new BdayWishes();
                    bdyWsh.wishCustomer(bdyCust);
                    //adding to the wished list
                    setList(bdyCust);
                }
                else
                { // if (!Object.Equals(newCustArrayList[i], custListArray[i]))
                   // bdyCust.custID != item.custID
                    
                    //getting all the customers n the array
                    foreach (CustomerWish item in custListArray)
                    {
                        if (bdyCust.custID == item.custID)
                        {
                            exists=true;
                        
                        
                        }
                    }

                    //comparing new customers with all ready wished customers
                    if (exists == false)
                    {
                       // Console.WriteLine("othetime" + (bdyCust.custID != item.custID) + bdyCust.custID);
                        //   MessageBox.Show((bdyCust.custID != item.custID).ToString()+"bdyCust.custID != item.custID ");
                        //sending emails and sms
                        BdayWishes bdyWsh = new BdayWishes();
                        bdyWsh.wishCustomer(bdyCust);
                        //adding to the wished list
                        setList(bdyCust);
                    }
                    else
                    {
                       // MessageBox.Show(item.custID);
                    }
                }                          
              

            }
            }
            catch (Exception)
            {
                
                
            }
 

        }

    }
}
